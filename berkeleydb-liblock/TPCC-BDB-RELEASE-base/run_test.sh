#!/bin/bash
#
# run_test.sh
# ===========
# (C) Jean-Pierre Lozi, 2011
#
# Example use:
# ./run_test.sh none 0 300 46 rcl 2
#

# Only use this if you have a valid database in the home-base-<OS>/ directory
# (it allows the benchmark to run faster, just use tpcc_load to generate a
# database in home-base-<OS>/.
# USE_HOME_BASE=1

source "../../inc/color-echo.sh"
shopt -s expand_aliases


PROFILER="none"
REQUEST_TYPE=0
N_ITERATIONS=300
LOCK_NAME="posix"
LIBLOCK_HW_THREADS="0:0 0:0 0:0 0:0 0:0 0:1 0:1 0:0 0:1 0:1 0:1"
INTERACTIVE=0



if [[ $# -ge 1 ]]; then PROFILER=$1;           fi
if [[ $# -ge 2 ]]; then REQUEST_TYPE=$2;       fi
if [[ $# -ge 3 ]]; then N_ITERATIONS=$3;       fi
if [[ $# -ge 4 ]]; then N_THREADS="$4";        fi
if [[ $# -ge 5 ]]; then LOCK_NAME=$5;          fi
if [[ $# -ge 6 ]]; then LIBLOCK_HW_THREADS=$6; fi


if [[ "$USE_SUPERUSER" != "1" ]]
then
    alias sudo=
fi

# This option wasn't used in the USENIX paper, removed for amd48.
if [[ "${HOSTNAME#*amd48}" == $HOSTNAME ]]
then
    PARAMS="-S"
fi


UNAME=$(uname)

if [[ $UNAME == "Linux" ]]; then
    N_CORES=$(cat /proc/cpuinfo | grep processor | wc -l)
elif [[ $UNAME == "SunOS" ]]; then
    N_CORES=$(kstat cpu_info | grep "instance" | wc -l | tr -d ' ')
else
    echo "Unsupported OS."
    exit
fi



_seq()
{
    if [[ $(uname) == "Linux" ]]; then
        seq $1 $2
    else
        nawk "BEGIN{ for(i=$1;i<=$2;i++) print i}"
    fi
}

generate_input()
{
	for i in `_seq 1 $N_THREADS`
    do
 	    echo $1
# Binding is ignored by tpcc_xact_server

#        if [[ $1 == "rcl" ]]; then
#		    echo ${CORES[$((i % (48 - N_SERVERS) + N_SERVERS))]}
#        else
#            echo ${CORES[$((i % 48))]}
#		 fi
        echo 0

        echo $N_ITERATIONS
 		echo 0
    done
	echo -1
}

clock()
{
	STEP=5
	NOW=0

	cecho "[start]" $red

	while true; do
		sleep $STEP
		NOW=$(($NOW+5))
		cecho "[${NOW}s]" $red
	done
}


if [[ $INTERACTIVE != "0" ]]
then
    generate_input $REQUEST_TYPE > input
fi

if [[ ! -d $HOME_DIR ]]
then
    mkdir $HOME_DIR

    if [[ $USE_HOME_BASE -eq 1 ]]
    then
        if [[ $UNAME == "Linux" ]]
        then
            sudo cp ../home-base-linux/* $HOME_DIR/
        elif [[ $UNAME == "SunOS" ]]
        then
            sudo cp ../home-base-solaris/* $HOME_DIR/
        else
            echo "Unsupported OS."
            exit
        fi
    else
        rm -f $HOME_DIR/*

        # sudo LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/../../liblock/           \
        #      LIBLOCK_LOCK_NAME="posix"                                       \
        #      gdb --args ./tpcc_load
        #          -h ~/svn-margaux/margaux/rcl/berkeleydb/home/ -w 1

        sudo LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/../../liblock/             \
             LIBLOCK_LOCK_NAME="posix"                                         \
             ./tpcc_load -h $HOME_DIR -w 1
    fi
fi


case $PROFILER in

    "none")

    if [[ $INTERACTIVE == "0" ]]
    then
        sudo LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/../../liblock/             \
             LIBLOCK_LOCK_NAME=$LOCK_NAME                                      \
             LIBLOCK_HW_THREADS=$LIBLOCK_HW_THREADS                            \
             VERBOSE=$VERBOSE BOUND_CLIENTS=$BOUND_CLIENTS                     \
             ./tpcc_xact_server -b -h $HOME_DIR                                \
             -N $N_ITERATIONS -A $REQUEST_TYPE -H $N_THREADS $PARAMS
    else
        sudo LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/../../liblock/             \
             LIBLOCK_LOCK_NAME=$LOCK_NAME                                      \
             LIBLOCK_HW_THREADS=$LIBLOCK_HW_THREADS                            \
             VERBOSE=$VERBOSE BOUND_CLIENTS=$BOUND_CLIENTS                     \
             ./tpcc_xact_server -b -h $HOME_DIR -i < input
    fi

    ;;

    "none-gdb")

    if [[ $INTERACTIVE == "0" ]]
    then
        sudo LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/../../liblock/             \
             LIBLOCK_LOCK_NAME=$LOCK_NAME                                      \
             LIBLOCK_HW_THREADS=$LIBLOCK_HW_THREADS                            \
             VERBOSE=$VERBOSE BOUND_CLIENTS=$BOUND_CLIENTS                     \
             gdb --args ./tpcc_xact_server -b -h $HOME_DIR -i < input
    fi

    ;;

    "none-mdb")

    echo -ne  "\e[1;38;05;118m[run_test.sh info] \e[1;37;208mOnce in gdb, "
    echo -e   "run:\e[0;37;208m"
    echo -ne  "\e[1;38;05;118m[run_test.sh info] \e[1;37;208mrun -b "
    echo -ne  "-h $HOME_DIR -n $N_ITERATIONS -t $REQUEST_TYPE "
    echo -e   "-c $N_THREADS $PARAMS\e[0;37;208m"

    sudo LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/../../liblock/                 \
         LIBLOCK_LOCK_NAME=$LOCK_NAME                                          \
         LIBLOCK_HW_THREADS=$LIBLOCK_HW_THREADS                                \
         VERBOSE=$VERBOSE BOUND_CLIENTS=$BOUND_CLIENTS                         \
         mdb ./tpcc_xact_server

    ;;

    "mutrace-rcl")

    if [[ $INTERACTIVE == "0" ]]
    then
        sudo LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/../../liblock/             \
             LIBLOCK_LOCK_NAME=$LOCK_NAME                                      \
             LIBLOCK_HW_THREADS=$LIBLOCK_HW_THREADS                            \
             VERBOSE=$VERBOSE BOUND_CLIENTS=$BOUND_CLIENTS                     \
             mutrace-rcl -d ./tpcc_xact_server -b -h $HOME_DIR                 \
            -N $N_ITERATIONS -A $REQUEST_TYPE -H $N_THREADS $PARAMS
    else
        sudo LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/../../liblock/             \
             LIBLOCK_LOCK_NAME=$LOCK_NAME                                      \
             LIBLOCK_HW_THREADS=$LIBLOCK_HW_THREADS                            \
             VERBOSE=$VERBOSE BOUND_CLIENTS=$BOUND_CLIENTS                     \
             mutrace-rcl -d ./tpcc_xact_server -b -h $HOME_DIR -i < input
    fi

    ;;

    "lock-profiler")

    if [[ $INTERACTIVE == "0" ]]
    then
        sudo LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/../../liblock/             \
             LIBLOCK_LOCK_NAME=$LOCK_NAME                                      \
             LIBLOCK_HW_THREADS=$LIBLOCK_HW_THREADS                            \
             VERBOSE=$VERBOSE BOUND_CLIENTS=$BOUND_CLIENTS                     \
             ../../lock-profiler/lock-profiler -b NONE -l NONE_CYCLES          \
             ./tpcc_xact_server -b -h $HOME_DIR -N $N_ITERATIONS               \
             -A $REQUEST_TYPE -H $N_THREADS $PARAMS
    else
        sudo LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/../../liblock/             \
             LIBLOCK_LOCK_NAME=$LOCK_NAME                                      \
             LIBLOCK_HW_THREADS=$LIBLOCK_HW_THREADS                            \
             ../../lock-profiler/lock-profiler ./tpcc_xact_server              \
                  -b -h $HOME_DIR -i < input
    fi

    ;;

    "prefix")

    if [[ $INTERACTIVE == "0" ]]
    then
        sudo LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/../../liblock/             \
             LIBLOCK_LOCK_NAME=$LOCK_NAME                                      \
             LIBLOCK_HW_THREADS=$LIBLOCK_HW_THREADS                            \
             VERBOSE=$VERBOSE BOUND_CLIENTS=$BOUND_CLIENTS                     \
             $PREFIX ./tpcc_xact_server -b -h $HOME_DIR -N $N_ITERATIONS       \
             -A $REQUEST_TYPE -H $N_THREADS $PARAMS
    else
        sudo LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/../../liblock/             \
             LIBLOCK_LOCK_NAME=$LOCK_NAME                                      \
             LIBLOCK_HW_THREADS=$LIBLOCK_HW_THREADS                            \
             VERBOSE=$VERBOSE BOUND_CLIENTS=$BOUND_CLIENTS                     \
             $PREFIX ./tpcc_xact_server -b -h $HOME_DIR -i < input
    fi

    ;;

    *)

    cecho "First argument is invalid." $red

    ;;

esac

# rm -f ../home

