#!/bin/bash
#
# cleanup.sh
# ==========
# (C) Jean-Pierre Lozi, 2011
#

cd $( dirname "${BASH_SOURCE[0]}")/..

sudo rm -rf db-5.2.28.NC-*
sudo rm -rf TPCC-BDB-RELEASE-original
sudo rm -rf TPCC-BDB-RELEASE-patched
sudo rm -f home/*
 
