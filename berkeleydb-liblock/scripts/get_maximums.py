#!/usr/bin/python
#
# get_maximums.py
# ===============
# (C) Jean-Pierre Lozi,
#     Florian David,
#     Gael Thomas,
#     Julia Lawall,
#     Gilles Muller
#     2013
#

import csv
import glob
import os
import sys

if len(sys.argv) < 2:
    print "usage: ", sys.argv[0], " <results-directory>"
    sys.exit(0)

os.chdir(sys.argv[1])

for exp in ['normal-3', 'normal-5']:

    posix_max = -1.0

    #print "%s\t" % exp
    print "%s\t" % exp,

    locks = ["orig", "posix", "spinlock", "mcs", "mcstp", "flat", "ccsynch", "dsmsynch", "rcl"]
    for lock in locks:

        i_file = open("%s-%s.csv" % (lock, exp), "rb")
        reader = csv.reader(i_file)

        row_num = 0
        for row in reader:

            col_num = 0
            for col in row:

                if col_num > 1:
                    continue

                if col == "":
                    break

                fcol=float(col)

                if row_num == 0 and col_num == 1:
                    maximum = float(col)
                else:
                    if col_num == 0:
                        index = fcol
                    if col_num == 1 and fcol > maximum:
                        maximum = fcol
                        max_index = int(index)

                col_num += 1

            row_num += 1

        if posix_max < 0:
            posix_max = maximum

        #print "%-65s: %.2f\t/ %3d" % (file, maximum, max_index)
    print "%9.6f  %3d  %9.6f" % (maximum / posix_max, max_index, maximum),

    i_file.close()

    print "  ?"
