#!/bin/bash
#
# benchmark.sh
# ============
# (C) Jean-Pierre Lozi, 2011
#

ulimit -c unlimited
shopt -s expand_aliases
cd $( dirname "${BASH_SOURCE[0]}")
cd ..


# _grep should be an alias to the location of GNU grep. The default Solaris
# grep doesn't handle the --line-buffered option.
if [[ "$(uname)" == "Linux" ]]
then
    alias _grep=grep
else
    # The standard Solaris grep implementation doesn't have the --line-buffered
    # option. We need GNU grep.
    alias _grep=/usr/grads/share/grep/2.13/bin/grep
fi

if [[ -z $EXECUTION_TYPE ]]; then EXECUTION_TYPE="NORMAL";                    fi
if [[ -z $R_TYPES ]];        then R_TYPES="3 5";                              fi
if [[ -z $N_REQS ]];         then N_REQS=300;                                 fi
if [[ -z $N_RUNS ]];         then N_RUNS=30;                                  fi
if [[ -z $THRESHOLD ]];      then THRESHOLD=100;                              fi

if [[ -z $HOME_DIR ]]
then
    if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
    then
        # Somehow, using a directory in /tmp seems to change results from what
        # we had in the USENIX paper. Which is odd because /tmp is on the same
        # disk... We keep ../home as the home directory on amd48/b, to be as
        # close as we can to paper's results.
        HOME_DIR=../home
    else
        HOME_DIR=/tmp/rcl-berkeleydb-home
    fi
fi

if [[ -z $NS_CLIENTS ]]
then
    NS_CLIENTS="1 16 32 48 64 80 96 112 128 152 192 256 384"
fi

if [[ -z $LOCKS ]]
then
    LOCKS="rcl orig posix spinlock mcstp flat ccsynch dsmsynch mcs"
fi

INFO=1


export USE_SUPERUSER
export HOME_DIR
export VERBOSE


# if [[ $USE_SUPERUSER = "1" ]]
# then
#     if [[ $(/usr/bin/id -u) -ne 0 ]]
#     then
#         echo -n "Error: must be run with superuser rights (e.g., sudo "
# 		echo    "./benchmark.sh)."
#         exit
#     fi
# fi


if [[ "$(uname)" == "Linux" ]]
then
    N_CORES=$(cat /proc/cpuinfo | grep processor | wc -l)
elif [[ "$(uname)" == "SunOS" ]]
then
	N_CORES=$(kstat cpu_info | grep "instance" | wc -l | tr -d ' ')
    export BOUND_CLIENTS=1
else
	echo "Unsupported architecture." >&2
	exit
fi


REQUEST_TYPES=("PAYMENT" "NEW ORDER" "ORDER STATUS" "DELIVERY" "STOCK LEVEL")



benchmark()
{
    for R_TYPE in $R_TYPES
    do
        if [[ $INFO != "0" ]]
        then
            echo -ne "\e[1;38;05;10m\e[1;48;05;16mRequest type = "
            echo -e ${REQUEST_TYPES[$((R_TYPE-1))]}"\e[0m"
        fi

        for LOCK in $LOCKS
        do
            if [[ $LOCK == "orig" ]]
            then
                TPCC_DIRECTORY=TPCC-BDB-RELEASE-original
            else
                TPCC_DIRECTORY=TPCC-BDB-RELEASE-patched
            fi

            cd $TPCC_DIRECTORY

            if [[ $EXECUTION_TYPE = "NORMAL" ]]
            then
                # Are there error log files?
                if ls *.txt &> /dev/null
                then
                    echo
                    echo "Warning: the following log files were found."
                    ls *.txt
                    exit
                fi
            fi

            if [[ $INFO != "0" ]]
            then
                echo -ne "\e[1;48;05;161mLock = "
                echo -e "$LOCK\e[0m"
            fi

            rm -f ../results/${LOCK}-${FILE_SUFFIX}-${R_TYPE}.csv


            if [[ ( $EXECUTION_TYPE = "WITH_USE_RATES" ||
                    $EXECUTION_TYPE = "WITH_FALSE_RATES" )
                  && $LOCK = "rcl" ]]
            then
                echo -ne "\e[1;38;05;161mPlotting use rates. Make sure the "
                echo -e  "liblock was compiled with PROFILING_ON.\e[0m"
            fi


            for N_CLIENTS in $NS_CLIENTS
            do

                if [[ $INFO != "0" ]]
                then
                    echo -ne "\e[1;38;05;99m["
                    printf "%4d" $N_CLIENTS
                    echo -ne " clients] \e[0m"
                fi

                echo -n "$N_CLIENTS," >> ../results/${LOCK}-${FILE_SUFFIX}-${R_TYPE}.csv

                I=0

                rm -f ../results/tmp*


                ### Run benchmark

                while [[ $I -lt $N_RUNS ]]
                do

                    # if [[ "$(uname)" == "Linux" ]]
                    # then
                    #     sudo sysctl -w vm.nr_hugepages=2000 > /dev/null
                    # fi

                    ### This is important. After too many runs, if we don't
                    ### remove these files, not enough memory chunks are
                    ### available and the benchmarks deadlock (one thread is
                    ### stuck in __env_alloc(), and the other ones are waiting
                    ### in __lwp_cond_wait, waiting for the stuck thread to
                    ### release its lock).

                    rm -f $HOME_DIR/__db.00*


                    if [[ $EXECUTION_TYPE = "NORMAL" ]]
                    then

                        if [[ -z "$LIBLOCK_HW_THREADS" ]]
                        then
                            RESULT=`./run_test.sh none $R_TYPE $N_REQS        \
                                $N_CLIENTS $LOCK 2> /dev/null                 \
                                | grep "Throughput:" | cut -d " " -f 2        \
                                | tr -d '\n'                                  \
                                | tee -a ../results/tmp_main_results`
                        else
                             RESULT=`./run_test.sh none $R_TYPE $N_REQS       \
                                $N_CLIENTS $LOCK "$LIBLOCK_HW_THREADS"        \
                                2> /dev/null | grep "Throughput:"             \
                                | cut -d " " -f 2 | tr -d '\n'                \
                                | tee -a ../results/tmp_main_results`
                        fi

                        echo -n $RESULT

                        echo >> ../results/tmp_main_results

                        # If the execution is too slow, we leave the loop.

                        STOP=$(awk -v n1="$RESULT" -v n2="$THRESHOLD"          \
                               'BEGIN{ print (n1 < n2)? 1 : 0 }')

                        if [[ "$N_CLIENTS" != "1" && "$I" == "0" && "$STOP" == "1" ]]
                        then

                            echo
                            echo -n "Transactions per second < $THRESHOLD,"
                            echo " interrupted."

                            echo $RESULT                                       \
                            >> ../results/${LOCK}-${FILE_SUFFIX}-${R_TYPE}.csv

                            break 2

                        fi

                        # Are there error log files?
                        if ls *.txt &> /dev/null
                        then

                            echo
                            echo "An error has occured (check logs)."

                            exit

                        fi

                    elif [[ $EXECUTION_TYPE = "WITH_OUTPUT" ]]
                    then

                        echo -ne "\e[1;38;05;15mRunning once and displaying "
                        echo -e  "output:\e[0m"
                        ./run_test.sh none $R_TYPE $N_REQS $N_CLIENTS $LOCK
                        exit

                    elif [[ $EXECUTION_TYPE = "WITH_GDB" ]]
                    then

                        echo -e "\e[1;38;05;15mLaunching GDB...\e[0m"
                        ./run_test.sh none-gdb $R_TYPE $N_REQS $N_CLIENTS      \
                                      $LOCK
                        exit

                    elif [[ $EXECUTION_TYPE = "WITH_MDB" ]]
                    then

                        echo -e "\e[1;38;05;15mLaunching MDB...\e[0m"
                        ./run_test.sh none-mdb $R_TYPE $N_REQS $N_CLIENTS      \
                                      $LOCK
                        exit

                    elif [[ $EXECUTION_TYPE = "WITH_USE_RATES" ]]
                    then

                        ./run_test.sh none $R_TYPE $N_REQS $N_CLIENTS          \
                            $LOCK 2> ../results/tmp_stderr                     \
                            | grep "Throughput:" | cut -d " " -f 2             \
                            | tr -d '\n' | tee -a ../results/tmp_main_results

                         cat ../results/tmp_stderr | grep "use rate:"          \
                            | cut -d " " -f 7 > ../results/tmp_use_rates

                         SERVER_ID=0

                         while read LINE
                         do
                             echo $LINE >> ../results/tmp_use_rates_$SERVER_ID
                             ((SERVER_ID++))
                         done < ../results/tmp_use_rates

                         rm ../results/tmp_stderr
                         rm ../results/tmp_use_rates

                    elif [[ $EXECUTION_TYPE = "WITH_FALSE_RATES" ]]
                    then

                        ./run_test.sh none $R_TYPE $N_REQS $N_CLIENTS          \
                            $LOCK 2> ../results/tmp_stderr                     \
                            | grep "Throughput:" | cut -d " " -f 2             \
                            | tr -d '\n' | tee -a ../results/tmp_main_results

                         cat ../results/tmp_stderr | grep "false rate:"        \
                            | cut -d " " -f 7 > ../results/tmp_false_rates

                         SERVER_ID=0

                         while read LINE
                         do
                             echo $LINE >> ../results/tmp_false_rates_$SERVER_ID
                             ((SERVER_ID++))
                         done < ../results/tmp_false_rates

                         rm ../results/tmp_stderr
                         rm ../results/tmp_false_rates

                    elif [[ $EXECUTION_TYPE = "WITH_TIMES_IN_CS" ]]
                    then

                        ./run_test.sh lock-profiler                            \
                            $R_TYPE $N_REQS $N_CLIENTS $LOCK 2>&1              \
                            > /dev/null                                        \
                            | grep "Global statistics:" | cut -d " " -f 4      \
                            | tr -d "%" | tr -d '\n' \
                            | tee -a ../results/tmp_main_results

                        echo >> ../results/tmp_main_results

                    elif [[ $EXECUTION_TYPE =                                  \
                            "WITH_TIMES_IN_CS_AND_DCMS_FOR_MCL" ]]
                    then
                        export PREFIX

                        if [[ $SINGLE_RUN != "0" ]]
                        then
                            eval "./run_test.sh prefix $R_TYPE $N_REQS         \
                                  $N_CLIENTS $LOCK $PARSING_COMMANDS"

                            return
                        else
                            eval "./run_test.sh prefix                         \
                                  $R_TYPE $N_REQS $N_CLIENTS $LOCK             \
                                  $PARSING_COMMANDS | tr -d '\n'               \
                                  | tee -a ../results/tmp_main_results"

                            echo >> ../results/tmp_main_results
                        fi

                    elif [[ $EXECUTION_TYPE = "WITH_LAST_TABLE_DATA" ]]
                    then

                        ./run_test.sh none $R_TYPE $N_REQS $N_CLIENTS          \
                            $LOCK "$LIBLOCK_HW_THREADS"                        \
                            2> ../results/tmp_stderr | grep "Throughput:"      \
                            | cut -d " " -f 2                                  \
                            | tr -d '\n' | tee -a ../results/tmp_main_results

                        echo >> ../results/tmp_main_results

                        for RATE in "use" "false" "slow_path"
                        do

                            if [[ $RATE == "slow_path" ]]
                            then
                                cat ../results/tmp_stderr                      \
                                    | grep "${RATE/_/ } rate"                  \
                                    | awk '{ print $4 }'                       \
                                    > ../results/tmp_${RATE}_rates
                            else
                                cat ../results/tmp_stderr                      \
                                    | grep "${RATE/_/ } rate"                  \
                                    | awk '{ print $3 }'                       \
                                    > ../results/tmp_${RATE}_rates
                            fi

                            SERVER_ID=0

                            while read LINE
                            do
                                echo $LINE                                     \
                                >> ../results/tmp_${RATE}_rates_$SERVER_ID
                                ((SERVER_ID++))
                            done < ../results/tmp_${RATE}_rates

                            rm ../results/tmp_${RATE}_rates

                        done

                        rm ../results/tmp_stderr

                    elif [[ $EXECUTION_TYPE = "WITH_VARYING_NREQS" ]]
                    then

                        ./run_test.sh lock-profiler                            \
                            $R_TYPE $N_REQS $N_CLIENTS $LOCK                   \
                            2>&1 > /dev/null                                   \
                            | grep "Global statistics:" | cut -d " " -f 4      \
                            | tr -d "%" | tr -d '\n' \
                            | tee -a ../results/tmp_main_results

                        echo >> ../results/tmp_main_results


                    elif [[ $EXECUTION_TYPE = "WITH_SERVER_CACHE_MISSES" ]]
                    then

                        if [[ -z "$LIBLOCK_HW_THREADS" ]]
                        then
                            ./run_test.sh none $R_TYPE $N_REQS                \
                                $N_CLIENTS $LOCK                              \
                                2>&1 > /dev/null                              \
                                | grep '\/cs' | awk '{print $2}'              \
                                | tr -d '\n'                                  \
                                | tee -a ../results/tmp_main_results
                        else
                             ./run_test.sh none $R_TYPE $N_REQS               \
                                $N_CLIENTS $LOCK "$LIBLOCK_HW_THREADS"        \
                                2>&1 > /dev/null                              \
                                | grep '\/cs' | awk '{print $2}'              \
                                | tr -d '\n'                                  \
                                | tee -a ../results/tmp_main_results
                        fi

                        echo >> ../results/tmp_main_results

                    fi

                    echo -ne " "

                    ((I++))

                    # if [[ "$(uname)" == "Linux" ]]
                    # then
                    #     sudo sysctl -w vm.nr_hugepages=0 > /dev/null
                    # fi

                done

                N_SERVERS=$SERVER_ID

                awk '{if(min==""){min=max=$1}; if($1>max) {max=$1}; if($1< min)\
                     {min=$1}; total+=$1; count+=1} END {printf "%f\n%f\n%f\n",\
                      total/count, min, max}' ../results/tmp_main_results      \
                     > ../results/tmp_main_results_comp


                if [[ $EXECUTION_TYPE = "WITH_USE_RATES" ]]
                then

                    SERVER_ID=0

                    while [[ $SERVER_ID -lt $N_SERVERS ]]
                    do

                        awk '{if(min==""){min=max=$1}; if($1>max) {max=$1};    \
                             if($1< min) {min=$1}; total+=$1; count+=1} END    \
                             {printf "%f\n%f\n%f\n", total/count, min, max}'   \
                             ../results/tmp_use_rates_${SERVER_ID}             \
                            > ../results/tmp_use_rates_${SERVER_ID}_comp

                        ((SERVER_ID++))

                    done

                elif [[ $EXECUTION_TYPE = "WITH_FALSE_RATES" ]]
                then

                    SERVER_ID=0

                    while [[ $SERVER_ID -lt $N_SERVERS ]]
                    do

                        awk '{if(min==""){min=max=$1}; if($1>max) {max=$1};    \
                             if($1< min) {min=$1}; total+=$1; count+=1} END    \
                             {printf "%f\n%f\n%f\n", total/count, min, max}'   \
                             ../results/tmp_false_rates_${SERVER_ID}           \
                            > ../results/tmp_false_rates_${SERVER_ID}_comp

                        ((SERVER_ID++))

                    done

                elif [[ $EXECUTION_TYPE = "WITH_LAST_TABLE_DATA" ]]
                then

                    for RATE in "use" "false" "slow_path"
                    do

                        SERVER_ID=0

                        while [[ $SERVER_ID -lt $N_SERVERS ]]
                        do

                            awk '{if(min==""){min=max=$1}; if($1>max) {max=$1};\
                                 if($1< min) {min=$1}; total+=$1; count+=1} END\
                                {printf "%f\n%f\n%f\n", total/count, min, max}'\
                                ../results/tmp_${RATE}_rates_${SERVER_ID}      \
                                > ../results/tmp_${RATE}_rates_${SERVER_ID}_comp

                           ((SERVER_ID++))

                        done

                    done

                fi


                ### Print info

                echo -ne "\e[0;38;05;118m"

                I=0

                while read line
                do

                    case $I in
                        0) echo -ne "avg=\e[0m" ;;
                        1) echo -ne "min=\e[0m" ;;
                        2) echo -ne "max=\e[0m" ;;
                    esac

                    echo -ne "$line \e[0;38;05;118m"

                    ((I++))

                done < ../results/tmp_main_results_comp

                echo -e "\e[0m"


                if [[ $EXECUTION_TYPE = "WITH_USE_RATES" ]]
                then

                    SERVER_ID=0

                    while [[ $SERVER_ID -lt $N_SERVERS ]]
                    do

                        echo -ne "  \e[1;38;05;11m["
                        printf "server %d" $SERVER_ID
                        echo -ne "] \e[0m"


                        I=0

                        while read line
                        do
                            echo -n "$line "
                            ((I++))
                        done < ../results/tmp_use_rates_${SERVER_ID}


                        I=0

                        echo -ne "\e[0;38;05;118m"

                        while read line
                        do

                            case $I in
                                0) echo -ne "avg=\e[0m" ;;
                                1) echo -ne "min=\e[0m" ;;
                                2) echo -ne "max=\e[0m" ;;
                            esac

                            echo -ne "$line \e[0;38;05;118m"

                            ((I++))

                        done < ../results/tmp_use_rates_${SERVER_ID}_comp


                        echo -e "\e[0m"

                        ((SERVER_ID++))

                    done

                elif [[ $EXECUTION_TYPE = "WITH_FALSE_RATES" ]]
                then

                    SERVER_ID=0

                    while [[ $SERVER_ID -lt $N_SERVERS ]]
                    do

                        echo -ne "  \e[1;38;05;11m["
                        printf "server %d" $SERVER_ID
                        echo -ne "] \e[0m"


                        I=0

                        while read line
                        do
                            echo -n "$line "
                            ((I++))
                        done < ../results/tmp_false_rates_${SERVER_ID}


                        I=0

                        echo -ne "\e[0;38;05;118m"

                        while read line
                        do

                            case $I in
                                0) echo -ne "avg=\e[0m" ;;
                                1) echo -ne "min=\e[0m" ;;
                                2) echo -ne "max=\e[0m" ;;
                            esac

                            echo -ne "$line \e[0;38;05;118m"

                            ((I++))

                        done < ../results/tmp_false_rates_${SERVER_ID}_comp


                        echo -e "\e[0m"

                        ((SERVER_ID++))

                    done

                elif [[ $EXECUTION_TYPE = "WITH_LAST_TABLE_DATA" ]]
                then

                    for RATE in "use" "false" "slow_path"
                    do

                        SERVER_ID=0

                        while [[ $SERVER_ID -lt $N_SERVERS ]]
                        do

                            echo -ne "  \e[1;38;05;11m["
                            printf "server %d" $SERVER_ID
                            echo -ne ", ${RATE/_/ } rate] \e[0m"


                            I=0

                            while read line
                            do
                                echo -n "$line "
                                ((I++))
                            done < ../results/tmp_${RATE}_rates_${SERVER_ID}


                            I=0

                            echo -ne "\e[0;38;05;118m"

                            while read line
                            do

                                case $I in
                                    0) echo -ne "avg=\e[0m" ;;
                                    1) echo -ne "min=\e[0m" ;;
                                    2) echo -ne "max=\e[0m" ;;
                                esac

                                echo -ne "$line \e[0;38;05;118m"

                                ((I++))

                            done < ../results/tmp_${RATE}_rates_${SERVER_ID}_comp


                            echo -e "\e[0m"

                            ((SERVER_ID++))

                        done

                    done

                fi


                ### Build results

                while read line
                do
                    echo -n "$line," >> ../results/${LOCK}-${FILE_SUFFIX}-${R_TYPE}.csv
                done < ../results/tmp_main_results_comp

                while read line
                do
                    echo -n "$line," >> ../results/${LOCK}-${FILE_SUFFIX}-${R_TYPE}.csv
                done < ../results/tmp_main_results

                echo >> ../results/${LOCK}-${FILE_SUFFIX}-${R_TYPE}.csv


                if [[ $EXECUTION_TYPE = "WITH_USE_RATES" ]]
                then

                    SERVER_ID=0

                    while [[ $SERVER_ID -lt $N_SERVERS ]]
                    do

                        echo -n "$N_CLIENTS," \
                             >> ../results/${LOCK}-use-rates-${SERVER_ID}-${R_TYPE}.csv

                        while read line
                        do
                            echo -n "$line," \
                            >> ../results/${LOCK}-use-rates-${SERVER_ID}-${R_TYPE}.csv
                        done < ../results/tmp_use_rates_${SERVER_ID}_comp

                        while read line
                        do
                            echo -n "$line," \
                            >> ../results/${LOCK}-use-rates-${SERVER_ID}-${R_TYPE}.csv
                        done < ../results/tmp_use_rates_${SERVER_ID}

                        echo >> ../results/${LOCK}-use-rates-${SERVER_ID}-${R_TYPE}.csv

                        ((SERVER_ID++))

                    done

                elif [[ $EXECUTION_TYPE = "WITH_FALSE_RATES" ]]
                then

                    SERVER_ID=0

                    while [[ $SERVER_ID -lt $N_SERVERS ]]
                    do

                        echo -n "$N_CLIENTS," \
                             >> ../results/${LOCK}-false-rates-${SERVER_ID}-${R_TYPE}.csv

                        while read line
                        do
                            echo -n "$line," \
                            >> ../results/${LOCK}-false-rates-${SERVER_ID}-${R_TYPE}.csv
                        done < ../results/tmp_false_rates_${SERVER_ID}_comp

                        while read line
                        do
                            echo -n "$line," \
                            >> ../results/${LOCK}-false-rates-${SERVER_ID}-${R_TYPE}.csv
                        done < ../results/tmp_false_rates_${SERVER_ID}

                        echo >> ../results/${LOCK}-false-rates-${SERVER_ID}-${R_TYPE}.csv

                        ((SERVER_ID++))

                    done

                elif [[ $EXECUTION_TYPE = "WITH_LAST_TABLE_DATA" ]]
                then

                    for RATE in "use" "false" "slow_path"
                    do

                        SERVER_ID=0

                        while [[ $SERVER_ID -lt $N_SERVERS ]]
                        do

                            RES_PREFIX=../results/${LOCK}_${RATE}_rates_
                            TMP_RES_PREFIX=../results/tmp_${RATE}_rates_

                            echo -n "$N_CLIENTS,"                              \
                                >> ${RES_PREFIX}${SERVER_ID}-${R_TYPE}.csv

                            while read line
                            do
                                echo -n "$line,"                               \
                                    >> ${RES_PREFIX}${SERVER_ID}-${R_TYPE}.csv
                            done < ${TMP_RES_PREFIX}${SERVER_ID}_comp

                            while read line
                            do
                                echo -n "$line,"                               \
                                >> ${RES_PREFIX}${SERVER_ID}-${R_TYPE}.csv
                            done < ${TMP_RES_PREFIX}${SERVER_ID}

                            echo >> ${TMP_RES_PREFIX}${SERVER_ID}-${R_TYPE}.csv

                            ((SERVER_ID++))

                        done

                    done

                fi

                rm -f ../results/tmp*
            done

            cd ..

            echo
        done
    done
}



# rm -f results/*.csv
rm -f $HOME_DIR/__db.00*

if [[ $EXECUTION_TYPE == "WITH_USE_RATES" ||
      $EXECUTION_TYPE == "WITH_FALSE_RATES" ||
      $EXECUTION_TYPE == "WITH_LAST_TABLE_DATA" ]]
then

    LOCKS="rcl"

    cd ../liblock
    echo "Compiling the liblock with profiling for rcl..."
    make distclean
    make ADDITIONAL_CFLAGS="$ADDITIONAL_CFLAGS -DPROFILING_ON"
    echo "Done."
    cd -
    trap "echo; echo 'Removing profiling from the liblock...';                 \
cd ../liblock; make distclean; make; cd -; exit" SIGINT

fi

if [[ $EXECUTION_TYPE == "NORMAL" ]]
then

    FILE_SUFFIX="normal"

    benchmark

elif [[  $EXECUTION_TYPE == "WITH_OUTPUT" ||
         $EXECUTION_TYPE == "WITH_GDB" ||
         $EXECUTION_TYPE == "WITH_MDB" ||
         $EXECUTION_TYPE == "WITH_USE_RATES" ||
         $EXECUTION_TYPE == "WITH_FALSE_RATES" ]]
then

    FILE_SUFFIX="$(echo $EXECUTION_TYPE | awk '{print substr(tolower($0), 5)}')"

    benchmark

elif [[ $EXECUTION_TYPE == "WITH_SERVER_CACHE_MISSES" ]]
then

    LOCKS="rcl"
    FILE_SUFFIX="server_cache_misses"

    if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
    then
        NS_CLIENTSS="48 128"
    elif [[ $HOSTNAME == "niagara2.cs.rochester.edu" ]]
    then
        NS_CLIENTSS="128 384"
        LIBLOCK_HW_THREADS="0:0 0:0 0:0 0:1 0:0 0:1 0:1 0:1 0:2 0:2 0:2"
    else
        echo "Unsupported machine."
        exit
    fi

    trap "echo; echo 'Removing profiling from the liblock...';                 \
cd ../liblock; make distclean; make; cd -; exit" SIGINT

    for R_TYPES in 3 5
    do

        if [[ "$(uname)" == "SunOS" && "$R_TYPES" == "3" ]]
        then
            SERVER_THREADS="0 1 2"
            LIBLOCK_HW_THREADS="0:0 0:0 0:0 0:1 0:0 0:1 0:1 0:1 0:2 0:2 0:2"
        else
            SERVER_THREADS="0 1"
        fi

        for SERVER_THREAD in $SERVER_THREADS
        do

            cd ../liblock
            echo "Compiling the liblock with DCM profiling for rcl..."

            make distclean

            make ADDITIONAL_CFLAGS="$ADDITIONAL_CFLAGS -DPRECISE_DCM_PROFILING_INSIDE_ON  \
                                    -DMONITORED_THREAD_ID=$SERVER_THREAD"

            echo "Done."
            cd -

            for NS_CLIENTS in $NS_CLIENTSS
            do

                echo -e "\e[1;48;05;91mServer $SERVER_THREAD\e[0m"

                if [[ -z "$LIBLOCK_HW_THREADS" ]]
                then
                    benchmark
                else
                    LIBLOCK_HW_THREADS="$LIBLOCK_HW_THREADS" benchmark
                fi

            done

        done

    done

elif [[ $EXECUTION_TYPE == "WITH_TIMES_IN_CS" ]]
then

    LOCKS="orig"
    R_TYPES="3 5"
    FILE_SUFFIX="times_in_cs"

    if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
    then
        NS_CLIENTS="1 4 8 16 32 48"
    elif [[ $HOSTNAME == "niagara2.cs.rochester.edu" ]]
    then
        NS_CLIENTS="1 4 8 16 32 64 128"
    else
        echo "Unsupported machine."
        exit
    fi

    benchmark

    R_TYPES="1 2 4"

    if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
    then
        NS_CLIENTS="48"
    elif [[ $HOSTNAME == "niagara2.cs.rochester.edu" ]]
    then
        NS_CLIENTS="128"
    else
        echo "Unsupported machine."
        exit
    fi

    benchmark

elif [[ $EXECUTION_TYPE == "WITH_TIMES_IN_CS_AND_DCMS_FOR_MCL" ]]
then

    LOCKS="orig"
    R_TYPES="1 2 3 4 5"

    if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
    then
        NS_CLIENTS="48"
        REQUEST_IDS="3 5"
    elif [[ $HOSTNAME == "niagara2.cs.rochester.edu" ]]
    then
        NS_CLIENTS="128"
        REQUEST_IDS="3 5"
    else
        echo "Unsupported machine."
        exit
    fi


    for REQUEST_ID in $REQUEST_IDS
    do

        echo -ne "\e[1;38;05;10m\e[1;48;05;16mRequest type = "
        echo -e ${REQUEST_TYPES[$((REQUEST_ID-1))]}"\e[0m"


        ########################################################################
        # ID of the most contended lock                                        #
        ########################################################################

        FILE_SUFFIX="mcl_ids"
        PREFIX="../../lock-profiler/lock-profiler -b NONE -l NONE_CYCLES -f"
        PARSING_COMMANDS="2>&1 > /dev/null | tee `pwd`/out | grep \"#\"        \
                          | grep -v \"mutex #\" | sort -k 6 -n | tail -n 1     \
                          | tr -d \#                                           \
                          | awk '{ print \$3\" \"\$4\" \"\$5\" \"\$6 }'"
        R_TYPES="$REQUEST_ID"
        SINGLE_RUN=1
        INFO=0

        OLD_N_RUNS=$N_RUNS
        N_RUNS=1

        RES=`benchmark`

        N_RUNS=$OLD_N_RUNS

        ID="`echo $RES | tr  ',' ' ' | awk '{ print $1 }'`"
        ALLOC_ID="`echo $RES | tr ',' ' ' | awk '{ print $2 }'`"
        ADDRESS="`echo $RES | tr ',' ' ' | awk '{ print $3 }'`"
        TIME_IN_CS="`echo $RES | tr ',' ' ' | awk '{ print $4 }'`"

        echo -n "Found that the lock with the most contention spends "
        echo    "$TIME_IN_CS% of its time in CS."
        echo "Lock info: id=$ID, alloc_id=$ALLOC_ID, address=$ADDRESS."
        echo
        echo "Backtrace for this lock:"
        echo

        cat out | grep $ALLOC_ID | grep "mutex \#" | tee out_filtered
        echo

        if [[ -z $DISCR ]]
        then
            DISCR=`cat out_filtered | grep __env_setup | awk '{ print $4 }'`
        fi

        echo -n "Using locks that contain DISCR=$DISCR in their stacktrace, "
        echo "i.e. with the following IDs:"
        echo

        echo >> out

        IDS=$(cat out | grep $DISCR | awk '{print $3}'                         \
              | sed -e 's/.*\(0x[0-9a-f]*\).*/\1\\|/g' | tr -d '\n' ; echo     \
              | sed -e 's/\\|\n$//')

        echo "$(echo $IDS | sed 's/\\|/ /g')"
        echo

        rm out


        ########################################################################
        # Time in CS                                                           #
        ########################################################################

        echo "Time in CS for this lock :"
        echo "=========================="

        FILE_SUFFIX="mcl_time_in_cs"
        PREFIX="../../lock-profiler/lock-profiler -b NONE -l NONE_CYCLES -a"
        PARSING_COMMANDS="2>&1 > /dev/null | egrep \'$IDS\'                    \
                          | awk '{ print \$6 };' | awk '{s+=\$1} END {print s}'"
        SINGLE_RUN=0

        eval benchmark


        ########################################################################
        # Data cache misses                                                    #
        ########################################################################

        echo "Data cache misses for this lock :"
        echo "================================="

        FILE_SUFFIX="mcl_l2_dcm"

        if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
        then
            PREFIX="../../lock-profiler/lock-profiler -l PAPI -e 0x80000002 -i \
                    -b BY_HW_THREAD -m PER_NODE -a"
        elif [[ $(hostname) == "niagara2.cs.rochester.edu" ]]
        then
            PREFIX="../../lock-profiler/lock-profiler -l CPC -e DC_miss -i    \
                    -b BY_HW_THREAD -m PER_CORE -a"
        else
            echo "Unsupported machine."
            exit
        fi

        PARSING_COMMANDS="2>&1 > /dev/null | egrep \'$IDS\'                    \
                          | awk '{ print \$7 };'                               \
                          | awk 'tolower(\$1)!=\"nan\" &&                      \
                                 tolower(\$1)!=\"-nan\"                        \
                                 {s+=\$1;n++} END {print s/n}'"

        eval benchmark

    done

elif [[ $EXECUTION_TYPE == "WITH_LAST_TABLE_DATA" ]]
then

    FILE_SUFFIX="last_table_data"

    if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
    then

        NS_CLIENTSS="48 96"

        CONFIGS_3=("0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0"
                   "0:0 0:0 0:0 0:0 0:0 0:1 0:1 0:0 0:1 0:1 0:1"
                   "0:0 0:0 0:0 0:1 0:0 0:1 0:1 0:1 0:2 0:2 0:2"
                   "0:0 0:0 0:0 0:1 0:1 0:2 0:1 0:2 0:2 0:3 0:3"
                   "0:0 0:1 0:2 0:3 0:4 0:5 1:0 1:1 1:2 1:3 1:4")

        CONFIGS_5=("0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0"
                   "0:0 0:0 0:0 0:0 0:0 0:1 0:1 0:0 0:1 0:1 0:1"
                   "0:0 0:1 0:2 0:3 0:4 0:5 1:0 1:1 1:2 1:3 1:4")

    elif [[ $HOSTNAME == "niagara2.cs.rochester.edu" ]]
    then

        NS_CLIENTSS="128 384"


        CONFIGS_3=("0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0"
                   "0:0 0:0 0:0 0:0 0:0 0:1 0:1 0:0 0:1 0:1 0:1"
                   "0:0 0:0 0:0 0:1 0:0 0:1 0:1 0:1 0:2 0:2 0:2"
                   "0:0 0:0 0:0 0:1 0:1 0:2 0:1 0:2 0:2 0:3 0:3"
                   "0:0 0:1 0:2 0:3 0:4 0:5 0:6 0:7 0:8 0:9 0:10")

        CONFIGS_5=("0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0 0:0"
                   "0:0 0:0 0:0 0:0 0:0 0:1 0:1 0:0 0:1 0:1 0:1"
                   "0:0 0:1 0:2 0:3 0:4 0:5 0:6 0:7 0:8 0:9 0:10")

    else
        echo "Unsupported machine."
        exit
    fi


    for NS_CLIENTS in $NS_CLIENTSS
    do

        for CONFIG in "${CONFIGS_3[@]}"
        do

            R_TYPES="3"
            echo -e "\e[1;48;05;91mConfiguration = $CONFIG\e[0m"
            LIBLOCK_HW_THREADS="$CONFIG"
            benchmark

        done

        for CONFIG in "${CONFIGS_5[@]}"
        do

            R_TYPES="5"
            echo -e "\e[1;48;05;91mConfiguration = $CONFIG\e[0m"
            LIBLOCK_HW_THREADS="$CONFIG"
            benchmark

        done

    done

elif [[ $EXECUTION_TYPE = "WITH_VARYING_NREQS" ]]
then

    LOCKS="orig"
    FILE_SUFFIX="varying_nreqs"

    for R_TYPES in "3" "5"
    do

        echo -ne "\e[1;38;05;10m\e[1;48;05;16mRequest type = "
        echo -e ${REQUEST_TYPES[$((R_TYPES-1))]}"\e[0m"
        echo

        if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
        then
            NS_CLIENTS="48"
            NS_REQS="1 10 20 50 100 200 500 1000 2000 5000 10000"
        elif [[ $HOSTNAME == "niagara2.cs.rochester.edu" ]]
        then
            NS_CLIENTS="128"
            NS_REQS="1 10 20 50 100 200 500 1000 2000 5000"
        else
            echo "Unsupported machine."
            exit
        fi

        for N_REQS in $NS_REQS
        do

            echo -ne "\e[1;38;05;3m\e[1;48;05;16mN_REQS = "
            printf "%6d" $N_REQS
            echo -ne "\e[0m "

            benchmark #| grep clients

        done

        echo

    done

fi

if [[ $EXECUTION_TYPE == "WITH_USE_RATES" ||
      $EXECUTION_TYPE == "WITH_FALSE_RATES" ||
      $EXECUTION_TYPE == "WITH_LAST_TABLE_DATA" ||
      $EXECUTION_TYPE == "WITH_SERVER_CACHE_MISSES" ]]
then

    cd ../liblock
    echo "Removing profiling from the liblock..."
    make distclean
    make
    echo "Done."
    cd -

fi

