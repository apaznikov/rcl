#!/bin/bash
#
# generate.sh
# ===========
# (C) Jean-Pierre Lozi, 2011
#

source "../../inc/color-echo.sh"
cd $( dirname "${BASH_SOURCE[0]}")/..


BDB_VERSION="5.2.28.NC"
PATCH_NAME="../cocci/bd.out"


if [[ $# -eq 1 ]]
then
	PATCH_NAME=$1
fi


if [[ $(uname) == "SunOS" ]]
then
    TAR=gtar
    MAKE=gmake
    PATCH=gpatch
    MAKEFILE_ORIGINAL=Makefile-original-solaris
    MAKEFILE_PATCHED=Makefile-patched-solaris
else
    TAR=tar
    MAKE=make
    PATCH=patch
    MAKEFILE_ORIGINAL=Makefile-original-linux
    MAKEFILE_PATCHED=Makefile-patched-linux
fi


if [[ $(uname) == "Linux" ]]
then
    N_HW_THREADS=$(cat /proc/cpuinfo | grep processor | wc -l)
elif [[ $(uname) == "SunOS" ]]
then
    N_HW_THREADS=$(kstat cpu_info | grep "instance" | wc -l | tr -d ' ')
else
    echo "Unsupported architecture." >&2
    exit
fi


gen_liblock()
{
    cecho "###################################################################"\
"#############" $ltred
    cecho "# Liblock                                                          "\
"            #" $ltred
    cecho "###################################################################"\
"#############" $ltred

    ##

    cecho "Compiling the liblock (output in ../logs/liblock-make.log)..." $cyan
    cd ../liblock/
    $MAKE distclean > ../berkeleydb-liblock/logs/liblock-make.log
    $MAKE >> ../berkeleydb-liblock/logs/liblock-make.log
    if [[ "$?" != "0" ]]
    then
        cecho "An error has occured: check the log. Exiting." $ltred
        exit
    else
        cecho "The liblock was compiled successfully." $ltgreen
    fi
    cd ../berkeleydb-liblock/
}

configure_make_berkeleydb()
{
	cecho "Running configure (output in ../logs/berkeleydb-configure.log)..." \
        $cyan
	cd db-$BDB_VERSION/build_unix/
#	env CC=gcc ../dist/configure > ../../logs/berkeleydb-configure.log
    ../dist/configure > ../../logs/berkeleydb-configure.log
	if [[ "$?" != "0" ]]
    then
		cecho "An error has occured: check the log. Exiting." $ltred
		exit
	else
		cecho "Berkeley DB was configured successfully." $ltgreen
	fi
	cd ../..

	##

	cecho "Replacing the Makefile... " $cyan -n
	if [[ ! -f files/$1 ]]
    then
		cecho "Couldn't find files/$1. Exiting." $ltred
		exit
	fi
	cp files/$1 db-$BDB_VERSION/build_unix/Makefile
	cecho "done." $ltgreen

	##

	cecho "Detecting the number of cores... " $cyan -n
	cecho "$N_HW_THREADS hardware threads found." $ltgreen

	##

	cecho "Compiling Berkeley DB using "$((N_HW_THREADS+1))\
" threads (output in ../logs/berkeleydb-make.[error]log)..." $cyan
	cd db-$BDB_VERSION/build_unix/
	$MAKE -j $((N_HW_THREADS+1)) libdb.a > ../../logs/berkeleydb-make.log \
        2> ../../logs/berkeleydb-make.errorlog
#	$MAKE libdb.a > ../../logs/berkeleydb-make.log
	if [[ "$?" != "0" ]]
    then
		cecho "An error has occured: check the log. Exiting." $ltred
		exit
	else
		cecho "Berkeley DB was compiled successfully." $ltgreen
	fi
	cd ../..
}

gen_berkeleydb_original()
{
    cecho "###################################################################"\
"#############" $ltred
    cecho "# Berkeley DB - original                                           "\
"            #" $ltred
    cecho "###################################################################"\
"#############" $ltred

    cecho "Deleting db-$BDB_VERSION and db-$BDB_VERSION-original... " $cyan -n
    rm -rf db-$BDB_VERSION db-$BDB_VERSION-original
    cecho "done." $ltgreen

    ##

    cecho "Looking for files/db-$BDB_VERSION-extracted... " $cyan -n
    if [[ -d files/db-$BDB_VERSION-extracted ]]
    then
        cecho "found." $ltgreen
        cecho "Found files/db-$BDB_VERSION-extracted. Copying... " \
            $cyan -n
        cp -r files/db-$BDB_VERSION-extracted db-$BDB_VERSION
        cecho "done." $ltgreen
    else
        cecho "not found." $ltred
        cecho "Backup plan, extracting archives/db-$BDB_VERSION.tar.gz... " \
            $cyan -n
        if [[ ! -f archives/db-$BDB_VERSION.tar.gz ]]
        then
            cecho "Couldn't find archives/db-$BDB_VERSION.tar.gz. Exiting."    \
                $ltred
            exit
        fi
        $TAR -xzf archives/db-$BDB_VERSION.tar.gz
        cecho "done." $ltgreen
    fi


    ##

    configure_make_berkeleydb $MAKEFILE_ORIGINAL

    ##

    cecho "Moving db-$BDB_VERSION to db-$BDB_VERSION-original... " \
        $cyan -n
    mv db-$BDB_VERSION db-$BDB_VERSION-original
    cecho "done." $ltgreen

    ##

    cecho "###################################################################"\
"#############" $ltred
    cecho "# TPC - C w/ Berkeley DB original                                  "\
"            #" $ltred
    cecho "###################################################################"\
"#############" $ltred

    ##

    cecho "Deleting TPCC-BDB-RELEASE-original... " $cyan -n
    rm -rf TPCC-BDB-RELEASE-original
    cecho "done." $ltgreen

    ##

    cecho "Copying TPCC-BDB-RELEASE-base to TPCC-BDB-RELEASE-original... " \
        $cyan -n
    cp -r TPCC-BDB-RELEASE-base TPCC-BDB-RELEASE-original
    cecho "done." $ltgreen

    ##

    cecho "Compiling TPC-C (output in ../logs/tpcc-original-make.log)..." $cyan
    cd TPCC-BDB-RELEASE-original
    sed "s/DBDIR=.*/DBDIR=..\/db-5.2.28.NC-original\/build_unix/" Makefile \
        > Makefile-mod
    mv Makefile-mod Makefile
    $MAKE clean > ../logs/tpcc-berkeleydb-original-make.log
    $MAKE >> ../logs/tpcc-berkeleydb-original-make.log
    if [[ "$?" != "0" ]]
    then
        cecho "An error has occured: check the log. Exiting." $ltred
        exit
    else
        cecho "TPC-C was compiled successfully." $ltgreen
    fi
    cd ..
}

gen_berkeleydb_patched()
{
    cecho "###################################################################"\
"#############" $ltred
    cecho "# Berkeley DB - patched                                            "\
"            #" $ltred
    cecho "###################################################################"\
"#############" $ltred

    ##

    cecho "Deleting db-$BDB_VERSION and db-$BDB_VERSION-patched... " $cyan -n
    rm -rf db-$BDB_VERSION db-$BDB_VERSION-patched
    cecho "done." $ltgreen

    ##

    cecho "Extracting archives/db-$BDB_VERSION.tar.gz... " $cyan -n
    if [[ ! -f archives/db-$BDB_VERSION.tar.gz ]]
    then
        cecho "Couldn't find archives/db-$BDB_VERSION.tar.gz. Exiting." $ltred
        exit
    fi
    $TAR -xzf archives/db-$BDB_VERSION.tar.gz
    cecho "done." $ltgreen

    ##

    cecho "Replacing dbreg.c with the modified version... " $cyan -n
    if [[ ! -f ./db-5.2.28.NC/src/dbreg/dbreg.c ]]
    then
        cecho "Couldn't find dbreg.c. Exiting." $ltred
        exit
    fi
    cp ./db-5.2.28.NC/src/dbreg/dbreg.c db-$BDB_VERSION/src/dbreg/
    cecho "done." $ltgreen

    ##

    cecho "Replacing db.c with the modified version... " $cyan -n
    if [[ ! -f ./db-5.2.28.NC/src/db/db.c ]]
    then
        cecho "Couldn't find ../bd-orig/db-5.2.28.NC/src/db/db.c. Exiting." \
            $ltred
        exit
    fi
    cp ./db-5.2.28.NC/src/db/db.c db-$BDB_VERSION/src/db/
    cecho "done." $ltgreen

    ##

    cecho "Deleting TPCC-BDB-RELEASE... " $cyan -n
    rm -rf TPCC-BDB-RELEASE
    cecho "done." $ltgreen

    ##

    cecho "Copying TPCC-BDB-RELEASE-base to TPCC-BDB-RELEASE... " $cyan -n
    cp -r TPCC-BDB-RELEASE-base TPCC-BDB-RELEASE
    cecho "done." $ltgreen

    ##

    cecho "Replacing txn_util.c with the modified version... " $cyan -n
    if [[ ! -f files/txn_util.c ]]
    then
        cecho "Couldn't find files/txn_util.c. Exiting." $ltred
        exit
    fi
    cp files/txn_util.c db-$BDB_VERSION/src/txn/
    cecho "done." $ltgreen

    ##

    cecho "Applying the patch on db-$BDB_VERSION and TPCC-BDB-RELEASE..." \
        $cyan
    $PATCH -p1 < $PATCH_NAME | sed "s/^/> /"
    if [[ "$?" != "0" ]]
    then
        cecho "The patch couldn't be applied. Exiting." $ltred
        exit
    else
        cecho "Patch successfully applied." $ltgreen
    fi

    cecho "Replacing mutex_int.h with the modified version... " $cyan -n
    if [[ ! -f files/mutex_int.h ]]
    then
        cecho "Couldn't find files/mutex_int.h. Exiting." $ltred
        exit
    fi
    cp db-$BDB_VERSION/src/dbinc/mutex_int.h files/mutex_int_backup.h
    cp files/mutex_int.h db-$BDB_VERSION/src/dbinc/
    cecho "done." $ltgreen

    ##

    cecho "Replacing db.c with the modified version... " $cyan -n
    if [[ ! -f files/db.c ]]
    then
        cecho "Couldn't find files/db.c. Exiting." $ltred
        exit
    fi
    cp db-$BDB_VERSION/src/db/db.c files/db_backup.c
    cp files/db.c db-$BDB_VERSION/src/db/
    cecho "done." $ltgreen

    ##

    cecho "Adding liblock-config.c to src/db/... " $cyan -n
    if [[ ! -f files/liblock-config.c ]]
    then
        cecho "Couldn't find files/liblock-config.c. Exiting." $ltred
        exit
    fi
    cp files/liblock-config.c db-$BDB_VERSION/src/db/
    cecho "done." $ltgreen

    ##

    cecho "Adding liblock-config.h to src/db/... " $cyan -n
    if [[ ! -f files/liblock-config.h ]]
    then
        cecho "Couldn't find files/liblock-config.h. Exiting." $ltred
        exit
    fi
    cp files/liblock-config.h db-$BDB_VERSION/src/db/
    cecho "done." $ltgreen


    ##

    cecho "Adding macro functions... " $cyan -n
    cat files/macros.c >> db-$BDB_VERSION/src/dbinc/mutex.h
    cecho "done." $ltgreen

    ##

    configure_make_berkeleydb $MAKEFILE_PATCHED

    ##

    cecho "Moving db-$BDB_VERSION to db-$BDB_VERSION-patched... " $cyan -n
    mv db-$BDB_VERSION db-$BDB_VERSION-patched
    cecho "done." $ltgreen

    ##

    cecho "###################################################################"\
"#############" $ltred
    cecho "# TPC - C w/ Berkeley DB patched                                   "\
"            #" $ltred
    cecho "###################################################################"\
"#############" $ltred

    ##

    cecho "Deleting TPCC-BDB-RELEASE-patched... " $cyan -n
    rm -rf TPCC-BDB-RELEASE-patched
    cecho "done." $ltgreen

    ##

    cecho "Moving TPCC-BDB-RELEASE to TPCC-BDB-RELEASE-patched... " $cyan -n
    mv TPCC-BDB-RELEASE TPCC-BDB-RELEASE-patched
    cecho "done." $ltgreen

    ##

    cecho "Compiling TPC-C (output in ../logs/tpcc-patched-make.log)..." $cyan
    cd TPCC-BDB-RELEASE-patched
    sed "s/DBDIR=.*/DBDIR=..\/db-5.2.28.NC-patched\/build_unix/" Makefile \
        > Makefile-mod
    mv Makefile-mod Makefile
    sed "s/CPPFLAGS=/CPPFLAGS=-DPATCHED /" Makefile > Makefile-mod
    mv Makefile-mod Makefile
    $MAKE clean > ../logs/tpcc-berkeleydb-patched-make.log
    $MAKE >> ../logs/tpcc-berkeleydb-patched-make.log
    if [[ "$?" != "0" ]]
    then
        cecho "An error has occured: check the log. Exiting." $ltred
        exit
    else
        cecho "TPC-C was compiled successfully." $ltgreen
    fi
    cd ..
}


gen_liblock
gen_berkeleydb_original
gen_berkeleydb_patched

