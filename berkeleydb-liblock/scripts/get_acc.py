#!/usr/bin/python
#
# get_maximums.py
# ===============
# (C) Jean-Pierre Lozi,
#     Florian David,
#     Gael Thomas,
#     Julia Lawall,
#     Gilles Muller
#     2013
#

import csv
import glob
import os
import sys

if len(sys.argv) < 2:
    print "usage: ", sys.argv[0], " <results-directory>"
    sys.exit(0)

os.chdir(sys.argv[1])

target_n_threadss = [1, 16, 32, 48, 64, 96, 112, 128, 152, 256, 384]

for exp in ['3', '5']:

    for target_n_threads in target_n_threadss:

        with_yields = [False, True];

        for with_yield in with_yields:

            base_val = 0

            #print "%s\t" % exp
            print "%s-%d%s" % (exp, target_n_threads, "-yield" if with_yield else "      "),

            locks = ["orig", "rcl"]
            for lock in locks:

                if with_yield and not (lock in ("orig", "posix", "spinlock")):
                    file_name = "%s-normal-yield-%s.csv"
                else:
                    file_name = "%s-normal-%s.csv"

                ifile = open(file_name % (lock, exp), "rb")

                reader = csv.reader(ifile)

                found = False

                row_num = 0
                for row in reader:

                    col_num = 0
                    for col in row:

                        if col_num == 0:

                            n_threads = int(col)

                            if n_threads != target_n_threads:
                                break

                        elif col_num == 1:

                            if lock == "orig":
                                base_val=float(col)

                            found = True

                            print "%9.6f  %9.1f  " % (float(col) / base_val, float(col)),


                        col_num += 1

                    else:

                        if found:
                            break
                        else:
                            continue

                    row_num += 1

                ifile.close()

                if not found:
                    print " -               -    ",

            print ""

