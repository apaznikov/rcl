#include <sched.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <liblock.h>
#include <liblock-fatal.h>
#include "liblock-memcached.h"

#ifdef __sun__
#include <errno.h>
#include <sys/lwp.h>
#include <sys/types.h>
#include <sys/processor.h>
#include <sys/procset.h>
#endif

#define NUM_LOCKS 11

#define INFO_PREFIX "\e[1;38;05;118m[liblock-config.c info] \e[1;38;05;15m"
#define WARNING_PREFIX "\e[1;38;05;161m[liblock-config.c warning] \e[1;38;05;15m"
#define SUFFIX "\e[0m"

#define INFO(str) (INFO_PREFIX str SUFFIX)
#define WARNING(str) (WARNING_PREFIX str SUFFIX)

const char* liblock_lock_name;
struct hw_thread** liblock_server_hw_threads;
int server_hw_thread_ids[NUM_LOCKS][2];
int n_server_hw_thread_ids = 0;

static int bound_clients = 0, verbose = 0;
static int volatile go = 0;
static int volatile current_nb_threads = 0;
static int volatile wait_nb_threads = 0;
static int volatile n_available_hw_threads = 0;
static int volatile cur_hw_thread = 0;
static int* client_hw_threads;


static void do_go(void);
static void liblock_splash(void);
volatile int is_rcl = 0;

static void do_go(void)
{
	go = 1;
}

#ifdef __linux__
__attribute__ ((constructor)) static void liblock_init(void)
#elif defined(__sun__)
__attribute__ ((constructor (103))) static void liblock_init(void)
#endif
{
	int i, j, k, n_client_hw_threads;
    int is_already_added, node, hw_thread;
    int is_server_hw_thread, non_server_hw_thread_id;
    char *tmp_env;
    char *tmp_str, *cur;

	liblock_start_server_threads_by_hand = 1;
	liblock_servers_always_up = 1;

    liblock_lock_name = getenv("LIBLOCK_LOCK_NAME");
	if (!liblock_lock_name)
		liblock_lock_name = "rcl";

	is_rcl = !strcmp(liblock_lock_name, "rcl") ||
             !strcmp(liblock_lock_name, "multircl");

    tmp_str = getenv("BOUND_CLIENTS");
    if (tmp_str && !strcmp(tmp_str, "1"))
        bound_clients = 1;

    tmp_str = getenv("VERBOSE");
    if (tmp_str && !strcmp(tmp_str, "1"))
        verbose = 1;

    // The liblock seems to require this array to be allocated even when we are
    // *not* using RCL.
    liblock_server_hw_threads = malloc(NUM_LOCKS * sizeof(struct hw_thread *));

    if (is_rcl)
    {
//        Fast config 1:
//
//        liblock_server_hw_threads[0] = topology->nodes[0].hw_threads[2];
//        liblock_server_hw_threads[1] = topology->nodes[0].hw_threads[2];
//        liblock_server_hw_threads[2] = topology->nodes[0].hw_threads[2];
//        liblock_server_hw_threads[3] = topology->nodes[0].hw_threads[2];
//        liblock_server_hw_threads[4] = topology->nodes[0].hw_threads[2];
//        liblock_server_hw_threads[5] = topology->nodes[0].hw_threads[2];
//        liblock_server_hw_threads[6] = topology->nodes[0].hw_threads[2];
//        liblock_server_hw_threads[7] = topology->nodes[0].hw_threads[1];
//        liblock_server_hw_threads[8] = topology->nodes[0].hw_threads[2];
//        liblock_server_hw_threads[9] = topology->nodes[0].hw_threads[2];
//        liblock_server_hw_threads[10] = topology->nodes[0].hw_threads[2];
//
//        Fast config 2:
//
//        liblock_server_hw_threads[0] = topology->nodes[0].hw_threads[1];
//        liblock_server_hw_threads[1] = topology->nodes[0].hw_threads[1];
//        liblock_server_hw_threads[2] = topology->nodes[0].hw_threads[1];
//        liblock_server_hw_threads[3] = topology->nodes[0].hw_threads[1];
//        liblock_server_hw_threads[4] = topology->nodes[0].hw_threads[1];
//        liblock_server_hw_threads[5] = topology->nodes[0].hw_threads[1];
//        liblock_server_hw_threads[6] = topology->nodes[0].hw_threads[1];
//        liblock_server_hw_threads[7] = topology->nodes[0].hw_threads[1];
//        liblock_server_hw_threads[8] = topology->nodes[0].hw_threads[1];
//        liblock_server_hw_threads[9] = topology->nodes[0].hw_threads[2];
//        liblock_server_hw_threads[10] = topology->nodes[0].hw_threads[1];
//
//        Conclusion: conflict between locks 7 and 9!

        tmp_str = getenv("LIBLOCK_HW_THREADS");
        tmp_str = tmp_str ?
                  tmp_str : "0:0 0:0 0:0 0:0 0:0 0:1 0:1 0:0 0:1 0:1 0:1";

        for(cur = tmp_str - 1, i = 0;
            cur && i < NUM_LOCKS;
            cur=strchr(cur, ' '), i++)
        {
            sscanf(++cur, "%d:%d", &node, &hw_thread);

            if(node >= topology->nb_nodes
                || hw_thread >= topology->nodes[node].nb_hw_threads)
                fatal("no such hw_thread: %d - %d", node, hw_thread);

            liblock_server_hw_threads[i] =
                topology->nodes[node].hw_threads[hw_thread];

            printf(INFO("Lock %d on hw_thread [%d,%d].\n"), i, node,
                    hw_thread);

            is_already_added = 0;

            for (j = 0; j < n_server_hw_thread_ids; j++)
            {
                if (server_hw_thread_ids[j][0] == node
                    && server_hw_thread_ids[j][1] == hw_thread)
                {
                    is_already_added = 1;
                }
            }

            if (!is_already_added)
            {
                server_hw_thread_ids[n_server_hw_thread_ids][0] = node;
                server_hw_thread_ids[n_server_hw_thread_ids][1] = hw_thread;
                n_server_hw_thread_ids++;
            }
        }

        for (i = 0; i < n_server_hw_thread_ids; i++)
            printf(INFO("I will reserve hw_thread [%d,%d] for a server.\n"),
                   server_hw_thread_ids[i][0], server_hw_thread_ids[i][1]);
    }

#ifdef __linux__
    char get_cmd[128];

    sprintf(get_cmd, "/proc/%d/cmdline", getpid());
	FILE* f=fopen(get_cmd, "r");

    if(!f)
    {
        printf(WARNING("unable to find command line.\n"));
	}

    char buf[1024];
	buf[0] = 0;

    if(!fgets(buf, 1024, f))
		fatal("fgets\n");

	printf(WARNING("Testing %s with lock %s\n"), buf, liblock_lock_name);
#endif

    /* Pre-bind */

    /*
     * We must find a core that is not reserved for a server. We will then bind
     * the current thread to that core to avoid all interferences with the
     * server.
     */

    if (!is_rcl)
    {
        i = j = 0;
        goto found_non_server_core;
    }

    for(i = 0; i < topology->nb_nodes; i++)
    {
        for(j = 0; j < topology->nodes[i].nb_hw_threads; j++)
        {
            is_server_hw_thread = 0;

            for (k = 0; k < n_server_hw_thread_ids; k++)
            {
                if (server_hw_thread_ids[k][0] == i &&
                    server_hw_thread_ids[k][1] == j)
                {
                    is_server_hw_thread = 1;
                    break;
                }
            }

            if (is_server_hw_thread == 0) goto found_non_server_core;
        }
    }

    fatal("all cores are servers");


found_non_server_core:

    printf(INFO("I will temporarily bind the main thread to non-server core "
                "[%d,%d].\n"), i, j);

    non_server_hw_thread_id = topology->nodes[i].hw_threads[j]->hw_thread_id;


#ifdef __linux__
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(non_server_hw_thread_id, &cpuset);
    if (sched_setaffinity(0, sizeof(cpu_set_t), &cpuset))
        fatal("pthread_setaffinity");
#elif defined(__sun__)
    if (processor_bind(P_LWPID, P_MYID, non_server_hw_thread_id, NULL))
        fatal("processor_bind (%s)", strerror(errno));
#endif
    /* /Pre-bind */

	if(is_rcl)
    {
		go = 0;

        for (k = 0; k < n_server_hw_thread_ids; k++)
        {
            int node = server_hw_thread_ids[k][0];
            int hw_thread = server_hw_thread_ids[k][1];

            liblock_reserve_hw_thread_for(topology->nodes[node].hw_threads[hw_thread],
                                          liblock_lock_name);
        }

        /* launch the liblock threads */
		liblock_lookup(liblock_lock_name)->run(do_go);

        while(!go)
			PAUSE();
	}

    client_hw_threads = malloc(sizeof(int) * topology->nb_hw_threads);

    for(i = 0, n_client_hw_threads = 0; i < topology->nb_nodes; i++)
    {
		for(j = 0; j < topology->nodes[i].nb_hw_threads; j++)
        {
            is_server_hw_thread = 0;

            if (is_rcl)
            {
                // FIXME: using server_hw_thread_ids here would be slightly
                // more efficient.
                for (k = 0; k < NUM_LOCKS; k++)
                    if(topology->nodes[i].hw_threads[j] ==
                       liblock_server_hw_threads[k])
                        is_server_hw_thread = 1;
            }

            if (!is_server_hw_thread)
                client_hw_threads[n_client_hw_threads++] =
                    topology->nodes[i].hw_threads[j]->hw_thread_id;
        }
    }

    n_available_hw_threads = n_client_hw_threads;

    printf(INFO("%d threads are available for clients.\n"),
            n_available_hw_threads);

	liblock_auto_bind();
}

void liblock_auto_bind(void)
{
    int i, j, found;

	if(!self.running_hw_thread)
    {
		struct hw_thread *hw_thread;

		do
        {
            int n = __sync_fetch_and_add(&cur_hw_thread, 1)
                    % n_available_hw_threads;

            hw_thread = &topology->hw_threads[client_hw_threads[n]];
		}
        while(hw_thread->server_type);

		self.running_hw_thread = hw_thread;

/*
		cpu_set_t cpuset;
		CPU_ZERO(&cpuset);
		CPU_SET(hw_thread->hw_thread_id, &cpuset);
*/

#ifdef __linux__
        cpu_set_t cpuset;
        CPU_ZERO(&cpuset);

        int i, j, k, is_server_hw_thread;

      	for(i = 0; i < topology->nb_nodes; i++)
        {
		    for(j = 0; j < topology->nodes[i].nb_hw_threads; j++)
            {
                is_server_hw_thread = 0;

                for (k = 0; k < n_server_hw_thread_ids; k++)
                {
                    if (server_hw_thread_ids[k][0] == i &&
                        server_hw_thread_ids[k][1] == j)
                    {
                        is_server_hw_thread = 1;
                        break;
                    }
                }

                if (is_server_hw_thread)
                    CPU_CLR(topology->nodes[i].hw_threads[j]->hw_thread_id,
                            &cpuset);
                else
                    CPU_SET(topology->nodes[i].hw_threads[j]->hw_thread_id,
                            &cpuset);
            }
        }

		if (pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset))
            fatal("pthread_setaffinity_np");
#elif defined(__sun__)
        /*
         * We cannot bind clients to a set of processors... We can, however,
         * bind each client to a processor, several clients being bound to the
         * same processor... The advantage being that they will never end up
         * on server cores. The issue with this is the poor scheduling.
         */
        if (bound_clients)
        {
            if (processor_bind(P_LWPID, P_MYID, hw_thread->hw_thread_id, NULL))
                fatal("processor_bind (%s)", strerror(errno));


            found = 0;

            for(i = 0; i < topology->nb_nodes && !found; i++)
            {
		        for(j = 0; j < topology->nodes[i].nb_hw_threads; j++)
                {
                    if(topology->nodes[i].hw_threads[j] == hw_thread)
                    {
                        found = 1;
                        break;
                    }
                }
            }


            if (verbose)
                printf(INFO("Bound thread #%d to core [%d,%d].\n"),
                        _lwp_self(), i - 1, j);
        }
#endif

//		__sync_fetch_and_add(&current_nb_threads, 1);

/*
		printf("autobind thread %d to hw_thread %d/%d (%d threads out of %d)\n",
               self.id, sched_getcpu(), self.running_hw_thread->hw_thread_id,
               current_nb_threads, wait_nb_threads);
*/
	}
}

void liblock_config_reset(void)
{
    cur_hw_thread = 0;
}

