#!/bin/bash
#
# generate.sh
# ===========
# (C) Jean-Pierre Lozi, 2013
#

. ../../../figures/bench.inc.sh


MACHINES=("amd48b" "niagara2")
BENCHMARKS=("normal-3" "normal-5")

if [[ $# -lt 2 ]]
then
    TAGS=("140224-phd-normal-execution" "140327-phd-normal-execution")
else
    TAGS=("$1" "$2")
fi


function replace_variable
{
    VARIABLE_NAME=$1
    VARIABLE=${!VARIABLE_NAME}

    cat tmp.plt | sed "s/@@$VARIABLE_NAME@@/$VARIABLE/g"                       \
        > tmp-tmp.plt

    mv tmp-tmp.plt tmp.plt
}

for MACHINE_ID in ${!MACHINES[*]}
do

    MACHINE=${MACHINES[$MACHINE_ID]}
    TAG=${TAGS[$MACHINE_ID]}

	for BENCHMARK_ID in ${!BENCHMARKS[*]}
    do

		for YIELD in "" "-yield"
		do

			BENCHMARK=${BENCHMARKS[$BENCHMARK_ID]}
			BENCHMARKY=${BENCHMARK/normal/normal$YIELD}

			RESULTS_PATH="../../all-results/results-$TAG-$MACHINE"
			RESULTS_PATH=$(echo $RESULTS_PATH | sed 's/\//\\\//g')

			if [[ $BENCHMARK == "normal-3" ]]
			then
				if [[ $MACHINE == "niagara2" ]]
				then
					YTICS_LINE="set ytics 10000 norangelimit"
				else
					YTICS_LINE="set ytics 5000 norangelimit"
				fi
			else
				YTICS_LINE="set ytics 500 norangelimit"
			fi

			for KEY in "" "-no-key"
			do

				cat base-${MACHINE}.plt > tmp.plt

				if [[ $KEY == "" ]]
				then
					KEY_LINE="set key"
				else
					KEY_LINE="unset key"
				fi

				replace_variable RESULTS_PATH
				replace_variable BENCHMARK
				replace_variable BENCHMARKY
				replace_variable KEY_LINE
				replace_variable YTICS_LINE
				replace_variable WIDTH

				replace_variable ORIG_SETTINGS
				replace_variable POSIX_SETTINGS
				replace_variable SPINLOCK_SETTINGS
				replace_variable MCS_SETTINGS
				replace_variable MCSTP_SETTINGS
				replace_variable FLAT_SETTINGS
				replace_variable CCSYNCH_SETTINGS
				replace_variable DSMSYNCH_SETTINGS
				replace_variable RCL_SETTINGS

				cat tmp.plt | LC_ALL="en_US.UTF-8" LC_NUMERIC="en_US.UTF-8" gnuplot \
					> output/berkeleydb-${BENCHMARKY}-${TAG}-${MACHINE}${KEY}.pdf \
					2> /dev/null

#				cp tmp.plt custom/other/berkeleydb-${BENCHMARKY}-${TAG}-${MACHINE}${KEY}.plt

				rm tmp.plt

			done

        done

    done

done

for KEY in "key" "key-wide" "key-yield" "key-wide-yield"
do

    cat ${KEY}.plt > tmp.plt

    replace_variable ORIG_SETTINGS
    replace_variable POSIX_SETTINGS
    replace_variable SPINLOCK_SETTINGS
    replace_variable MCS_SETTINGS
    replace_variable MCSTP_SETTINGS
    replace_variable FLAT_SETTINGS
    replace_variable CCSYNCH_SETTINGS
    replace_variable DSMSYNCH_SETTINGS
    replace_variable RCL_SETTINGS

    cat tmp.plt | gnuplot > output/${KEY}.pdf 2> /dev/null

	cp tmp.plt custom/other/${KEY}.plt

    rm tmp.plt

done

