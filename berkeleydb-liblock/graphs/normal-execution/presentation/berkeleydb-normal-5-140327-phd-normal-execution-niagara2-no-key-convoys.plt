#!/usr/bin/gnuplot -persist
#
#
#    	G N U P L O T
#    	Version 4.6 patchlevel 0    last modified 2012-03-04
#    	Build System: Linux x86_64
#
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2012
#    	Thomas Williams, Colin Kelley and many others
#
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')
set terminal pdfcairo  transparent enhanced font "Helvetica, 16" fontscale 0.5 dashed size 6.50in, 3.00in
unset clip points
set clip one
unset clip two
set bar 1.000000 front
set border 31 front linetype -1 linewidth 1.000
set timefmt z "%d/%m/%y,%H:%M"
set zdata
set timefmt y "%d/%m/%y,%H:%M"
set ydata
set timefmt x "%d/%m/%y,%H:%M"
set xdata
set timefmt cb "%d/%m/%y,%H:%M"
set timefmt y2 "%d/%m/%y,%H:%M"
set y2data
set timefmt x2 "%d/%m/%y,%H:%M"
set x2data
set boxwidth
set style fill  empty border
set style rectangle back fc lt -3 fillstyle   solid 1.00 border lt -1
set style circle radius graph 0.02, first 0, 0
set style ellipse size graph 0.05, 0.03, first 0 angle 0 units xy
set dummy x,y
set format x "% g"
set decimal locale 
set format y "%'g"
set format x2 "% g"
set format y2 "% g"
set format z "% g"
set format cb "% g"
set format r "% g"
set angles radians
set grid nopolar
set grid xtics nomxtics ytics nomytics noztics nomztics \
 nox2tics nomx2tics noy2tics nomy2tics nocbtics nomcbtics
set grid layerdefault   linetype 0 linecolor rgb "#555555"  linewidth 3.000,  linetype 0 linecolor rgb "#555555"  linewidth 3.000
set raxis
set key title ""
set key tmargin center horizontal Right noreverse enhanced autotitles nobox
set key noinvert samplen 4 spacing 1 width 0 height 0
set key maxcolumns 0 maxrows 0
set key noopaque
unset key
unset label
unset arrow
set style increment default
unset style line
unset style arrow
set style histogram clustered gap 2 title  offset character 0, 0, 0
unset logscale
set offsets 0, 0, 0, 0
set pointsize 1
set pointintervalbox 1
set encoding default
unset polar
unset parametric
set view 60, 30, 1, 1
set samples 100, 100
set isosamples 10, 10
set surface
unset contour
set clabel '%8.3g'
set mapping cartesian
set datafile separator ","
unset hidden3d
set cntrparam order 4
set cntrparam linear
set cntrparam levels auto 5
set cntrparam points 5
set size ratio 0 1,1
set origin 0,0
set style data points
set style function lines
set xzeroaxis linetype -2 linewidth 1.000
set yzeroaxis linetype -2 linewidth 1.000
set zzeroaxis linetype -2 linewidth 1.000
set x2zeroaxis linetype -2 linewidth 1.000
set y2zeroaxis linetype -2 linewidth 1.000
set ticslevel 0.5
set mxtics default
set mytics 1.000000
set mztics default
set mx2tics default
set my2tics default
set mcbtics default
set xtics border in scale 1,0.5 mirror norotate  offset character 0, 0, 0 autojustify
set xtics norangelimit
set xtics (1, 48, 96, 128, 256, 384)
set ytics border in scale 1,0.5 mirror norotate  offset character 0, 0, 0 autojustify
set ytics 500 norangelimit
set ztics border in scale 1,0.5 nomirror norotate  offset character 0, 0, 0 autojustify
set ztics autofreq  norangelimit
set nox2tics
set noy2tics
set cbtics border in scale 1,0.5 mirror norotate  offset character 0, 0, 0 autojustify
set cbtics autofreq  norangelimit
set rtics axis in scale 1,0.5 nomirror norotate  offset character 0, 0, 0 autojustify
set rtics autofreq  norangelimit
set title ""
set title  offset character 0, 0, 0 font "" norotate
set timestamp bottom
set timestamp ""
set timestamp  offset character 0, 0, 0 font "" norotate
set rrange [ * : * ] noreverse nowriteback
set trange [ * : * ] noreverse nowriteback
set urange [ * : * ] noreverse nowriteback
set vrange [ * : * ] noreverse nowriteback
set xlabel ""
set xlabel  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set x2label ""
set x2label  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set xrange [ 1.00000 : 384.0000 ] noreverse nowriteback
set x2range [ * : * ] noreverse nowriteback
set ylabel ""
set ylabel  offset character 0, 0, 0 font "" textcolor lt -1 rotate by -270
set y2label ""
set y2label  offset character 0, 0, 0 font "" textcolor lt -1 rotate by -270
set yrange [ 100.100000 : * ] noreverse nowriteback
set ytics add (100)
set y2range [ * : * ] noreverse nowriteback
set zlabel ""
set zlabel  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set zrange [ * : * ] noreverse nowriteback
set cblabel ""
set cblabel  offset character 0, 0, 0 font "" textcolor lt -1 rotate by -270
set cbrange [ * : * ] noreverse nowriteback
set zero 1e-08
set lmargin  -1
set bmargin  -1
set rmargin  3
set tmargin  -1
set pm3d explicit at s
set pm3d scansautomatic
set pm3d interpolate 1,1 flush begin noftriangles nohidden3d corners2color mean
set palette positive nops_allcF maxcolors 0 gamma 1.5 color model RGB
set palette rgbformulae 7, 5, 15
set colorbox default
set colorbox vertical origin screen 0.9, 0.2, 0 size screen 0.05, 0.6, 0 front bdefault
set style boxplot candles range  1.50 outliers pt 7 separation 1 labels auto unsorted
set loadpath
set fontpath
set psdir
set fit noerrorvariables
set ylabel "Global # requests / second"
set xlabel "# clients"
GNUTERM = "wxt"
set arrow from 128,100 to 128,2500 nohead lc rgb "red" lw 6
plot "../../../all-results/results-140327-phd-normal-execution-niagara2/rcl-normal-5.csv" using 1:2 with linespoints notitle lc rgb "#AC2B50" lt 1 lw 6 pt 7 pointsize 0.5 axes x1y1, \
"../../../all-results/results-140327-phd-normal-execution-niagara2/dsmsynch-normal-5.csv" using 1:2 with linespoints notitle lc rgb "#523069" lw 6 lt 3 pt 10 axes x1y1, \
"../../../all-results/results-140327-phd-normal-execution-niagara2/ccsynch-normal-5.csv" using 1:2 with linespoints notitle lc rgb "#70428E" lw 6 lt 2 pt 12 axes x1y1, \
"../../../all-results/results-140327-phd-normal-execution-niagara2/mcs-normal-5.csv" using 1:2 with linespoints notitle lc rgb "#62AA2A" lw 6 lt 2 pt 2 axes x1y1, \
NaN with linespoints title "Original" lc rgb "#BA4829" lw 6 lt 1 pt 12 axes x1y1,\
NaN with linespoints title "POSIX" lc rgb "#BA4829" lw 6 lt 1 pt 4 axes x1y1,\
NaN with linespoints title "Spinlock" lc rgb "#BA9029" lw 6 lt 1 pt 1 axes x1y1,\
NaN with linespoints title "MCS" lc rgb "#62AA2A" lw 6 lt 2 pt 2 axes x1y1,\
NaN with linespoints title "MCS-TP" lc rgb "#4C8521" lw 6 lt 3 pt 3 axes x1y1,\
NaN with linespoints title "Flat Combining" lc rgb "#015C65" lw 6 lt 1 pt 6 axes x1y1,\
NaN with linespoints title "CC-Synch" lc rgb "#70428E" lw 6 lt 2 pt 12 axes x1y1,\
NaN with linespoints title "DSM-Synch" lc rgb "#523069" lw 6 lt 3 pt 10 axes x1y1,\
NaN with linespoints title "RCL" lc rgb "#AC2B50" lt 1 lw 6 pt 7 pointsize 0.5 axes x1y1
#    EOF
