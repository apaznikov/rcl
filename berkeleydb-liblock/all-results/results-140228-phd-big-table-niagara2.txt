[1;38;05;161m#####################################################################
# Left part                                                         #
#####################################################################
[0m
[1;38;05;10m[1;48;05;16mRequest type = ORDER STATUS[0m
[1;48;05;161mLock = orig[0m
[1;38;05;99m[   1 clients] [0m0.17 0.18 0.17 0.17 0.18 [0;38;05;118mavg=[0m0.174000 [0;38;05;118mmin=[0m0.170000 [0;38;05;118mmax=[0m0.180000 [0;38;05;118m[0m
[1;38;05;99m[   4 clients] [0m0.10 0.11 0.12 0.10 0.10 [0;38;05;118mavg=[0m0.106000 [0;38;05;118mmin=[0m0.100000 [0;38;05;118mmax=[0m0.120000 [0;38;05;118m[0m
[1;38;05;99m[   8 clients] [0m0.09 0.09 0.09 0.07 0.06 [0;38;05;118mavg=[0m0.080000 [0;38;05;118mmin=[0m0.060000 [0;38;05;118mmax=[0m0.090000 [0;38;05;118m[0m
[1;38;05;99m[  16 clients] [0m0.07 0.37 0.06 0.06 0.08 [0;38;05;118mavg=[0m0.128000 [0;38;05;118mmin=[0m0.060000 [0;38;05;118mmax=[0m0.370000 [0;38;05;118m[0m
[1;38;05;99m[  32 clients] [0m41.76 45.18 44.67 44.19 42.98 [0;38;05;118mavg=[0m43.756000 [0;38;05;118mmin=[0m41.760000 [0;38;05;118mmax=[0m45.180000 [0;38;05;118m[0m
[1;38;05;99m[  64 clients] [0m65.74 63.37 62.49 62.85 63.49 [0;38;05;118mavg=[0m63.588000 [0;38;05;118mmin=[0m62.490000 [0;38;05;118mmax=[0m65.740000 [0;38;05;118m[0m
[1;38;05;99m[ 128 clients] [0m75.59 75.27 78.52 78.10 74.75 [0;38;05;118mavg=[0m76.446000 [0;38;05;118mmin=[0m74.750000 [0;38;05;118mmax=[0m78.520000 [0;38;05;118m[0m

[1;38;05;10m[1;48;05;16mRequest type = STOCK LEVEL[0m
[1;48;05;161mLock = orig[0m
[1;38;05;99m[   1 clients] [0m0.17 0.17 0.16 0.19 0.18 [0;38;05;118mavg=[0m0.174000 [0;38;05;118mmin=[0m0.160000 [0;38;05;118mmax=[0m0.190000 [0;38;05;118m[0m
[1;38;05;99m[   4 clients] [0m0.01 0.18 0.17 0.17 0.18 [0;38;05;118mavg=[0m0.142000 [0;38;05;118mmin=[0m0.010000 [0;38;05;118mmax=[0m0.180000 [0;38;05;118m[0m
[1;38;05;99m[   8 clients] [0m0.06 0.15 0.13 0.16 0.18 [0;38;05;118mavg=[0m0.136000 [0;38;05;118mmin=[0m0.060000 [0;38;05;118mmax=[0m0.180000 [0;38;05;118m[0m
[1;38;05;99m[  16 clients] [0m0.06 0.01 0.07 0.07 0.08 [0;38;05;118mavg=[0m0.058000 [0;38;05;118mmin=[0m0.010000 [0;38;05;118mmax=[0m0.080000 [0;38;05;118m[0m
[1;38;05;99m[  32 clients] [0m56.36 55.50 54.98 55.34 55.42 [0;38;05;118mavg=[0m55.520000 [0;38;05;118mmin=[0m54.980000 [0;38;05;118mmax=[0m56.360000 [0;38;05;118m[0m
[1;38;05;99m[  64 clients] [0m78.57 78.12 77.91 77.67 78.53 [0;38;05;118mavg=[0m78.160000 [0;38;05;118mmin=[0m77.670000 [0;38;05;118mmax=[0m78.570000 [0;38;05;118m[0m
[1;38;05;99m[ 128 clients] [0m87.42 86.22 88.01 87.55 86.40 [0;38;05;118mavg=[0m87.120000 [0;38;05;118mmin=[0m86.220000 [0;38;05;118mmax=[0m88.010000 [0;38;05;118m[0m

[1;38;05;10m[1;48;05;16mRequest type = PAYMENT[0m
[1;48;05;161mLock = orig[0m
[1;38;05;99m[ 128 clients] [0m1.83 1.84 1.82 1.85 1.85 [0;38;05;118mavg=[0m1.838000 [0;38;05;118mmin=[0m1.820000 [0;38;05;118mmax=[0m1.850000 [0;38;05;118m[0m

[1;38;05;10m[1;48;05;16mRequest type = NEW ORDER[0m
[1;48;05;161mLock = orig[0m
[1;38;05;99m[ 128 clients] [0m0.35 0.34 0.34 0.35 0.35 [0;38;05;118mavg=[0m0.346000 [0;38;05;118mmin=[0m0.340000 [0;38;05;118mmax=[0m0.350000 [0;38;05;118m[0m

[1;38;05;10m[1;48;05;16mRequest type = DELIVERY[0m
[1;48;05;161mLock = orig[0m
[1;38;05;99m[ 128 clients] [0m0.82 0.82 0.81 0.79 0.81 [0;38;05;118mavg=[0m0.810000 [0;38;05;118mmin=[0m0.790000 [0;38;05;118mmax=[0m0.820000 [0;38;05;118m[0m



[1;38;05;161m#####################################################################
# Right part                                                        #
#####################################################################
[0m
[1;38;05;10m[1;48;05;16mRequest type = ORDER STATUS[0m
Found that the lock with the most contention spends 76.7071539870% of its time in CS.
Lock info: id=67028, alloc_id=0x994535cf, address=0x97ba8020.

Backtrace for this lock:

[0;38;05;118m[lock-profiler][0m [mutex #67028/0x994535cf]	Library = posix-mutrace, backtrace:
[0;38;05;118m[lock-profiler][0m [mutex #67028/0x994535cf]	0xff103fcc /u1/jlozi/svn-margaux/margaux/rcl/lock-profiler/lock-profiler.so:0xff100000 pthread_mutex_init:0xff103f74
[0;38;05;118m[lock-profiler][0m [mutex #67028/0x994535cf]	0x77cf4    /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    __db_pthread_mutex_init:0x77cb8   
[0;38;05;118m[lock-profiler][0m [mutex #67028/0x994535cf]	0xf6574    /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    __mutex_alloc_int:0xf61ec   
[0;38;05;118m[lock-profiler][0m [mutex #67028/0x994535cf]	0xfa168    /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    __env_setup:0xf9f30   
[0;38;05;118m[lock-profiler][0m [mutex #67028/0x994535cf]	0x2eda4    /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    __db_open:0x2eb90   
[0;38;05;118m[lock-profiler][0m [mutex #67028/0x994535cf]	0x10b4c8   /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    __db_open_pp:0x10aeb0  
[0;38;05;118m[lock-profiler][0m [mutex #67028/0x994535cf]	0x28ca4    /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    open_db:0x28c70   
[0;38;05;118m[lock-profiler][0m [mutex #67028/0x994535cf]	0x26274    /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    main:0x25c00   
[0;38;05;118m[lock-profiler][0m [mutex #67028/0x994535cf]	0x26638    /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    _start:0x265dc   

Using locks that contain DISCR=0xfa168 in their stacktrace, i.e. with the following IDs:

0xe98e2d3c 0x77393b93 0x1a78527e 0x5e92d783 0xeecdb890 0x51df0d6c 0x88f325ec 0xdcbd9545 0x6db37959 0x994535cf 0x124f2f59 

Time in CS for this lock :
==========================
77.8785 78.2398 77.7986 75.9264 75.1856 [0;38;05;118mavg=[0m77.005780 [0;38;05;118mmin=[0m75.185600 [0;38;05;118mmax=[0m78.239800 [0;38;05;118m[0m

Data cache misses for this lock :
=================================
3.96269 3.98097 4.12689 4.54352 3.48724 [0;38;05;118mavg=[0m4.020262 [0;38;05;118mmin=[0m3.487240 [0;38;05;118mmax=[0m4.543520 [0;38;05;118m[0m

[1;38;05;10m[1;48;05;16mRequest type = STOCK LEVEL[0m
Found that the lock with the most contention spends 53.9200145839% of its time in CS.
Lock info: id=67018, alloc_id=0xdcbd9545, address=0x97ba7b70.

Backtrace for this lock:

[0;38;05;118m[lock-profiler][0m [mutex #67018/0xdcbd9545]	Library = posix-mutrace, backtrace:
[0;38;05;118m[lock-profiler][0m [mutex #67018/0xdcbd9545]	0xff103fcc /u1/jlozi/svn-margaux/margaux/rcl/lock-profiler/lock-profiler.so:0xff100000 pthread_mutex_init:0xff103f74
[0;38;05;118m[lock-profiler][0m [mutex #67018/0xdcbd9545]	0x77cf4    /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    __db_pthread_mutex_init:0x77cb8   
[0;38;05;118m[lock-profiler][0m [mutex #67018/0xdcbd9545]	0xf6574    /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    __mutex_alloc_int:0xf61ec   
[0;38;05;118m[lock-profiler][0m [mutex #67018/0xdcbd9545]	0xfa168    /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    __env_setup:0xf9f30   
[0;38;05;118m[lock-profiler][0m [mutex #67018/0xdcbd9545]	0x2eda4    /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    __db_open:0x2eb90   
[0;38;05;118m[lock-profiler][0m [mutex #67018/0xdcbd9545]	0x10b4c8   /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    __db_open_pp:0x10aeb0  
[0;38;05;118m[lock-profiler][0m [mutex #67018/0xdcbd9545]	0x28ca4    /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    open_db:0x28c70   
[0;38;05;118m[lock-profiler][0m [mutex #67018/0xdcbd9545]	0x261c4    /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    main:0x25c00   
[0;38;05;118m[lock-profiler][0m [mutex #67018/0xdcbd9545]	0x26638    /u1/jlozi/svn-margaux/margaux/rcl/berkeleydb-liblock/TPCC-BDB-RELEASE-original/tpcc_xact_server:0x10000    _start:0x265dc   

Using locks that contain DISCR=0xfa168 in their stacktrace, i.e. with the following IDs:

0xe98e2d3c 0x77393b93 0x1a78527e 0x5e92d783 0xeecdb890 0x51df0d6c 0x88f325ec 0xdcbd9545 0x6db37959 0x994535cf 0x124f2f59 

Time in CS for this lock :
==========================
87.4569 87.5841 87.4787 87.655 87.5868 [0;38;05;118mavg=[0m87.552300 [0;38;05;118mmin=[0m87.456900 [0;38;05;118mmax=[0m87.655000 [0;38;05;118m[0m

Data cache misses for this lock :
=================================
3.42243 3.41861 3.47595 3.48175 3.44822 [0;38;05;118mavg=[0m3.449392 [0;38;05;118mmin=[0m3.418610 [0;38;05;118mmax=[0m3.481750 [0;38;05;118m[0m

