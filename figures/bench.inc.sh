RCL="rcl"

benchs="$RCL posix spinlock mcs mcstp flat ccsynch dsmsynch"


# Not used by all benchmarks.
bench_definitions=(
    'posix'    'POSIX'    '[posix] liblock: '          'posix'
    'lc rgb "#BA4829" lw 2 lt 1 pt 4'               '' '-F posix'
    'spinlock' 'SL'       '[spinlock] liblock: '       'sl'
    'lc rgb "#BA9029" lw 2 lt 1 pt 1'               '' '-F spinlock'
    'mcs'      'MCS'      '[mcs] liblock: '            'mcsl'
    'lc rgb "#62AA2A" lw 2 lt 2 pt 2'               '' '-F mcs'
    'mcstp'    'MCS-TP'    '[mcstp] liblock: '          'mcstpl'
    'lc rgb "#62AA2A" lw 2 lt 3 pt 3'               '' '-F mcstp'
    'flat'     'FC'       '[flat combining] liblock: ' 'fc'
    'lc rgb "#015C65" lw 2 lt 1 pt 6'               '' '-F flat'
    'ccsynch'  'CCSYNCH'  '[ccsynch] liblock: '        'ccsynch'
    'lc rgb "#70428E" lw 2 lt 2 pt 12'              '' '-F ccsynch'
    'dsmsynch' 'DSMSYNCH' '[dsmsynch] liblock: '       'dsmsynch'
    'lc rgb "#70428E" lw 2 lt 3 pt 10'              '' '-F dsmsynch'
    'rcl'      'RCL'      '[rcl] liblock: '            'rcl'
    'lc rgb "#AC2B50" lt 1 lw 5 pt 7 pointsize 0.5' '' '-F rcl'
)

BENCH_DEFINITIONS=$bench_definitions

ORIG_SETTINGS='lc rgb "#BA4829" lw 2 lt 1 pt 12'
POSIX_SETTINGS='lc rgb "#BA4829" lw 2 lt 1 pt 4'
SPINLOCK_SETTINGS='lc rgb "#BA9029" lw 2 lt 1 pt 1'
MCS_SETTINGS='lc rgb "#62AA2A" lw 2 lt 2 pt 2'
MCSTP_SETTINGS='lc rgb "#4C8521" lw 2 lt 3 pt 3'
FLAT_SETTINGS='lc rgb "#015C65" lw 2 lt 1 pt 6'
CCSYNCH_SETTINGS='lc rgb "#70428E" lw 2 lt 2 pt 12'
DSMSYNCH_SETTINGS='lc rgb "#523069" lw 2 lt 3 pt 10'
RCL_SETTINGS='lc rgb "#AC2B50" lt 1 lw 5 pt 7 pointsize 0.5'

ORIG_SETTINGS_TALK='lc rgb "#BA4829" lw 2 lt 1 pt 12'
POSIX_SETTINGS_TALK='lc rgb "#BA4829" lw 2 lt 1 pt 4'
SPINLOCK_SETTINGS_TALK='lc rgb "#BA9029" lw 2 lt 1 pt 1'
MCS_SETTINGS_TALK='lc rgb "#62AA2A" lw 2 lt 2 pt 2'
MCSTP_SETTINGS_TALK='lc rgb "#4C8521" lw 2 lt 3 pt 3'
FLAT_SETTINGS_TALK='lc rgb "#015C65" lw 2 lt 1 pt 6'
CCSYNCH_SETTINGS_TALK='lc rgb "#70428E" lw 2 lt 2 pt 12'
DSMSYNCH_SETTINGS_TALK='lc rgb "#523069" lw 2 lt 3 pt 10'
RCL_SETTINGS_TALK='lc rgb "#AC2B50" lt 1 lw 5 pt 7 pointsize 0.5'

ORIG_SETTINGS_NP='lc rgb "#BA4829" lw 2 lt 1'
POSIX_SETTINGS_NP='lc rgb "#BA4829" lw 2 lt 1'
SPINLOCK_SETTINGS_NP='lc rgb "#BA9029" lw 2 lt 1'
MCS_SETTINGS_NP='lc rgb "#62AA2A" lw 2 lt 2'
MCSTP_SETTINGS_NP='lc rgb "#4C8521" lw 2 lt 3'
FLAT_SETTINGS_NP='lc rgb "#015C65" lw 2 lt 1'
CCSYNCH_SETTINGS_NP='lc rgb "#70428E" lw 2 lt 2'
DSMSYNCH_SETTINGS_NP='lc rgb "#523069" lw 2 lt 3'
RCL_SETTINGS_NP='lc rgb "#AC2B50" lt 1 lw 5 pt 7'

ORIG_SETTINGS_NP_TALK='lc rgb "#BA4829" lw 2 lt 1'
POSIX_SETTINGS_NP_TALK='lc rgb "#BA4829" lw 2 lt 1'
SPINLOCK_SETTINGS_NP_TALK='lc rgb "#BA9029" lw 2 lt 1'
MCS_SETTINGS_NP_TALK='lc rgb "#62AA2A" lw 2 lt 2'
MCSTP_SETTINGS_NP_TALK='lc rgb "#4C8521" lw 2 lt 3'
FLAT_SETTINGS_NP_TALK='lc rgb "#015C65" lw 2 lt 1'
CCSYNCH_SETTINGS_NP_TALK='lc rgb "#70428E" lw 2 lt 2'
DSMSYNCH_SETTINGS_NP_TALK='lc rgb "#523069" lw 2 lt 3'
RCL_SETTINGS_NP_TALK='lc rgb "#AC2B50" lt 1 lw 5 pt 7'


on_bench()
{
	bench=$1
    shift
    let ARG_NUM=0
		FOUND=0

    while [ $ARG_NUM -lt ${#bench_definitions[*]} ]
    do
		if [ ${bench_definitions[$ARG_NUM]} = $bench ]
        then
			$@ "${bench_definitions[$ARG_NUM]}" "${bench_definitions[$ARG_NUM+1]}" "${bench_definitions[$ARG_NUM+2]}" \
               "${bench_definitions[$ARG_NUM+3]}" "${bench_definitions[$ARG_NUM+4]}" "${bench_definitions[$ARG_NUM+5]}" \
               ${bench_definitions[$ARG_NUM+6]}
			FOUND=1
		fi
        let ARG_NUM=$ARG_NUM+7
    done

	if [ $FOUND = 0 ]; then
		echo "FATAL: unable to find benchmark '$bench'"
		exit 42
	fi
}

gen_var()
{
		point=$1
		let T=0
		A=0
		V=0
		while read line; do
				let T=$T+1
				A="$A+$line"
				V="$V+($line - a)^2"
		done
		avg=$(echo "($A)/$T" | bc -l)
		var=$(echo "a=$avg; sqrt($V)" | bc -l)
		echo "$point,$avg,$var"
}

