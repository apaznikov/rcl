/* ########################################################################## */
/* (C) UPMC, 2010-2014                                                        */
/*     Authors:                                                               */
/*       Jean-Pierre Lozi <jean-pierre.lozi@lip6.fr>                          */
/*       Gaël Thomas <gael.thomas@lip6.fr                                     */
/*       Florian David <florian.david@lip6.fr>                                */
/*       Julia Lawall <julia.lawall@lip6.fr>                                  */
/*       Gilles Muller <gilles.muller@lip6.fr>                                */
/* -------------------------------------------------------------------------- */
/* ########################################################################## */
#include <pthread.h>
#include <errno.h>
#include <dlfcn.h>
#include <assert.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <papi.h>
#ifdef __sun__
#include <libcpc.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/processor.h>
#include <sys/procset.h>
#else
#include <execinfo.h>
#endif

#include "liblock.h"

#include "hash.h"
#ifdef __linux__
#include "backtrace-symbols.h"
#endif


/******************************************************************************/
/* Definitions                                                                */
/******************************************************************************/
#define conds                     1
#define MAX_THREADS               256
#define NUM_HASHES                65536
#define MAX_NESTED                8
#define MAX_STACK                 64
#define MAX_STR_LENGTH            1024
#ifndef PAGE_SIZE
#define PAGE_SIZE                 4096
#endif
#ifndef CACHE_LINE_SIZE
#define CACHE_LINE_SIZE           64
#endif

#define posix_id                  "posix-mutrace"


#define REAL(name)                real_ ## name
#define S(_)                      #_

#define R_ALIGN(n, r)             (((n) + (r) - 1) & -(r))
#define CACHE_ALIGN(n)            r_align(n , CACHE_LINE_SIZE)
#define PAD_TO_CACHE_LINE(n)      (CACHE_ALIGN(n) - (n))

#ifdef __sun__
#define PREPTR                    "0x"
#else
#define PREPTR                    ""
#endif


#define LOAD_FUNC(name, E)                                                     \
    do                                                                         \
    {                                                                          \
        real_##name = dlsym(RTLD_NEXT, S(name));                               \
        if(E && !real_##name)                                                  \
            fprintf(stderr, "WARNING: unable to find symbol: %s.\n", S(name)); \
    }                                                                          \
    while (0)
#ifdef __sun__
// Solaris doesn't provide dlvsym().
#define LOAD_FUNC_VERSIONED(name, E, version)       LOAD_FUNC(name, E)
#else
#define LOAD_FUNC_VERSIONED(name, E, version)                                  \
    do                                                                         \
    {                                                                          \
        real_##name = dlvsym(RTLD_NEXT, S(name), version);                     \
        if(E && !real_##name)                                                  \
            fprintf(stderr, "WARNING: unable to find symbol: %s.\n", S(name)); \
    }                                                                          \
    while (0)
#endif

#ifdef __sun__
#define READ_ASR17(res)           asm volatile ("rd %%asr17, %0\n" : "=r" (res))
#else
#define READ_ASR17(res)           echo_error("libcpc requires Solaris");
#endif

#define READ_EVENT_COUNTERS(res)                                               \
    do                                                                         \
    {                                                                          \
        if (library == LIB_PAPI) { read_papi_event_counters(&(res)); }         \
        else if (library == LIB_CPC) { READ_ASR17((res)); }                    \
        else { (res) = get_cycles(); }                                         \
    }                                                                          \
    while (0)


/******************************************************************************/
/* Enums                                                                      */
/******************************************************************************/
typedef enum {
    TB_NONE, TB_BY_HW_THREAD, TB_BY_CORE, TB_BY_NODE
} thread_binding_t;

typedef enum {
    TM_PER_NODE, TM_PER_CORE
} thread_monitoring_t;

typedef enum {
    LIB_PAPI, LIB_CPC, LIB_NONE_CYCLES
} library_t;


/******************************************************************************/
/* Structures                                                                 */
/******************************************************************************/
struct nested {
    int depth;
    unsigned long long nb_outer_cs;

    unsigned long long first_event;
    unsigned long long real_last_event;
    //unsigned long long real_last_event;

#define PAD_NESTED ((PAGE_SIZE - sizeof(int) - 3 * sizeof(unsigned long long)) \
                    / sizeof(unsigned long long))

    unsigned long long last_event[PAD_NESTED];
};

struct lock_info_thread {
    unsigned long long num_events;
    unsigned long long num_cs;

    char pad[PAD_TO_CACHE_LINE(sizeof(unsigned long long))];
};

struct lock_info {
    struct lock_info *next;
    const char *lib;
    void *lock;
    int n_stack_frames;
    int mutex_id, owner_id;
    uint32_t alloc_id;

    char pad0[PAD_TO_CACHE_LINE(3 * sizeof(void *) + 3 * sizeof(int))
              + sizeof(uint32_t)];

    struct lock_info_thread per_threads[MAX_THREADS];
    void *stack[MAX_STACK];
};

struct hash_table {
    int n_hash;
    struct lock_info *volatile table[NUM_HASHES];
};

struct routine {
    void *(*fct)(void *);
    void *arg;
};

/*
struct self_id_assoc {
    size_t thread_id;
    struct lock_info *lock_info;
    char pad[PAD_TO_CACHE_LINE(sizeof(size_t) + sizeof(void *))];
};
*/


/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static unsigned long long                   event_id;
static char                                 *event_id_str;
static thread_binding_t                     thread_binding = TB_BY_HW_THREAD;
static thread_monitoring_t                  thread_monitoring = TB_BY_NODE;
static library_t                            library = LIB_PAPI;
static int                                  inside = 0;
static int                                  offset = 0;
static int                                  max_thread_id = 0;
static int                                  inited = 0;
static size_t                               cur_mutex_id = 0;
static struct hash_table                    table = { NUM_HASHES };
static struct lock_info_thread              info_threads[MAX_THREADS];
static struct nested                        nesteds[MAX_THREADS];
//static pthread_mutex_t                    global_mutex;
//static struct self_id_assoc               self_id_assocs[MAX_THREADS];
#ifdef __sun__
static cpc_t                                *rcw_cpc;
#endif

static __thread int                         recurse = 1;
static __thread int                         monitored = 0;
static __thread int                         thread_id = -1, hw_thread_id = -1;
static __thread int                         event_set = PAPI_NULL;
#ifdef __sun__
static __thread cpc_set_t                   *cpc_set;
static __thread int                         cpc_index;
#endif


/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static void
bind_thread                         (void);

#ifdef __sun__
static int
context_walker                      (uintptr_t pc,
                                     int sig,
                                     void *_lock_info);
#endif

static void
echo                                (const char *msg, ...);

static void
echo_error                          (const char *msg, ...);

#ifdef __sun__
static void
emt_handler                         (int sig, siginfo_t *sip, void *arg);
#endif

static inline unsigned long long
get_cycles                          (void);

static void
get_global_info                     (struct lock_info_thread *infos,
                                     double *p_in_cs,
                                     unsigned long long *p_num_cs,
                                     double *p_events_per_cs);

static struct lock_info *
ht_get                              (const char *lib,
                                     void *lock);

static void
ht_foreach                          (void (*fct)(struct lock_info *));

static void
init                                (void);

static void
init_thread                         (void);

static void *
lp_start_routine                    (void *_arg);

static void
print_lock_stack                    (struct lock_info *ginfo);

static void
print_lock_info                     (struct lock_info *info);

static inline void                  __attribute__((always_inline))
read_papi_event_counters                 (unsigned long long *result);

static void
stop                                (void);

static int
(*REAL(pthread_mutex_init))         (pthread_mutex_t *mutex,
                                     const pthread_mutexattr_t *attr) = 0;

static int
(*REAL(pthread_mutex_destroy))      (pthread_mutex_t *mutex);

static int
(*REAL(pthread_mutex_lock))         (pthread_mutex_t *mutex);

static int
(*REAL(pthread_mutex_trylock))      (pthread_mutex_t *mutex);

static int
(*REAL(pthread_mutex_unlock))       (pthread_mutex_t *mutex);

static int
(*REAL(pthread_create))             (pthread_t *thread,
                                     const pthread_attr_t *attr,
                                     void *(*start_routine) (void *),
                                     void *arg);

#ifdef conds
static int
(*REAL(pthread_cond_timedwait))     (pthread_cond_t *cond,
                                     pthread_mutex_t *mutex,
                                     const struct timespec *abstime);
static int
(*REAL(pthread_cond_wait))          (pthread_cond_t *cond,
                                     pthread_mutex_t *mutex);
/*
static int
(*real_pthread_cond_signal)         (pthread_cond_t *cond);

static int
(*real_pthread_cond_broadcast)      (pthread_cond_t *cond);
*/
#endif

static void
(*REAL(exit))                       (int status) __attribute__((noreturn));

static void
(*REAL(_exit))                      (int status) __attribute__((noreturn));

static void
(*REAL(_Exit))                      (int status) __attribute__((noreturn));

#if 0
static void *
(*REAL(liblock_exec))               (liblock_lock_t *lock,
                                     void *(*pending)(void*),
                                     void *val);

static int
(*REAL(liblock_cond_wait))          (liblock_cond_t *cond,
                                     liblock_lock_t *lock);

static int
(*REAL(liblock_cond_timedwait))     (liblock_cond_t *cond,
                                     liblock_lock_t *lock,
                                     struct timespec *abstime);

static int
(*REAL(liblock_lock_init))          (const char *type,
                                     struct hw_thread *hw_thread,
                                     liblock_lock_t *lock,
                                     void *arg);
#endif

void
liblock_on_server_thread_start      (const char *lib,
                                     unsigned int thread_id);

void
liblock_on_server_thread_end        (const char *lib,
                                     unsigned int thread_id);

void
liblock_on_create                   (const char *lib,
                                     void *lock);

void
liblock_on_destroy                  (const char *lib,
                                     void *lock);


/******************************************************************************/
/* Function definitions                                                       */
/******************************************************************************/
static void bind_thread(void)
{
    int i, j, k = 0;

    switch (thread_binding)
    {
        case TB_BY_HW_THREAD:
            for (;;)
            {
                for(i = 0; i < topology->nb_nodes; i++)
                {
                    for(j = 0; j < topology->nodes[i].nb_hw_threads; j++)
                    {
                        if (thread_id == k++)
                        {
                            hw_thread_id =
                                topology->nodes[i].hw_threads[j]->hw_thread_id;
                            goto bind_thread_endswitch;
                        }
                    }
                }
            }
            break;

        case TB_BY_NODE:
            for (;;)
            {
                for(j = 0; j < topology->nodes[0].nb_hw_threads; j++)
                {
                    for(i = 0; i < topology->nb_nodes; i++)
                    {
                        if (topology->nodes[0].nb_hw_threads
                            != topology->nodes[i].nb_hw_threads)
                            echo_error("Nodes have different amounts of cores. "
                                       "Unsupported configuration.");

                        if (thread_id == k++)
                        {
                            hw_thread_id =
                                topology->nodes[i].hw_threads[j]->hw_thread_id;
                            goto bind_thread_endswitch;
                        }
                    }
                }
            }
            break;

        case TB_BY_CORE:
            for (;;)
            {
                for(j = 0; j < topology->cores[0].nb_hw_threads; j++)
                {
                    for(i = 0; i < topology->nb_cores; i++)
                    {
                        if (topology->cores[0].nb_hw_threads
                            != topology->cores[i].nb_hw_threads)
                            echo_error("Nodes have different amounts of cores. "
                                       "Unsupported configuration.");

                        if (thread_id == k++)
                        {
                            hw_thread_id =
                                topology->cores[i].hw_threads[j]->hw_thread_id;
                            goto bind_thread_endswitch;
                        }
                    }
                }
            }
            break;

        default:
            hw_thread_id = -1;
            break;
    }
bind_thread_endswitch:;

#ifdef __sun__
    if(processor_bind(P_LWPID, P_MYID, hw_thread_id, NULL))
        echo_error("processor_bind() failed");
#else
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(hw_thread_id, &cpuset);

    if(pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset))
        echo_error("pthread_setaffinity_np() failed");
#endif
}

#ifdef __sun__
static int context_walker(uintptr_t pc, int sig, void *_lock_info)
{
    struct lock_info *lock_info = (struct lock_info *)_lock_info;
    int n = lock_info->n_stack_frames++;

/*
    Dl_info dlip;
    if (!dladdr((void *)pc, &dlip))
        echo_error("dladdr() failed.");
*/

    lock_info->stack[n] = (void *)pc;

    return 0;
}
#endif

static void echo(const char *msg, ...)
{
    va_list va;
    char str[MAX_STR_LENGTH];

    str[0] = '\0';
    strcat(str, "\e[0;38;05;118m[lock-profiler]\e[0m ");
    strcat(str, msg);
    strcat(str, "\n");

    va_start(va, msg);
    vfprintf(stderr, str, va);
    fflush(stderr);
    va_end(va);
}

static void echo_error(const char *msg, ...)
{
    va_list va;
    char str[MAX_STR_LENGTH];

    str[0] = '\0';
    strcat(str, "\e[0;38;05;118m[lock-profiler]\e[0m ");
    strcat(str, "ERROR: ");
    strcat(str, msg);
    strcat(str, "\n");

    va_start(va, msg);
    vfprintf(stderr, str, va);
    fflush(stderr);
    va_end(va);

    exit(EXIT_FAILURE);
}

#ifdef __sun__
static void
emt_handler(int sig, siginfo_t *sip, void *arg)
{
    echo_error("A hardware counter has overflowed.\n");

/*
    if (sig != SIGEMT || sip->si_code != EMT_CPCOVF)
       return;

    if (!overflowed) overflowed = 1;

    for (i = 0; i < max_thread_id; i++)
        for (j = 0; j < MAX_NESTED; j++)
            nesteds[i].last_event[j] = 0;


    if (cpc_request_preset(cpc, cpc_index, 0) != 0)
        error("cpc_request_preset() failed");

    if (cpc_set_restart(cpc, set) != 0)
        error("cpc_request_preset() failed");
*/
}
#endif

#if (defined(__i386__) || defined(__x86_64__))
__attribute__((always_inline))
inline static unsigned long long get_cycles(void)
{
    long long ret = 0;

#ifdef __x86_64__
/*
    unsigned int a, d;

    asm volatile ( "rdtsc":"=a" (a), "=d" (d));
    ret = ((long long)a) | (((long long)d) << 32);
*/
    uint64_t rax, rdx;
    uint32_t aux;

    asm volatile ( "rdtscp\n" : "=a" (rax), "=d" (rdx), "=c" (aux): : );
    ret = (rdx << 32) + rax;
#else
    __asm__ __volatile__("rdtsc" : "=A"(ret):);
#endif

    return ret;
}
#elif defined(__sparc__)
__attribute__((always_inline))
inline static unsigned long long get_cycles(void)
{
/*
 * Implementation #1
 *
 * Doesn't work properly on 32-bit sparc.
 *

    register unsigned long ret asm("g1");

    asm volatile(".word 0x83410000" // rd %tick, %g1
                 : "=r"(ret));

    return ret;
*/

/*
 * Implementation #2
 *
 * Probably induces an overhead.
 *
    return gethrtime() * frequency;
*/


/*
 * Implementation #3
 *
 * Works well on 32-bit sparc. Implementation #1 might be better for 64-bit
 * sparc, though.
 *
 */
    union {
        uint64_t v64;
        struct {
            uint32_t high;
            uint32_t low;
        } v32;
    } ret;

    asm volatile("rd %%tick, %%g1; srlx %%g1, 32, %0; mov %%g1, %1"
                 : "=r"(ret.v32.high), "=r"(ret.v32.low) : : "g1");

    return ret.v64;
}
#else
#error Unsupported architecture.
#endif

static void get_global_info(struct lock_info_thread *infos,
                            double *p_in_cs,
                            unsigned long long *p_num_cs,
                            double *p_events_per_cs)
{
    size_t i;
    unsigned long long total = 0;
    unsigned long long num_events = 0;
    unsigned long long num_cs = 0;

    for (i = 0; i < max_thread_id; i++)
    {
        total += nesteds[i].real_last_event - nesteds[i].first_event;
        num_events += infos[i].num_events;
        num_cs += infos[i].num_cs;
    }

    if (total)
    {
        *p_in_cs = 100 * num_events / (double)total;
        *p_events_per_cs = num_events / (double)num_cs;
        *p_num_cs = num_cs;
    }
    else
    {
        *p_in_cs = 0;
        *p_events_per_cs = 0;
        *p_num_cs = 0;
    }
}

static struct lock_info *ht_get(const char *lib, void *lock)
{
    struct lock_info *volatile *entry =
        &table.table[((uintptr_t)lock >> 4) % table.n_hash];
    struct lock_info *attempt;
    struct lock_info *cur;

    for (;;)
    {
        attempt = *entry;

        for(cur = attempt; cur; cur = cur->next)
        {
            if(cur->lock == lock)
                return cur;
        }

        recurse = 0;

        struct lock_info *res = calloc(1, sizeof(struct lock_info));

        if (res == NULL)
            echo_error("calloc() failed");


        res->lock     = lock;
        res->lib      = lib ? lib : "<unknown>";
        res->next     = attempt;
        res->mutex_id = __sync_fetch_and_add(&cur_mutex_id, 1);

        if(__sync_val_compare_and_swap(entry, attempt, res) == attempt)
        {
            recurse = 1;
#ifdef __sun__
            ucontext_t ucp;

            if (getcontext(&ucp))
                echo_error("getcontext() failed");

            if (walkcontext(&ucp, context_walker, res))
                echo_error("walkcontext() failed.");

            res->alloc_id = hash((char *)res->stack,
                                 res->n_stack_frames * sizeof(res->stack[0]));
#else
            res->n_stack_frames = backtrace(res->stack, MAX_STACK);

            res->alloc_id = hash((char *)(res->stack + 2),
                                 (res->n_stack_frames - 4) * sizeof(res->stack[0]));
#endif

            return res;
        }
        else
        {
            free(res);
            recurse = 1;
        }
    }
}

static void ht_foreach(void (*fct)(struct lock_info *))
{
    size_t i;
    struct lock_info *cur;

    for(i = 0; i < table.n_hash; i++)
        for(cur = table.table[i]; cur; cur = cur->next)
            fct(cur);
}

static void init(void)
{
    const char *tmp_str;

    if (!__sync_val_compare_and_swap(&inited, 0, 1))
    {
        LOAD_FUNC(pthread_mutex_init, 1);
        LOAD_FUNC(pthread_mutex_destroy, 1);
        LOAD_FUNC(pthread_mutex_lock, 1);
        LOAD_FUNC(pthread_mutex_trylock, 1);
        LOAD_FUNC(pthread_mutex_unlock, 1);
#ifdef conds
        LOAD_FUNC_VERSIONED(pthread_cond_timedwait, 1, "GLIBC_2.3.2");
        LOAD_FUNC_VERSIONED(pthread_cond_wait, 1, "GLIBC_2.3.2");
/*
        LOAD_FUNC(pthread_cond_signal, 1);
        LOAD_FUNC(pthread_cond_broadcast, 1);
*/
#endif
        LOAD_FUNC(pthread_create, 1);
        LOAD_FUNC(exit, 1);
        LOAD_FUNC(_exit, 1);
        LOAD_FUNC(_Exit, 1);
/*
        LOAD_FUNC(liblock_exec, 0);
        LOAD_FUNC(liblock_cond_wait, 0);
        LOAD_FUNC(liblock_cond_timedwait, 0);
        LOAD_FUNC(liblock_lock_init, 0);
*/


        tmp_str = getenv("LOCK_PROFILER_THREAD_BINDING");

        if (!tmp_str || !strcmp(tmp_str, "BY_HW_THREAD"))
        {
            thread_binding = TB_BY_HW_THREAD;
            echo("Thread binding by hardware thread");
        }
        else if (!strcmp(tmp_str, "BY_CORE"))
        {
            thread_binding = TB_BY_CORE;
            echo("Thread binding by core");
        }
        else if (!strcmp(tmp_str, "BY_NODE"))
        {
            thread_binding = TB_BY_NODE;
            echo("Thread binding by node");
        }
        else if (!strcmp(tmp_str, "NONE"))
        {
            thread_binding = TB_NONE;
            echo("No thread binding");
        }


        tmp_str = getenv("LOCK_PROFILER_THREAD_MONITORING");

        if (!tmp_str || !strcmp(tmp_str, "PER_NODE"))
        {
            thread_monitoring = TM_PER_NODE;
            echo("Thread monitoring by node");
        }
        else if (!strcmp(tmp_str, "PER_CORE"))
        {
            thread_monitoring = TM_PER_CORE;
            echo("Thread monitoring by core");
        }


        tmp_str = getenv("LOCK_PROFILER_LIBRARY");

        if (!tmp_str || !strcmp(tmp_str, "PAPI"))
            library = LIB_PAPI;
        else if (!strcmp(tmp_str, "CPC"))
            library = LIB_CPC;
        else if (!strcmp(tmp_str, "NONE_CYCLES"))
            library = LIB_NONE_CYCLES;


        tmp_str = getenv("LOCK_PROFILER_EVENT_ID");
        event_id_str = tmp_str ? strdup(tmp_str) : NULL;
        event_id = tmp_str ? strtoll(tmp_str, 0, 0) : 0;


        if (library == LIB_PAPI)
        {
            echo("Using libpapi (will count cycles if EVENT=0 or unset).");

            if (event_id)
                echo("Monitoring PAPI event id=0x%llx.", event_id);
            else
                echo("Counting cycles (no event provided, EVENT=\"0\" or the "
                     "event name couldn't be parsed).");
        }
        else if (library == LIB_CPC)
        {
            echo("Using libcpc.");

            if (event_id_str)
                echo("Monitoring CPC event \"%s\".", event_id_str);
            else
                echo_error("Event unspecified.");
        }
        else
        {
            echo("Counting cycles (native).");
        }


        tmp_str = getenv("LOCK_PROFILER_OFFSET");
        offset = tmp_str ? strtol(tmp_str, 0, 0) : 0;
        echo("Offset = %d.", offset);


        tmp_str = getenv("LOCK_PROFILER_INSIDE");
        inside = tmp_str ? strtol(tmp_str, 0, 0) : 0;
        if (inside) echo("Measurements inside critical sections.");
        else echo("Measurements outside critical sections.");


        if (library == LIB_PAPI)
        {
            if (PAPI_library_init(PAPI_VER_CURRENT) != PAPI_VER_CURRENT)
                echo_error("PAPI_library_init() failed");

            PAPI_set_granularity(PAPI_GRN_THR);
        }
        else if (library == LIB_CPC)
        {
#ifdef __sun__
            if ((rcw_cpc = cpc_open(CPC_VER_CURRENT)) == NULL)
                echo_error("libcpc version mismatch");

            struct sigaction act;

            /* We register the overflow handler */
            act.sa_sigaction = emt_handler;
            bzero(&act.sa_mask, sizeof(act.sa_mask));
            act.sa_flags = SA_RESTART | SA_SIGINFO;

            if (sigaction(SIGEMT, &act, NULL) == -1)
                echo_error("sigaction() failed");
#else
            echo_error("libcpc requires Solaris");
#endif
        }


//      REAL(pthread_mutex_init)(&global_mutex, 0);

        init_thread();
    }
}

static void init_thread(void)
{
    struct hw_thread *self_p;
    struct core *core_p;
    struct numa_node *node_p;


    /* We make sure that the liblock is initialized before this library,
     * because we need the topology information it provides. */
    liblock_init_library();

    thread_id = __sync_fetch_and_add(&max_thread_id, 1);
    if((thread_id) >= MAX_THREADS)
        echo_error("Too many threads, recompile.");


    if (library == LIB_PAPI)
        PAPI_thread_init((unsigned long (*)(void))pthread_self);


    if (thread_binding == TB_NONE || library == LIB_NONE_CYCLES
          || (library == LIB_PAPI && !event_id))
    {
        // No binding, or counting cycles? Monitor all the threads.
        monitored = 1;
    }
    else
    {
        bind_thread();

        self_p = &topology->hw_threads[hw_thread_id];

        if (thread_monitoring == TM_PER_CORE)
        {
            core_p = self_p->core;
            /* We only monitor the first hardware thread of each core. */
            monitored = (self_p == core_p->hw_threads[offset]);
        }
        else
        {
            node_p = self_p->node;
            /* We only monitor the first hardware thread of each node. */
            monitored = (self_p == node_p->hw_threads[offset]);
        }
    }


    if (monitored)
    {
        echo("Thread n°%d (%d) is monitored", thread_id, hw_thread_id);

        if (library == LIB_PAPI)
        {
            if (event_id)
            {
                if (PAPI_create_eventset(&event_set) != PAPI_OK)
                    echo_error("PAPI_create_eventset() failed");

                if (PAPI_add_event(event_set, event_id) != PAPI_OK)
                    echo_error("PAPI_add_event() failed");

                if (PAPI_start(event_set) != PAPI_OK)
                    echo_error("PAPI_start() failed");
            }
        }
        else if (library == LIB_CPC)
        {
#ifdef __sun__
            if ((cpc_set = cpc_set_create(rcw_cpc)) == NULL)
                echo_error("cpc_set_create() failed");

            cpc_index =
                cpc_set_add_request(rcw_cpc, cpc_set, event_id_str, 0,
                                    CPC_COUNT_USER | CPC_COUNT_SYSTEM |
                                    CPC_BIND_EMT_OVF, 0, NULL);

            if (cpc_index < 0)
                echo_error("cpc_set_add_request() failed");

            if (cpc_bind_curlwp(rcw_cpc, cpc_set, 0) < 0)
                echo_error("cpc_bind_curlwp() failed");
#endif
        }
    }
}

static void *lp_start_routine(void *_arg)
{
    struct routine *r = _arg;
    void *(*fct)(void*) = r->fct;
    void *arg = r->arg;
    void *res;

    free(r);

    init_thread();

    res = fct(arg);

    return res;
}

static void print_lock_stack(struct lock_info *ginfo)
{
    int    i;

#ifndef __sun__
    char **backtrace;

    backtrace = /*hack_*/backtrace_symbols(ginfo->stack, ginfo->n_stack_frames);

    echo("[mutex #%lu/%p]\tLibrary = %s, backtrace:",
         ginfo->mutex_id,  ginfo->alloc_id, ginfo->lib);

    for(i = 2 ; i < (ginfo->n_stack_frames - 2) ; i++)
        echo("[mutex #%lu/%p]\t"PREPTR"%-8p %s",
             ginfo->mutex_id, ginfo->alloc_id, ginfo->stack[i], backtrace[i]);

    echo("");
#else
    Dl_info dlip;

    echo("[mutex #%lu/0x%p]\tLibrary = %s, backtrace:",
         ginfo->mutex_id,  ginfo->alloc_id, ginfo->lib);

    for(i = 0 ; i < ginfo->n_stack_frames ; i++)
    {
        if (!dladdr((void *)ginfo->stack[i], &dlip))
            echo("[mutex #%lu/0x%p]\t"PREPTR"%-8p ???",
                 ginfo->mutex_id, ginfo->alloc_id, ginfo->stack[i]);
        else
            echo("[mutex #%lu/0x%p]\t"PREPTR"%-8p %s:"PREPTR"%-8p %s:"
                 PREPTR"%-8p",
                 ginfo->mutex_id, ginfo->alloc_id, ginfo->stack[i],
                 dlip.dli_fname, dlip.dli_fbase, dlip.dli_sname,
                 dlip.dli_saddr);
    }
    echo("");
#endif
}

static void print_lock_info(struct lock_info *info)
{
    double in_cs, events_per_cs;
    unsigned long long num_cs;

    get_global_info(info->per_threads, &in_cs, &num_cs, &events_per_cs);

#ifdef __sun__
    echo("%-15s  #%-6lu  0x%-8p  0x%-16p  %13.10f  %17.10f  %6llu",
         info->lib, info->mutex_id, info->alloc_id, info->lock, in_cs,
         events_per_cs, num_cs);
#else
    echo("%-15s  #%-6lu  %-10p  %-18p  %13.10f  %17.10f  %6llu",
         info->lib, info->mutex_id, info->alloc_id, info->lock, in_cs,
         events_per_cs, num_cs);
#endif
}

__attribute__((always_inline))
static inline void read_papi_event_counters(unsigned long long *result)
{
    if (event_id != 0)
        PAPI_read(event_set, (long long *)result);
    else
        *result = PAPI_get_real_cyc();
}

static void stop(void)
{
    static int volatile already_stopped = 0;
    const char *str_is_full = getenv("LOCK_PROFILER_FULL");
    int is_full = str_is_full ? atoi(str_is_full) : 0;
    double in_cs = -1, events_per_cs = -1;
    unsigned long long num_cs;

    recurse = 0;

    if(__sync_val_compare_and_swap(&already_stopped, 0, 1))
        return;

    if(is_full > 1)
        ht_foreach(print_lock_stack);

    if(is_full)
    {
        echo("Family           ID       Alloc ID    Address             "
             "Pct. in CS     Evts. per CS       # CS");
        ht_foreach(print_lock_info);
    }

    get_global_info(info_threads, &in_cs, &num_cs, &events_per_cs);

    echo("Global statistics: %2.2f%% in CS, %2.2f events per CS (%llu CS)",
         in_cs, events_per_cs, num_cs);
}

int pthread_mutex_init(pthread_mutex_t *mutex, const pthread_mutexattr_t *attr)
{
    int res;

    if(!inited)
        init();

    res = REAL(pthread_mutex_init)(mutex, attr);

    liblock_on_create(posix_id, mutex);

    return res;
}

int pthread_mutex_lock(pthread_mutex_t *mutex)
{
    if(!inited)
        init();

    if(monitored && recurse)
    {
        int res = -1;
        unsigned long long before = 0;
        struct nested *nested = &nesteds[thread_id];

        if(nested->depth >= MAX_NESTED)
            echo_error("Too many nested critical sections");

        if (inside)
        {
            res = REAL(pthread_mutex_lock)(mutex);
            READ_EVENT_COUNTERS(before);
        }
        else
        {
            READ_EVENT_COUNTERS(before);
            res = REAL(pthread_mutex_lock)(mutex);
        }

        nested->last_event[nested->depth++] = before;

        return res;
    }
    else
    {
        return REAL(pthread_mutex_lock)(mutex);
    }
}

int pthread_mutex_trylock(pthread_mutex_t *mutex)
{
    if(!inited)
        init();

    if(monitored && recurse)
    {
        int res;
        unsigned long long before;
        struct nested *nested = &nesteds[thread_id];

        if(nested->depth >= MAX_NESTED)
            echo_error("Too many nested critical sections");

        if (inside)
        {
            if ((res = REAL(pthread_mutex_trylock)(mutex))) return res;
            READ_EVENT_COUNTERS(before);
        }
        else
        {
            READ_EVENT_COUNTERS(before);
            if ((res = REAL(pthread_mutex_trylock)(mutex))) return res;
        }

        nested->last_event[nested->depth++] = before;

        return res;
    }
    else
    {
        return REAL(pthread_mutex_trylock)(mutex);
    }
}

int pthread_mutex_unlock(pthread_mutex_t *mutex)
{
/*
//    Unnecessary, removed from the critical path.

    if(!inited)
        init();
*/

    if(monitored && recurse)
    {
        int res;
        unsigned long long after, last;
        long long num_events;
        struct nested *nested;
        struct lock_info_thread *ginfo, *linfo;
        struct lock_info *info;

        if (inside)
        {
            READ_EVENT_COUNTERS(after);
            res = REAL(pthread_mutex_unlock)(mutex);
        }
        else
        {
            res = REAL(pthread_mutex_unlock)(mutex);
            READ_EVENT_COUNTERS(after);
        }


        nested = &nesteds[thread_id];
        last = nested->last_event[--nested->depth];

        if (library == LIB_CPC)
        {
            /* The result is stored in the high word. */
            after >>= 32;
            last >>= 32;
        }


        if(!nested->first_event) nested->first_event = last;
        nested->real_last_event = after;

        info = ht_get(posix_id, mutex);
        linfo = &info->per_threads[thread_id];
        num_events = after - last;
        linfo->num_events += num_events;
        linfo->num_cs++;


        if(!nested->depth)
        {
            ginfo = &info_threads[thread_id];
            ginfo->num_events += after - last;
            ginfo->num_cs++;
        }


        return res;
    }
    else
    {
        return REAL(pthread_mutex_unlock)(mutex);
    }
}

#ifdef conds
int pthread_cond_timedwait(pthread_cond_t *cond, pthread_mutex_t *mutex,
                           const struct timespec *abstime)
{
/*
    Unnecessary, removed from the critical path.

    if(!inited)
        init();
*/

    if (monitored && recurse)
    {
        int res;
        unsigned long long before, after, last;
        long long num_events;
        struct lock_info *info;
        struct lock_info_thread *linfo, *ginfo;
        struct nested *nested;

        READ_EVENT_COUNTERS(after);


        nested = &nesteds[thread_id];
        last = nested->last_event[--nested->depth];

        if (library == LIB_CPC)
        {
            after >>= 32;
            last >>= 32;
        }

        if(!nested->first_event) nested->first_event = last;
        nested->real_last_event = after;

        info = ht_get(posix_id, mutex);
        linfo = &info->per_threads[thread_id];
        num_events = after - last;
        linfo->num_events += num_events;
        linfo->num_cs++;


        if(!nested->depth)
        {
            ginfo = &info_threads[thread_id];
            ginfo->num_events += num_events;
            ginfo->num_cs++;
        }


        res = REAL(pthread_cond_timedwait)(cond, mutex, abstime);


        if(nested->depth >= MAX_NESTED)
            echo_error("Too many nested critical sections");

        READ_EVENT_COUNTERS(before);
        nested->last_event[nested->depth++] = before;


        return res;
    }
    else
    {
        return REAL(pthread_cond_timedwait)(cond, mutex, abstime);
    }
}

int pthread_cond_wait(pthread_cond_t *cond, pthread_mutex_t *mutex)
{
/*
    Unnecessary, removed from the critical path.

    if(!inited)
        init();
*/

    if (monitored && recurse)
    {
        int res;
        unsigned long long before, after, last;
        long long num_events;
        struct lock_info *info;
        struct lock_info_thread *linfo, *ginfo;
        struct nested *nested;

        READ_EVENT_COUNTERS(after);

        nested = &nesteds[thread_id];
        last = nested->last_event[--nested->depth];

        if (library == LIB_CPC)
        {
            after >>= 32;
            last >>= 32;
        }

        if(!nested->first_event) nested->first_event = last;
        nested->real_last_event = after;

        info = ht_get(posix_id, mutex);
        linfo = &info->per_threads[thread_id];
        num_events = after - last;
        linfo->num_events += num_events;
        linfo->num_cs++;


        if(!nested->depth)
        {
            ginfo = &info_threads[thread_id];
            ginfo->num_events += after - last;
            ginfo->num_cs++;
        }

        res = REAL(pthread_cond_wait)(cond, mutex);


        if(nested->depth >= MAX_NESTED)
            echo_error("Too many nested critical sections");


        READ_EVENT_COUNTERS(before);
        nested->last_event[nested->depth++] = before;


        return res;
    }
    else
    {
        return REAL(pthread_cond_wait)(cond, mutex);
    }
}

/*
int pthread_cond_broadcast(pthread_cond_t *cond) {
//  init();

    int res;

//  REAL(pthread_mutex_lock)(&global_mutex);
    res = REAL(pthread_cond_broadcast)(cond);
//  REAL(pthread_mutex_unlock)(&global_mutex);

    return res;
}

int pthread_cond_signal(pthread_cond_t *cond) {
//  init();

    int res;

//  REAL(pthread_mutex_lock)(&global_mutex);
    res = REAL(pthread_cond_signa)l(cond);
//  REAL(pthread_mutex_unlock)(&global_mutex);

    return res;
}
*/
#endif

int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
                   void *(*start_routine) (void *), void *arg)
{
    struct routine *r = malloc(sizeof(struct routine));

    r->fct = start_routine;
    r->arg = arg;

    return REAL(pthread_create)(thread, attr, lp_start_routine, r);
}

static __attribute__ ((destructor)) void destroy()
{
    stop();
}

static __attribute ((constructor (1000))) void start()
{
    init();
}

__attribute__((noreturn)) void exit(int status)
{
    stop();
    REAL(exit)(status);
}

__attribute__((noreturn)) void _exit(int status)
{
    stop();
    REAL(_exit)(status);
}

__attribute__((noreturn)) void _Exit(int status)
{
    stop();
    REAL(_Exit)(status);
}

void liblock_on_server_thread_start(const char *lib, unsigned int thread_id)
{}

void liblock_on_server_thread_end(const char *lib, unsigned int thread_id)
{}

void liblock_on_create(const char *lib, void *lock)
{
    ht_get(lib, lock);
}

void liblock_on_destroy(const char *lib, void *lock)
{}

#if 0
/*
 *  Hooks
 */
int liblock_lock_init(const char *type, struct hw_thread *hw_thread,
                         liblock_lock_t *lock, void *arg)
{
    int res;

    if(!inited)
        init();

    res = REAL(liblock_lock_init)(type, hw_thread, lock, arg);

    liblock_on_create(type, lock);

    return res;
}

void liblock_rcl_execute_op_for(liblock_lock_t *lock, size_t id)
{
//  ht_get(0, lock)->owner_id = self_id_to_thread_id[id];
//  self_id_assocs[id].lock_info->owner_id = self_id_assocs[id].thread_id;
}

void *liblock_exec(liblock_lock_t *lock, void *(*pending)(void*), void *val)
{
    if(!inited)
        init();

    if(recurse)
    {
        struct nested *nested          = &nesteds[thread_id];
        struct lock_info *info         = ht_get(0, lock);
        struct lock_info_thread *linfo = &info->per_threads[thread_id];
        struct lock_info_thread *ginfo = &info_threads[thread_id];
        long_long before, after;
        void *res;

        read_papi_event_counters(&before);
        nested->real_last_event = before;

        if(!nested->first_event)
            nested->first_event = before;

        nested->last_event[nested->depth++] = before;

        self_id_assocs[self.id].thread_id = thread_id;
        self_id_assocs[self.id].lock_info = info;

        res = REAL(liblock_exec)(lock, pending, val);

        nested->depth--;

        read_papi_event_counters(&after);
        nested->real_last_event = after;

/*
        printf("%llu %p- %p - %lld - %lld - %lld\n", nested->depth, lock,
               info, before, after, after - before);
*/

        linfo->num_events += after - before;
        linfo->num_cs++;

        if(!nested->depth)
        {
            ginfo->num_events += after - before;
            ginfo->num_cs++;
        }

        return res;
    }
    else
    {
        return REAL(liblock_exec)(lock, pending, val);
    }
}

int liblock_cond_wait(liblock_cond_t *cond, liblock_lock_t *lock)
{
    if(!inited)
        init();

    int res;
    size_t owner_id = thread_id;

    if (recurse)
    {
        struct lock_info *info         = ht_get(0, lock);
        owner_id                       = info->owner_id;
        struct lock_info_thread *linfo = &info->per_threads[owner_id];
        struct nested *nested          = &nesteds[owner_id];
        struct lock_info_thread  ginfo = &info_threads[owner_id];
        long_long after, last;

        last = nested->last_event[--nested->depth];
        read_papi_event_counters(&after);
        nested->real_last_event = after;

        linfo->num_events += after - last;
        linfo->num_cs++;

        if(!nested->depth)
        {
            ginfo->num_events += after - last;
            ginfo->num_cs++;
        }
    }

//  REAL(pthread_mutex_lock)(&global_mutex);
//  pthread_mutex_unlock(mutex);
//  res = REAL(liblock_cond_wait)(cond, &global_mutex);
    res = REAL(liblock_cond_wait)(cond, lock);
//  REAL(pthread_mutex_unlock)(&global_mutex);
//  pthread_mutex_lock(mutex);

    if (recurse)
    {
        struct nested *nested = &nesteds[owner_id];

        long long before;

        if(nested->depth >= MAX_NESTED)
            echo_error("Too many nested critical sections");

        read_papi_event_counters(&before);

        if(!nested->first_event)
            nested->first_event = before;

        nested->last_event[nested->depth++] = before;
        nested->real_last_event = before;
    }

    return res;
}

int liblock_cond_timedwait(liblock_cond_t *cond, liblock_lock_t *lock,
                             struct timespec *abstime)
{
    if(!inited)
        init();

    int res;
    size_t owner_id = thread_id;

    if (recurse)
    {
        struct lock_info *info         = ht_get(0, lock);
        owner_id                       = info->owner_id;
        struct lock_info_thread *linfo = &info->per_threads[owner_id];
        struct nested *nested          = &nesteds[owner_id];
        struct lock_info_thread *ginfo = &info_threads[owner_id];
        long long after, last;

        last = nested->last_event[--nested->depth];
        read_papi_event_counters(&after);
        nested->real_last_event = after;

        linfo->num_events += after - last;
        linfo->num_cs++;

        if(!nested->depth)
        {
            ginfo->num_events += after - last;
            linfo->num_cs++;
        }
    }

//  REAL(pthread_mutex_lock)(&global_mutex);
//  pthread_mutex_unlock(mutex);
    res = REAL(liblock_cond_timedwait)(cond, /*&global_mutex*/lock, abstime);
//  REAL(pthread_mutex_unlock)(&global_mutex);
//  pthread_mutex_lock(mutex);

    if (recurse)
    {
        struct nested *nested = &nesteds[owner_id];

        long long before;

        if(nested->depth >= MAX_NESTED)
            echo_error("Too many nested critical sections");

        read_papi_event_counters(&before);

        if(!nested->first_event)
            nested->first_event = before;

        nested->last_event[nested->depth++] = before;
        nested->real_last_event = before;
/*
        read_papi_event_counters(&before);
        nested->real_last_event = before;

        if(!nested->first_event)
            nested->first_event = before;

        nested->last_event[nested->depth++] = before;
        nested->real_last_event = before;
*/
    }

    return res;
}
#endif

