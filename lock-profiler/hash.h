/* ########################################################################## */
/* (C) UPMC, 2010-2014                                                        */
/*     Authors:                                                               */
/*       Jean-Pierre Lozi <jean-pierre.lozi@lip6.fr>                          */
/*       Gaël Thomas <gael.thomas@lip6.fr                                     */
/*       Florian David <florian.david@lip6.fr>                                */
/*       Julia Lawall <julia.lawall@lip6.fr>                                  */
/*       Gilles Muller <gilles.muller@lip6.fr>                                */
/* -------------------------------------------------------------------------- */
/* ########################################################################## */
#ifndef HASH_H
#define HASH_H

/* Hash function from http://www.azillionmonkeys.com/qed/hash.html */
uint32_t hash(const char * data, int len);

#endif

