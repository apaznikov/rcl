/* ########################################################################## */
/* (C) UPMC, 2010-2014                                                        */
/*     Authors:                                                               */
/*       Jean-Pierre Lozi <jean-pierre.lozi@lip6.fr>                          */
/*       Gaël Thomas <gael.thomas@lip6.fr                                     */
/*       Florian David <florian.david@lip6.fr>                                */
/*       Julia Lawall <julia.lawall@lip6.fr>                                  */
/*       Gilles Muller <gilles.muller@lip6.fr>                                */
/* -------------------------------------------------------------------------- */
/* ########################################################################## */
#ifndef BACKTRACE_SYMBOLS_H
#define BACKTRACE_SYMBOLS_H
#ifndef __sun__

char **hack_backtrace_symbols(void *const *buffer, int size);

#endif /* __sun__ */
#endif /* BACKTRACE_SYMBOLS_H */

