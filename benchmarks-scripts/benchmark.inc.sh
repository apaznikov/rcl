#!/bin/sh

RCL_HOME=$HOME/rcl/rcl

# LD_PRELOAD=$RCL_HOME/affin_opt/rcl_affin_opt.so

function run_bench
{
    if [ -n "$LD_PRELOAD" ]; then
        echo "LD_PRELOAD=$LD_PRELOAD $@"
        eval LD_PRELOAD=$LD_PRELOAD $@
    else
        echo "$@"
        eval $@
    fi
}
