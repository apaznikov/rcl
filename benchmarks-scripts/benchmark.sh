# 
# benchmark.sh: Simple benchmark script for RCL
# (C) 2015-2016 Alexey Paznikov <apaznikov@gmail.com> 
# 

#!/bin/sh

NRUNS=2

BENCHDIR=benchmarks
BENCHLIST=`ls $BENCHDIR`
ncpus=`cat /proc/cpuinfo | grep "processor" | wc -l`
NTHREADS_HW="`seq 2 $ncpus`"

# Temporary files
TIME_TMP="/tmp/time.tmp"
RESULTS_TMP="/tmp/results.tmp"

# Timer functions
get_elapsed_time=get_elapsed_time2
wtime_ser=-1

function get_elapsed_time1
{
    time=`$@ | egrep "Elapsed time" | awk '{print $3}'`
    echo $time >$TIME_TMP
}

function get_elapsed_time2
{
    /usr/bin/time -f %e -o $TIME_TMP $@ 2>&1
}

function get_elapsed_time3
{
    time="$( TIMEFORMAT='%3R';time ( $@ ) 2>&1 1>/dev/null )"
    echo $time >$TIME_TMP
}

function measure_time
{
    $get_elapsed_time $@
    time=`cat $TIME_TMP`
    echo $time >>$RESULTS_TMP
}

function measure_speedup
{
    $get_elapsed_time $@
    wtime=`cat $TIME_TMP`
    speedup=`echo $wtime_ser $wtime | awk '{ print $1 / $2 }'`
    echo $speedup >>$RESULTS_TMP
}

function measure_throughput
{
    time=`$get_elapsed_time $@`
    throughput=`echo $niters $time | awk '{ print $1 / $2 / 1000 }'`
    echo $throughput >>$RESULTS_TMP
}

function measure_cache_misses
{
    perf stat -e cache-references,cache-misses $@ 2>&1 \
        | egrep "cache-misses" | awk '{ print $4 }' >>$RESULTS_TMP
}

function measure 
{
    bench_script=$1
    local_measure_type=$2
    nthreads=$3
    measure_func=measure_$local_measure_type

    cat /dev/null >$RESULTS_TMP

    for ((i = 1; i <= NRUNS; i++)); do
        cmd="$bench_script $nthreads"
        echo $cmd

        $measure_func $cmd
    done

    avg=`awk '{ total += $1; count++ } END { print total / count }' $RESULTS_TMP`
    echo $avg >$RESULTS_TMP
}

[ ! -d "results" ] && mkdir results

# Make experiments thread number dependency
function exp_nthreads
{
    measure_type=$1
    [ -n "$2" ] && units=", $2"

    for bench in $BENCHLIST; do
        datfile="results/${bench}_${measure_type}.dat"

        echo -e "nthreads \t $measure_type$units" >$datfile

        if [ "$measure_type" = "speedup" ]; then
            measure $BENCHDIR/$bench time 1
            wtime_ser=`cat $RESULTS_TMP`
        fi

        for nthreads in $NTHREADS_HW; do
            measure $BENCHDIR/$bench $measure_type $nthreads
            result=`cat $RESULTS_TMP`
            echo -e "$nthreads \t\t $result" >>$datfile
        done
    done
}

# exp_nthreads speedup
exp_nthreads time s
