==============
RCL Benchmarks
==============
(C) Jean-Pierre Lozi, Florian David, Gaël Thomas, Julia Lawall, Gilles Muller
    2014

Contents:
---------
(0) Introduction
(1) Liblock
(2) Microbenchmark
(3) SPLASH-2
(4) Phoenix 2
(5) Memcached
(6) Berkeley DB
(7) Miscellaneous microbenchmarks
(8) Profiler
(9) Reengineering


(0) Introduction
================

This file explains how to compile the benchmarks and run them. For more
information, send an e-mail to jean-pierre@lozi.org.

The first step for all benchmarks is to set the ROOT variable in the
'Makefile.config' file to the local path of your rcl_benchmarks directory after
extraction.

The PAPI [http://icl.cs.utk.edu/papi/] library is needed for some benchmarks.
On linux, libnuma is needed.

Machines used in the PhD thesis:

amd48b (a.k.a. magnycours-48):
------------------------------
uname -a: Linux amd48b-systeme 3.9.7 #3 SMP Mon Jun 24 17:59:20 CEST 2013
          x86_64 x86_64 x86_64 GNU/Linux
gcc: gcc version 4.6.1 (Ubuntu/Linaro 4.6.1-9ubuntu3)
PAPI: PAPI Version: 5.2.0.0

niagara2 (a.k.a. niagara2-128):
-------------------------------
uname -a: SunOS niagara2.cs.rochester.edu 5.10 Generic_137137-09 sun4v sparc
          SUNW,T5240
gcc: gcc version 4.7.1 (GCC)
PAPI: PAPI Version: 4.1.4.0


(1) Liblock
===========

All benchmarks require the liblock.

Building the liblock
--------------------

1. Enter the liblock directory
2. Run 'make'


(2) Microbenchmark
==================

Building the benchmark
----------------------

$ cd ~/svn-margaux/margaux/rcl/microbenchmark/src
$ make distclean
$ make

Running the benchmark
---------------------

$ cd rcl/microbenchmark/scripts
$ sudo rm -rf ../results/*
$ sudo ./main_measurements.sh

For the results from Figure 5.1 in the PhD thesis, run:

$ sudo ./max_combiner_iterations.sh

The results will be stored the results/ directory. Some results are available
in the all-results/ directory. Scripts used to plot graphs can be found in the
graphs/ directory.

The results/, all-results/ and graphs/ directory can also be found for most
of the following benchmarks.


(3) SPLASH-2
============

Building the benchmark
----------------------

$ cd rcl/splash2-liblock/src
$ ./compile_all.sh

Running the benchmark
---------------------

$ cd rcl/splash2-liblock/scripts
$ sudo rm ../results/*
$ sudo N_RUNS=5 ./benchmark.sh

Speedups can be computed using the compute_speedups.sh script. This is also the
case for Phoenix 2 and Memcached. Maximums can be computed using the
./get_maximums.py scripts. This is also the case for Phoenix 2, Memcached and
Berkeley DB.


(4) Phoenix 2
=============

Building the benchmark
----------------------

$ cd rcl/phoenix-liblock/phoenix-rcl
$ make clean
$ make
$ cd rcl/phoenix-liblock/datasets
$ ./download_datasets.sh

Running the benchmark
---------------------

$ cd rcl/phoenix-liblock/scripts
$ sudo rm ../results/*
$ sudo N_RUNS=5 ./benchmark.sh


(5) Memcached
=============

Building the benchmark
----------------------

$ cd rcl/memcached-liblock/src/memcached-1.4.6
$ ./configure; make
$ cd rcl/memcached-liblock/src/memcached-1.4.6-patched
$ ./configure; make
$ cd rcl/memcached-liblock/src/libmemcached-1.0.2
$ ./configure; make

Running the benchmark
---------------------

$ cd rcl/memcached-liblock/scripts
$ sudo rm -rf ../results/*
$ sudo N_RUNS=5 ./benchmark.sh


(6) Berkeley DB
===============

Building the benchmark
----------------------

$ cd berkeleydb-liblock/scripts
$ ./generate.sh

Running the benchmark
---------------------

$ sudo rm ../results/*
$ sudo R_TYPES="3 5" N_RUNS=5                                                  \
  LOCKS="rcl orig posix spinlock mcstp flat mcs ccsynch dsmsynch" ./benchmark.sh

For the results of Figure 5.15 in the PhD thesis, recompile the liblock using
-DWITH_YIELD.

To get the numbers from Figure 5.13 in the PhD thesis, use:

$ sudo EXECUTION_TYPE="WITH_LAST_TABLE_DATA" N_RUNS=5 ./benchmark.sh           \
    2>&1 | tee whatever_output_file.txt


(7) Miscellaneous microbenchmarks
=================================

Other microbenchmarks used in the PhD thesis can be found in the following
directories:

 - memal
 - instruction-microbenchmark
 - dcm-profiling-microbenchmark

The use of these benchmarks should be straightforward, don't hesitate to send
an e-mail to jean-pierre@lozi.org if you run into problems.


(8) Profiler
============

1. Enter the lock-profiler/ directory.
2. Run 'make'.
3. The 'lock-profiler' script can be used to profile applications.

In order to automatically get the profiling results from Figures 5.5 and 5.6 in
the PhD thesis, scripts named get_big_table_numbers.sh and
get_server_cache_misses.sh can be found in the scripts/ directory.


(9) Reengineering
=================

The reengineering tool is available in the reengineering/ directory. The .cocci
file should be used with Coccinelle, see [http://coccinelle.lip6.fr/].

