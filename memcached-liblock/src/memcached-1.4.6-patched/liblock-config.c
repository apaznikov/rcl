#define _GNU_SOURCE
#include <sched.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#ifdef __sun__
#include <errno.h>
#include <sys/types.h>
#include <sys/processor.h>
#include <sys/procset.h>
#endif

#include <liblock.h>
#include <liblock-fatal.h>

#include "liblock-memcached.h"

const  char* liblock_lock_name;
struct hw_thread* liblock_server_core_1;

static int verbose = 0;
static int n_available_cores = 0;
static int volatile go = 0;
static int volatile current_nb_threads = 0;
static int volatile wait_nb_threads = 0;
static int* client_hw_threads;

struct hw_thread* get_liblock_server_core_1(void);
static void liblock_memcached(void);
static void do_go();
void liblock_auto_bind();

static void do_go()
{
	go = 1;
}

struct hw_thread* get_liblock_server_core_1()
{
	return liblock_server_core_1;
}

#ifdef __linux__
__attribute__ ((constructor)) static void liblock_memcached()
#elif defined(__sun__)
__attribute__ ((constructor (103))) static void liblock_memcached()
#endif
{
	int is_rcl;
    char *tmp_str;

	liblock_lock_name = getenv("LIBLOCK_LOCK_NAME");
	if(!liblock_lock_name)
		liblock_lock_name = "rcl";

    tmp_str = getenv("VERBOSE");
    if (tmp_str && !strcmp(tmp_str, "1"))
        verbose = 1;

    tmp_str = getenv("N_AVAILABLE_CORES");
    if (!tmp_str) tmp_str = 0;
    else n_available_cores = strtol(tmp_str, NULL, 0);

	liblock_server_core_1 = topology->nodes[0].hw_threads[0];

	is_rcl = !strcmp(liblock_lock_name, "rcl") ||
             !strcmp(liblock_lock_name, "multircl");

	liblock_start_server_threads_by_hand = 1;
	liblock_servers_always_up = 1;

#ifdef __linux__
	char get_cmd[128];

    sprintf(get_cmd, "/proc/%d/cmdline", getpid());
	FILE* f=fopen(get_cmd, "r");
	if(!f) {
		printf("!!! warning: unable to find command line\n");
	}
	char buf[1024];
	buf[0] = 0;
	if(!fgets(buf, 1024, f))
		printf("fgets\n");

	printf("**** testing %s with lock %s placed on hw_thread %d\n",
           buf, liblock_lock_name, liblock_server_core_1->hw_thread_id);
#endif

	if(is_rcl)
    {
		go = 0;

		liblock_reserve_hw_thread_for(liblock_server_core_1, liblock_lock_name);
		liblock_lookup(liblock_lock_name)->run(do_go);

		while(!go)
			PAUSE();
	}

	client_hw_threads = malloc(sizeof(int)*topology->nb_hw_threads);

/*
	int i, j, z;

	for(i = 0, z = 0; i < topology->nb_nodes; i++)
        for (j = 0; j < topology->nodes[i].nb_hw_threads; j++)
		    if(topology->nodes[i].hw_threads[j] != liblock_server_core_1)
			    client_hw_threads[z++] =
                    topology->nodes[i].hw_threads[j]->hw_thread_id;
*/
	int i, z;

	for(i = 0, z = 0; i < topology->nb_hw_threads; i++)
		if(&topology->hw_threads[i] != liblock_server_core_1)
			client_hw_threads[z++] =
                 topology->hw_threads[i].hw_thread_id;

	client_hw_threads[z++] = liblock_server_core_1->hw_thread_id;

    // FIXME: should enable this?
    // if(nthreads) {
    liblock_auto_bind();
        // wait_nb_threads = atoi(nthreads);
    // }
}

void liblock_auto_bind()
{
    int i;

	if(!self.running_hw_thread)
    {
		static int cur_hw_thread = 0;
		struct hw_thread* hw_thread;

		do
        {
			i = __sync_fetch_and_add(&cur_hw_thread, 1) %
                    topology->nb_hw_threads;

			hw_thread = &topology->hw_threads[client_hw_threads[i %
                                                  topology->nb_hw_threads]];
		}
        while(hw_thread->server_type);

		self.running_hw_thread = hw_thread;

#ifdef __linux__
		cpu_set_t    cpuset;
		CPU_ZERO(&cpuset);

        if (!n_available_cores)
        {
		    CPU_SET(hw_thread->hw_thread_id, &cpuset);
        }
        else
        {
            for (i = 0 ; i < n_available_cores ; i++)
                CPU_SET(client_hw_threads[i], &cpuset);
        }

		if(pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset))
			fatal("pthread_setaffinity_np");
#elif defined(__sun__)
        if(processor_bind(P_LWPID, P_MYID, hw_thread->hw_thread_id, NULL))
            fatal("processor_bind (%s)", strerror(errno));
#endif

		__sync_fetch_and_add(&current_nb_threads, 1);

        if (verbose)
        {
		    printf("Autobinding thread %d to hw_thread %d (%d threads out of"
                   " %d)...\n", self.id, //sched_getcpu(),
                   self.running_hw_thread->hw_thread_id, current_nb_threads,
                   wait_nb_threads);
        }

        // FIXME: should enable this?
        // while(current_nb_threads < wait_nb_threads)
        //     PAUSE();
	}
}

