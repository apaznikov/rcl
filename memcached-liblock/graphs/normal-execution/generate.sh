#!/bin/bash
#
# generate.sh
# ===========
# (C) Jean-Pierre Lozi, 2013
#


. ../../../figures/bench.inc.sh

# Useful for generating the graph for a single machine/benchmark.
if [[ -z "$MACHINES" ]];   then MACHINES=("amd48b" "niagara2");               fi
if [[ -z "$BENCHMARKS" ]]; then BENCHMARKS=("get" "set");                     fi

if [[ $# -eq 0 ]]
then
    TAGS=("140211-phd" "140211-phd")
else
    TAGS=("$@")
fi

function replace_variable
{
    VARIABLE_NAME=$1
    VARIABLE=${!VARIABLE_NAME}

    cat tmp.plt | sed "s/@@$VARIABLE_NAME@@/$VARIABLE/g"             \
        > tmp-tmp.plt

    mv tmp-tmp.plt tmp.plt
}

for FR in "" "-fr"
do
   for MACHINE_ID in ${!MACHINES[*]}
   do
       MACHINE=${MACHINES[$MACHINE_ID]}
       TAG=${TAGS[$MACHINE_ID]}
   
       for BENCHMARK_ID in ${!BENCHMARKS[*]}
       do
   
           BENCHMARK=${BENCHMARKS[$BENCHMARK_ID]}
   
           RESULTS_PATH="../../all-results/results-$TAG-$MACHINE"
           RESULTS_PATH=$(echo $RESULTS_PATH | sed 's/\//\\\//g')
   
           for KEY in "" "-no-key"
           do
   
               cat base-${MACHINE}${FR}.plt > tmp.plt
   
               if [[ $KEY == "" ]]
               then
                   KEY_LINE="set key"
               else
                   KEY_LINE="unset key"
               fi
   
               if [[ $BENCHMARK == "get" ]]
               then
                   YTICS_LINE="set ytics 1"
               else
                   YTICS_LINE="set ytics 0.5"
               fi
   
               replace_variable RESULTS_PATH
               replace_variable BENCHMARK
               replace_variable KEY_LINE
               replace_variable YTICS_LINE
   
               replace_variable POSIX_SETTINGS
               replace_variable SPINLOCK_SETTINGS
               replace_variable MCS_SETTINGS
               replace_variable MCSTP_SETTINGS
               replace_variable FLAT_SETTINGS
               replace_variable CCSYNCH_SETTINGS
               replace_variable DSMSYNCH_SETTINGS
               replace_variable RCL_SETTINGS
   
               cat tmp.plt | gnuplot \
                   > output/memcached-${BENCHMARK}-${TAG}-${MACHINE}${KEY}${FR}.pdf
   
			   cp tmp.plt presentation/other/memcached-${BENCHMARK}-${TAG}-${MACHINE}${KEY}${FR}.plt

               rm tmp.plt
   
           done
       done
   done
done

for KEY in "key" "key-wide"
do

    cat ${KEY}.plt > tmp.plt

    replace_variable POSIX_SETTINGS
    replace_variable SPINLOCK_SETTINGS
    replace_variable MCS_SETTINGS
    replace_variable MCSTP_SETTINGS
    replace_variable FLAT_SETTINGS
    replace_variable CCSYNCH_SETTINGS
    replace_variable DSMSYNCH_SETTINGS
    replace_variable RCL_SETTINGS

    cat tmp.plt | gnuplot > output/${KEY}.pdf 2> /dev/null

	cp tmp.plt presentation/other/${KEY}.plt

    rm tmp.plt

done

