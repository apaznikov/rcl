#!/bin/bash

for FILE in *.plt
do
	    cat $FILE | LC_ALL="en_US.UTF-8" LC_NUMERIC="en_US.UTF-8" gnuplot > ${FILE%.plt}.pdf 2> /dev/null
done

