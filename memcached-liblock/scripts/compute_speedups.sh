#!/bin/bash
#
# compute_accelerations.sh
# ========================
# (C) Jean-Pierre Lozi, 2013
#

RESULTS_FOLDER=$1
PREFIX=$2

if [[ -z "$PREFIX" ]]
then
    PREFIX="memcached_1_4_6"
fi


compute_acceleration()
{
	BASE=$1
	N=0

	while read LINE
    do

        I=0
		VALS=$(echo $LINE | tr -s ',' ' ')
		M=$(echo $LINE | cut -d"," -f2)
		EXP=0

        for V in $VALS
        do

            if [[ I -eq 0 ]]
            then
                N=$V
            elif [[ I -ge 2 ]]
            then
			    EXP="$EXP + ($BASE/$V - $BASE/$M)^2"
            fi

		    I=$I+1

        done

        if [ $LOCK = "rcl" ]
        then
			echo $(echo $N+1|bc),$(echo $BASE/$M | bc -l),$(echo $EXP | bc -l)
		else
 			echo $N,$(echo $BASE/$M | bc -l),$(echo $EXP | bc -l)
		fi

    done < $RESULTS_FOLDER/$PREFIX\_$LOCK\_$EXP.csv
}

build_data()
{
	for EXP in "get" "set"
    do
        if [[ -f $RESULTS_FOLDER/$PREFIX\_posix\_$EXP.csv ]]
        then
            BASE=$(cat $RESULTS_FOLDER/$PREFIX\_posix_$EXP.csv |               \
                   head -n 1 | cut -d"," -f2)

 		    for LOCK in posix spinlock mcs rcl
            do
                if [[ -f $RESULTS_FOLDER/$PREFIX\_$LOCK\_$EXP.csv ]]
                then
                    echo "Computing acceleration for $LOCK/$EXP..."

                    cat $RESULTS_FOLDER/$PREFIX\_$LOCK\_$EXP.csv  |            \
                        compute_acceleration $BASE                             \
                        > $RESULTS_FOLDER/acc-$LOCK-$EXP.csv
                fi
 		    done
        fi
	done

    echo "Done."
}

build_data

