#!/bin/bash
#
# benchmark.sh
# ============
# (C) Jean-Pierre Lozi, 2013
#

FILE_PREFIX=memcached_1_4_6


if [[ -z "$EXECUTE_NUMBER" ]];       then EXECUTE_NUMBER=0;                   fi
if [[ -z "$EXPERIMENTS" ]];          then EXPERIMENTS="get set";              fi
if [[ -z "$FAST" ]];                 then FAST=0;                             fi
if [[ -z "$LIBMEMCACHED_VERSION" ]]; then LIBMEMCACHED_VERSION="1.0.2";       fi
if [[ -z "$MEMCACHED_VERSION" ]];    then MEMCACHED_VERSION="1.4.6";          fi
if [[ -z "$METRIC" ]];               then METRIC="ELAPSED_TIME";              fi
if [[ -z "$N_RUNS" ]];               then N_RUNS=1;                           fi
if [[ -z "$OFFSET" ]];               then OFFSET=0;                           fi
if [[ -z "$SCALE" ]];                then SCALE=6;                            fi
if [[ -z "$STEP" ]];                 then STEP=1;                             fi
if [[ -z "$BENCHES" ]]
then
    BENCHES="rcl mcs spinlock posix"
fi


HOSTNAME=$(hostname)

DEFAULT_GET_INITIAL_LOAD_OPTION="--initial-load=30000"
DEFAULT_SET_INITIAL_LOAD_OPTION=""


if [[ "$FAST" != "0" ]]
then
    # Fast configuration
    CONCURRENCY=1
else
    if [[ "${HOSTNAME#*amd48}" != "$HOSTNAME" ]]
    then
        # Normal concurrency level
        CONCURRENCY=480
    elif [[ "$HOSTNAME" == "niagara2.cs.rochester.edu" ]]
    then
        CONCURRENCY=128
    # else
    #     CONCURRENCY=488
    fi
fi


if [[ "$(uname)" == "Linux" ]]
then
    N_PROCS=$(cat /proc/cpuinfo | grep processor | wc -l)
else
    N_PROCS=$(kstat cpu_info | grep "instance" | wc -l | tr -d ' ')
fi


FIRST_HW_THREAD=$((N_PROCS / 2))
export FIRST_HW_THREAD

trap "cleanup; exit" SIGHUP SIGINT SIGTERM


# if [[ "$(uname)" != "SunOS" && "$(id -u)" != "0" ]]
# then
#     echo "Must be root"
#     exit 1
# fi


function _seq
{
    if [[ "$(uname)" == "Linux" ]]
    then
        seq $1 $3 $2
    else
        nawk "BEGIN{ for (i = $1 ; i <= $2 ; i += $3) print i}"
    fi
}


function cleanup
{
    rm -f ../results/sum
    rm -f ../results/list
    rm -f ../results/memslap_out
    rm -f ../results/memslap_err
    rm -f ../results/memcached_err
}


function benchmark
{
    OUTPUT_FILE=$2
    QUIET=$4
    AVERAGE=$5

    if [[ "$OUTPUT_FILE" = "0" ]]; then OUTPUT_FILE="/dev/null"; fi

    if [[ "$(uname)" == "Linux" ]]
    then
        killall -9 -q memcached 2> /dev/null
    else
        kill `ps -ef | grep memcached | grep -v grep | awk '{print $2}'`       \
             2> /dev/null
    fi

    cleanup
    rm -f ../results/$2.csv

    if [[ "$QUIET" == "0" ]]
    then
        echo "Starting generating file results/"$2".csv..."
        date
    fi

    if [[ "$1" == "rcl" ]]
    then
        # MAX_THREADS=21
        MAX_THREADS=$((N_PROCS / 2 - 2))
    else
        # MAX_THREADS=22
        MAX_THREADS=$((N_PROCS / 2 - 1))
    fi

    if [[ -z "$NS_THREADS" ]]
    then
        STEP_M1="$(($STEP - 1))"

        if [[ $STEP -gt 1 ]]
        then
            if [[ "$1" == "rcl" ]]
            then
                NS_THREADS="1 `_seq $STEP_M1 $MAX_THREADS $STEP`"
            else
                NS_THREADS="1 `_seq $STEP $MAX_THREADS $STEP`"
            fi
        else
            NS_THREADS="`_seq 1 $MAX_THREADS $STEP`"
        fi
    fi

    if [[ "$QUIET" == "0" ]]
    then
        echo "Lock: $1, experiment: $3, metric : $METRIC."
    fi

    for N in $NS_THREADS
    do
        echo -n "$N," >> ../results/$2.csv
        echo -n "scale=$SCALE;(" > ../results/sum
        echo -n > ../results/list

        echo -n "$N,"

        for M in `_seq 1 $N_RUNS 1`
        do
            if [[ "${HOSTNAME#*amd48}" != "$HOSTNAME" ]]
            then
                     LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"../../liblock/"         \
                     LIBLOCK_LOCK_NAME=$1 $PREFIX                              \
                     ../src/memcached-$MEMCACHED_VERSION/memcached             \
                     -t $N -u root > ../results/memcached_out                  \
                     2> ../results/memcached_err &

            elif [[ "$HOSTNAME" == "niagara2.cs.rochester.edu" ]]
            then
                LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"../../liblock/"              \
                LIBLOCK_LOCK_NAME=$1 $PREFIX                                   \
                ../src/memcached-$MEMCACHED_VERSION/memcached                  \
                -t $N -l 127.0.0.1 -p 11211 > /dev/null                        \
                2> ../results/memcached_err &
            else
                 LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"../../liblock/"         \
                 LIBLOCK_LOCK_NAME=$1 $PREFIX                              \
                 ../src/memcached-$MEMCACHED_VERSION/memcached             \
                 -t $N -u root > ../results/memcached_out                  \
                 2> ../results/memcached_err &
            fi

            sleep 1

            if [[ -z "$INITIAL_LOAD" ]]
            then
                if [[ "$3" == "get" ]]
                then
                    INITIAL_LOAD_OPTION="$DEFAULT_GET_INITIAL_LOAD_OPTION"
                else
                    INITIAL_LOAD_OPTION="$DEFAULT_SET_INITIAL_LOAD_OPTION"
                fi
            else
                INITIAL_LOAD_OPTION="--initial-load=$INITIAL_LOAD"
            fi

            ../src/libmemcached-$LIBMEMCACHED_VERSION/clients/memslap          \
                --flush --servers=127.0.0.1:11211 --concurrency=$CONCURRENCY   \
                --test=$3 $INITIAL_LOAD_OPTION                                 \
                --execute-number=$EXECUTE_NUMBER                               \
                $4                                                             \
                > ../results/memslap_out 2> ../results/memslap_err

            if [[ "$(uname)" == "Linux" ]]
            then
                    killall -SIGTERM -w -v memcached                          \
                    2>> ../results/memcached_err
            else
                exec 3>&2 2> /dev/null
                kill -SIGTERM `ps -ef | grep memcached | grep -v grep          \
                             | awk '{print $2}'` 2>> ../results/memcached_err
                sleep 1
                exec 2>&3
            fi

            eval $PARSING_COMMANDS

            echo -n "("`cat ../results/memslap_err | wc -l | tr -d '\n'`")"

            echo -n ","

            if [[ "$AVERAGE" != "0" ]]
            then
                if [ $M -lt $N_RUNS ]
                then
                    echo -n '+' >> ../results/sum
                    echo -n ',' >> ../results/list
                fi
            fi
        done

        if [[ "$AVERAGE" != "0" ]]
        then
            echo ")/$N_RUNS" >> ../results/sum
            cat ../results/sum | bc | tr -d '\n' >> ../results/$2.csv
            echo -n "," >> ../results/$2.csv
            cat ../results/list >> ../results/$2.csv
            echo >> ../results/$2.csv
        fi

        echo
    done

    if [[ "$QUIET" == "0" ]]
    then
        echo "File ../results/"$2".csv contents:"
        cat ../results/$2.csv
    fi

    cleanup

    if [[ "$QUIET" == "0" ]]
    then
        echo "Done."
        date
        echo
    fi
}


# if [[ "$(uname)" == "Linux" ]]
# then
#     sysctl -w vm.nr_hugepages=2000
#     echo
# fi

if [[ "$METRIC" == "ELAPSED_TIME" ]]
then

    METRIC_SUFFIX=""
    PARSING_COMMANDS="grep \"Took\" ../results/memslap_out                     \
                      | awk '{ printf \"%s\",\$2; }'                           \
                      | tee -a ../results/sum ../results/list"

    MEMCACHED_VERSION=$MEMCACHED_VERSION-patched

    for EXPERIMENT in $EXPERIMENTS
    do
        for BENCH in $BENCHES
        do
            benchmark $BENCH ${FILE_PREFIX}_${BENCH}_${EXPERIMENT}${METRIC_SUFFIX} $EXPERIMENT 0 1
        done
    done

elif [[ "$METRIC" == "USE_RATE" ]]
then

    # To be checked. Might not work anymore.
    METRIC_SUFFIX="_use_rates"
    PARSING_COMMANDS="grep \"use rate\" ../results/memcached_err               \
                      | cut -d \" \" -f 7 | tr -d \"\n\"                       \
                      | tee -a ../results/sum ../results/list"

    benchmark rcl ${FILE_PREFIX}_rcl_get${METRIC_SUFFIX} get 0 1
    benchmark rcl ${FILE_PREFIX}_rcl_set${METRIC_SUFFIX} set 0 1

elif [[ "$METRIC" == "SERVER_CACHE_MISSES" ]]
then

    cd ../../liblock
    echo "Compiling the liblock with DCM profiling for rcl..."

    make distclean

    make ADDITIONAL_CFLAGS="-DPRECISE_DCM_PROFILING_INSIDE_ON"

    echo "Done."
    cd -
    trap "echo; echo 'Removing profiling from the liblock...';                 \
cd ../../liblock; make distclean; make; cd -; exit" SIGINT

    if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
    then
        NS_THREADS="21"
    elif [[ "$HOSTNAME" == "niagara2.cs.rochester.edu" ]]
    then
        NS_THREADS="61"
    # else
    #     NS_THREADS="21"
    fi

    METRIC_SUFFIX="_server_cache_misses"
    PARSING_COMMANDS="grep '\/cs' ../results/memcached_err | awk '{print \$2}' \
                      | tr -d '\n' | tee -a ../results/sum ../results/list"

    MEMCACHED_VERSION=$MEMCACHED_VERSION-patched

    benchmark rcl ${FILE_PREFIX}_rcl_get${METRIC_SUFFIX} get 0 1
    benchmark rcl ${FILE_PREFIX}_rcl_set${METRIC_SUFFIX} set 0 1

    cd ../../liblock
    echo "Removing profiling from the liblock..."
    make distclean
    make
    echo "Done."
    cd -

elif [[ "$METRIC" == "TIME_IN_CS" ]]
then

    if [[ "${HOSTNAME#*amd48}" != "$HOSTNAME" ]]
    then
        NS_THREADS="1 4 8 16 22"
    elif [[ "$HOSTNAME" == "niagara2.cs.rochester.edu" ]]
    then
        NS_THREADS="1 4 8 16 32 62"
    # else
    #     NS_THREADS="1 4 8 16 22"
    fi

    METRIC_SUFFIX="_times_in_cs"
    PARSING_COMMANDS="grep \"Global statistics:\" ../results/memcached_err     \
                      | cut -d \" \" -f 4 | tr -d \"%\n\"                      \
                      | tee -a ../results/sum ../results/list"
    PREFIX="$(pwd)/../../lock-profiler/lock-profiler -b BY_HW_THREAD
            -l NONE_CYCLES"

    benchmark posix ${FILE_PREFIX}_posix_get${METRIC_SUFFIX} get 0 1
    benchmark posix ${FILE_PREFIX}_posix_set${METRIC_SUFFIX} set 0 1

elif [[ "$METRIC" == "TIME_IN_CS_AND_DCM_FOR_MCL" ]]
then

    BENCHMARK_NAMES=('get' 'set')
    BENCHMARK_COMMANDS=("benchmark posix ${FILE_PREFIX}_posix_get"
                        "benchmark posix ${FILE_PREFIX}_posix_set")

    if [[ "${HOSTNAME#*amd48}" != "$HOSTNAME" ]]
    then
        NS_THREADS="22"
    elif [[ "$HOSTNAME" == "niagara2.cs.rochester.edu" ]]
    then
        NS_THREADS="62"
    # else
    #     NS_THREADS="22"
    fi

    for I in 0 1
    do
        BENCHMARK_NAME=${BENCHMARK_NAMES[$I]}
        BENCHMARK_COMMAND=${BENCHMARK_COMMANDS[$I]}

        echo
        echo "BENCHMARK = "$BENCHMARK_NAME
        echo


        ########################################################################
        # ID of the most contended lock                                        #
        ########################################################################

        OUTPUT_FILE_SUFFIX="_mcl_ids"
        PREFIX="$(pwd)/../../lock-profiler/lock-profiler -b BY_HW_THREAD -a
                -l NONE_CYCLES"
        PARSING_COMMANDS="grep \"#\" ../results/memcached_err | sort -k 6 -n   \
                          | tail -n 1 | tr -d \# | awk                         \
                          '{ print \$3\" \"\$4\" \"\$5\" \"\$6 }'"
        OLD_N_RUNS=$N_RUNS
        N_RUNS=1

        RES=`eval $BENCHMARK_COMMAND$OUTPUT_FILE_SUFFIX $BENCHMARK_NAME 1 0`
        N_RUNS=$OLD_N_RUNS

        ID=`echo $RES | awk -F'[, ]' '{ print $2 }'`
        ALLOC_ID=`echo $RES | awk -F'[, ]' '{ print $3 }'`
        ADDRESS=`echo $RES | awk -F'[, ]' '{ print $4 }'`
        TIME_IN_CS=`echo $RES | awk -F'[, ]' '{ print $5 }'`

        echo -n "Found that the lock with the most contention spends "
        echo    "$TIME_IN_CS% of its time in CS."
        echo "Lock info: id=$ID, alloc_id=$ALLOC_ID, address=$ADDRESS."
        echo


        ########################################################################
        # Time in CS                                                           #
        ########################################################################

        echo "Time in CS for this lock :"
        echo "=========================="

        OUTPUT_FILE_SUFFIX="_mcl_time_in_cs"
        PARSING_COMMANDS="grep \"$ALLOC_ID \" ../results/memcached_err         \
                          | awk '{ print \$6 }' | tr -d \"\n\"                 \
                          | tee -a ../results/sum ../results/list"

        PREFIX="$(pwd)/../../lock-profiler/lock-profiler -b BY_HW_THREAD -a    \
                -l NONE_CYCLES"

        eval $BENCHMARK_COMMAND$OUTPUT_FILE_SUFFIX $BENCHMARK_NAME 0 1
        echo


        ########################################################################
        # Data cache misses                                                    #
        ########################################################################

        echo "Data cache misses for this lock :"
        echo "================================="

        if [[ "${HOSTNAME#*amd48}" != "$HOSTNAME" ]]
        then

            echo "OFFSET=0"
            echo "--------"

            echo "WARNING: check that the offset is alright, I didn't have "
            echo "any offset issues because the monitoring was per core instead"
            echo "of per node!!!"

            OUTPUT_FILE_SUFFIX="_mcl_l2_dcm_o1"
            PREFIX="$(pwd)/../../lock-profiler/lock-profiler -l PAPI           \
                    -e 0x80000002 -i -b BY_HW_THREAD -m PER_NODE -a -o 0"
            PARSING_COMMANDS="grep \"$ALLOC_ID \" ../results/memcached_err     \
                              | awk '{ print \$7 }' | tr -d \"\n\"             \
                              | tee -a ../results/sum ../results/list"

            FILE="${FILE_PREFIX}_posix_$BENCHMARK_NAME$OUTPUT_FILE_SUFFIX"
            eval $BENCHMARK_COMMAND$OUTPUT_FILE_SUFFIX $BENCHMARK_NAME 0 1
            RESULT_1=`cat ../results/$FILE.csv`

            echo

        elif [[ "$HOSTNAME" == "niagara2.cs.rochester.edu" ]]
        then

            echo "OFFSET=1"
            echo "--------"

            OUTPUT_FILE_SUFFIX="_mcl_l2_dcm_o1"
            PREFIX="$(pwd)/../../lock-profiler/lock-profiler -l CPC            \
                    -e DC_miss -i -b BY_HW_THREAD -m PER_CORE -a -o 1"
            PARSING_COMMANDS="grep \"$ALLOC_ID \" ../results/memcached_err     \
                              | awk '{ print \$7 }' | tr -d \"\n\"             \
                              | tee -a ../results/sum ../results/list"

            FILE="${FILE_PREFIX}_posix_$BENCHMARK_NAME$OUTPUT_FILE_SUFFIX"
            eval $BENCHMARK_COMMAND$OUTPUT_FILE_SUFFIX $BENCHMARK_NAME 0 1
            RESULT_1=`cat ../results/$FILE.csv`

            echo

            ####################################################################

            if [[ "$BENCHMARK_NAME" == "get" ]]
            then
                echo "OFFSET=2"
                echo "--------"

                OUTPUT_FILE_SUFFIX="_mcl_l2_dcm_o2"
                PREFIX="$(pwd)/../../lock-profiler/lock-profiler -l CPC        \
                        -e DC_miss -i -b BY_HW_THREAD -m PER_CORE -a -o 2"
                PARSING_COMMANDS="grep \"$ALLOC_ID \" ../results/memcached_err \
                                  | awk '{ print \$7 }' | tr -d \"\n\"         \
                                  | tee -a ../results/sum ../results/list"

                FILE="${FILE_PREFIX}_posix_$BENCHMARK_NAME$OUTPUT_FILE_SUFFIX"
                eval $BENCHMARK_COMMAND$OUTPUT_FILE_SUFFIX $BENCHMARK_NAME 0 1
                RESULT_2=`cat ../results/$FILE.csv`

                echo

                RESULT_1=`echo $RESULT_1 | cut -d "," -f 2`
                RESULT_2=`echo $RESULT_2 | cut -d "," -f 2`

                echo -n "Averaged result = "
                echo $(echo "scale=2;($RESULT_1+$RESULT_2)/2" | bc)

                echo
            fi

        fi
    done

fi

# if [[ "$(uname)" == "Linux" ]]
# then
#     sysctl -w vm.nr_hugepages=0
#     echo
# fi





#benchmark rcl ${FILE_PREFIX}_rcl_get_${METRIC_SUFFIX} mget --binary
#benchmark mcs ${FILE_PREFIX}_mcs_get_${METRIC_SUFFIX} mget --binary
#benchmark spinlock ${FILE_PREFIX}_rcl_get_${METRIC_SUFFIX} mget --binary
#benchmark posix ${FILE_PREFIX}_mcs_get_${METRIC_SUFFIX} mget --binary

