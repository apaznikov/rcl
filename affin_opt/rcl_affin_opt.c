#define _GNU_SOURCE
#include <stdio.h>
#include <dlfcn.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <hwloc.h>

#include "liblock.h"

#define MAX_CPUS 100

/* #define _RCL_AFFIN_DEBUG */

static int (*pthread_create_orig)(pthread_t *thread, 
                                  const pthread_attr_t *attr,
                                  void *(*start_routine) (void *), 
                                  void *arg) = NULL;

#ifdef _RCL_AFFIN_DEBUG
// print_elem: Print object info (DEBUG)
static void print_elem(hwloc_obj_t obj);
#endif

// is_regular_thread: Check by pthread attributes if it's ordinary
// thread for which one have to set affinity.
static int is_regular_thread(const pthread_attr_t *attr);

// find_cover_obj: Find object covering obj with more 
// child objects than obj.
static int find_cover_obj(hwloc_obj_t obj, hwloc_obj_t *cover_obj, 
                          int system_nb_pu);

// find_free_cpu: Find free pu covered by object, 
// return number of hw_thread or -1 if can't find.
static int find_free_cpu(hwloc_obj_t cover_obj, 
                         int *busy_hw_threads, int nthreads_per_cpu);

// set_affinity: Set affinity to the core nearby the RCL.
static void set_affinity(const pthread_attr_t *attr, pthread_attr_t *new_attr);

// set_pthread_affinity: Set affinity to CPU in attributes
static void set_pthread_affinity(const pthread_attr_t *attr, 
                                 pthread_attr_t *new_attr, int cpu);

// pthread_create: Set affinity to the core nearby the RCL.
int pthread_create(pthread_t *thread, 
                   const pthread_attr_t *attr,
                   void *(*start_routine) (void *), 
                   void *arg)
{
    dlerror();

    if (pthread_create_orig == NULL)
        pthread_create_orig = dlsym(RTLD_NEXT, "pthread_create");

    char *error;
    if ((error = dlerror()) != NULL)  {
        fprintf(stderr, "Can't load malloc: %s\n", error);
        return -1;
    }

    if (is_regular_thread(attr)) {
        printf("regular thread\n");
#ifdef _RCL_AFFIN_DEBUG
        printf("regular thread\n");
#endif
        pthread_attr_t new_attr;
        pthread_attr_init(&new_attr);

        set_affinity(attr, &new_attr);

        return pthread_create_orig(thread, &new_attr, start_routine, arg);
    } else {
#ifdef _RCL_AFFIN_DEBUG
        printf("non-regular thread\n");
#endif
        return pthread_create_orig(thread, attr, start_routine, arg);
    }
}

// is_regular_thread: Check by pthread attributes if it's ordinary
// thread for which one have to set affinity
static int is_regular_thread(const pthread_attr_t *attr)
{
    if (attr == NULL) 
        return 1;

    // If affinity is set, don't set affinity
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);

    int rc = pthread_attr_getaffinity_np(attr, sizeof(cpu_set_t), &cpuset);

    if (rc != 0) {
        fprintf(stderr, "pthread_attr_getaffinity_np() failed\n");
        return 0;
    }

    if (CPU_COUNT(&cpuset) > 0) 
        return 0;

    // Schedpolicy
    int schedpolicy = 0;
    rc = pthread_attr_getschedpolicy(attr, &schedpolicy);
    if (rc != 0) {
        fprintf(stderr, "pthread_attr_getschedpolicy() failed\n");
        return 0;
    }
    
    if (schedpolicy != SCHED_OTHER) 
        return 0;

    // Scheduling parameters
    struct sched_param schedparam;
    pthread_attr_getschedparam(attr, &schedparam);
    if (rc != 0) {
        fprintf(stderr, "pthread_attr_getschedparam() failed\n");
        return 0;
    }

    if (schedparam.sched_priority > 0)
        return 0;

    return 1;
}

#ifdef _RCL_AFFIN_DEBUG
// print_elem: Print object info (DEBUG)
static void print_elem(hwloc_obj_t obj)
{
    char type[32], attr[1024];
    /* int depth = obj->depth; */

    hwloc_obj_type_snprintf(type, sizeof(type), obj, 0);
    /* printf("%*s%s", 2*depth, "", type); */
    if (obj->os_index != (unsigned) -1)
        printf("#%u", obj->os_index);
    hwloc_obj_attr_snprintf(attr, sizeof(attr), obj, " ", 0);
    if (*attr)
        printf("(%s)", attr);
    putchar('\n');
}
#endif

// find_cover_obj: Find object covering obj with more 
// child objects than obj.
static int find_cover_obj(hwloc_obj_t obj, hwloc_obj_t *cover_obj, 
                          int system_nb_pu)
{
#ifdef _RCL_AFFIN_DEBUG
    printf("find_cover_obj for ");
    print_elem(obj);
#endif

    *cover_obj = obj;

    int prev_nb_child_pu = 
        hwloc_get_nbobjs_inside_cpuset_by_type(hwloc_topology, 
                                               obj->allowed_cpuset, 
                                               HWLOC_OBJ_PU);

    if (prev_nb_child_pu >= system_nb_pu)
        return -1;

    // Find the object covering the PU
    for (;;) {
        *cover_obj = (*cover_obj)->parent;
#ifdef _RCL_AFFIN_DEBUG
        printf("try ");
        print_elem(*cover_obj);
#endif

        const int nb_child_pu = 
            hwloc_get_nbobjs_inside_cpuset_by_type(hwloc_topology, 
                                                   (*cover_obj)->allowed_cpuset,
                                                   HWLOC_OBJ_PU);
        if ((nb_child_pu > prev_nb_child_pu) && (nb_child_pu > 1))
            break;
        else 
            prev_nb_child_pu = nb_child_pu;
    }

#ifdef _RCL_AFFIN_DEBUG
    printf("\n");
#endif

    return 0;
}

// find_free_cpu: Find free pu covered by object, 
// return number of hw_thread or -1 if can't find.
static int find_free_cpu(hwloc_obj_t cover_obj, 
                         int *busy_hw_threads, int nthreads_per_cpu)
{
    hwloc_cpuset_t cover_cpuset = cover_obj->allowed_cpuset;
    const int cover_nb_pu = 
        hwloc_get_nbobjs_inside_cpuset_by_type(hwloc_topology, 
                                               cover_cpuset, 
                                               HWLOC_OBJ_PU);

    int cover_pu_idx = 0;
#ifdef _RCL_AFFIN_DEBUG
    printf("find_free_cpu() for ");
    print_elem(cover_obj);
    printf("%d children:\n", cover_nb_pu);
#endif
    for (cover_pu_idx = 0; cover_pu_idx < cover_nb_pu; cover_pu_idx++) {
        hwloc_obj_t cover_pu = 
            hwloc_get_obj_inside_cpuset_by_type(hwloc_topology, 
                                                cover_cpuset, 
                                                HWLOC_OBJ_PU, 
                                                cover_pu_idx);

        if ((is_hw_thread_free(&topology->hw_threads[cover_pu->os_index])) &&
            (busy_hw_threads[cover_pu->os_index] <= nthreads_per_cpu)) {
#ifdef _RCL_AFFIN_DEBUG
            printf("\tFREE\t");
            print_elem(cover_pu);
#endif
            return cover_pu->os_index;
        } else {
#ifdef _RCL_AFFIN_DEBUG
            if (!is_hw_thread_free(&topology->hw_threads[cover_pu->os_index]))
                printf("\tRCL\t");
            else
                printf("\tTHREAD\t");
            print_elem(cover_pu);
#endif
        }
    }

#ifdef _RCL_AFFIN_DEBUG
    printf("\n");
#endif

    return -1;
}

#ifdef _RCL_AFFIN_DEBUG
static void print_busy_hw_threads(int *busy_hw_threads, int len)
{
    int i = 0;
    printf("Busy hw_threads: ");
    for (i = 0; i < len; i++)
        printf("%2d", busy_hw_threads[i]);
    printf("\n");
}
#endif

// set_pthread_affinity: Set affinity to CPU in attributes
static void set_pthread_affinity(const pthread_attr_t *attr, 
                                 pthread_attr_t *new_attr, int cpu)
{
    if (attr != NULL) {
        // Copy all stuff from attr to new_attr
        
        // Maybe just: 
        // memcpy(new_attr, attr, __SIZEOF_PTHREAD_ATTR_T + sizeof(long int));
        // ?
        int detachstate = 0;
        pthread_attr_getdetachstate(attr, &detachstate);
        pthread_attr_setdetachstate(new_attr, detachstate);

        size_t guardsize = 0;
        pthread_attr_getguardsize(attr, &guardsize);
        pthread_attr_setguardsize(new_attr, guardsize);

        int inheritsched = 0;
        pthread_attr_getinheritsched(attr, &inheritsched);
        pthread_attr_setinheritsched(new_attr, inheritsched);

        struct sched_param param;
        pthread_attr_getschedparam(attr, &param);
        pthread_attr_setschedparam(new_attr, &param);

        int policy = 0;
        pthread_attr_getschedpolicy(attr, &policy);
        pthread_attr_setschedpolicy(new_attr, policy);

        int scope = 0;
        pthread_attr_getscope(attr, &scope);
        pthread_attr_setscope(new_attr, scope);

        void *stackaddr = NULL;
        size_t stacksize = 0;
        pthread_attr_getstack(attr, &stackaddr, &stacksize);
        pthread_attr_setstack(new_attr, stackaddr, stacksize);
    }

    // Set affinity attribute
    cpu_set_t linux_cpu_set;
    CPU_ZERO(&linux_cpu_set);
    CPU_SET(cpu, &linux_cpu_set);

    pthread_attr_setaffinity_np(new_attr, sizeof(cpu_set_t), &linux_cpu_set);

    return;
}

// set_affinity: Set affinity to the core nearby the RCL.
static void set_affinity(const pthread_attr_t *attr, pthread_attr_t *new_attr)
{
    // Get cpuset for whole system
    hwloc_const_cpuset_t cpuset = 
        hwloc_topology_get_allowed_cpuset(hwloc_topology);

    int pu_idx = 0;
    const int nb_pu = hwloc_get_nbobjs_inside_cpuset_by_type(hwloc_topology, 
                                                             cpuset, 
                                                             HWLOC_OBJ_PU);

    if (MAX_CPUS < nb_pu) {
        fprintf(stderr, "set_affinity() failed: MAX_CPUS < nb_pu\n");
        return;
    }

    static int busy_hw_threads[MAX_CPUS] = { 0 };
    static int nthreads_per_cpu = 0;

#ifdef _RCL_AFFIN_DEBUG
    printf("================= set_affinity() ======================\n");
    print_busy_hw_threads(busy_hw_threads, nb_pu);
#endif

    // Look through PU (hw_threads) and find RCL-servers
    for (pu_idx = 0; pu_idx < nb_pu; pu_idx++) {
        hwloc_obj_t pu = 
            hwloc_get_obj_inside_cpuset_by_type(hwloc_topology, cpuset, 
                                                HWLOC_OBJ_PU, pu_idx);
#ifdef _RCL_AFFIN_DEBUG
        printf("PU %d: %d ", pu_idx, pu->os_index);
#endif

        if (!is_hw_thread_free(&topology->hw_threads[pu->os_index])) {
#ifdef _RCL_AFFIN_DEBUG
            printf("RCL\n");
#endif
            
            // Find free hw_thread nearby RCL core
            int free_cpu = -1;
            hwloc_obj_t cover_obj = pu;

            do {
                hwloc_obj_t obj = cover_obj;

                int rc = find_cover_obj(obj, &cover_obj, nb_pu);
                if (rc < 0) {
                    obj = pu;
                    nthreads_per_cpu++;
                    rc = find_cover_obj(obj, &cover_obj, nb_pu);
                    if (rc < 0) {
                        fprintf(stderr, "find_cover_obj() failed\n");
                        return;
                    }
                }

                free_cpu = find_free_cpu(cover_obj, 
                                         busy_hw_threads, nthreads_per_cpu);
            } while (free_cpu < 0);

            busy_hw_threads[free_cpu]++;
#ifdef _RCL_AFFIN_DEBUG
            print_busy_hw_threads(busy_hw_threads, nb_pu);
            printf("FREE: %d\n", free_cpu);
#endif
            set_pthread_affinity(attr, new_attr, free_cpu);

            break;
        }
        printf("\n");
    }

#ifdef _RCL_AFFIN_DEBUG
    printf("=======================================================\n");
#endif

    return;
}
