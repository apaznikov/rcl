#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>

#include <sys/mman.h>

#include <numa.h>

#include "liblock.h"

liblock_lock_t lock1, lock2, lock3, lock4, lock5,
               lock6, lock7, lock8, lock9, lock10,
               lock11, lock12, lock13, lock14, lock15,
               lock16, lock17, lock18, lock19, lock20;

volatile int cntr = 0;

enum {
    /* NTHREADS     = 15, */
    NTHREADS     = 10,
    NITERS       = 100,
    /* SIZE         = 2000000000 */
    SIZE         = 900000000
};

void *cs(void *arg)
{
    cntr++;
    return NULL;
}

void *thread(void *arg)
{
    int i;

    /* printf("hello i'm 0x%llx\n", (unsigned long long) pthread_self()); */

    for (i = 0; i < NITERS; i++) {
        liblock_exec(&lock1, cs, NULL);
    }

    /* sleep(1000); */

    return NULL;
}

void *dummy_thread(void *arg)
{
    return NULL;
}

int main(int argc, char **argv)
{
    liblock_rcl_lock_init(&lock1);
    liblock_rcl_lock_init(&lock2);
    liblock_rcl_lock_init(&lock3);

    /* liblock_rcl_lock_init(&lock4); */
    /* liblock_rcl_lock_init(&lock5); */

    pthread_t tids[NTHREADS];

    for (int i = 0; i < NTHREADS; i++) 
        /* pthread_create(&tids[i], NULL, thread, NULL); */
        /* pthread_create(&tids[i], NULL, dummy_thread, NULL); */
        liblock_thread_create(&tids[i], NULL, dummy_thread, NULL);

    for (int i = 0; i < NTHREADS; i++) 
        pthread_join(tids[i], NULL);

    liblock_lock_destroy(&lock1);
    liblock_lock_destroy(&lock2);
    liblock_lock_destroy(&lock3);

    /* liblock_lock_destroy(&lock4); */
    /* liblock_lock_destroy(&lock5); */

    return 0;
}
