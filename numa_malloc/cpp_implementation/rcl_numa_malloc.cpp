#include <iostream>
#include <memory>
#include <atomic>
#include <list>

#include <cstring>

#include <dlfcn.h>
#include <numa.h>
#include <numaif.h>
#include <sys/mman.h>

#include "liblock.h"

#define _LOCK_PAGES_ON

using malloc_t = void* (*)(size_t size);
static malloc_t malloc_orig = nullptr;

struct malloc_item {
    // Real pointer address
    void *addr = nullptr;

    size_t size = 0;

    // NUMA-node affinity (number of node or -1 if none)
    int node_affinity = -1;
};

std::list<malloc_item> malloc_list;

// do_malloc: Allocate memory and add to list
static void *do_malloc(size_t size)
{
    // FIXME This code cause segfault in libstdc++
    // malloc_list.emplace_back(malloc_item{malloc_orig(size), size, -1});
    return malloc_orig(size);
}

std::list<int> testlist;

// malloc: RCL-aware malloc. Allocate on NUMA-node.
void *malloc(size_t size)
{
    dlerror();

    if (malloc_orig == nullptr)
        malloc_orig = reinterpret_cast<malloc_t>(dlsym(RTLD_NEXT, "malloc"));

    auto *error = dlerror();
    if (error != nullptr)  {
        std::cerr << "Can't load malloc: " << error << std::endl;
        return nullptr;
    }

    // To prevent recursive call
    volatile static std::atomic<bool> is_allocating(false);
    if (is_allocating.load())
        return do_malloc(size);

    // FIXME This code cause segfault in libstdc++
    // std::cerr << "0";

    // Find RCL servers and test if all of them are runned on one NUMA node
    auto rcl_is_launched = false, is_on_same_node = false; 
    auto rcl_server_node = 0;

    for (auto i = 0; i < topology->nb_hw_threads; i++) {
        if (topology->hw_threads[i].server_type &&
            !strcmp(topology->hw_threads[i].server_type, "rcl")) {

            std::cout << "server " << i << " launched" << std::endl;
            if (!rcl_is_launched) {
                rcl_is_launched = true;
                is_on_same_node = true;
                rcl_server_node = topology->hw_threads[i].node->node_id;

            } else if (topology->hw_threads[i].node->node_id != 
                       rcl_server_node) {
                is_on_same_node = false;
                break;
            }
        }
    }

    if (rcl_is_launched) {
        if (is_on_same_node) {
            // Allocate on RCL NUMA node
            std::cout << "RCL servers are on the same node" << std::endl;

            is_allocating.store(true);
            std::shared_ptr<bitmask> nodemask(numa_get_mems_allowed(),
                                              numa_free_nodemask);
            is_allocating.store(false);

            numa_bitmask_clearall(nodemask.get());
            numa_bitmask_setbit(nodemask.get(), rcl_server_node);
            numa_set_bind_policy(1);
            numa_set_membind(nodemask.get());

            std::cerr << "allocate on node " << rcl_server_node << std::endl;
            auto ptr = do_malloc(size);

            if (ptr == nullptr) {
                std::cerr << "malloc() failed" << std::endl;
                return nullptr;
            }

#ifdef _LOCK_PAGES_ON
            // Lock pages to prevent swapping
            // mlock(ptr, size);
#endif
            // Touch pages
            std::memset(ptr, 0, size);

            return ptr;
        } else {
            std::cout << "RCL servers are NOT on the same node" << std::endl;
            return do_malloc(size);
        }
    } else {
        return do_malloc(size);
    }
}
