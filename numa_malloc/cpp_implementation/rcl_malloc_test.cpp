#include <iostream>
#include <string>
#include <vector>

#include <cstdio>
#include <cstdlib>

#include <pthread.h>
#include <unistd.h>

#include "liblock.h"

liblock_lock_t lock1, lock2, lock3;
volatile auto cntr = 0;

const auto NTHREADS = 15;
const auto NITERS   = 100;
const auto SIZE     = 200000000;

void test_malloc(void)
{
    auto *p = (int *) malloc(sizeof(int) * SIZE);
    if (p == nullptr)
        std::cerr << "malloc() failed" << std::endl;

    auto sum = 0.0l;
    for (auto i = 0; i < SIZE; i++) {
        p[i] = i;
        sum += p[i];
    }

    std::cout << "PROG: sum = " << sum << std::endl;

    // sleep(100);

    free(p);
}

void *cs(void *arg)
{
    cntr++;
    return nullptr;
}

void *thread(void *arg)
{
    /* printf("hello i'm 0x%llx\n", (unsigned long long) pthread_self()); */

    for (auto i = 0; i < NITERS; i++) 
        liblock_exec(&lock1, cs, NULL);

    /* test_malloc(); */

    return nullptr;
}

int main(int argc, char **argv)
{
    const auto liblock_name = std::string("rcl");
    liblock_lock_init(liblock_name.c_str(), &topology->hw_threads[3], 
                      &lock1, 0);

    /* test_malloc(); */

    liblock_lock_init(liblock_name.c_str(), &topology->hw_threads[1], 
                      &lock2, 0);

    test_malloc();

    liblock_lock_init(liblock_name.c_str(), &topology->hw_threads[4], 
                      &lock3, 0);

    std::vector<pthread_t> threadvec(NTHREADS);

    for (auto &t: threadvec)
        pthread_create(&t, nullptr, thread, nullptr);

    for (auto &t: threadvec)
        pthread_join(t, nullptr);

    /* test_malloc(); */

    liblock_lock_destroy(&lock1);
    liblock_lock_destroy(&lock2);
    liblock_lock_destroy(&lock3);

    return 0;
}
