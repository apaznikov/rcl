#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>

#include <sys/mman.h>

#include <numa.h>

#include "liblock.h"

liblock_lock_t lock1, lock2, lock3, lock4, lock5,
               lock6, lock7, lock8, lock9, lock10,
               lock11, lock12, lock13, lock14, lock15,
               lock16, lock17, lock18, lock19, lock20,
               lock21;

volatile int cntr = 0;

enum {
    NTHREADS     = 15,
    NITERS       = 100,
    /* SIZE         = 2000000000 */
    SIZE         = 1000000000
};

void test_malloc(void)
{
    int *p = (int *) malloc(sizeof(int) * SIZE);
    if (p == NULL)
        printf("p is NULL\n");

    int i, sum = 0;
    for (i = 0; i < SIZE; i++) {
        p[i] = i;
        sum += p[i];
    }
    printf("PROG: sum = %d\n", sum);

    /* sleep(100); */

    free(p);
}

void *cs(void *arg)
{
    cntr++;
    return NULL;
}

void *thread(void *arg)
{
    int i;

    /* printf("hello i'm 0x%llx\n", (unsigned long long) pthread_self()); */

    for (i = 0; i < NITERS; i++) {
        liblock_exec(&lock1, cs, NULL);
    }

    /* test_malloc(); */

    return NULL;
}

int main(int argc, char **argv)
{
    test_malloc();
    test_malloc();

    /* char liblock_name[] = "rcl"; */
    /* liblock_lock_init(liblock_name, &topology->hw_threads[3], &lock1, 0); */

    liblock_rcl_lock_init(&lock1);

    test_malloc();

    liblock_rcl_lock_init(&lock2);

    test_malloc();

    liblock_rcl_lock_init(&lock3);
    liblock_rcl_lock_init(&lock4);

    test_malloc();

    liblock_rcl_lock_init(&lock5);

    test_malloc();

    liblock_rcl_lock_init(&lock6);
    liblock_rcl_lock_init(&lock7);
    liblock_rcl_lock_init(&lock8);
    liblock_rcl_lock_init(&lock9);
    liblock_rcl_lock_init(&lock10);
    liblock_rcl_lock_init(&lock11);
    liblock_rcl_lock_init(&lock12);
    liblock_rcl_lock_init(&lock13);
    liblock_rcl_lock_init(&lock14);
    liblock_rcl_lock_init(&lock15);
    liblock_rcl_lock_init(&lock16);
    liblock_rcl_lock_init(&lock17);
    liblock_rcl_lock_init(&lock18);
    liblock_rcl_lock_init(&lock19);
    liblock_rcl_lock_init(&lock20);
    liblock_rcl_lock_init(&lock21);

    test_malloc();

    pthread_t tids[NTHREADS];

    for (int i = 0; i < NTHREADS; i++) 
        pthread_create(&tids[i], NULL, thread, NULL);

    for (int i = 0; i < NTHREADS; i++) 
        pthread_join(tids[i], NULL);

    /* test_malloc(); */

    liblock_lock_destroy(&lock1);
    liblock_lock_destroy(&lock2);
    liblock_lock_destroy(&lock3);
    liblock_lock_destroy(&lock4);
    liblock_lock_destroy(&lock5);
    /* liblock_lock_destroy(&lock6); */
    /* liblock_lock_destroy(&lock7); */
    /* liblock_lock_destroy(&lock8); */
    /* liblock_lock_destroy(&lock9); */
    /* liblock_lock_destroy(&lock10); */
    /* liblock_lock_destroy(&lock11); */
    /* liblock_lock_destroy(&lock12); */
    /* liblock_lock_destroy(&lock13); */
    /* liblock_lock_destroy(&lock14); */
    /* liblock_lock_destroy(&lock16); */
    /* liblock_lock_destroy(&lock17); */
    /* liblock_lock_destroy(&lock18); */
    /* liblock_lock_destroy(&lock19); */
    /* liblock_lock_destroy(&lock20); */
    /* liblock_lock_destroy(&lock21); */

    return 0;
}
