#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <errno.h>

#include <sys/mman.h>

#include <numa.h>

#include "liblock.h"

liblock_lock_t lock1, lock2, lock3;
volatile int cntr = 0;

enum {
    NTHREADS     = 15,
    NITERS       = 100,
    SIZE         = 2000000000
};

void test_malloc(void)
{
    int *p = (int *) malloc(sizeof(int) * SIZE);
    if (p == NULL)
        printf("p is NULL\n");

    int i, sum = 0;
    for (i = 0; i < SIZE; i++) {
        p[i] = i;
        sum += p[i];
    }
    printf("PROG: sum = %d\n", sum);

    sleep(100);

    free(p);

    /* numa_free(p, sizeof(int) * SIZE); */
    /* if (msync(p, sizeof(int) * SIZE, 0) == 0) { */
    /*     printf("YES mapped\n"); */
    /*     sleep(300); */
    /* } else { */
    /*     printf("NOT mapped: %s\n", strerror(errno)); */
    /* } */
}

void *cs(void *arg)
{
    cntr++;
    return NULL;
}

void *thread(void *arg)
{
    int i;

    /* printf("hello i'm 0x%llx\n", (unsigned long long) pthread_self()); */

    for (i = 0; i < NITERS; i++) {
        liblock_exec(&lock1, cs, NULL);
    }

    /* test_malloc(); */

    return NULL;
}

int main(int argc, char **argv)
{
    /* test_malloc(); */
    //
    //
    struct bitmask *nodemask = numa_get_mems_allowed();

    numa_bitmask_clearall(nodemask);

    int rcl_server_node = 0;
    numa_bitmask_setbit(nodemask, rcl_server_node);

    numa_set_membind(nodemask);

    char liblock_name[] = "rcl";
    liblock_lock_init(liblock_name, &topology->hw_threads[3], &lock1, 0);

    /* test_malloc(); */

    liblock_lock_init(liblock_name, &topology->hw_threads[1], &lock2, 0);

    test_malloc();

    liblock_lock_init(liblock_name, &topology->hw_threads[4], &lock3, 0);

    pthread_t tids[NTHREADS];

    for (int i = 0; i < NTHREADS; i++) 
        pthread_create(&tids[i], NULL, thread, NULL);

    for (int i = 0; i < NTHREADS; i++) 
        pthread_join(tids[i], NULL);

    /* test_malloc(); */

    liblock_lock_destroy(&lock1);
    liblock_lock_destroy(&lock2);
    liblock_lock_destroy(&lock3);

    return 0;
}
