#define _GNU_SOURCE
#include <stdio.h>
#include <dlfcn.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

#include <numa.h>
#include <numaif.h>
#include <sys/mman.h>

#include "liblock.h"

#define _LOCK_PAGES_ON

static void* (*malloc_orig)(size_t size) = NULL;

void *malloc(size_t size)
{
    dlerror();

    if (malloc_orig == NULL)
        malloc_orig = dlsym(RTLD_NEXT, "malloc");

    char *error;
    if ((error = dlerror()) != NULL)  {
        fprintf(stderr, "Can't load malloc: %s\n", error);
        return NULL;
    }

    // To prevent recursive call
    volatile static int is_allocating = 0;
    int val = 0;
    __atomic_load(&is_allocating, &val, __ATOMIC_SEQ_CST);
    if (val == 1) {
        /* printf("allocate orig\n"); */
        return malloc_orig(size);
    }

    // Find RCL servers and test if all of them are runned on one NUMA node
    int i, is_launched = 0, is_on_same_node = 0, rcl_server_node = 0;
    for(i = 0; i < topology->nb_hw_threads; i++) {
        if(topology->hw_threads[i].server_type &&
           !strcmp(topology->hw_threads[i].server_type, "rcl")) {

            printf("server %d launched\n", i);
            if (!is_launched) {
                is_launched = 1;
                is_on_same_node = 1;
                rcl_server_node = topology->hw_threads[i].node->node_id;
            } else if (topology->hw_threads[i].node->node_id != 
                       rcl_server_node) {
                is_on_same_node = 0;
                break;
            }
        }
    }

    if (is_launched) {
        if (is_on_same_node) {
            // Allocate on RCL NUMA node
            printf("RCL servers are on the same node\n");

            int val = 1;
            __atomic_store(&is_allocating, &val, __ATOMIC_SEQ_CST);
            __atomic_thread_fence(__ATOMIC_SEQ_CST);

            struct bitmask *nodemask = numa_get_mems_allowed();

            val = 0;
            __atomic_store(&is_allocating, &val, __ATOMIC_SEQ_CST);
            __atomic_thread_fence(__ATOMIC_SEQ_CST);

            numa_bitmask_clearall(nodemask);

            numa_bitmask_setbit(nodemask, rcl_server_node);

            numa_set_bind_policy(1);
            numa_set_membind((struct bitmask*) nodemask);

            numa_free_nodemask(nodemask);

            fprintf(stderr, "allocate on node %d\n", rcl_server_node);
            void *ptr = malloc_orig(size);

            if (ptr == NULL) {
                fprintf(stderr, "malloc() failed\n");
                return NULL;
            }

#ifdef _LOCK_PAGES_ON
            // Lock pages to prevent swapping
            /* mlock(ptr, size); */
#endif
            // Touch pages
            memset(ptr, 0, size);

            return ptr;
        } else {
            printf("RCL servers are NOT on the same node\n");
            return malloc_orig(size);
        }
    } else {
        /* printf("RCL server is not launched\n"); */
        return malloc_orig(size);
    }
}
