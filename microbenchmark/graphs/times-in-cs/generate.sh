#!/bin/bash
#
# generate.sh
# ===========
# (C) Jean-Pierre Lozi, 2013
#


. ../../../figures/bench.inc.sh

MACHINES=("amd48b" "niagara2")

if [[ $# -lt 2 ]]
then
    TAGS=("140122-phd-main" "140217-phd-main")
else
    TAGS=("$1" "$2")
fi


function replace_variable
{
    VARIABLE_NAME=$1
    VARIABLE=${!VARIABLE_NAME}

    cat base-modified.plt | sed "s/@@$VARIABLE_NAME@@/$VARIABLE/g"             \
        > tmp-base-modified.plt

    mv tmp-base-modified.plt base-modified.plt
}

for FR in "" "-fr"
do
    for MACHINE_ID in ${!MACHINES[*]}
    do

        TAG=${TAGS[$MACHINE_ID]}
        MACHINE=${MACHINES[$MACHINE_ID]}

        RESULTS_PATH="../../all-results/results-$TAG-$MACHINE/"
        RESULTS_PATH=$(echo $RESULTS_PATH | sed 's/\//\\\//g')

        cat base-${MACHINE}${FR}.plt > base-modified.plt

        replace_variable POSIX_SETTINGS
        replace_variable RESULTS_PATH

        cat base-modified.plt | gnuplot \
                > output/microbenchmark-times-in-cs-${MACHINE}${FR}.pdf

#		cp base-modified.plt presentation/microbenchmark-times-in-cs-${MACHINE}${FR}.plt

        rm base-modified.plt

    done
done

