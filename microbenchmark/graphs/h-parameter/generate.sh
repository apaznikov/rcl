#!/bin/bash
#
# generate.sh
# ===========
# (C) Jean-Pierre Lozi, 2013
#

. ../../../figures/bench.inc.sh

MACHINES=("amd48b" "niagara2")

#if [[ $# -lt 3 ]]
#then
#    TAGS=("140307-phd-h-parameter" "140307-phd-h-parameter")
#else
#    TAGS=("$1" "$2")
#fi


function replace_variable
{
    VARIABLE_NAME=$1
    VARIABLE=${!VARIABLE_NAME}

    cat tmp.plt | sed "s/@@$VARIABLE_NAME@@/$VARIABLE/g"                       \
        > tmp-tmp.plt

    mv tmp-tmp.plt tmp.plt
}


#for MACHINE_ID in ${!MACHINES[*]}
#do

#    MACHINE=${MACHINES[$MACHINE_ID]}
#    TAG=${TAGS[$MACHINE_ID]}

#    RESULTS_PATH="../../all-results/results-$TAG-$MACHINE/"
#    RESULTS_PATH=$(echo $RESULTS_PATH | sed 's/\//\\\//g')

    for KEY in "" #"-no-key"
    do

        cat base${KEY}.plt > tmp.plt

#        replace_variable RESULTS_PATH

        replace_variable CCSYNCH_SETTINGS_NP
        replace_variable DSMSYNCH_SETTINGS_NP

        cat tmp.plt | gnuplot #\
#            > output/microbenchmark-h-parameter-${MACHINE}${KEY}.pdf

        rm tmp.plt

    done

#done

#for KEY in "key" "key-wide"
#do
#
#    cat ${KEY}.plt > tmp.plt
#
#    replace_variable CCSYNCH_SETTINGS
#    replace_variable DSMSYNCH_SETTINGS
#
#    cat tmp.plt | gnuplot > output/${KEY}.pdf 2> /dev/null
#
#    rm tmp.plt
#
#done

