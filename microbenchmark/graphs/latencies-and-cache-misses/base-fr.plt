#!/usr/bin/gnuplot -persist
#
#
#    	G N U P L O T
#    	Version 4.6 patchlevel 0    last modified 2012-03-04
#    	Build System: Linux x86_64
#
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2012
#    	Thomas Williams, Colin Kelley and many others
#
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')
set terminal pdfcairo transparent enhanced dashed font "Helvetica, 16" fontscale 0.5 size 5.50in, 4.00in
unset clip points
set clip one
unset clip two
set bar 1.000000 front
set border 31 front linetype -1 linewidth 1.000
set timefmt z "%d/%m/%y,%H:%M"
set zdata
set timefmt y "%d/%m/%y,%H:%M"
set ydata
set timefmt x "%d/%m/%y,%H:%M"
set xdata
set timefmt cb "%d/%m/%y,%H:%M"
set timefmt y2 "%d/%m/%y,%H:%M"
set y2data
set timefmt x2 "%d/%m/%y,%H:%M"
set x2data
set boxwidth
set style fill  empty border
set style rectangle back fc  lt -3 fillstyle   solid 1.00 border lt -1
set style circle radius graph 0.02, first 0, 0
set style ellipse size graph 0.05, 0.03, first 0 angle 0 units xy
set dummy x,y
set format x "% g"
set format y "% g"
set format x2 "% g"
set format y2 "% g"
set format z "% g"
set format cb "% g"
set format r "% g"
set angles radians
unset grid
set raxis
set key title ""
set key at 2e+06, 1100, 0 right top horizontal Right noreverse enhanced autotitles nobox
set key noinvert samplen 4 spacing 1 width 0 height 0
set key maxcolumns 0 maxrows 0
set key noopaque
unset key
unset label
unset arrow
set style increment default
unset style line
unset style arrow
set style histogram clustered gap 2 title  offset character 0, 0, 0
unset logscale
set logscale x 10
set logscale y 10
set offsets 0, 0, 0, 0
set pointsize 0.6
set pointintervalbox 1
set encoding default
unset polar
unset parametric
unset decimalsign
set view 60, 30, 1, 1
set samples 100, 100
set isosamples 10, 10
set surface
unset contour
set clabel '%8.3g'
set mapping cartesian
set datafile separator ","
unset hidden3d
set cntrparam order 4
set cntrparam linear
set cntrparam levels auto 5
set cntrparam points 5
set size ratio 0 1,0.45
set origin -0.03,0.13
set style data points
set style function lines
set xzeroaxis linetype -2 linewidth 1.000
set yzeroaxis linetype -2 linewidth 1.000
set zzeroaxis linetype -2 linewidth 1.000
set x2zeroaxis linetype -2 linewidth 1.000
set y2zeroaxis linetype -2 linewidth 1.000
set ticslevel 0.5
set mxtics default
set mytics default
set mztics default
set mx2tics default
set my2tics default
set mcbtics default
set xtics border in scale 1,0.5 mirror norotate  offset character 0, 0, 0 autojustify
set xtics 10 norangelimit textcolor rgb "black"
set ytics border in scale 1,0.5 mirror norotate  offset character 0, 0, 0 autojustify
set ytics 10 norangelimit
set ztics border in scale 1,0.5 nomirror norotate  offset character 0, 0, 0 autojustify
set ztics autofreq  norangelimit
set nox2tics
set noy2tics
set cbtics border in scale 1,0.5 mirror norotate  offset character 0, 0, 0 autojustify
set cbtics autofreq  norangelimit
set rtics border in scale 1,0.5 nomirror norotate  offset character 0, 0, 0 autojustify
set rtics autofreq  norangelimit
set title ""
set title  offset character 0, 0, 0 font "" norotate
set timestamp bottom
set timestamp ""
set timestamp  offset character 0, 0, 0 font "" norotate
set rrange [ * : * ] noreverse nowriteback
set trange [ * : * ] noreverse nowriteback
set urange [ * : * ] noreverse nowriteback
set vrange [ * : * ] noreverse nowriteback
set xlabel  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set x2label ""
set x2label  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set xrange [@@X_RANGE@@] noreverse nowriteback
set x2range [ * : * ] noreverse nowriteback
set ylabel ""
set ylabel  offset character 0, 0, 0 font "" textcolor lt -1 rotate by -270
set y2label "Temps d'exécution (cycles)"
set y2label  offset character 0, 0, 0 font "" textcolor lt -1 rotate by -270
set yrange [ 1000.00 : 2.00000e+06 ] noreverse nowriteback
set y2range [ * : * ] noreverse nowriteback
set zlabel ""
set zlabel  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set zrange [ * : * ] noreverse nowriteback
set cblabel ""
set cblabel  offset character 0, 0, 0 font "" textcolor lt -1 rotate by -270
set cbrange [ * : * ] noreverse nowriteback
set zero 1e-08
set lmargin  0
set bmargin  0
set rmargin  1
set tmargin  0
set locale "fr_FR.UTF-8"
set pm3d explicit at s
set pm3d scansautomatic
set pm3d interpolate 1,1 flush begin noftriangles nohidden3d corners2color mean
set palette positive nops_allcF maxcolors 0 gamma 1.5 color model RGB
set palette rgbformulae 7, 5, 15
set colorbox default
set colorbox vertical origin screen 0.9, 0.2, 0 size screen 0.05, 0.6, 0 front bdefault
set style boxplot candles range  1.50 outliers pt 7 separation 1 labels auto unsorted
set loadpath
set fontpath
set psdir
set fit noerrorvariables
set multiplot
set lmargin 10
set size 1,0.450
set origin -0.03,0.42
set xtics textcolor rgb "white"
set xlabel ""
set yrange [@@Y_BOTTOM_RANGE@@]
set y2label "Temps d'exécution (cycles)"
set key right
set key at @@KEY_LOCATION@@
set grid lc rgb "#aaaaaa"
plot "@@RESULTS_PATH@@latencies_rcl_@@EXPERIMENT@@_0_@@N_RUNS@@.csv" using 1:2 with linespoints notitle  @@RCL_SETTINGS@@ axes x1y1, \
"@@RESULTS_PATH@@latencies_dsmsynch_@@EXPERIMENT@@_0_@@N_RUNS@@.csv" using 1:2 with linespoints notitle  @@DSMSYNCH_SETTINGS@@  axes x1y1, \
"@@RESULTS_PATH@@latencies_ccsynch_@@EXPERIMENT@@_0_@@N_RUNS@@.csv" using 1:2 with linespoints notitle  @@CCSYNCH_SETTINGS@@ axes x1y1, \
"@@RESULTS_PATH@@latencies_fc_@@EXPERIMENT@@_0_@@N_RUNS@@.csv" using 1:2 with linespoints notitle  @@FLAT_SETTINGS@@ axes x1y1, \
"@@RESULTS_PATH@@latencies_mcstpl_@@EXPERIMENT@@_0_@@N_RUNS@@.csv" using 1:2 with linespoints notitle  @@MCSTP_SETTINGS@@ axes x1y1, \
"@@RESULTS_PATH@@latencies_mcsl_@@EXPERIMENT@@_0_@@N_RUNS@@.csv" using 1:2 with linespoints notitle  @@MCS_SETTINGS@@ axes x1y1,\
 "@@RESULTS_PATH@@latencies_sl_@@EXPERIMENT@@_0_@@N_RUNS@@.csv" using 1:2 with linespoints notitle  @@SPINLOCK_SETTINGS@@ axes x1y1, \
"@@RESULTS_PATH@@latencies_posix_@@EXPERIMENT@@_0_@@N_RUNS@@.csv" using 1:2 with linespoints notitle @@POSIX_SETTINGS@@ axes x1y1, \
NaN with linespoints title "POSIX" @@POSIX_SETTINGS@@ axes x1y1,\
NaN with linespoints title "Spinlock" @@SPINLOCK_SETTINGS@@ axes x1y1,\
NaN with linespoints title "MCS" @@MCS_SETTINGS@@ axes x1y1,\
NaN with linespoints title "MCS-TP" @@MCSTP_SETTINGS@@ axes x1y1,\
NaN with linespoints title "Flat Combining" @@FLAT_SETTINGS@@ axes x1y1,\
NaN with linespoints title "CC-Synch" @@CCSYNCH_SETTINGS@@ axes x1y1,\
NaN with linespoints title "DSM-Synch" @@DSMSYNCH_SETTINGS@@ axes x1y1,\
NaN with linespoints title "RCL" @@RCL_SETTINGS@@ axes x1y1
set size 1,0.30
set origin -0.03,0.12
set xtics 10
set yrange [@@Y_TOP_RANGE@@]
set ytics 10
set y2label "Nb. déf. cache @@CACHE_MISS_TYPE@@"
set tmargin 0
unset key
set xtics textcolor rgb "black"
set xlabel "Delai (cycles)"
set xtics ("100" 103, "1 000" 1000, "10 000" 10000, "100 000" 100000, "1000000" 1000000, "10000000" 10000000)
set xtics add ("100" 103)
plot "< paste -d ',' @@RESULTS_PATH@@client_cache_misses_rcl_@@EXPERIMENT@@_0_@@N_RUNS@@.csv @@RESULTS_PATH@@server_cache_misses_rcl_@@EXPERIMENT@@_0_@@N_RUNS@@.csv | awk -F ',' '{print $1,$2+$9};' | tr ' ' ','" using 1:2 with linespoints notitle @@RCL_SETTINGS@@ axes x1y1, \
"@@RESULTS_PATH@@client_cache_misses_dsmsynch_@@EXPERIMENT@@_0_@@N_RUNS@@.csv" using 1:2 with linespoints notitle @@DSMSYNCH_SETTINGS@@ axes x1y1, \
"@@RESULTS_PATH@@client_cache_misses_ccsynch_@@EXPERIMENT@@_0_@@N_RUNS@@.csv" using 1:2 with linespoints notitle @@CCSYNCH_SETTINGS@@ axes x1y1, \
"@@RESULTS_PATH@@client_cache_misses_fc_@@EXPERIMENT@@_0_@@N_RUNS@@.csv" using 1:2 with linespoints notitle @@FLAT_SETTINGS@@ axes x1y1, \
"@@RESULTS_PATH@@client_cache_misses_mcstpl_@@EXPERIMENT@@_0_@@N_RUNS@@.csv" using 1:2 with linespoints notitle @@MCSTP_SETTINGS@@ axes x1y1, \
"@@RESULTS_PATH@@client_cache_misses_mcsl_@@EXPERIMENT@@_0_@@N_RUNS@@.csv" using 1:2 with linespoints notitle @@MCS_SETTINGS@@ axes x1y1, \
"@@RESULTS_PATH@@client_cache_misses_sl_@@EXPERIMENT@@_0_@@N_RUNS@@.csv" using 1:2 with linespoints notitle @@SPINLOCK_SETTINGS@@ axes x1y1, \
"@@RESULTS_PATH@@client_cache_misses_posix_@@EXPERIMENT@@_0_@@N_RUNS@@.csv" using 1:2 with linespoints notitle @@POSIX_SETTINGS@@ axes x1y1
#    EOF
