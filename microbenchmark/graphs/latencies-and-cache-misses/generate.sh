#!/bin/bash
#
# generate.sh
# ===========
# (C) Jean-Pierre Lozi, 2013
#

. ../../../figures/bench.inc.sh

N_RUNS="5"
CACHE_MISS_TYPE="L2"
X_RANGES=("103:2000000" "110:20000000")
MACHINES=("amd48b" "niagara2")
EXPERIMENTS=("1" "5" "diff")
Y_TOP_RANGES=("1:500" "1:500" "1:500"
              "1:2000" "1:2000" "1:2000")
Y_BOTTOM_RANGES=("500:20000000" "500:20000000"  "500:20000000"
                 "500:20000000" "500:20000000"  "500:20000000")
KEY_LOCATIONS=("2000000,300000000" "2000000,300000000" "2000000,300000000"
               "20000000,300000000" "20000000,300000000" "20000000,300000000")

if [[ $# -lt 2 ]]
then
    TAGS=("140122-phd-main" "140217-phd-main")
else
    TAGS=("$1" "$2")
fi


function replace_variable
{
    VARIABLE_NAME=$1
    VARIABLE=${!VARIABLE_NAME}

    cat tmp.plt | sed "s/@@$VARIABLE_NAME@@/$VARIABLE/g"             \
        > tmp-tmp.plt

    mv tmp-tmp.plt tmp.plt
}


for FR in "" "-fr"
do
    for MACHINE_ID in ${!MACHINES[*]}
    do

        MACHINE=${MACHINES[$MACHINE_ID]}
        TAG=${TAGS[$MACHINE_ID]}

        X_RANGE=${X_RANGES[$MACHINE_ID]}

        OUTPUT_PREFIX="output/microbenchmark-latencies-and-cache-misses-"
        RESULTS_PATH="../../all-results/results-$TAG-$MACHINE/"

        for FILE in $(ls $RESULTS_PATH*1_0_5.csv)
        do
            paste -d ',' $FILE ${FILE/1_0_5.csv/5_0_5.csv} \
                | awk -F ',' '{print $1,$(4+5)-$2}' \
                | tr ' ' ',' > ${FILE/1_0_5.csv/diff_0_5.csv}
        done

        RESULTS_PATH=$(echo $RESULTS_PATH | sed 's/\//\\\//g')

        for EXPERIMENT_ID in ${!EXPERIMENTS[*]}
        do

            N_EXPERIMENTS=${#EXPERIMENTS[@]}
            EXPERIMENT=${EXPERIMENTS[$EXPERIMENT_ID]}
            Y_TOP_RANGE=${Y_TOP_RANGES[$(($MACHINE_ID * $N_EXPERIMENTS         \
                                       + $EXPERIMENT_ID))]}
            Y_BOTTOM_RANGE=${Y_BOTTOM_RANGES[$(($MACHINE_ID * $N_EXPERIMENTS   \
                                             + $EXPERIMENT_ID))]}
            KEY_LOCATION=${KEY_LOCATIONS[$(($MACHINE_ID * $N_EXPERIMENTS       \
                                         + $EXPERIMENT_ID))]}

            if [[ $MACHINE == "amd48b" ]]
            then
                CACHE_MISS_TYPE="L2"
            elif [[ $MACHINE == "niagara2" ]]
            then
                CACHE_MISS_TYPE="L1"
            else
                echo "Unsupported machine."
                exit
            fi

            for KEY in "" "-no-key"
            do

                cat base${KEY}${FR}.plt > tmp.plt

                replace_variable N_RUNS
                replace_variable RESULTS_PATH
                replace_variable EXPERIMENT
                replace_variable CACHE_MISS_TYPE
                replace_variable X_RANGE
                replace_variable Y_TOP_RANGE
                replace_variable Y_BOTTOM_RANGE
                replace_variable KEY_LOCATION

                replace_variable POSIX_SETTINGS
                replace_variable SPINLOCK_SETTINGS
                replace_variable MCS_SETTINGS
                replace_variable MCSTP_SETTINGS
                replace_variable FLAT_SETTINGS
                replace_variable CCSYNCH_SETTINGS
                replace_variable DSMSYNCH_SETTINGS
                replace_variable RCL_SETTINGS

                cat tmp.plt | LC_ALL="en_US.UTF-8" LC_NUMERIC="en_US.UTF-8" gnuplot \
                    > ${OUTPUT_PREFIX}${EXPERIMENT}-${MACHINE}${KEY}${FR}.pdf

#				cp tmp.plt presentation/${OUTPUT_PREFIX}${EXPERIMENT}-${MACHINE}${KEY}${FR}.plt

                rm tmp.plt

            done

        done
    done
done

for KEY in "key" "key-wide"
do

    cat ${KEY}.plt > tmp.plt

    replace_variable POSIX_SETTINGS
    replace_variable SPINLOCK_SETTINGS
    replace_variable MCS_SETTINGS
    replace_variable MCSTP_SETTINGS
    replace_variable FLAT_SETTINGS
    replace_variable CCSYNCH_SETTINGS
    replace_variable DSMSYNCH_SETTINGS
    replace_variable RCL_SETTINGS

    cat tmp.plt | gnuplot > output/${KEY}.pdf 2> /dev/null

    rm tmp.plt

done
