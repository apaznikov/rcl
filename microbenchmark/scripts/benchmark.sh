#!/bin/bash
#
# benchmark.sh
# ============
# (C) Jean-Pierre Lozi, 2013
#
#
# Examples:
# ---------
# Latencies:   $ ./benchmark.sh
# DCMs client: $ MEASUREMENT="CLIENT_CACHE_MISSES" ./benchmark.sh
# DCMs server: $ MEASUREMENT="SERVER_CACHE_MISSES" ./benchmark.sh
#
# On Linux, prefix these commands with sudo.
# Don't forget to specify the N_RUNS.
#

. ../../figures/bench.inc.sh
benchs=mcs

SCRIPT_NAME=`basename $0`
EXPERIMENT_NAME=${SCRIPT_NAME%.*}
BENCHMARK=../bin/benchmark

export TOP_PID=$$

if [[ -z $N_RUNS ]];       then N_RUNS=30;                                    fi
if [[ -z $N_SAMPLES ]];    then N_SAMPLES=50;                                 fi
if [[ -z $N_ITERATIONS ]]; then N_ITERATIONS=10000;                           fi
if [[ -z $MIN ]];          then MIN=100;                                      fi
if [[ -z $MAX ]];          then MAX=2000000;                                  fi
if [[ -z $MEASUREMENT ]];  then MEASUREMENT="LATENCIES";                      fi
if [[ -z $ACCESSES ]];     then ACCESSES="1 5";                               fi
if [[ -z $BENCHES ]]
then
    BENCHES="rcl posix spinlock mcs mcstp flat ccsynch dsmsynch"
fi

rm -Rf ../results/$SCRIPT_NAME* >/dev/null 1>&0
mkdir -p ../results

echo "Experiment no." $EXPERIMENT_NAME


# if [[ $(uname) != "SunOS" && "$(id -u)" != "0" ]]
# then
#     echo "Must be root"
#     exit 1
# fi


function measure
{
    STEP=`nawk "BEGIN {printf(\"%.12g\", $MAX ^ (1/($N_SAMPLES)))}";`
    DELAY=$STEP
    C=$1;G=$2;L=$3
    shift 3
    I=1
    while [[ I -le $N_SAMPLES ]]
    do

        DELAY=`echo "$DELAY * $STEP" | bc`

        if [ `nawk "BEGIN {if ($DELAY > $MIN) print 1; else print 0;}"` -eq 1 ]
        then

            INTDELAY=`echo "$DELAY / 1" | bc`

            echo -n $INTDELAY
            echo -n ","
            echo -n "scale=6;(" > ../results/sum
            echo -n > ../results/list

            J=1

            while [[ J -le $N_RUNS ]]
            do
                if [[ $MEASUREMENT = "LATENCIES" ]]
                then

                        $BENCHMARK -m -1 -n $N_ITERATIONS -d $INTDELAY         \
                        -A 1 -c $C -s 1 -u -g $G -l $L $@ 2> /dev/null         \
                        | tr -d ',\n' | tee -a ../results/sum ../results/list  \
                        >> /dev/null

                elif [[ $MEASUREMENT = "CLIENT_CACHE_MISSES" ]]
                then

                    if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
                    then

                        $BENCHMARK -m -1 -e 0x80000002 -I per_node             \
                        -n $N_ITERATIONS -d $INTDELAY -A 1 -c $C -s 1          \
                        -u -g $G -l $L $@ 2> /dev/null | tr -d ',\n'           \
                        | tee -a ../results/sum ../results/list >> /dev/null

                    elif [[ $(hostname) == "niagara2.cs.rochester.edu" ]]
                    then

                        $BENCHMARK -m -1 -e DC_miss -I per_core                \
                        -n $N_ITERATIONS -d $INTDELAY -A 1 -c $C -s 1          \
                        -u -g $G -l $L $@ 2> /dev/null | tr -d ',\n'           \
                        | tee -a ../results/sum ../results/list >> /dev/null

                    else

                        echo "Unsupported machine."
                        exit

                    fi

                elif [[ $MEASUREMENT = "SERVER_CACHE_MISSES" ]]
                then

                    $BENCHMARK -m -1 -n $N_ITERATIONS -d $INTDELAY             \
                        -A 1 -c $C -s 1 -g $G -l $L $@ 2>&1 | grep "\/cs"      \
                        | awk '{print $2}' | tr -d ',\n'                       \
                        | tee -a ../results/sum ../results/list >> /dev/null

                elif [[ $MEASUREMENT = "TIMES_IN_CS" ]]
                then

                    ACCESSES="1"

                    ../../lock-profiler/lock-profiler -b NONE -l NONE_CYCLES   \
                    $BENCHMARK -m -1 -n $N_ITERATIONS -d $INTDELAY             \
                          -A 1 -c $C -s 1 -g $G -l $L $@ 2>&1 > /dev/null      \
                        | grep "Global statistics:" | cut -d " " -f 4          \
                        | tr -d '%\n,' | tee -a ../results/sum ../results/list \
                        >> /dev/null

                elif [[ $MEASUREMENT = "USE_RATES" ]]
                then

                    $BENCHMARK -m -1 -n $N_ITERATIONS -d $INTDELAY             \
                        -A 1 -c $C -s 1 -u -g $G -l $L $@                      \
                        2> tmp_stderr > /dev/null

                    cat tmp_stderr | grep "use rate:" | cut -d " " -f 7        \
                        | tr -d '\n' | tee -a ../results/sum ../results/list   \
                        >> /dev/null

                    rm tmp_stderr

                elif [[ $MEASUREMENT = "GDB" ]]
                then

                    echo
                    echo "Launching gdb..."
                    echo

                    gdb --args $BENCHMARK -m -1 -n $N_ITERATIONS               \
                        -d $INTDELAY -A 1 -c $C -s 1 -u -g $G -l $L $@

                    kill -9 $TOP_PID
                    exit

                else

                    echo "Invalid measurement ($MEASUREMENT)."
                    exit

                fi

                if [[ $J -lt $N_RUNS ]]
                then
                    echo -n '+' >> ../results/sum
                    echo -n ',' >> ../results/list
                fi

                ((J++))

            done

            echo ")/$N_RUNS" >> ../results/sum
            cat ../results/sum | bc | tr -d '\n'
            echo -n ","
            cat ../results/list
            echo

        fi

        ((I++))

    done
}


function apply
{
    I=$1
    N=$3
    F=$5

    if [[ -z "$N_CORES" ]]
    then
        if [[ $(uname) == "SunOS" ]]
        then
            C=`kstat cpu_info | grep "instance" | wc -l | tr -d ' '`
        else
            C=`cat /proc/cpuinfo | grep "processor" | wc -l`
        fi
    else
        C=$N_CORES
    fi

    C=$(($C-1))

    if [[ "$N" == "RCL" ]]
    then
        C=$(($C-1))
    fi

    shift 7
    echo "$N, $I shared variables ($C threads)"
    echo "====================================================================="

    measure $C $I 0 $@ -o -N -x dependent \
        | tee ../results/${EXPERIMENT_NAME}_${F}_${I}_0_${N_RUNS}.csv
}

hugepages_flag="0"
# if [[ $(uname) = "Linux" && $(sysctl vm.nr_hugepages) != "vm.nr_hugepages = 2000" ]]
# then
#     sudo sysctl -w vm.nr_hugepages=2000
#     hugepages_flag="1"
# fi

if [[ $MEASUREMENT = "LATENCIES" ]]
then
    EXPERIMENT_NAME="latencies"
elif [[ $MEASUREMENT = "CLIENT_CACHE_MISSES" ]]
then
    EXPERIMENT_NAME="client_cache_misses"
elif [[ $MEASUREMENT = "SERVER_CACHE_MISSES" ]]
then
    EXPERIMENT_NAME="server_cache_misses"
    BENCHES="rcl"

    cd ../../liblock
    echo "Compiling the liblock with DCM profiling for rcl..."

    make distclean

    if [[ $(uname) != "SunOS" ]]
    then
        make ADDITIONAL_CFLAGS="-DDCM_PROFILING_ON"
    else
        make ADDITIONAL_CFLAGS="-DPRECISE_DCM_PROFILING_OUTSIDE_ON"
    fi

    echo "Done."
    cd -
    trap "echo; echo 'Removing profiling from the liblock...';                 \
cd ../../liblock; make distclean; make; cd -; kill -9 $TOP_PID; exit" SIGINT
elif [[ $MEASUREMENT = "TIMES_IN_CS" ]]
then
    EXPERIMENT_NAME="times_in_cs"
    BENCHES="posix"
elif [[ $MEASUREMENT = "USE_RATES" ]]
then
    EXPERIMENT_NAME="use_rates"
    BENCHES="rcl"
fi

# Interdependent accesses
for I in $ACCESSES
do
    for B in $BENCHES; do
        on_bench $B apply $I
        rm -rf ../results/sum
        rm -rf ../results/list
    done
done

if [[ $MEASUREMENT = "SERVER_CACHE_MISSES" ]]
then
    cd ../../liblock
    echo "Removing profiling from the liblock..."
    make distclean
    make
    echo "Done."
    cd -
fi

# if [[ $(uname) = "Linux" && $hugepages_flag == "1" ]]
# then
#     sudo sysctl -w vm.nr_hugepages=0
# fi

# echo -e "Subject: amd48 ../results\n\nLoop ${LOOPINFO} done." \
#     | /usr/sbin/ssmtp jp.lozi@gmail.com

