#!/bin/bash

if [[ -z "$N_RUNS" ]]; then N_RUNS=5; fi

DIR=$(cd $(dirname "$0"); pwd)
cd $DIR

MAKE=make
make -v >/dev/null 2>&1 || MAKE=gmake


if [[ $(uname) == "SunOS" ]]
then
    N_THREADS=`kstat cpu_info | grep "instance" | wc -l | tr -d ' '`
else
    N_THREADS=`cat /proc/cpuinfo | grep "processor" | wc -l`
fi

N_THREADS=$(($N_THREADS-1))


#for BENCH in "ccsynch" "dsmsynch"
for BENCH in "dsmsynch"
do

#    for G in 1 5
    for G in 5
    do

        for H in {1..1000}
        do

            echo -n "$H,"
            echo -n "scale=6;(" > ../results/sum
            echo -n > ../results/list

            J=1

            while [[ $J -le $N_RUNS ]]
            do

                cd ../../liblock/

                rm -f liblock.so $BENCH.o

                CFLAGS="-DMAX_COMBINER_ITERATIONS=$H" $MAKE liblock.so > /dev/null

                cd - > /dev/null

                cd ../bin

                ./benchmark -m -1 -n 10000 -d 110 -A 1 -c $N_THREADS -s 1 -u -g $G \
                            -l 0 -F $BENCH -o -N -x dependent 2> /dev/null         \
                            | tr -d ',\n' | tee -a ../results/sum ../results/list  \
                            > /dev/null

                if [[ $J -lt $N_RUNS ]]
                then
                    echo -n '+' >> ../results/sum
                    echo -n ',' >> ../results/list
                fi

                ((J++))

                cd - > /dev/null

            done

            echo ")/$N_RUNS" >> ../results/sum
            cat ../results/sum | bc | tr -d '\n'
            echo -n ","
            cat ../results/list
            echo

        done | tee ../results/bench_mci_${BENCH}_${G}.csv

        rm ../results/sum
        rm ../results/list

    done

done

