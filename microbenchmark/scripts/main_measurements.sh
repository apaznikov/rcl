#!/bin/bash
#
# all_measurements.sh
# ====================
# (C) Jean-Pierre Lozi, 2013
#

if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
then
    MAX=2000000
elif [[ $(hostname) == "niagara2.cs.rochester.edu" ]]
then
    MAX=20000000
else
    # echo "Unsupported machine."
    # exit
    MAX=2000000
fi

N_RUNS=5 MAX=$MAX ./benchmark.sh
N_RUNS=5 MAX=$MAX MEASUREMENT="CLIENT_CACHE_MISSES" ./benchmark.sh
N_RUNS=5 MAX=$MAX MEASUREMENT="SERVER_CACHE_MISSES" ./benchmark.sh
N_RUNS=5 MAX=$MAX MEASUREMENT="TIMES_IN_CS" ./benchmark.sh

