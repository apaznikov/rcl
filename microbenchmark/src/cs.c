/* ########################################################################## */ 
/* cs.c                                                                       */ 
/* (C) Jean-Pierre Lozi, 2010-2011                                            */ 
/* (C) Gaël Thomas, 2010-2011                                                 */ 
/* -------------------------------------------------------------------------- */ 
/* ########################################################################## */ 
#include "cs.h"


/* Fast random number generator */
extern inline int _rand(int next);

/* This function accesses one variable per cache line. */
extern inline void access_variables(volatile uint64_t *memory_area,
                                    int first_variable_number,
                                    int number_of_variables,
                                    int access_order,
                                    int *permutations_array);

