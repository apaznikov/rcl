/* ########################################################################## */
/* cs.h                                                                       */
/* (C) Jean-Pierre Lozi, 2010-2011                                            */
/* (C) Gaël Thomas, 2010-2011                                                 */
/* -------------------------------------------------------------------------- */
/* ########################################################################## */
#ifndef __CRITICAL_SECTION_H
#define __CRITICAL_SECTION_H

#include <papi.h>
#include <stdint.h>
#include <stdlib.h>
#include "globals.h"


/* Fast random number generator */
__attribute__((always_inline)) inline int _rand(int next)
{
    next = next * 1103515245 + 12345;

    return (unsigned int)(next/65536) % 32768;
}

/* This function accesses one variable per cache line. */
__attribute__((always_inline)) inline void access_variables(volatile uint64_t *memory_area,
                             int first_variable_number,
                             int number_of_variables,
                             int access_order,
                             int *permutations_array)

{
    int k, random_number;

    int i = 0;

    k = 0;

//if (number_of_variables)
//{
//before:
// slow
/*
        (*((volatile uint64_t *)
                           ((uintptr_t)memory_area + (i % 2) * sizeof(uint64_t)
                            + (k + first_variable_number) *
                           2 * sizeof(cache_line_t))))++;
*/
// fast
/*
        (*((volatile uint64_t *)
                           ((uintptr_t)memory_area
                            + (first_variable_number) *
                           2 * sizeof(cache_line_t))))++;
*/
// ??
//puerh:
//PAUSE();
//        (*((volatile uint64_t *)
//                           ((uintptr_t)memory_area
//                            + (first_variable_number) *
//                           2 * sizeof(cache_line_t))))++;
//PAUSE();
//__sync_synchronize();
//        PAUSE();
//        for (i = 0; i < 0; i++)
//            ;
//after:
//}
//    return;

    switch (access_order)
    {
        case AO_SEQUENTIAL:
            {
                for (k = 0; k < number_of_variables; k++) {
                    (*((volatile uint64_t *)
                       ((uintptr_t)memory_area + (k + first_variable_number) *
                       2 * sizeof(cache_line_t))))++;
                }
            }
            break;

        case AO_DEPENDENT:
            {
                for (k = 0; k < number_of_variables; k++) {
                    i = (*((volatile uint64_t *)
                           ((uintptr_t)memory_area + (i % 2) * sizeof(uint64_t)
                            + (k + first_variable_number) *
                           2 * sizeof(cache_line_t))))++;
                }
            }
            break;

        case AO_RANDOM:
            {
                for (k = 0; k < number_of_variables; k++)
                    (*((volatile uint64_t *)
                       ((uintptr_t)memory_area +
                        (rand() % number_of_variables + first_variable_number) *
                        2 * sizeof(cache_line_t))))++;
            }
            break;

        case AO_CUSTOM_RANDOM:
            {
                random_number = (int)get_cycles();

                for (k = 0; k < number_of_variables; k++) {
                    random_number = _rand(random_number);

                    (*((volatile uint64_t *)
                       ((uintptr_t)memory_area +
                        (random_number % number_of_variables
                         + first_variable_number) *
                        2 * sizeof(cache_line_t))))++;
                }
            }
            break;

        case AO_PERMUTATIONS:
            {
                for (k = 0; k < number_of_variables; k++)
                    (*((volatile uint64_t *)
                       ((uintptr_t)memory_area +
                        (permutations_array[k] + first_variable_number) *
                        2 * sizeof(cache_line_t))))++;
            }
            break;
    }
}

#endif

