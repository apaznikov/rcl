/* ########################################################################## */
/* benchmark.c                                                                */
/* (C) Jean-Pierre Lozi, 2010-2011                                            */
/* (C) Gaël Thomas, 2010-2011                                                 */
/* -------------------------------------------------------------------------- */
/* This program should be compiled with the -O0 compiler flag.                */
/* ########################################################################## */


/* ########################################################################## */
/* Headers                                                                    */
/* ########################################################################## */
#include "benchmark.h"

#include <errno.h>
#include <math.h>
#include <getopt.h>
#include <papi.h>
#include <pthread.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#ifdef __linux__
#include <numa.h>
#endif
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/mman.h>

#include "liblock-fatal.h"
#include "liblock.h"

#include "client.h"
#include "cs.h"
#include "globals.h"
#include "liblock_cs.h"
#include "server.h"
#include "util.h"


/* ########################################################################## */
/* Command-line options                                                       */
/* ########################################################################## */
static struct option long_options[] =
{
    {"cst_null_rpcs",                            no_argument,       0, 'R'},
    {"cst_lock_acquisitions",                    no_argument,       0, 'L'},
    {"cst_lock_acquisitions_mcs",                no_argument,       0, 'M'},
    {"cst_liblock",                              required_argument, 0, 'F'},
    {"em_multiple_runs_averaged",                required_argument, 0, 'A'},
    {"em_single_runs_sampled",                   required_argument, 0, 'S'},
    {"em_single_run_ordered",                    no_argument,       0, 'O'},
    {"server_core",                              required_argument, 0, 's'},
    {"n_hw_threads",                             required_argument, 0, 'k'},
    {"n_clients",                                required_argument, 0, 'c'},
    {"n_iterations_per_client",                  required_argument, 0, 'n'},
    {"delay",                                    required_argument, 0, 'd'},
    {"read_only_wait",                           no_argument,       0, 'r'},
    {"n_context_variables",                      required_argument, 0, 'l'},
    {"n_shared_variables",                       required_argument, 0, 'g'},
    {"access_order",                             required_argument, 0, 'x'},
    {"use_multiple_locks",                       no_argument,       0, 'o'},
    {"numa_aware",                               no_argument,       0, 'N'},
    {"skip_first_cs",                            no_argument,       0, 'w'},
    {"mm_count_event",                           required_argument, 0, 'e'},
    {"mm_failed_attempts",                       no_argument,       0, 'f'},
    {"mt_lock_acquisitions",                     no_argument,       0, 'a'},
    {"mt_critical_sections",                     no_argument,       0, 'u'},
    {"ml_clients",                               no_argument,       0, 'm'},
    {"average_client_results",                   no_argument,       0, '1'},
    {"mu_cycles_per_iteration",                  no_argument,       0, 'y'},
    {"mu_total_cycles_max",                      no_argument,       0, 't'},
    {"compute_standard_deviation_and_variance",  no_argument,       0, 'p'},
    {"end_output_without_a_newline",             no_argument,       0, 'i'},
    {"monitoring_location",                      required_argument, 0, 'I'},
    {"verbose",                                  no_argument,       0, 'v'},
    {"help",                                     no_argument,       0, 'h'},
    {0,                                          0,                 0, 0}
};


/* ########################################################################## */
/* Functions                                                                  */
/* ########################################################################## */
/* Main function */
int main(int argc, char **argv)
{
    int i, j;
    int command;
    int result;
    double *results;
    double *average, *variance;
    /* FIXME: use global variables instead */
    int number_of_samples = 0;

    /* We use both a server and number_of_clients clients. The clients call
       remote procedures from the server. */
    //    pthread_t server;
    pthread_t *clients;

    /* This array contains the argument blocks passed to the clients. */
    thread_arguments_block_t *svs_memory_area = NULL,
                             *thread_argument_blocks = NULL,
                             *context_variables_global_memory_area = NULL,
                             *shared_variables_memory_area = NULL;

    /* We initialize the random number generator. */
    srand(time(NULL));

    /* We get the clock speed and the core ordering. */
    get_cpu_info();

#ifdef NO_PAPI
    /* We get the base time for gethrtime(). */
    __base_gethrtime = gethrtime();
#endif

    /*
       Command-line arguments

       Critical sections
       =================

       -R
       Perform null RPCs. This is the default behavior.

       -L
       Perform lock acquisitions instead of null RPCs. In this mode, there is
       no server.

       If both -R and -L are specified, the last one wins.

       -M
       Perform lock acquisitions using the MCS lock.

       Execution mode
       ==============

       -A number_of_runs
       Return the average number of cycles, with variance and standard
       deviation, over the given number of runs. This is the default mode.

       -S number_of_iterations_per_sample
       Return sampled results over a single run, using the given number of
       iterations per sample.

       -O
       In this mode, each client enters its critical section only once, and the
       order in which the clients were serviced is returned. Only one run is
       performed and the results are not averaged.

       If several execution modes are specified, the last one wins.

       Execution settings
       ==================

       -s core
       Core on which the server thread runs if the critical section consists of
       performing null RPCs, or core of the first client if the critical section
       consists of lock acquisitions.

       -c number_of_clients
       Number of clients.

       -n number_of_iterations_per_client
       Number of RPCs/lock acquisitions per client.

       -d delay
       Number of cycles wasted (busy waiting) by clients after a RPC is
       serviced. If 0, no cycles are wasted (apart from the execution time of a
       comparison), otherwise the accuracy of the delay is within 50~150 cycles.

       -r
       Read-only wait mode : wait using comparisons, and only perform a CAS
       after a successful comparison (test-and-test-and-set).

       -l number_of_context_variables
       Number of context (local) variables.

       -g number_of_shared_variables
       Number of shared (global) variables.

       -x access_type
       Change the access type
       - access_type = dependent
       Dependent cache line accesses.
       - access_type = custom_random
       Randomized cache line accesses (using glibc's rand()).
       - access_type = random
       Randomized cache line accesses (custom rand() fnction).
       - access_type = permutations
       Randomized cache line accesses with permutations.

       -w
       Don't count the result for the first critical section.

       Measurements
       ============

       -e event_type
       Count events of type event_type instead of measuring the elapsed time.
       Event_type is the type of a PAPI event.

       -a
       Measure latencies instead of elapsed time.

       -u
       Measure full latencies instead of elapsed time.

       -f
       Count number of failed attempts to get the global lock.

       -m
       Count cycles (or events) on the clients instead of the server. This is
       always the case if -L is on.

       -1
       Display a single result: the average of the results across clients.

       -y
       Returns results in cycles per iteration instead of the number of
       iterations per second.

       -t
       Return results in total cycles instead of the number of iterations per
       second. Only the maximum value is returned.

       -I
       Monitor one thread per core or per node? per_core and per_node are the
       two possible options.

       Output
       ======

       -p
       Return the standard deviation and variance if applicable.

       -i
       End the results with a newline. This is left as an option, to allow for
       more flexibility with the CSV results.
    */

    opterr = 0;

    /* Default values */
    g_critical_sections_type = CST_NULL_RPCS;
    g_use_mcs = 0;

    g_execution_mode = EM_MULTIPLE_RUNS_AVERAGED;
    g_number_of_runs = DEFAULT_NUMBER_OF_RUNS;
    g_number_of_iterations_per_sample = DEFAULT_NUMBER_OF_ITERATIONS_PER_SAMPLE;

    g_server_started = 1;

    g_server_core = DEFAULT_SERVER_CORE;
    g_number_of_clients = -1;
    g_number_of_iterations_per_client = DEFAULT_NUMBER_OF_ITERATIONS_PER_CLIENT;
    g_delay = 0;
    g_read_only_wait = 0;

    g_number_of_context_variables = DEFAULT_NUMBER_OF_CONTEXT_VARIABLES;
    g_number_of_shared_variables = DEFAULT_NUMBER_OF_SHARED_VARIABLES;
    g_access_order = AO_SEQUENTIAL;
    g_use_multiple_locks = 0;
    g_numa_aware = 0;
    g_skip_first_cs = 0;

    g_measurement_metric = MM_NUMBER_OF_CYCLES;
    g_measurement_type = MT_GLOBAL;
    g_measurement_location = ML_SERVER;
    g_average_client_results = 0;
    g_measurement_unit = MU_THROUGHPUT;
    g_monitoring_location = ML_PER_NODE;

    g_compute_standard_deviation_and_variance = 0;
    g_end_output_with_a_newline = 1;
    g_verbose = 0;

    /* FIXME: use long commands */
    while ((command =
                getopt_long(argc, argv,
                            "RLMF:A:S:Os:c:n:d:rl:g:x:oNwtTe:m1yaufpiI:vh",
                            long_options, NULL)) != -1)
    {
        switch (command)
        {
            /*
               Critical sections
             */
            case 'R':
                /* Actually useless (default value). */
                g_critical_sections_type = CST_NULL_RPCS;
                break;
            case 'L' :
                g_critical_sections_type = CST_LOCK_ACQUISITIONS;
                break;
            case 'M' :
                g_critical_sections_type = CST_LOCK_ACQUISITIONS;
                g_use_mcs = 1;
                break;
            case 'F' :
                g_critical_sections_type = CST_LIBLOCK;
                g_server_started = 0;
                liblock_start_server_threads_by_hand = 1;
                liblock_servers_always_up = 0;
                liblock_rcl_no_local_cas = 1;
                g_liblock_name = optarg;
                break;

            /*
              Execution mode
            */
            case 'A':
                g_execution_mode = EM_MULTIPLE_RUNS_AVERAGED;
                g_number_of_runs = atoi(optarg);
                break;

            case 'S':
                g_execution_mode = EM_SINGLE_RUN_SAMPLED;
                g_number_of_iterations_per_sample = atoi(optarg);
                break;

            case 'O':
                g_execution_mode = EM_SINGLE_RUN_ORDERED;
                break;

            /*
              Execution settings
            */
            case 's':
                g_server_core = atoi(optarg);
                break;

            case 'k':
                g_number_of_hw_threads = atoi(optarg);
                break;

            case 'c':
                g_number_of_clients = atoi(optarg);
                break;

            case 'n':
                g_number_of_iterations_per_client = atoi(optarg);
                break;

            case 'd':
                g_delay = atoi(optarg);
                break;

            case 'r':
                g_read_only_wait = 1;
                break;

            case 'l':
                g_number_of_context_variables = atoi(optarg);
                break;

            case 'g':
                g_number_of_shared_variables = atoi(optarg);
                break;

            case 'x':
                if (!strcmp(optarg, "random"))
                    g_access_order = AO_RANDOM;
                else if (!strcmp(optarg, "custom_random"))
                    g_access_order = AO_CUSTOM_RANDOM;
                else if (!strcmp(optarg, "permutations"))
                    g_access_order = AO_PERMUTATIONS;
                else if (!strcmp(optarg, "dependent"))
                    g_access_order = AO_DEPENDENT;
                else fatal ("invalid access order");
                break;

            case 'o':
                g_use_multiple_locks = 1;
                break;

            case 'N':
                g_numa_aware = 1;
                break;

            case 'w':
                g_skip_first_cs = 1;
                break;

            /*
              Measurements
            */
            case 'e':
                g_measurement_metric = MM_NUMBER_OF_EVENTS;
                g_monitored_event_id = strtol(optarg, NULL, 16);
                g_monitored_event_id_str = strdup(optarg);
                break;

            case 'f':
                g_measurement_type = MM_FAILED_ATTEMPTS;
                break;

            case 'a':
                g_measurement_type = MT_LOCK_ACQUISITIONS;
                break;

            case 'u':
                g_measurement_type = MT_CRITICAL_SECTIONS;
                break;

            case 'm':
                g_measurement_location = ML_CLIENTS;
                break;

            case '1':
                g_average_client_results = 1;
                break;

            case 'y':
                g_measurement_unit = MU_CYCLES_PER_ITERATION;
                break;

            case 't':
                g_measurement_unit = MU_TOTAL_CYCLES_MAX;
                break;

            /*
              Output
            */
            case 'p':
                g_compute_standard_deviation_and_variance = 1;
                break;

            case 'i':
                g_end_output_with_a_newline = 0;
                break;

            case 'I':
                if (!strcmp(optarg, "per_node"))
                    g_monitoring_location = ML_PER_NODE;
                else if (!strcmp(optarg, "per_core"))
                    g_monitoring_location = ML_PER_CORE;
                else fatal("invalid monitoring location");
                break;

            case 'v':
                g_verbose = 1;
                break;

            /*
              Other
            */
            case 'h': wrong_parameters_error(argv[0]);
            default : fatal("getopt");
        }
    }

    /* Default values for the number of clients */
    if (g_number_of_clients <= 0)
        g_number_of_clients = g_number_of_hw_threads - 1;

    if (g_critical_sections_type == CST_LOCK_ACQUISITIONS)
    {
        /* Lock acquisitions are only compatible with the MULTIPLE_RUNS_AVERAGED
           execution mode. */
        if (g_execution_mode != EM_MULTIPLE_RUNS_AVERAGED)
            fatal("configuration not supported");

        /* In this mode, measurements take place on the clients. */
        g_measurement_location = ML_CLIENTS;
    }

    if (g_execution_mode == EM_SINGLE_RUN_SAMPLED)
    {
        /* For now, the SINGLE_RUN_SAMPLED execution mode in incompatible with
           blocking locks. */
#ifdef USE_BLOCKING_LOCKS
        fatal("configuration not supported");
#endif

        /* For now, in the SINGLE_RUN_SAMPLED mode, events can't be monitored.
           Also, all measurements must take place on the server. */
        if (g_measurement_metric != MM_NUMBER_OF_CYCLES
                && g_measurement_location != ML_SERVER)
            fatal("configuration not supported");

        /* Single run */
        g_number_of_runs = 1;

        if (g_number_of_iterations_per_sample >
                g_number_of_iterations_per_client * g_number_of_clients)
            fatal("too many iterations per sample");
    }

    /* In the SINGLE_RUN_ORDERED execution mode... */
    if (g_execution_mode == EM_SINGLE_RUN_ORDERED)
    {
        /* ...there's just one iteration per client... */
        g_number_of_iterations_per_client = 1;

        /* ...and a single run. */
        g_number_of_runs = 1;
    }

/*
    if (g_measurement_unit == MU_THROUGHPUT &&
            g_measurement_metric != MM_NUMBER_OF_CYCLES &&
            g_measurement_type != MT_GLOBAL)
    {
        fatal("throughput can only be measured for global number of cycles");
    }
*/

#ifdef __linux__
    /* We need the maximum priority (for performance measurements). */
    if (getuid() == 0 && setpriority(PRIO_PROCESS, 0, -20) < 0)
        fatal("setpriority");
#endif

    generate_permutations_array(&g_global_permutations_array,
            g_number_of_shared_variables);

    generate_permutations_array(&g_local_permutations_array,
            g_number_of_context_variables);

    liblock_bind_thread(pthread_self(),
                        get_core(g_server_core),
                        g_liblock_name);
    liblock_define_hw_thread(get_core(g_server_core));

    if (g_critical_sections_type == CST_LIBLOCK) {
        liblock_lock_init(g_liblock_name,
                          self.running_hw_thread,
                          &g_liblock_lock, 0);
    }

    // printf("binding main to %d\n",
    //        g_physical_to_virtual_hw_thread_id[g_server_core]);

    /* ====================================================================== */
    /* [v] Memory allocations                                                 */
    /* ====================================================================== */
    /* The following arrays' sizes depend on the parameters, we need to allocate
       them dynamically. */
    clients                = alloc(g_number_of_clients * sizeof(pthread_t));
    thread_argument_blocks = alloc((g_number_of_clients + 1) *
                                sizeof(thread_arguments_block_t));
    g_ready                = alloc((g_number_of_clients + 1) * sizeof(int));
    g_iteration_result     = alloc((g_number_of_clients + 1) * sizeof(double));
    results                = alloc((g_number_of_clients + 1) *
                                (g_number_of_runs * sizeof(double)));
    average                = alloc((g_number_of_clients + 1) * sizeof(double));
    variance               = alloc((g_number_of_clients + 1) * sizeof(double));

    /* If we need to return the number in which RPCs are serviced... */
    if (g_execution_mode == EM_SINGLE_RUN_ORDERED)
    {
        /* ...we allocate the 'order' array dynamically. */
        g_order = alloc(g_number_of_clients * sizeof(double));

        // latencies = alloc(number_of_clients * sizeof(double));
    }

    /* FIXME: float? */
    g_latencies = alloc(g_number_of_clients * sizeof(double));
    for (i = 0 ; i < g_number_of_clients ; i++)
        g_latencies[i] = -1;

    /* We always allocate the sampling-related arrays. */
    g_rpc_done_addresses = alloc((g_number_of_clients + 1) * sizeof(void *));
    number_of_samples = g_number_of_iterations_per_client * g_number_of_clients
        / g_number_of_iterations_per_sample;

    /* FIXME: float? */
    g_multiple_samples_results = alloc(number_of_samples * sizeof(double));


    g_multiple_samples_rpc_done_addrs =
        alloc(number_of_samples * sizeof(void *));

    if (g_critical_sections_type != CST_NULL_RPCS || !g_numa_aware)
    {
        /* Memory area containing the synchronization variables. Each synchroni-
           zation variable is allocated on its own pair of cache lines. */
        result = MEMALIGN(&svs_memory_area,
                          2 * CACHE_LINE_SIZE,
                          (g_number_of_clients + 1) *
                          2 * sizeof(cache_line_t));

        if (result < 0 || svs_memory_area == NULL)
            fatal("memalign");
    }
    else
    {
#ifdef __linux__
        svs_memory_area = numa_alloc_local((g_number_of_clients + 1) * 2 *
                sizeof(cache_line_t));

        if (svs_memory_area == NULL)
            fatal("numa_alloc_local");
#elif defined(__solaris__)
        svs_memory_area = mmap(0, (g_number_of_clients + 1) * 2 *
                               sizeof(cache_line_t), PROT_READ | PROT_WRITE,
                               MAP_ANONYMOUS | MAP_PRIVATE, 0, 0);

        if (svs_memory_area == NULL)
            fatal("numa_alloc_local");
#endif

        // fatal("mmap");
    }

    memset(svs_memory_area, 0,
            (g_number_of_clients + 1) *
            2 * sizeof(cache_line_t));


    /* FIXME: allocate on clients? might be more realistic */
    result = MEMALIGN(&context_variables_global_memory_area,
            2 * CACHE_LINE_SIZE,
            (g_number_of_clients + 1) *
            (g_number_of_context_variables + 1) *
            2 * sizeof(cache_line_t));

    if (result < 0 || context_variables_global_memory_area == NULL)
        fatal("memalign");

    memset(context_variables_global_memory_area, 0,
            (g_number_of_clients + 1) *
            (g_number_of_context_variables + 1) *
            2 * sizeof(cache_line_t));


    if (g_critical_sections_type != CST_NULL_RPCS || !g_numa_aware)
    {
        result = MEMALIGN(&shared_variables_memory_area,
                2 * CACHE_LINE_SIZE/*128 1024*/,
                (g_number_of_shared_variables) *
                2 * sizeof(cache_line_t));

        if (result < 0 || shared_variables_memory_area == NULL)
            fatal("memalign: %s", strerror(errno));
    }
    else
    {
#ifdef __linux__
        shared_variables_memory_area =
            numa_alloc_local((g_number_of_shared_variables) *
                    2 * sizeof(cache_line_t));
#elif defined(__solaris__)
        shared_variables_memory_area = mmap(0, g_number_of_shared_variables *
                                            2 * sizeof(cache_line_t),
                                            PROT_READ | PROT_WRITE,
                                            MAP_ANONYMOUS | MAP_PRIVATE,
                                            0, 0);
#endif

        if (svs_memory_area == NULL)
            fatal("numa_alloc_local");
        // fatal("mmap");
    }

    g_shared_variables_memory_area =
        (volatile uint64_t *)shared_variables_memory_area;
    memset(shared_variables_memory_area, 0,
            (g_number_of_shared_variables) *
            2 * sizeof(cache_line_t));


    /* MCS Lock */
    result = MEMALIGN(&g_mcs_m_ptr,
            2 * CACHE_LINE_SIZE,
            2 * CACHE_LINE_SIZE);

		*g_mcs_m_ptr = 0;

    if (result < 0 || g_mcs_m_ptr == NULL)
        fatal("memalign");

    /* ====================================================================== */
    /* [^] Memory allocations                                                 */
    /* ====================================================================== */

#ifndef __sun__
#ifndef NO_PAPI
    /* We initialize PAPI. */
    if (PAPI_is_initialized() == PAPI_NOT_INITED &&
        PAPI_library_init(PAPI_VER_CURRENT) != PAPI_VER_CURRENT)
		fatal("PAPI_library_init");

    PAPI_set_granularity(PAPI_GRN_THR);
#endif
#else
    if ((rcw_cpc = cpc_open(CPC_VER_CURRENT)) == NULL)
        fatal("libcpc version mismatch");

    struct sigaction act;

    /* We register the overflow handler */
    act.sa_sigaction = emt_handler;
    bzero(&act.sa_mask, sizeof(act.sa_mask));
    act.sa_flags = SA_RESTART | SA_SIGINFO;

    if (sigaction(SIGEMT, &act, NULL) == -1)
        fatal("sigaction() failed");
#endif

    /* If we use blocking locks, then some mutexes and conditions should be
       initialized. */
#ifdef USE_BLOCKING_LOCKS
    pthread_mutex_init(&mutex_rpc_done_addr_not_null, NULL);
    pthread_mutex_init(&mutex_rpc_done_positive, NULL);
    pthread_mutex_init(&mutex_rpc_done_addr_null, NULL);

    if (pthread_cond_init(&cond_rpc_done_addr_not_null, NULL) < 0
            || pthread_cond_init(&cond_rpc_done_positive, NULL) < 0
            || pthread_cond_init(&cond_rpc_done_addr_null, NULL) < 0)
        fatal("pthread_cond_init");
#endif

    if (g_critical_sections_type == CST_NULL_RPCS)
        pthread_barrier_init(&barrier, NULL, 48);
    else
        pthread_barrier_init(&barrier, NULL, 47);

    /* We iterate to average the results. */
    for (i = 0; i < g_number_of_runs; i++)
    {
        /* No rpc started for now. */
        *((uint64_t **)svs_memory_area) = 0;

        /* None of the clients are ready. */
        for (j = 0; j <= g_number_of_clients; j++)
            g_ready[j] = 0;

        /* Initialisations related to sampling. */
        if (g_execution_mode == EM_SINGLE_RUN_SAMPLED)
        {
            for (j = 0; j < number_of_samples; j++)
                g_multiple_samples_results[j] = 0;

            for (j = 0; j < number_of_samples; j++)
                g_multiple_samples_rpc_done_addrs[j] = 0;
        }


        /* We create the client threads. */
        for (j = 0; j < g_number_of_clients; j++)
        {
            thread_argument_blocks[j + 1].id = j;
            thread_argument_blocks[j + 1].client_core = (g_server_core + 1 +
                    (j % (g_number_of_hw_threads - 1))) % g_number_of_hw_threads;

            thread_argument_blocks[j + 1].null_rpc_global_sv =
                (volatile uint64_t * volatile *)(&svs_memory_area[0]);
            if (g_use_multiple_locks)
            {
                thread_argument_blocks[j + 1].null_rpc_local_sv =
                    (volatile uint64_t *)
                    ((uintptr_t)svs_memory_area +
                     (j+1) * 2 * sizeof(cache_line_t));
            }
            else
            {
                thread_argument_blocks[j + 1].null_rpc_local_sv =
                    (volatile uint64_t *)
                    ((uintptr_t)context_variables_global_memory_area +
                     j * (g_number_of_context_variables + 1) *
                     2 * sizeof(cache_line_t));
            }

            thread_argument_blocks[j + 1].lock_acquisitions_global_sv =
                (volatile uint64_t *)(&svs_memory_area[0]);

            thread_argument_blocks[j + 1].context_variables_global_memory_area =
                (volatile uint64_t *)
                ((uintptr_t)context_variables_global_memory_area);
            thread_argument_blocks[j + 1].shared_variables_memory_area =
                (volatile uint64_t *)(shared_variables_memory_area);

            /* We send i directly instead of sending a pointer to avoid having
               to create a variable per thread containing the value. */
            liblock_thread_create_and_bind(
                get_core(thread_argument_blocks[j+1].client_core), 0,
                &clients[j], NULL, client_main, &thread_argument_blocks[j + 1]);
        }

        /* If we perform null RPCs in the critical sections... */
        if (g_critical_sections_type == CST_NULL_RPCS)
        {
            thread_argument_blocks[0].id = 0;
            thread_argument_blocks[0].null_rpc_global_sv =
                (volatile uint64_t * volatile *)(&svs_memory_area[0]);
            thread_argument_blocks[0].null_rpc_local_sv = NULL;

            /* FIXME: global variable? */
            thread_argument_blocks[0].context_variables_global_memory_area =
                (volatile uint64_t *)(context_variables_global_memory_area);
            thread_argument_blocks[0].shared_variables_memory_area =
                (volatile uint64_t *)(shared_variables_memory_area);

            /* ...we create a server thread. */
            // liblock_thread_create(&server, NULL, server_main,
            //                       &thread_argument_blocks[0])

            server_main((void *)&thread_argument_blocks[0]);
        }

        /* Launch the liblock threads, if needed. */
        if (g_critical_sections_type == CST_LIBLOCK)
            liblock_lookup(g_liblock_name)->run(server_started);

        /* We wait for the clients to finish their work. */
        for (j = 0; j < g_number_of_clients; j++)
        {
            if (pthread_join(clients[j], NULL) < 0)
                fatal("pthread_join");
        }

        /* If there is a server thread... */
        //        if (g_critical_sections_type == CST_NULL_RPCS)
        //        {
        /* ...we wait for it to finish its work. */
        //            if (pthread_join(server, NULL) < 0)
        //                fatal("pthread_join");
        //        }

        /* We save the results. */
        for (j = 0 ; j <= g_number_of_clients; j++)
        {
/*
            // This can happen since not all threads are monitored.

            if (g_iteration_result[j] < 0) {
                fprintf(stderr, "Error: negative iteration result.\n");
                exit(-1);
            }
*/
            /* FIXME: failed_attempts */
            if (g_measurement_unit == MU_THROUGHPUT &&
                g_measurement_type == MT_GLOBAL &&
                g_measurement_metric == MM_NUMBER_OF_CYCLES)
                g_iteration_result[j] = TO_THROUGHPUT(g_iteration_result[j]);


            results[j * g_number_of_runs + i] = g_iteration_result[j];
        }

		if (g_measurement_location == ML_SERVER &&
		    g_measurement_metric == MM_NUMBER_OF_EVENTS) {
			results[0] = (double)g_event_count;
			results[0] /= (g_number_of_iterations_per_client * g_number_of_clients);
		}
	}

    int k;

    if (g_verbose)
    {
        printf("Global: ");

        /* FIXME: access_variables */
        for (k = 0; k < g_number_of_shared_variables; k++)
            printf("%llu,", (unsigned long long)(*((volatile uint64_t *)
                            ((uintptr_t)shared_variables_memory_area +
                             k * 2 * sizeof(cache_line_t)))));
        printf("\n");

        printf("Local: ");

        for (j = 0; j < g_number_of_clients + 1; j++)
        {
            for (k = 0; k < g_number_of_context_variables; k++)
                printf("%llu,",
                       (unsigned long long)(*((volatile uint64_t *)
                       ((uintptr_t)context_variables_global_memory_area +
                        j * (g_number_of_context_variables + 1) *
                        2 * sizeof(cache_line_t) +
                        (k + 1) * 2 * sizeof(cache_line_t)))));

            printf("\n");
        }

        printf("\n");
    }

#if 0
    for (k = 0; k < g_number_of_shared_variables; k++)
        printf("_%lld_", (unsigned long long)(*((volatile uint64_t *)
                        ((uintptr_t)shared_variables_memory_area +
                         k * 2 * sizeof(cache_line_t)))));

    for (k = 0; k < g_number_of_context_variables; k++)
        printf("_%lld_", (unsigned long long)(*((volatile uint64_t *)
                        ((uintptr_t)context_variables_global_memory_area +
                         (k + 1) * 2 * sizeof(cache_line_t)))));
#endif

    /* In the ordered mode, we print the order in which the RPCs were
       serviced. */
    if (g_execution_mode == EM_SINGLE_RUN_ORDERED)
    {
        for (i = 0; i < g_number_of_clients; i++)
        {
            j = 0;

            while (g_order[j] != i)
                j++;

            printf("%d", j);
        }

        printf(",");
    }

    /* If sampling is on, we print the result of each sample followed by the ID
       of the last core whose RPC has been serviced. */
    if (g_execution_mode == EM_SINGLE_RUN_SAMPLED)
    {
        for (i = 0; i < number_of_samples; i++)
        {
            for (j = 0; j < g_number_of_clients + 1; j++)
            {
                if (g_multiple_samples_rpc_done_addrs[i] ==
                        g_rpc_done_addresses[j])
                    break;
            }

            /* We convert the result to the right unit if needed. */
            if (g_measurement_unit == MU_THROUGHPUT)
                g_multiple_samples_results[i] =
                    TO_THROUGHPUT(g_multiple_samples_results[i]);

            /* We return the result. */
            printf("%f,%d\n", g_multiple_samples_results[i], j);
        }
    }

    /* If we're using MU_TOTAL_CYCLES_MAX, we have to find out the maximum
     * of all measurements. */
    if (g_measurement_unit == MU_TOTAL_CYCLES_MAX)
    {
        for (i = 0; i < g_number_of_runs; i++)
        {
            double max = 0;

            for (j = 1; j <= g_number_of_clients; j++)
            {
                if (max < results[j * g_number_of_runs + i])
                    max = results[j * g_number_of_runs + i];
            }

            /* FIXME: TOTAL_CYCLES_MAX is a bad name. Not a unit either.
               Terrible hack. */
            results[i] = TO_THROUGHPUT(max /
                    (g_number_of_iterations_per_client *
                     g_number_of_clients));
        }
    }
    else if (g_measurement_location == ML_CLIENTS && g_average_client_results)
    {
        for (i = 0; i < g_number_of_runs; i++)
        {
            double result = 0;
            int number_of_valid_results = 0;

            if (g_measurement_metric == MM_NUMBER_OF_EVENTS)
            {
                for (j = 1; j <= g_number_of_clients; j++)
                {
                    if (results[j * g_number_of_runs + i] >= 0)
                    {
                        result += results[j * g_number_of_runs + i];
                        number_of_valid_results++;
                    }
                }

                results[i] = result / number_of_valid_results;
            }
            else
            {
                for (j = 1; j <= g_number_of_clients; j++)
                {
                    result += results[j * g_number_of_runs + i];
                }

                results[i] = result / g_number_of_clients;
            }
        }
    }

    /* We compute the average and the variance (if needed). */
    for (j = 0; j <= g_number_of_clients; j++)
    {
        /* Initialization */
        for (i = 0; i < g_number_of_runs; i++)
        {
            average[j] = 0;
            variance[j] = 0;
        }

        /* We compute the average. */
        for (i = 0; i < g_number_of_runs; i++)
        {
            average[j] += results[j * g_number_of_runs + i];
        }

        average[j] /= g_number_of_runs;

        /* We compute the variance (only if needed). */
        if (g_compute_standard_deviation_and_variance)
        {
            for (i = 0; i < g_number_of_runs; i++)
            {
                variance[j] +=
                    (results[j * g_number_of_runs + i] - average[j]) *
                    (results[j * g_number_of_runs + i] - average[j]);
            }

            variance[j] /= g_number_of_runs;
        }
    }

    /* We print the result. If the measurements took place on the server... */
    if (g_measurement_location == ML_SERVER || g_average_client_results)
    {
        /* ...we return the average... */
        printf("%f,", average[0]);

        if (g_compute_standard_deviation_and_variance)
            /* ...and, if needed, the variance and standard deviation. */
            printf("%f,%f,", variance[0], sqrt(variance[0]));
    }
    /* Otherwise... */
    else
    {
        /* ...it's the same, but for each client. */
        for (j = 1; j <= g_number_of_clients; j++)
        {
            printf("%f,", average[j]);

            if (g_compute_standard_deviation_and_variance)
                /* ...and, if needed, the variance and standard deviation. */
                printf("%f,%f,", variance[j], sqrt(variance[j]));
        }
    }


    /* If sampling is enabled, we need to specify a value for the core ID of the
       average. We choose -1. */
    if (g_execution_mode == EM_SINGLE_RUN_SAMPLED)
        printf("%d,", -1);

    /* Should we end the input with a newline? */
    if (g_end_output_with_a_newline)
        printf("\n");
    else
        /* If we don't, we make sure to flush the standard output. */
        fflush(NULL);

    /* ====================================================================== */
    /* [v] Cleanup (unnecessary)                                              */
    /* ====================================================================== */
    free((int *)g_physical_to_virtual_hw_thread_id);
    free(clients);
    free(thread_argument_blocks);
    free((int *)g_ready);
    free((double *)g_iteration_result);
    free(results);
    free(average);
    free(variance);
    if (g_execution_mode == EM_SINGLE_RUN_ORDERED) free((int *)g_order);
    if (g_execution_mode == EM_SINGLE_RUN_SAMPLED)
    {
        free(g_rpc_done_addresses);
        free((float *)g_multiple_samples_results);
        free((void **)g_multiple_samples_rpc_done_addrs);
    }

#ifdef USE_BLOCKING_LOCKS
    if (pthread_mutex_destroy(&mutex_rpc_done_addr_not_null) < 0
            || pthread_mutex_destroy(&mutex_rpc_done_positive) < 0
            || pthread_mutex_destroy(&mutex_rpc_done_addr_null) < 0)
        ERROR("pthread_mutex_destroy");

    if (pthread_cond_destroy(&cond_rpc_done_addr_not_null) < 0
            || pthread_cond_destroy(&cond_rpc_done_positive) < 0
            || pthread_cond_destroy(&cond_rpc_done_addr_null) < 0)
        ERROR("pthread_cond_destroy");
#endif
    /* ====================================================================== */
    /* [^] Cleanup (unnecessary)                                              */
    /* ====================================================================== */

    /* Everything went as expected. */
    return EXIT_SUCCESS;
}

