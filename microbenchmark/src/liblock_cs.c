/* ########################################################################## */
/* liblock_cs.c                                                               */
/* (C) Jean-Pierre Lozi, 2010-2011                                            */
/* (C) Gaël Thomas, 2010-2011                                                 */
/* -------------------------------------------------------------------------- */
/* ########################################################################## */
#include "liblock_cs.h"

#include <papi.h>
#include <pthread.h>
#include "cs.h"
#include "liblock.h"
#include "liblock-fatal.h"


#if !defined(NO_PAPI) && !defined(__sun__)
static int g_event_set = PAPI_NULL;
#endif
static volatile int g_events_monitored_server = -1;
static volatile int g_events_number_of_threads = 0;

const char*          g_liblock_name = 0;
liblock_lock_t       g_liblock_lock;

__thread volatile uint64_t *t_shared_variables_memory_area;
__thread int t_number_of_context_variables;
__thread int t_number_of_shared_variables;
__thread int t_access_order;
__thread int *t_local_permutations_array;
__thread int *t_global_permutations_array;


void* liblock_cs(void *context_variables_local_memory_area)
{
    /* We access context variables */
    access_variables(context_variables_local_memory_area,
            0,
            g_number_of_context_variables,
            t_access_order,
            t_local_permutations_array);

    /* We access shared variables */
    access_variables(g_shared_variables_memory_area,
            0,
            g_number_of_shared_variables,
            t_access_order,
            t_global_permutations_array);

    return NULL;
}

void *liblock_cs_g1l0_dependent(void *context_variables_local_memory_area)
{
    (*g_shared_variables_memory_area)++;

    return NULL;
}

void *liblock_cs_g5l0_dependent(void *context_variables_local_memory_area)
{
    int i = 0;

    volatile uint64_t *memory_area = g_shared_variables_memory_area;

/*
    for (k = 0; k < 5; k++)
    {
        i = (*(volatile uint64_t *)((uintptr_t)memory_area +
                                    k * 2 * sizeof(cache_line_t) +
                                    (i % 2) * sizeof(uint64_t)))++;
    }
*/

    i = (*(volatile uint64_t *)((uintptr_t)memory_area))++;

    i = (*(volatile uint64_t *)((uintptr_t)memory_area +
                                2 * sizeof(cache_line_t) +
                                (i % 2) * sizeof(uint64_t)))++;

    i = (*(volatile uint64_t *)((uintptr_t)memory_area +
                                4 * sizeof(cache_line_t) +
                                (i % 2) * sizeof(uint64_t)))++;

    i = (*(volatile uint64_t *)((uintptr_t)memory_area +
                                6 * sizeof(cache_line_t) +
                                (i % 2) * sizeof(uint64_t)))++;

    i = (*(volatile uint64_t *)((uintptr_t)memory_area +
                                8 * sizeof(cache_line_t) +
                                (i % 2) * sizeof(uint64_t)))++;

    return NULL;
}

void server_started()
{
    g_server_started = 1;
}

void liblock_on_server_thread_start(const char* lib, unsigned int thread_id)
{
#if !defined(NO_PAPI) && !defined(__sun__)
	long long values[1];

	PAPI_thread_init((unsigned long (*)(void))pthread_self);
#endif

	if (g_monitored_event_id != 0)
    {
		if(__sync_val_compare_and_swap(&g_events_monitored_server,
                                       -1,
                                       self.running_hw_thread->hw_thread_id))
        {
#if !defined(NO_PAPI) && !defined(__sun__)
            if(PAPI_create_eventset(&g_event_set) != PAPI_OK)
				warning("PAPI_create_eventset");
			else if(PAPI_add_event(g_event_set,
                                   g_monitored_event_id) != PAPI_OK)
				warning("PAPI_add_events");
			/* This seemingly helps increasing PAPI's accuracy. */
			else
            {
				if(PAPI_start(g_event_set) != PAPI_OK)
					warning("PAPI_start");
				if (PAPI_stop(g_event_set, values) != PAPI_OK)
					warning("PAPI_stop");
				if (PAPI_start(g_event_set) != PAPI_OK)
					warning("PAPI_start");
			}
#endif
		}
		if(g_events_monitored_server == self.running_hw_thread->hw_thread_id)
			__sync_fetch_and_add(&g_events_number_of_threads, 1);
	}
}

void liblock_on_server_thread_end(const char* lib, unsigned int thread_id)
{
#if !defined(NO_PAPI) && !defined(__sun__)
    long long values[1];
#endif

	if (g_monitored_event_id != 0
			&& g_events_monitored_server == self.running_hw_thread->hw_thread_id
			&& !__sync_sub_and_fetch(&g_events_number_of_threads, 1)) {

#if !defined(NO_PAPI) && !defined(__sun__)
		if(PAPI_stop(g_event_set, values) != PAPI_OK)
			warning("PAPI_stop");
		else
			g_event_count = values[0];
#endif
	}
}

struct hw_thread* get_core(unsigned int physical_core)
{
    return &topology->
               hw_threads[g_physical_to_virtual_hw_thread_id[physical_core]];
}

/* Don't print stats. */
void liblock_print_stats() {}

/* Avoid useless warning from libnuma. */
void numa_warn(int number, char *where, ...) {}

