/* ########################################################################## */
/* client.h                                                                   */
/* (C) Jean-Pierre Lozi, 2010-2011                                            */
/* (C) Gaël Thomas, 2010-2011                                                 */
/* -------------------------------------------------------------------------- */
/* ########################################################################## */
#ifndef __CLIENT_H
#define __CLIENT_H

#include <signal.h>


extern void *client_main(void *thread_arguments_block_pointer);
extern void emt_handler(int sig, siginfo_t *sip, void *arg);

#endif
