/* ########################################################################## */
/* client.c                                                                   */
/* (C) Jean-Pierre Lozi, 2010-2011                                            */
/* (C) Gaël Thomas, 2010-2011                                                 */
/* -------------------------------------------------------------------------- */
/* ########################################################################## */
#include "client.h"

#include <papi.h>
#include <sys/mman.h>
#ifdef __sun__
#include <libcpc.h>
#endif
#include "liblock.h"
#include "liblock-fatal.h"
#include "benchmark.h"
#include "cs.h"
#include "liblock_cs.h"
#include "util.h"


/******************************************************************************/
/* Macros                                                                     */
/******************************************************************************/
#ifdef __sun__
#define READ_ASR17(res)           asm volatile ("rd %%asr17, %0\n" : "=r" (res))
#else
#define READ_ASR17(res)           fatal("libcpc only exists on Solaris");
#endif

#ifndef __sun__
#ifndef NO_PAPI
#define READ_EVENT_COUNTERS(res)                                               \
    do                                                                         \
    {                                                                          \
        if (PAPI_read(event_set, (long long *)(&(res))) != PAPI_OK)            \
            perror("PAPI_read");                                               \
    }                                                                          \
    while(0);
#else
#define READ_EVENT_COUNTERS(res) ;
#endif
#else
#define READ_EVENT_COUNTERS(res) READ_ASR17(res)
#endif


/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
#ifdef __sun__
static __thread cpc_set_t *cpc_set;
#endif


/******************************************************************************/
/* Function executed by the clients                                           */
/******************************************************************************/
void *client_main(void *thread_arguments_block_pointer)
{

    int i;
    //printf("client %d is running\n", self.id);

    while(!g_server_started)
        PAUSE();

    /* This is needed because the CAS function requires an address. */
    //    const int one = 1;

    /* We avoid using global variables in (and around) the critical section
       to limit data cache misses. */
    const int number_of_clients = g_number_of_clients;
    const int number_of_iterations_per_client =
        g_number_of_iterations_per_client;
    const int delay = g_delay;

    const int number_of_context_variables = g_number_of_context_variables;
    const int number_of_shared_variables = g_number_of_shared_variables;

    const int measurement_metric = g_measurement_metric;
    const int measurement_type = g_measurement_type;
    const int access_order = g_access_order;
    const int use_multiple_locks = g_use_multiple_locks;

    const int read_only_wait = g_read_only_wait;
    const int execution_mode = g_execution_mode;

    const int measurement_location = g_measurement_location;

    const int critical_sections_type = g_critical_sections_type;
    const int use_mcs = g_use_mcs;

    const int measurement_unit = g_measurement_unit;

    const int skip_first_cs = g_skip_first_cs;

    /* Argument block */
    thread_arguments_block_t thread_arguments_block;

    /* We copy the contents of the argument block into these variables to
       improve the readability of the code. */
    volatile uint64_t *volatile *null_rpc_global_sv;
    volatile uint64_t *null_rpc_local_sv;
    volatile uint64_t *lock_acquisitions_global_sv;
    volatile uint64_t *context_variables_local_memory_area,
             *shared_variables_memory_area;

    int client_id, client_core;
    /* PAPI variables */
#if !defined(NO_PAPI) && !defined(__sun__)
    int event_set = PAPI_NULL;
    int events[1];
#endif
    long long values[1];
    /* FIXME: rename */
    long long start_cycles = 0, end_cycles;
    long long cycles;

    /* Latency */
    long long main_lock_acquisition_beginning = 0, main_lock_acquisition_end;
    /* FIXME: names */
    double total_latency = 0;
    // double average_number_of_failed_attempts = 0;

    /* Failures */
    unsigned long long number_of_failed_attempts = 0;

    int *local_permutations_array, *global_permutations_array;

    /* FIXME */
    // int random_delay;

    mcs_lock_t *l_mcs_me_ptr = NULL;
    // Variables used for PAPI measurements.
#ifndef NO_PAPI
    unsigned long long events_begin = -1, events_end = -1;
#endif
    long long n_events = 0;

    struct hw_thread *self_p;
    struct core *core_p;
    struct numa_node *node_p;
    int monitored = 0;

    /* We get the client id. */
    thread_arguments_block =
        *((thread_arguments_block_t *)thread_arguments_block_pointer);

    client_id = thread_arguments_block.id;
    client_core = thread_arguments_block.client_core;
    null_rpc_local_sv = thread_arguments_block.null_rpc_local_sv;
    null_rpc_global_sv = thread_arguments_block.null_rpc_global_sv;
    lock_acquisitions_global_sv =
        thread_arguments_block.lock_acquisitions_global_sv;

    self_p = get_core(client_core);

    if (g_monitoring_location == ML_PER_NODE)
    {
        node_p = self_p->node;
        monitored = (self_p == node_p->hw_threads[0]);
    }
    else
    {
        core_p = self_p->core;
        monitored = (self_p == core_p->hw_threads[0]);
    }

    context_variables_local_memory_area =
        (volatile uint64_t *)
        ((uintptr_t)thread_arguments_block.context_variables_global_memory_area
         + 2 * sizeof(cache_line_t) * (number_of_context_variables + 1) *
         client_id + 2 * sizeof(cache_line_t));
    shared_variables_memory_area =
        thread_arguments_block.shared_variables_memory_area;

    local_permutations_array = alloc(number_of_context_variables * sizeof(int));

    for (i = 0 ; i < number_of_context_variables ; i++) {
        local_permutations_array[i] = g_local_permutations_array[i];
    }

    global_permutations_array = alloc(number_of_shared_variables * sizeof(int));

    for (i = 0 ; i < number_of_shared_variables ; i++) {
        global_permutations_array[i] = g_global_permutations_array[i];
    }

    /* MCS Lock */
    if (critical_sections_type == CST_LOCK_ACQUISITIONS && use_mcs)
    {
        if (!g_numa_aware)
        {
            if(MEMALIGN(&l_mcs_me_ptr,
                         2 * CACHE_LINE_SIZE,
                         2 * CACHE_LINE_SIZE))
			    fatal("MEMALIGN");

            //if (l_mcs_me_ptr == NULL)
            //    fatal("posix_memalign");
        }
        else
        {
            //l_mcs_me_ptr = numa_alloc_local(2 * CACHE_LINE_SIZE);
            l_mcs_me_ptr = (mcs_lock_t *)mmap(0,  2 * sizeof(cache_line_t),
                                              PROT_READ | PROT_WRITE,
                                              MAP_ANONYMOUS | MAP_PRIVATE, 0, 0);

            if (l_mcs_me_ptr == NULL)
                //fatal("numa_alloc_local");
                fatal("mmap");
        }
    }

    /* If sampling is on... */
    if (execution_mode == EM_SINGLE_RUN_SAMPLED)
    {
        /* ...we register the address of our rpc_done local variable. */
        g_rpc_done_addresses[client_core] = null_rpc_local_sv;
    }

    if (measurement_location == ML_CLIENTS)
    {
        /* Additional initialization is needed if we are counting events. */
        if (measurement_metric == MM_NUMBER_OF_EVENTS)
        {
            if (monitored)
            {
#if !defined(NO_PAPI) && !defined(__sun__)
                /* We will use PAPI in this thread. */
                PAPI_thread_init((unsigned long (*)(void))pthread_self);

                events[0] = g_monitored_event_id;

                if (PAPI_create_eventset(&event_set) != PAPI_OK)
                    fatal("PAPI_create_eventset");
                if (PAPI_add_events(event_set, events, 1) != PAPI_OK)
                    fatal("PAPI_add_events");
#else
                int cpc_index;

                if ((cpc_set = cpc_set_create(rcw_cpc)) == NULL)
                    fatal("cpc_set_create() failed");

                cpc_index =
                    cpc_set_add_request(rcw_cpc, cpc_set,
                                        (char *)g_monitored_event_id_str, 0,
                                        CPC_COUNT_USER | CPC_COUNT_SYSTEM |
                                        CPC_BIND_EMT_OVF, 0, NULL);

                if (cpc_index < 0)
                    fatal("cpc_set_add_request() failed (event=%s)\n",
                          g_monitored_event_id_str);
#endif
            }

        }
    }

    t_shared_variables_memory_area = shared_variables_memory_area;
    t_number_of_shared_variables = number_of_shared_variables;
    t_number_of_context_variables = number_of_context_variables;
    t_access_order = access_order;
    t_local_permutations_array = local_permutations_array;
    t_global_permutations_array = global_permutations_array;

    /* We're ready. */
    g_ready[client_id + 1] = 1;


    /* Synchronization barriers */
    if (critical_sections_type == CST_NULL_RPCS)
    {
        //pthread_barrier_wait(&barrier);
        for (i = 0; i <= number_of_clients; i++)
            while (!g_ready[i])
                PAUSE();
    }
    else
    {
        //pthread_barrier_wait(&barrier);
        for (i = 1; i <= number_of_clients; i++)
            while (!g_ready[i])
                PAUSE();
    }

    if (measurement_location == ML_CLIENTS)
    {
        /* Does the user wish to measure the number of elapsed cycles? */
        if (measurement_metric == MM_NUMBER_OF_CYCLES
                && measurement_type == MT_GLOBAL)
        {
            /* If so, get the current cycle count. We could also use
             * PAPI_start with the TOT_CYC event. */
            start_cycles = get_cycles();
        }
        else if (measurement_metric == MM_NUMBER_OF_EVENTS && monitored)
        {
#ifndef __sun__
#ifndef NO_PAPI
            /* Otherwise we start counting events. */
            if (PAPI_start(event_set) != PAPI_OK)
                fatal("PAPI_start");
#endif
#else
            if (cpc_bind_curlwp(rcw_cpc, cpc_set, 0) < 0)
                fatal("cpc_bind_curlwp");
#endif
        }
    }

    /* ###################################################################### */
    /* # Main loop (client)                                                 # */
    /* ###################################################################### */
    /* First implementation : using spinlocks. */
#ifndef USE_BLOCKING_LOCKS
    /* ###################################################################### */
    /* NULL RPCs                                                              */
    /* ###################################################################### */
    if (critical_sections_type == CST_NULL_RPCS)
    {
        /* We execute number_of_iterations_per_client RPCs. */
        for (i = 0; i < number_of_iterations_per_client; i++)
        {
            if (!use_multiple_locks)
                *null_rpc_local_sv = 0;

            /* We access context variables */
            access_variables(context_variables_local_memory_area,
                    0,
                    number_of_context_variables,
                    0,
                    local_permutations_array);


            if (measurement_type == MT_LOCK_ACQUISITIONS ||
                    measurement_type == MT_CRITICAL_SECTIONS)
            {
                if (measurement_metric == MM_NUMBER_OF_CYCLES)
                {
                    main_lock_acquisition_beginning = get_cycles();
                }
#if !defined(NO_PAPI)
                else if (monitored)
                {
                    READ_EVENT_COUNTERS(events_begin);
                }
#endif
            }

            /* ============================================================== */
            /* BEGIN Lock acquisition                                         */
            /* ============================================================== */
            if (use_multiple_locks)
            {
                *null_rpc_local_sv = 1;
            }
            else
            {
                if (measurement_metric == MM_FAILED_ATTEMPTS)
                {
                    /* FIXME: doesn't work with multiple locks! */
                    /* FIXME: handle the g_skip_first_cs parameter? */
                    if (!read_only_wait)
                    {
                        /* We use a compare and swap to avoid mutexes. */
                        while (!__sync_bool_compare_and_swap(null_rpc_global_sv,
                                    0,
                                    null_rpc_local_sv))
                        {
                            number_of_failed_attempts++;
                            PAUSE();
                        }
                    }
                    else
                    {
                        for (;;)
                        {
                            if (!(*null_rpc_global_sv))
                            {
                                if (!__sync_bool_compare_and_swap
                                        (null_rpc_global_sv,
                                         0,
                                         null_rpc_local_sv))
                                {
                                    number_of_failed_attempts++;
                                    continue;
                                }
                                else break;
                            }

                            PAUSE();
                        }
                    }
                }
                else
                {
                    if (!read_only_wait)
                    {
                        /* We use a compare and swap to avoid mutexes. */
                        while (!__sync_bool_compare_and_swap(null_rpc_global_sv,
                                    0,
                                    null_rpc_local_sv))
                            PAUSE();
                    }
                    else
                    {
                        for (;;)
                        {
                            if (!(*null_rpc_global_sv))
                            {
                                if (!__sync_bool_compare_and_swap
                                        (null_rpc_global_sv,
                                         0,
                                         null_rpc_local_sv))
                                    continue;
                                else break;
                            }

                            PAUSE();
                        }
                    }
                }
            }
            /* ============================================================== */
            /* END Lock acquisition                                           */
            /* ============================================================== */

            if (measurement_type == MT_LOCK_ACQUISITIONS &&
                (!skip_first_cs || i > 0))
            {
                if (measurement_metric == MM_NUMBER_OF_CYCLES)
                {
                    main_lock_acquisition_end = get_cycles();

                    total_latency += main_lock_acquisition_end -
                        main_lock_acquisition_beginning;

                    if (execution_mode == EM_SINGLE_RUN_SAMPLED)
                    {
                        g_latencies[client_id] =
                            main_lock_acquisition_end -
                            main_lock_acquisition_beginning;
                    }
                }
                else if (monitored)
                {
                    READ_EVENT_COUNTERS(events_end);

#ifdef __sun__
                    events_begin >>= 32;
                    events_end >>= 32;
#endif

                    n_events += events_end - events_begin;
                }
            }
            /*
               else if (measurement_type == FAILED_ATTEMPTS)
               {
               local_average_number_of_failed_attempts +=
               (double)number_of_failed_attempts /
               local_number_of_iterations_per_client;

               number_of_failed_attempts = 0;
               }
             */

            /* ============================================================== */
            /* BEGIN Lock release                                             */
            /* ============================================================== */
            if (use_multiple_locks)
            {
                // if (PAPI_reset(event_set) != PAPI_OK)
                //     perror("PAPI_reset");
                while (*null_rpc_local_sv == 1)
                    PAUSE();
                // if (PAPI_accum(event_set, values) != PAPI_OK)
                //     perror("PAPI_accum");
            }
            else
            {
                while (*null_rpc_local_sv == 0)
                    PAUSE();
            }
            /* ============================================================== */
            /* END Lock release                                               */
            /* ============================================================== */

            if (measurement_type == MT_CRITICAL_SECTIONS &&
                (!skip_first_cs || i > 0))
            {
                if (measurement_metric == MM_NUMBER_OF_CYCLES)
                {
                    main_lock_acquisition_end = get_cycles();

                    total_latency +=
                        main_lock_acquisition_end -
                        main_lock_acquisition_beginning;

                    if (execution_mode == EM_SINGLE_RUN_SAMPLED)
                    {
                        g_latencies[client_id] = main_lock_acquisition_end -
                            main_lock_acquisition_beginning;
                    }
                }
#if !defined(NO_PAPI)
                else if (monitored)
                {
                    READ_EVENT_COUNTERS(events_end);

#ifdef __sun__
                    events_begin >>= 32;
                    events_end >>= 32;
#endif

                    n_events += events_end - events_begin;
                }
#endif
            }

            /* We could avoid the delay introduced by the test by moving the
               if outside of the loop, however, tests show that no delay or
               a small delay doesn't alter the results significantly. */
            if (delay > 0)
            {
                /* Delay */
                cycles = get_cycles();
                while ((get_cycles() - cycles) < delay)
                    ;

                /*
                   cycles = get_cycles();
                   random_delay = rand() % local_delay;

                   while ((get_cycles() - cycles) < random_delay)
                   ;
                 */
            }
        }
    }
    /* ###################################################################### */
    /* Spin locks                                                             */
    /* ###################################################################### */
    else if (critical_sections_type == CST_LOCK_ACQUISITIONS)
    {
        if (execution_mode != EM_SINGLE_RUN_SAMPLED)
        {
            for (i = 0; i < number_of_iterations_per_client; i++)
            {
                /* We access context variables */
                access_variables(context_variables_local_memory_area,
                        0,
                        number_of_context_variables,
                        0,
                        local_permutations_array);

                /*
                   access_variables(null_rpc_local_sv,
                   1,
                   local_number_of_context_variables,
                   0);
                 */

                if (measurement_type == MT_LOCK_ACQUISITIONS ||
                        measurement_type == MT_CRITICAL_SECTIONS)
                {
                    if (measurement_metric == MM_NUMBER_OF_CYCLES)
                    {
                        main_lock_acquisition_beginning = get_cycles();
                    }
#if !defined(NO_PAPI)
                    else if (monitored)
                    {
                        READ_EVENT_COUNTERS(events_begin);
                    }
#endif
                }

                /* ========================================================== */
                /* BEGIN Lock acquisition                                     */
                /* ========================================================== */
                if (measurement_metric == MM_FAILED_ATTEMPTS)
                {
                    if (!read_only_wait)
                    {
                        if (use_mcs)
                        {
                            lock_mcs(g_mcs_m_ptr, l_mcs_me_ptr);
                        }
                        else
                        {
                            while (!__sync_bool_compare_and_swap
                                    (lock_acquisitions_global_sv, 0, 1))
                            {
                                number_of_failed_attempts++;
                                PAUSE();
                            }
                        }
                    }
                    else
                    {
                        for (;;)
                        {
                            if (!*null_rpc_global_sv)
                            {
                                if (!__sync_bool_compare_and_swap
                                        (lock_acquisitions_global_sv, 0, 1))
                                {
                                    number_of_failed_attempts++;
                                    continue;
                                }

                                else break;
                            }

                            PAUSE();
                        }
                    }
                }
                else
                {
                    if (!read_only_wait)
                    {
                        if (use_mcs)
                        {
                            lock_mcs(g_mcs_m_ptr, l_mcs_me_ptr);
                        }
                        else
                        {
                            while (!__sync_bool_compare_and_swap
                                    (lock_acquisitions_global_sv, 0, 1))
                                PAUSE();
                        }
                    }
                    else
                    {
                        for (;;)
                        {
                            if (!*null_rpc_global_sv)
                            {
                                if (!__sync_bool_compare_and_swap
                                        (lock_acquisitions_global_sv, 0, 1))
                                    continue;

                                else break;
                            }

                            PAUSE();
                        }
                    }
                }
                /* ========================================================== */
                /* END Lock acquisition                                       */
                /* ========================================================== */

                //__sync_synchronize();

                if (measurement_type == MT_LOCK_ACQUISITIONS &&
                    (!skip_first_cs || i > 0))
                {
                    if (measurement_metric == MM_NUMBER_OF_CYCLES)
                    {
                        main_lock_acquisition_end = get_cycles();

                        total_latency +=
                            main_lock_acquisition_end -
                            main_lock_acquisition_beginning;
                    }
                    else if (monitored)
                    {
                        READ_EVENT_COUNTERS(events_end);

#ifdef __sun__
                        events_begin >>= 32;
                        events_end >>= 32;
#endif

                        n_events += events_end - events_begin;
                    }
                }

                /* We access context variables */
                access_variables(context_variables_local_memory_area,
                        0,
                        number_of_context_variables,
                        access_order,
                        local_permutations_array);

                /* We access shared variables */
                access_variables(shared_variables_memory_area,
                        0,
                        number_of_shared_variables,
                        access_order,
                        global_permutations_array);

                //__sync_synchronize();

                /* ========================================================== */
                /* BEGIN Lock release                                         */
                /* ========================================================== */
                if (use_mcs)
                {
                    unlock_mcs(g_mcs_m_ptr, l_mcs_me_ptr);
                }
                else
                {
                    *lock_acquisitions_global_sv = 0;
                }
                /* ========================================================== */
                /* END Lock release                                           */
                /* ========================================================== */

                if (measurement_type == MT_CRITICAL_SECTIONS &&
                    (!skip_first_cs || i > 0))
                {
                    if (measurement_metric == MM_NUMBER_OF_CYCLES)
                    {
                        main_lock_acquisition_end = get_cycles();

                        total_latency += main_lock_acquisition_end -
                           main_lock_acquisition_beginning;
                    }
#if !defined(NO_PAPI)
                    else if (monitored)
                    {
                        READ_EVENT_COUNTERS(events_end);
#ifdef __sun__
                        events_begin >>= 32;
                        events_end >>= 32;
#endif

                        n_events += events_end - events_begin;
                    }
#endif
                }

                if (delay > 0)
                {
                    /* Delay */
                    cycles = get_cycles();
                    while ((get_cycles() - cycles) < delay)
                        ;

                    /*
                       cycles = get_cycles();
                       random_delay = rand() % delay;
                       while ((get_cycles() - cycles) < random_delay)
                       ;
                     */
                }
            }
        }
    }
    /* ###################################################################### */
    /* Liblock                                                                */
    /* ###################################################################### */
    else
    {
        void *(*cs)(void *);
        cs = liblock_cs;

        if (number_of_context_variables == 0 && access_order == AO_DEPENDENT)
        {
            if (number_of_shared_variables == 1)
                cs = liblock_cs_g1l0_dependent;
            else if (number_of_shared_variables == 5)
                cs = liblock_cs_g5l0_dependent;
        }

        for (i = 0; i < number_of_iterations_per_client; i++)
        {
            /* We access context variables */
            access_variables(context_variables_local_memory_area,
                    0,
                    number_of_context_variables,
                    0,
                    local_permutations_array);

            /* FIXME: MT_LOCK_ACQUISTIONS can't be handled. Warning message
              (@main()) */

          if (measurement_type == MT_CRITICAL_SECTIONS)
            {
                if (measurement_metric == MM_NUMBER_OF_CYCLES)
                {
                    main_lock_acquisition_beginning = get_cycles();
                }
#if !defined(NO_PAPI)
                else if (monitored)
                {
                    READ_EVENT_COUNTERS(events_begin);
                }
#endif
            }

            //__sync_synchronize();

            // LIBLOCK -> fill me here
            //{ static int zzz = 0; if(!(__sync_fetch_and_add(&zzz, 1) % 500))
            //printf("executes %d for client %d\n", zzz, self.id); }
            //long long res = (long long)
            liblock_exec(&g_liblock_lock, cs,
                         (void *)context_variables_local_memory_area);
            //__sync_synchronize();

            if (measurement_type == MT_CRITICAL_SECTIONS &&
                (!skip_first_cs || i > 0))
            {
                if (measurement_metric == MM_NUMBER_OF_CYCLES)
                {
                    main_lock_acquisition_end = get_cycles();

                    total_latency += main_lock_acquisition_end -
                        main_lock_acquisition_beginning;;


                    //total_latency += res;
                }
#if !defined(NO_PAPI)
                else if (monitored)
                {
                    READ_EVENT_COUNTERS(events_end);

#ifdef __sun__
                    events_begin >>= 32;
                    events_end >>= 32;
#endif

                    n_events += events_end - events_begin;

                    if (n_events < 0)
                        fprintf(stderr,
                                "n_events=%lld, event_end=%llu, events_begin=%llu\n",
                                n_events, events_end, events_begin);
                }
#endif
            }

            if (delay > 0)
            {
                /* Delay */
                cycles = get_cycles();
                while ((get_cycles() - cycles) < delay)
                    ;
            }
        }
    }
    /* Second implementation : using blocking locks. Deprecated. */
#else
    /* We execute number_of_iterations_per_client RPCs. */
    for (i = 0; i < local_number_of_iterations_per_client; i++)
    {
        *null_rpc_local_sv = 0;

        /* This lock corresponds to the CAS from the previous implementation. */
        pthread_mutex_lock(&mutex_rpc_done_addr_null);

        /* We wait for the server to be ready. */
        while(global->rpc_done_addr != 0)
            pthread_cond_wait(&cond_rpc_done_addr_null,
                    &mutex_rpc_done_addr_null);

        /* The server is ready, let's execute a RPC. */
        pthread_mutex_lock(&mutex_rpc_done_addr_not_null);
        *null_rpc_global_sv = null_rpc_local_sv;
        pthread_cond_signal(&cond_rpc_done_addr_not_null);
        pthread_mutex_unlock(&mutex_rpc_done_addr_not_null);

        pthread_mutex_unlock(&mutex_rpc_done_addr_null);

        /* We wait until the server is done with our RPC. */
        pthread_mutex_lock(&mutex_rpc_done_positive);
        while (*null_rpc_local_sv == 0)
            pthread_cond_wait(&cond_rpc_done_positive,
                    &mutex_rpc_done_positive);
        pthread_mutex_unlock(&mutex_rpc_done_positive);
    }
#endif
    /* ###################################################################### */
    /* # End                                                                # */
    /* ###################################################################### */

    if (measurement_location == ML_CLIENTS)
    {
        /* Are we counting cycles? */
        if (measurement_metric == MM_NUMBER_OF_CYCLES)
        {
            if (measurement_type == MT_GLOBAL)
            {
                /* If so, get the current cycle count. */
                end_cycles = get_cycles();

                if (measurement_unit != MU_TOTAL_CYCLES_MAX)
                {
                    /* We return the number of cycles per RPC. */
                    g_iteration_result[client_id + 1] =
                        (double)(end_cycles - start_cycles) /
                        number_of_iterations_per_client;
                }
                else
                {
                    g_iteration_result[client_id + 1] =
                        (double)(end_cycles - start_cycles);
                }
            }
            else if (measurement_type == MT_LOCK_ACQUISITIONS ||
                    measurement_type == MT_CRITICAL_SECTIONS)
            {
                if (total_latency < 0)
                {
                    fprintf(stderr, "Error: total_latency < 0\n");
                    exit(-1);
                }

                g_iteration_result[client_id + 1] =
                    total_latency /
                    (number_of_iterations_per_client - (skip_first_cs ? 1 : 0));
            }
        }
        else if (measurement_metric == MM_NUMBER_OF_EVENTS)
        {
#if !defined(NO_PAPI) && !defined(__sun__)
            if (measurement_type == MT_GLOBAL)
            {
                if (PAPI_stop(event_set, values) != PAPI_OK)
                    fatal("PAPI_stop");
            }
#endif

            /* When using the liblock, we rely on an improved implementation
               that uses PAPI_read. */
            if (critical_sections_type == CST_LIBLOCK)
            {
                if (monitored)
                {
                    g_iteration_result[client_id + 1] =
                        (double)n_events / number_of_iterations_per_client;
                }
                else
                {
                    g_iteration_result[client_id + 1] = -1;
                }
            }
            else
            {
                g_iteration_result[client_id + 1] =
                    (double)values[0] / number_of_iterations_per_client;
            }
        }
        else /* if (local_measurement_metric == MM_FAILED_ATTEMPTS) */
        {
            /*
               iteration_result[client_id + 1] =
               local_average_number_of_failed_attempts;
             */
            g_iteration_result[client_id + 1] =
                (double)number_of_failed_attempts /
                (number_of_iterations_per_client - (skip_first_cs ? 1 : 0));
        }

    }

    /* In the ordered mode, we save the order in which RPCs are serviced. */
    if (execution_mode == EM_SINGLE_RUN_ORDERED)
    {
        g_order[client_id] = *null_rpc_local_sv;
    }

#if !defined(NO_PAPI) && !defined(__sun__)
    if (measurement_metric == MM_NUMBER_OF_EVENTS && monitored)
    {
        PAPI_unregister_thread();
    }
#endif

    if(__sync_add_and_fetch(&g_number_of_finished_clients, 1) ==
       g_number_of_clients)
        if(g_critical_sections_type == CST_LIBLOCK) {
            liblock_lock_destroy(&g_liblock_lock);
        }

    return NULL;
}

void emt_handler(int sig, siginfo_t *sip, void *arg)
{
    fatal("A hardware counter has overflowed.\n");
}

