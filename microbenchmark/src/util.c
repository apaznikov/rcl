/* ########################################################################## */
/* util.c                                                                     */
/* (C) Jean-Pierre Lozi, 2010-2011                                            */
/* (C) Gaël Thomas, 2010-2011                                                 */
/* -------------------------------------------------------------------------- */
/* ########################################################################## */
#include "util.h"

#include <stdlib.h>
#include <string.h>

#include "liblock.h"
#include "liblock-fatal.h"


/* CPU frequency, in MHz */                                                      
extern volatile float g_cpu_frequency;
/* Number of hw_threads */                                                       
extern volatile int g_number_of_hw_threads;
/* This array maps physical to virtual core IDs. */                              
extern volatile int *g_physical_to_virtual_hw_thread_id;


/* Wrapper for malloc that checks for errors. */
void *alloc(int size)
{
    void *result;

    /* We just call malloc and check for errors. */
    if ((result = malloc(size)) == NULL)
        fatal("malloc");

    /* We return the result. */
    return result;
}

/* This function gets the CPU speed then allocates and fills the 
   virtual_to_physical_hw_thread_id array. */
void get_cpu_info()
{
    int i = 0, j, n;
    float previous_core_frequency = -1, core_frequency = 0;

    /* Compute frequency. */
    for(i=0; i < topology->nb_hw_threads; i++)
    {
        core_frequency = topology->hw_threads[i].frequency;
        if(previous_core_frequency == -1)
            previous_core_frequency = core_frequency;
        else if(previous_core_frequency != core_frequency)
            warning("all processing hw_threads must use the same clock "
                  "frequency");
//            fatal("all processing hw_threads must use the same clock "
//                  "frequency");
    }

    /* We update the global cpu_frequency variable. */
    g_cpu_frequency = core_frequency;

    g_number_of_hw_threads = topology->nb_hw_threads;

    /* We allocate the physical_to_virtual_hw_thread_id array. */
    g_physical_to_virtual_hw_thread_id =
        malloc(g_number_of_hw_threads * sizeof(int));

    for(i=0, n=0; i < topology->nb_nodes; i++)
    {
        for(j=0; j < topology->nodes[i].nb_hw_threads; j++)
        {
            g_physical_to_virtual_hw_thread_id[n++] =
                topology->nodes[i].hw_threads[j]->hw_thread_id;
        }
    }
}

void generate_permutations_array(volatile int **permutations_array, int size)
{
    int i, r;
    int *seen;

    /* FIXME: allocate outside, and free */
    *permutations_array = alloc(size * sizeof(int));
    seen = alloc(size * sizeof(int));

    memset(seen, 0, size * sizeof(int));

    for (i = 0; i < size; i++)
    {
        do
        {
            r = rand() % size;
        }
        while (seen[r]);

        seen[r] = 1;

        (*permutations_array)[i] = r;
    }

    free(seen);
}


/* This function writes the usage string to stderr and exits the application. */
void wrong_parameters_error(char *application_name)
{
    const char *usage = "usage: see source code";

    /* TODO: update usage string. */

//  Old usage string:
//
//        "usage: %s [-R] [-L] \n"
//        "       [-A number_of_runs] [-S #_iterations_per_sample] [-O]\n"
//        "       [-s core] [-c #_clients] [-n #_iterations_per_client] "
//        "[-d delay] [-r] [-l #_of_local_variables] "
//        "[-g #_of_context_variables] [-z] [-x] [-p]\n"
//        "       [-e event_type] [-a] [-f] [-m] [-y] [-t] [-v] [-i]\n";

    fprintf(stderr, usage, application_name);
    exit(EXIT_FAILURE);
}

