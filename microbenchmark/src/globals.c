/* ########################################################################## */
/* globals.c                                                                  */
/* (C) Jean-Pierre Lozi, 2010-2011                                            */
/* (C) Gaël Thomas, 2010-2011                                                 */
/* -------------------------------------------------------------------------- */
/* ########################################################################## */
#include "globals.h"

#include <getopt.h>


/* ########################################################################## */
/* FIXME: some variables may not need to be declared volatile. We declare all */
/* global variables as volatile for now.                                      */
/* ########################################################################## */
/* Environment data ========================================================= */
/* Number of hw_threads */
volatile int g_number_of_hw_threads;
/* CPU frequency, in MHz */
volatile float g_cpu_frequency;
/* This array maps physical to virtual core IDs. */
volatile int *g_physical_to_virtual_hw_thread_id;
/* Has the server started? */
volatile int g_server_started;

/* Execution parameters ===================================================== */
/*
   Critical sections
 */
/* Critical sections can either be null RPCs (serviced by a single server) or
   lock acquisitions. */
volatile critical_sections_type_t g_critical_sections_type;
/* In the case of lock acquisitions, shall we use regular spinlocks or MCS? */
volatile int g_use_mcs;

/*
   Execution mode
 */
/* Three execution modes are available :
   - MULTIPLE_RUNS_AVERAGED means that the experiment is run multiple times and
   the averaged results are returned (along with the variance and standard
   deviation).
   - SINGLE_RUN_SAMPLED means that the experiment is only run once, and
   statistics are gathered during the execution.
   - SINGLE_RUN_ORDERED means that the experiment is only run once and that the
   order in which clients had their critical section executed is returned. */
volatile execution_mode_t g_execution_mode;
/* Number of runs over which the results are averaged (used in the
   MULTIPLE_RUNS_AVERAGED mode only). */
volatile int g_number_of_runs;
/* Number of iterations per sample (used in the SINGLE_RUN_SAMPLED only). */
volatile int g_number_of_iterations_per_sample;

/*
   Execution settings
 */
/* Core on which the server runs */
volatile int g_server_core;
/* Number of clients */
volatile int g_number_of_clients;
/* Number of iterations per client */
volatile int g_number_of_iterations_per_client;
/* Delay between RPCs, in cycles. */
volatile int g_delay;
/* ROW mode activated? */
volatile int g_read_only_wait;
/* Number of context variables */
volatile int g_number_of_context_variables;
/* Number of shared variables */
volatile int g_number_of_shared_variables;
/* shared area */
volatile uint64_t *g_shared_variables_memory_area;
/* Use one lock per client in RPC mode? */
volatile int g_use_multiple_locks;
/* Be NUMA-aware? */
volatile int g_numa_aware;
/* Skip measurements for the first critical section? */
volatile int g_skip_first_cs;
/* Number of clients done executing critical sections. */
volatile int g_number_of_finished_clients = 0;

/*
   Measurements
 */
/* Shall we count cycles, events or failed attempts? */
volatile measurement_metric_t g_measurement_metric;
/* Shall we measure events/cycles globally (around the experiment, divided by
 * the number of iterations), around the lock acquisition, or around the
 * critical section (including the lock/unlock functions)? This parameter is
 * ignored if the metric is MM_FAILED_ATTEMPTS. */
volatile measurement_type_t g_measurement_type;
/* ID of the monitored PAPI event if measurement_type = NUMBER_OF_EVENTS */
volatile int g_monitored_event_id;
volatile char *g_monitored_event_id_str;
/* Shall we perform the measurements on the server or the clients? */
volatile measurement_location_t g_measurement_location;
/* Should the results be averaged across clients? */
volatile int g_average_client_results;
/* Shall we return the result in throughput or cycles? */
volatile measurement_unit_t g_measurement_unit;
/* Randomized cache line accesses? */
volatile access_order_t g_access_order;
/* Monitoring type */
volatile monitoring_location_t g_monitoring_location;

/*
   Output settings
 */
volatile int g_compute_standard_deviation_and_variance;
/* End output with a newline? */
volatile int g_end_output_with_a_newline;
/* Verbose output? */
volatile int g_verbose;

/* Execution variables ====================================================== */
/* Adresses of the rpc_done addresses for each thread */
volatile void ** volatile g_rpc_done_addresses;
/* Synchronization variables for the barrier between the server and the
   clients, before the main loop */
volatile int * volatile g_ready;
/* Number of events */
volatile unsigned long long g_event_count;

/* Data specific to the MULTIPLE_RUNS_AVERAGED mode */
/* Results (cycles or number of events) for each iteration */
volatile double * volatile g_iteration_result;

/* Data specific to the SINGLE_RUN_SAMPLED mode */
/* Number of iterations per sample */
volatile float * volatile g_multiple_samples_results;
/* Address of the rpc_done variable from the core whose RPC was serviced last.
   Translated into the core ID at the end of the computation. */
volatile void ** volatile g_multiple_samples_rpc_done_addrs;

/* Data specific to the SINGLE_RUN_ORDERED mode */
/* Order in which the RPCs are processed */
volatile int * volatile g_order;
/* FIXME: should be merged with g_iteration_result? */
volatile double * volatile g_latencies;

/* FIXME: rename */
/* Permutation arrays for global variables */
volatile int *g_global_permutations_array;
/* Permutation arrays for local variables */
volatile int *g_local_permutations_array;

/* MCS lock head */
mcs_lock *g_mcs_m_ptr;

/* Barrier used to synchronize the clients */
pthread_barrier_t barrier;

/* Used by the GETHRTIME() macro. */
long long __base_gethrtime;

/* Libcpc Environment. */
#ifdef __sun__
cpc_t *rcw_cpc;
#endif

/* Mutexes and conditions used by non-blocking locks ======================== */
#ifdef USE_BLOCKING_LOCKS
pthread_mutex_t mutex_rpc_done_addr_not_null;
pthread_cond_t cond_rpc_done_addr_not_null;
pthread_mutex_t mutex_rpc_done_positive;
pthread_cond_t cond_rpc_done_positive;
pthread_mutex_t mutex_rpc_done_addr_null;
pthread_cond_t cond_rpc_done_addr_null;
#endif


