/* ########################################################################## */
/* util.h                                                                     */
/* (C) Jean-Pierre Lozi, 2010-2011                                            */
/* (C) Gaël Thomas, 2010-2011                                                 */
/* -------------------------------------------------------------------------- */
/* ########################################################################## */
#ifndef __UTIL_H
#define __UTIL_H

extern void *alloc(int size);
extern void get_cpu_info();
extern void generate_permutations_array(volatile int **permutations_array,
                                        int size);
extern void wrong_parameters_error(char *application_name);

#endif

