/* ########################################################################## */
/* server.c                                                                   */
/* (C) Jean-Pierre Lozi, 2010-2011                                            */
/* (C) Gaël Thomas, 2010-2011                                                 */
/* -------------------------------------------------------------------------- */
/* ########################################################################## */
#include "server.h"

#include <papi.h>
#include "benchmark.h"
#include "liblock-fatal.h"
#include "cs.h"
#include "util.h"


/******************************************************************************/
/* Function executed by the server                                            */
/******************************************************************************/
void *server_main(void *thread_arguments_block_pointer)
{
    int i, j;
    /* We avoid using global variables in the main loop to limit data cache
       misses. */
    const int number_of_clients = g_number_of_clients;
    const int number_of_iterations =
        g_number_of_iterations_per_client * g_number_of_clients;

    /* Sampling-related variables. */
    const int number_of_samples = number_of_iterations
        / g_number_of_iterations_per_sample;
    const int number_of_iterations_per_sample_m1 =
        g_number_of_iterations_per_sample - 1;
    int sample_start_cycles, sample_end_cycles;

    const int number_of_context_variables = g_number_of_context_variables;
    const int number_of_shared_variables = g_number_of_shared_variables;

    const int measurement_metric = g_measurement_metric;
    const int measurement_type = g_measurement_type;
    const int access_order = g_access_order;
    const int use_multiple_locks = g_use_multiple_locks;

    const int measurement_location = g_measurement_location;

    const int measurement_unit = g_measurement_unit;
    const int execution_mode = g_execution_mode;

    thread_arguments_block_t thread_arguments_block;

    volatile uint64_t * volatile *null_rpc_global_sv;

    volatile uint64_t *context_variables_global_memory_area,
             *shared_variables_memory_area;

#ifdef __linux__
    /* This variable is used to pin the thread to the right core. */
    cpu_set_t cpuset;
#endif

    /* PAPI variables */
#ifndef NO_PAPI
    int event_set = PAPI_NULL;
    int events[1];
#endif
    long long values[1];
    long long start_cycles = 0, end_cycles;

    int *local_permutations_array, *global_permutations_array;

#ifdef __linux__
    /* We pin this thread to the right core. */
    CPU_ZERO(&cpuset);
    CPU_SET(g_physical_to_virtual_hw_thread_id[g_server_core], &cpuset);
#endif

    /* TODO: free */
    local_permutations_array = alloc(number_of_context_variables *
            sizeof(int));

    for (i = 0 ; i < number_of_context_variables ; i++) {
        local_permutations_array[i] = g_local_permutations_array[i];
    }

    /* TODO: free */
    global_permutations_array = alloc(number_of_shared_variables * sizeof(int));

    for (i = 0 ; i < number_of_shared_variables ; i++) {
        global_permutations_array[i] = g_global_permutations_array[i];
    }

    thread_arguments_block =
        *((thread_arguments_block_t *)thread_arguments_block_pointer);

    null_rpc_global_sv = thread_arguments_block.null_rpc_global_sv;

    context_variables_global_memory_area =
        thread_arguments_block.context_variables_global_memory_area;
    shared_variables_memory_area =
        thread_arguments_block.shared_variables_memory_area;

#ifndef NO_PAPI
    /* Are we monitoring cycles/events on the server? */
    if (measurement_location == ML_SERVER)
    {
        /* If so, we will use PAPI in this thread. */
        PAPI_thread_init((unsigned long (*)(void))pthread_self);

        /* If we are counting events... */
        if (measurement_metric == MM_NUMBER_OF_EVENTS)
        {
            events[0] = g_monitored_event_id;

            /* We initialize the even set. */
            if (PAPI_create_eventset(&event_set) != PAPI_OK)
                fatal("PAPI_create_eventset");
            if (PAPI_add_events(event_set, events, 1) != PAPI_OK)
                fatal("PAPI_add_events");

            /* Starting and stopping PAPI once before measurements seems to
               improve accuracy. */
            if (PAPI_start(event_set) != PAPI_OK)
                fatal("PAPI_start");
            if (PAPI_stop(event_set, values) != PAPI_OK)
                fatal("PAPI_stop");
        }
    }
#endif

    /* We are ready. */
    g_ready[0] = 1;

    /* We want all the clients to be ready before we start servicing RPCs. */
    for (i = /*1*/0; i <= number_of_clients; i++)
        while (!g_ready[i]);

    PAUSE();
    // pthread_barrier_wait(&barrier);

    /* If the measurements take place on the server... */
    if (measurement_location == ML_SERVER &&
            measurement_type == MT_GLOBAL)
    {
        /* ...and if we're county cycles... */
        if (measurement_metric == MM_NUMBER_OF_CYCLES)
        {
            /* ...we get the current cycle count. */
            start_cycles = get_cycles();
        }
        else /* if (local_measurement_metric == MM_NUMBER_OF_EVENTS) */
        {
#ifndef NO_PAPI
            /* We start the event counter. */
            if (PAPI_start(event_set) != PAPI_OK)
                fatal("PAPI_start");
#endif
        }
    }

    j = 1;

    /* ###################################################################### */
    /* # Main loop (server)                                                 # */
    /* ###################################################################### */
    /* First implementation : using spinlocks. */
#ifndef USE_BLOCKING_LOCKS
    /* Sampling is off. */
    if (execution_mode != EM_SINGLE_RUN_SAMPLED)
    {
        /* We start answering RPCs. */
        for (i = 0; i < number_of_iterations; i++)
        {
            if (use_multiple_locks)
            {
                for(;;)
                {
                    for (j = 1; j <= number_of_clients; j++)
                    {
                        if (*((volatile uint64_t *)
                                    ((uintptr_t)null_rpc_global_sv + j * 2 *
                                     sizeof(cache_line_t))))
                            goto out;

                    }
                    PAUSE();
                }
            }
            else
            {

                /* No address received from a client? That means no RPC has been
                   requested. */
                while (!(*null_rpc_global_sv))
                    PAUSE();
            }

out:

            //__sync_synchronize();

            if (use_multiple_locks)
            {
                access_variables((volatile uint64_t *)
                        ((uintptr_t)context_variables_global_memory_area
                         + (j-1) * 2 * sizeof(cache_line_t)
                         * (number_of_context_variables + 1)),
                        1,
                        number_of_context_variables,
                        access_order,
                        local_permutations_array);
            }
            else
            {
                access_variables((volatile uint64_t *)(*null_rpc_global_sv),
                        1,
                        number_of_context_variables,
                        access_order,
                        local_permutations_array);
            }

            /* We access shared variables */
            access_variables(shared_variables_memory_area,
                    0,
                    number_of_shared_variables,
                    access_order,
                    global_permutations_array);

            //__sync_synchronize();

            if (use_multiple_locks)
            {
                *((volatile uint64_t *)
                        ((uintptr_t)null_rpc_global_sv
                         + j * 2 * sizeof(cache_line_t))) = 0;
            }
            else
            {
                /* We notify the client we are done with his RPC by setting the
                   variable it provided to i. */
                **null_rpc_global_sv = /*i + 1*/ 1;

                /* We are done with our RPC. */
                *null_rpc_global_sv = 0;
            }

            //__sync_synchronize();
        }
    }
    /* Sampling is on. */
    else
    {
        /* Outer loop: one iteration per sample. */
        for (j = 0; j < number_of_samples; j++)
        {
            /* Same as before, except now we get cycle statistics for each
               sample. */
            sample_start_cycles = get_cycles();

            for (i = 0; i < number_of_iterations_per_sample_m1; i++)
            {
                while (!(*null_rpc_global_sv))
                    PAUSE();

                access_variables((volatile uint64_t *)(*null_rpc_global_sv),
                        1,
                        number_of_context_variables,
                        access_order,
                        local_permutations_array);

                /* We access shared variables */
                access_variables(shared_variables_memory_area,
                        0,
                        number_of_shared_variables,
                        access_order,
                        global_permutations_array);

                **null_rpc_global_sv = /* i + 1 */ 1;
                *null_rpc_global_sv = 0;
            }

            while (!(*null_rpc_global_sv))
                PAUSE();

            access_variables((volatile uint64_t *)(*null_rpc_global_sv),
                    1,
                    number_of_context_variables,
                    access_order,
                    local_permutations_array);

            /* We access shared variables */
            access_variables(shared_variables_memory_area,
                    0,
                    number_of_shared_variables,
                    access_order,
                    global_permutations_array);


            **null_rpc_global_sv = /* i + 1 */ 1;

            sample_end_cycles = get_cycles();

            /* We need to know which core was serviced last. */
            g_multiple_samples_rpc_done_addrs[j] = *null_rpc_global_sv;

            if (measurement_type == MT_LOCK_ACQUISITIONS ||
                    measurement_type == MT_CRITICAL_SECTIONS)
            {
                /* FIXME */
                int k;

                /* FIXME: too slow, can be improved (if the client returns the
                 * pointer to a struct containing the right info) */
                k = 0;

                /*
                   for (k = 0; k < number_of_clients; k++)
                   {
                   if (multiple_samples_rpc_done_addrs[j] ==
                   rpc_done_addresses[k])
                   break;
                   }
                 */

                g_multiple_samples_results[j] = g_latencies[k];
            }
            else
            {
                /* We save the results. */
                g_multiple_samples_results[j] =
                    (double)(sample_end_cycles - sample_start_cycles)
                    / g_number_of_iterations_per_sample; /* FIXME: g */
            }

            *null_rpc_global_sv = 0;
        }
    }
    /* Second implementation : using blocking locks. Deprecated. */
#else
    /* We start answering RPCs. */
    for (i = 0; i < number_of_iterations; i++)
    {
        /* No address received from a client? That means no RPC has been
           requested. */
        pthread_mutex_lock(&mutex_rpc_done_addr_not_null);
        while (!(*null_rpc_global_sv))
            pthread_cond_wait(&cond_rpc_done_addr_not_null,
                    &mutex_rpc_done_addr_not_null);
        pthread_mutex_unlock(&mutex_rpc_done_addr_not_null);

        /* We notify the client we are done with his RPC by setting the
           variable it provided to i. */
        pthread_mutex_lock(&mutex_rpc_done_positive);
        **null_rpc_global_sv = i;
        pthread_cond_signal(&cond_rpc_done_positive);
        pthread_mutex_unlock(&mutex_rpc_done_positive);

        /* We are done with our RPC. */
        pthread_mutex_lock(&mutex_rpc_done_addr_null);
        *null_rpc_global_sv = 0;
        pthread_cond_signal(&cond_rpc_done_addr_null);
        pthread_mutex_unlock(&mutex_rpc_done_addr_null);
    }
#endif
    /* ###################################################################### */
    /* # End                                                                # */
    /* ###################################################################### */

    /* We gather statistics if measurements are to take place on the server. */
    if (measurement_location == ML_SERVER)
    {
        /* Are we counting cycles? */
        if (measurement_metric == MM_NUMBER_OF_CYCLES)
        {
            /* If so, get the current cycle count. */
            end_cycles = get_cycles();

            if (measurement_unit != MU_TOTAL_CYCLES_MAX)
            {
                /* We return the number of cycles per RPC. */
                g_iteration_result[0] =
                    (double)(end_cycles - start_cycles) / number_of_iterations;
            }
            else
            {
                g_iteration_result[0] = (double)(end_cycles - start_cycles);
            }
        }
        /* We're counting events. */
        else /* if (local_measurement_metric == MM_NUMBER_OF_EVENTS) */
        {
#ifndef NO_PAPI
            /* Otherwise, we were counting events, therefore, we read the number
               of events. */
            if (PAPI_stop(event_set, values) != PAPI_OK)
                fatal("PAPI_stop");
#endif

            /* We return the number of events. */
            g_iteration_result[0] = (double)values[0] / number_of_iterations;
        }
    }

#ifndef NO_PAPI
    PAPI_unregister_thread();
#endif

    return NULL;
}

