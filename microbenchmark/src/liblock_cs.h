/* ########################################################################## */
/* liblock_cs.h                                                               */
/* (C) Jean-Pierre Lozi, 2010-2011                                            */
/* (C) Gaël Thomas, 2010-2011                                                 */
/* -------------------------------------------------------------------------- */
/* ########################################################################## */
#ifndef __LIBLOCK_CS_H
#define __LIBLOCK_CS_H

#include <stdint.h>
#include "liblock.h"


extern const char*          g_liblock_name;
extern liblock_lock_t       g_liblock_lock;

extern __thread volatile uint64_t *t_shared_variables_memory_area;
extern __thread int t_number_of_context_variables;
extern __thread int t_number_of_shared_variables;
extern __thread int t_access_order;
extern __thread int *t_local_permutations_array;
extern __thread int *t_global_permutations_array;


extern void* liblock_cs(void *context_variables_local_memory_area);
extern void * liblock_cs_g1l0_dependent(void *);
extern void * liblock_cs_g5l0_dependent(void *);
extern void server_started();
extern struct hw_thread* get_core(unsigned int physical_core);

#endif

