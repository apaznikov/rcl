/* ########################################################################## */
/* globals.h                                                                  */
/* (C) Jean-Pierre Lozi, 2010-2011                                            */
/* (C) Gaël Thomas, 2010-2011                                                 */
/* ########################################################################## */
#ifndef __GLOBALS_H
#define __GLOBALS_H

#include <pthread.h>
#include <stdint.h>
#ifdef __sun__
#include <libcpc.h>
#endif
#include "benchmark.h"
#include "mcs_lock.h"


/* ########################################################################## */
/* FIXME: some variables may not need to be declared volatile. We declare all */
/* global variables as volatile for now.                                      */
/* ########################################################################## */
/* Environment data ========================================================= */
/* Number of hw_threads */
extern volatile int g_number_of_hw_threads;
/* CPU frequency, in MHz */
extern volatile float g_cpu_frequency;
/* This array maps physical to virtual core IDs. */
extern volatile int *g_physical_to_virtual_hw_thread_id;
/* Has the server started? */
extern volatile int g_server_started;

/* Execution parameters ===================================================== */
/*
   Critical sections
 */
/* Critical sections can either be null RPCs (serviced by a single server) or
   lock acquisitions. */
typedef enum _critical_sections_type {
    CST_NULL_RPCS,
    CST_LOCK_ACQUISITIONS,
    CST_LIBLOCK
} critical_sections_type_t;
extern volatile critical_sections_type_t g_critical_sections_type;
/* In the case of lock acquisitions, shall we use regular spinlocks or MCS? */
extern volatile int g_use_mcs;

/*
   Execution mode
 */
/* Three execution modes are available :
   - MULTIPLE_RUNS_AVERAGED means that the experiment is run multiple times and
   the averaged results are returned (along with the variance and standard
   deviation).
   - SINGLE_RUN_SAMPLED means that the experiment is only run once, and
   statistics are gathered during the execution.
   - SINGLE_RUN_ORDERED means that the experiment is only run once and that the
   order in which clients had their critical section executed is returned. */
typedef enum _execution_mode_t {
    EM_MULTIPLE_RUNS_AVERAGED,
    EM_SINGLE_RUN_SAMPLED,
    EM_SINGLE_RUN_ORDERED
} execution_mode_t;
extern volatile execution_mode_t g_execution_mode;
/* Number of runs over which the results are averaged (used in the
   MULTIPLE_RUNS_AVERAGED mode only). */
extern volatile int g_number_of_runs;
/* Number of iterations per sample (used in the SINGLE_RUN_SAMPLED only). */
extern volatile int g_number_of_iterations_per_sample;

/*
   Execution settings
 */
/* Core on which the server runs */
extern volatile int g_server_core;
/* Number of clients */
extern volatile int g_number_of_clients;
/* Number of iterations per client */
extern volatile int g_number_of_iterations_per_client;
/* Delay between RPCs, in cycles. */
extern volatile int g_delay;
/* ROW mode activated? */
extern volatile int g_read_only_wait;
/* Number of context variables */
extern volatile int g_number_of_context_variables;
/* Number of shared variables */
extern volatile int g_number_of_shared_variables;
/* shared area */
extern volatile uint64_t *g_shared_variables_memory_area;
/* Use one lock per client in RPC mode? */
extern volatile int g_use_multiple_locks;
/* Be NUMA-aware? */
extern volatile int g_numa_aware;
/* Skip measurements for the first critical section? */
extern volatile int g_skip_first_cs;
/* Number of clients done executing critical sections. */
extern volatile int g_number_of_finished_clients;

/*
   Measurements
 */
/* Shall we count cycles, events or failed attempts? */
typedef enum _measurement_metric {
    MM_NUMBER_OF_CYCLES,
    MM_NUMBER_OF_EVENTS,
    MM_FAILED_ATTEMPTS
} measurement_metric_t;
extern volatile measurement_metric_t g_measurement_metric;
/* Shall we measure events/cycles globally (around the experiment, divided by
 * the number of iterations), around the lock acquisition, or around the
 * critical section (including the lock/unlock functions)? This parameter is
 * ignored if the metric is MM_FAILED_ATTEMPTS. */
typedef enum _measurement_type {
    MT_GLOBAL,
    MT_LOCK_ACQUISITIONS,
    MT_CRITICAL_SECTIONS
} measurement_type_t;
extern volatile measurement_type_t g_measurement_type;
/* ID of the monitored PAPI event if measurement_type = NUMBER_OF_EVENTS */
extern volatile int g_monitored_event_id;
extern volatile char *g_monitored_event_id_str;
/* Shall we perform the measurements on the server or the clients? */
typedef enum _measurement_location {
    ML_SERVER,
    ML_CLIENTS
} measurement_location_t;
extern volatile measurement_location_t g_measurement_location;
/* Should the results be averaged across clients? */
extern volatile int g_average_client_results;
/* Shall we return the result in throughput or cycles? */
/* FIXME: doesn't make much sense. Should be split. */
typedef enum _measurement_unit {
    MU_THROUGHPUT,
    MU_CYCLES_PER_ITERATION,
    MU_TOTAL_CYCLES_MAX /* FIXME: not a unit. */
} measurement_unit_t;
extern volatile measurement_unit_t g_measurement_unit;
/* Order in which global and local variables are accessed. */
typedef enum _access_order {
    AO_SEQUENTIAL,
    AO_DEPENDENT,
    AO_RANDOM,
    AO_CUSTOM_RANDOM,
    AO_PERMUTATIONS
} access_order_t;
/* Randomized cache line accesses? */
extern volatile access_order_t g_access_order;
/* Monitoring type */
typedef enum _monitoring_location {
    ML_PER_NODE,
    ML_PER_CORE
} monitoring_location_t;
extern volatile monitoring_location_t g_monitoring_location;

/*
   Output settings
 */
extern volatile int g_compute_standard_deviation_and_variance;
/* End output with a newline? */
extern volatile int g_end_output_with_a_newline;
/* Verbose output? */
extern volatile int g_verbose;

/* Execution variables ====================================================== */
/* Adresses of the rpc_done addresses for each thread */
extern volatile void ** volatile g_rpc_done_addresses;
/* Synchronization variables for the barrier between the server and the
   clients, before the main loop */
extern volatile int * volatile g_ready;
/* Number of events */
extern volatile unsigned long long g_event_count;

/* Data specific to the MULTIPLE_RUNS_AVERAGED mode */
/* Results (cycles or number of events) for each iteration */
extern volatile double * volatile g_iteration_result;

/* Data specific to the SINGLE_RUN_SAMPLED mode */
/* Number of iterations per sample */
extern volatile float * volatile g_multiple_samples_results;
/* Address of the rpc_done variable from the core whose RPC was serviced last.
   Translated into the core ID at the end of the computation. */
extern volatile void ** volatile g_multiple_samples_rpc_done_addrs;

/* Data specific to the SINGLE_RUN_ORDERED mode */
/* Order in which the RPCs are processed */
extern volatile int * volatile g_order;
/* FIXME: should be merged with g_iteration_result? */
extern volatile double * volatile g_latencies;

/* FIXME: rename */
/* Permutation arrays for global variables */
extern volatile int *g_global_permutations_array;
/* Permutation arrays for local variables */
extern volatile int *g_local_permutations_array;

/* MCS lock head */
extern mcs_lock *g_mcs_m_ptr;

/* Barrier used to synchronize the clients */
extern pthread_barrier_t barrier;

/* Used by the GETHRTIME() macro. */
extern long long __base_gethrtime;

/* Libcpc Environment. */
#ifdef __sun__
extern cpc_t *rcw_cpc;
#endif

/* Mutexes and conditions used by non-blocking locks ======================== */
#ifdef USE_BLOCKING_LOCKS
extern pthread_mutex_t mutex_rpc_done_addr_not_null;
extern pthread_cond_t cond_rpc_done_addr_not_null;
extern pthread_mutex_t mutex_rpc_done_positive;
extern pthread_cond_t cond_rpc_done_positive;
extern pthread_mutex_t mutex_rpc_done_addr_null;
extern pthread_cond_t cond_rpc_done_addr_null;
#endif


/* ########################################################################## */
/* Type definitions                                                           */
/* ########################################################################## */
/* This structure models a cache line. It should be allocated on a 64-byte
   boundary. */
typedef char cache_line_t[CACHE_LINE_SIZE];

/*
   This structure models the data passed to threads upon their creation.
   Variables for both the 'NULL RPCs' and 'lock acquisitions' modes are passed
   in order to simplify the code.
 */
typedef struct _thread_arguments_block_t {
    /* Logical thread id (local, not provided by pthreads). */
    int id;

    /* Physical core id. */
    int client_core;

    /* 'NULL RPCs' mode synchronization variables =========================== */
    /*
       Global synchronization variable used to limit the number of NULL RPCs
       serviced concurrently to 1. Its value can either be:
       - 0 if no RPC is requested.
       - The address to a client-bound variable that shall be set to 1 upon
       completion of the RPC otherwise.
     */
    volatile uint64_t *volatile *null_rpc_global_sv;
    /*
       Local synchronization variable set to a >= 0 value by the server to
       signal the completion of a RPC.
     */
    volatile uint64_t *null_rpc_local_sv;

    /* 'Lock acquisitions' mode synchronization variables =================== */
    /*
       Global lock repeatedly acquired by all clients.
     */
    volatile uint64_t *lock_acquisitions_global_sv;

    volatile uint64_t *context_variables_global_memory_area;
    volatile uint64_t *shared_variables_memory_area;
} thread_arguments_block_t;


/* ########################################################################## */
/* Prototypes                                                                 */
/* ########################################################################## */
extern void *client_main(void *thread_arguments_block);
extern void *server_main(void *thread_arguments_block);

#endif

