/* ########################################################################## */
/* benchmark.h                                                                */
/* (C) Jean-Pierre Lozi, 2010-2011                                            */
/* (C) Gaël Thomas, 2010-2011                                                 */
/* -------------------------------------------------------------------------- */
/* This program should be compiled with the -O0 compiler flag.                */
/* ########################################################################## */
#ifndef __BENCHMARK_H
#define __BENCHMARK_H

#include <pthread.h>
#include <stdint.h>
#include "liblock.h"
#include "mcs_lock.h"


/* ########################################################################## */
/* Definitions                                                                */
/* ########################################################################## */
/*
 *** DEPRECATED ***

 Use blocking locks?

 This macro is experimental. When it is defined, the benchmark uses blocking
 locks. Only certain configurations are supported, however (no sampling,
 for instance).
 */
/* #define USE_BLOCKING_LOCKS */

/* Default values for the command-line arguments. */
#define DEFAULT_NUMBER_OF_RUNS 1
#define DEFAULT_NUMBER_OF_ITERATIONS_PER_SAMPLE 10
#define DEFAULT_SERVER_CORE 0
#define DEFAULT_NUMBER_OF_ITERATIONS_PER_CLIENT 1000
#define DEFAULT_NUMBER_OF_CONTEXT_VARIABLES 0
#define DEFAULT_NUMBER_OF_SHARED_VARIABLES 0

/* Maximum number of hw_threads, used to avoid malloc/realloc cycles. */
#define MAX_NUMBER_OF_CORES 1024

/* Maximum line size used when reading results from unix commands. */
#define MAX_LINE_SIZE 32

/* This macro performs the conversion between cycles and throughput. */
#define TO_THROUGHPUT(x) (g_cpu_frequency * 1000000 / (x))


#if (defined(__i386__) || defined(__x86_64__))
__attribute__((always_inline))
inline long long get_cycles(void)
{
    long long ret = 0;

#ifdef __x86_64__
/*
    unsigned int a, d;

    asm volatile ( "rdtsc":"=a" (a), "=d" (d));
    ret = ((long long)a) | (((long long)d) << 32);
*/
    uint64_t rax, rdx;
    uint32_t aux;

    asm volatile ( "rdtscp\n" : "=a" (rax), "=d" (rdx), "=c" (aux): : );
    ret = (rdx << 32) + rax;
#else
    __asm__ __volatile__("rdtsc" : "=A"(ret):);
#endif

    return ret;
}
#elif defined(__sparc__)
__attribute__((always_inline))
inline long long get_cycles(void)
{
/*
 * Implementation #1
 *
 * Doesn't work properly on 32-bit sparc.
 *

    register unsigned long ret asm("g1");

    asm volatile(".word 0x83410000" // rd %tick, %g1
                 : "=r"(ret));

    return ret;
*/

/*
 * Implementation #2
 *
 * Probably induces an overhead.
 *
    return gethrtime() * frequency;
*/


/*
 * Implementation #3
 *
 * Works well on 32-bit sparc. Implementation #1 might be better for 64-bit
 * sparc, though.
 *
 */
    union {
        uint64_t v64;
        struct {
            uint32_t high;
            uint32_t low;
        } v32;
    } ret;

    asm volatile("rd %%tick, %%g1; srlx %%g1, 32, %0; mov %%g1, %1"
                 : "=r"(ret.v32.high), "=r"(ret.v32.low) : : "g1");

    return ret.v64;
}
#else
#error Unsupported architecture.
#endif

#endif

