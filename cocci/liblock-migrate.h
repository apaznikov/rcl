#ifndef LIBLOCK_CONFIG
#define LIBLOCK_CONFIG
#define TYPE_POSIX "posix"
#define DEFAULT_ARG NULL
#define TYPE_NOINFO TYPE_POSIX
#define ARG_NOINFO DEFAULT_ARG
#endif

// ../migrate/heating.c:73
// Eight_cores
// mutex 1 	locked 136380517 	cont 28422491

#define TYPE_MIGRATE_1 TYPE_POSIX
#define ARG_MIGRATE_1 DEFAULT_ARG

// ../migrate/heating.c:75
// Eight_cores
// mutex 2 	locked 20077360 	cont 758871

#define TYPE_MIGRATE_2 TYPE_POSIX
#define ARG_MIGRATE_2 DEFAULT_ARG

