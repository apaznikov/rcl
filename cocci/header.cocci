// --------------------------------------------------------------------
// before any transformation

@getfile@
identifier virtual.pre_mutex_lock;
identifier virtual.pre_mutex_unlock;
position p;
@@

(
pre_mutex_lock@p(...)
|
pre_mutex_unlock@p(...)
|
pthread_create@p(...)
|
pthread_cond_wait@p(...)
|
pthread_cond_timedwait@p(...)
|
pthread_cond_signal@p(...)
|
pthread_cond_broadcast@p(...)
|
pthread_mutex_destroy@p(...)
)

@tgetfile@
typedef pthread_mutex_t;
@@

pthread_mutex_t

@script:ocaml@
p << getfile.p;
@@

    let fl = (List.hd p).file in
    if Filename.check_suffix fl ".c"
    then cfile := fl

// --------------------------------------------------------------------

@script:ocaml include_help depends on getfile || tgetfile@
full_project_header_file << virtual.full_project_header_file;
pinc;
@@
pinc := Printf.sprintf "#include <%s>\n//" full_project_header_file

@@
identifier include_help.pinc;
@@

#include <...>
+#include <liblock.h>
+pinc;
+#include <stdint.h>
