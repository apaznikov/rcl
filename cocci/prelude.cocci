// pre check

// prevent later parsing problems due to a label with no following statement
@@
identifier virtual.mutex_unlock;
identifier l;
@@

l:
+{}
mutex_unlock(...);

@lock_without_unlock exists@
identifier virtual.mutex_lock;
identifier virtual.mutex_unlock;
expression list e;
position p;
@@

mutex_lock@p(e)
... when != mutex_unlock(e)

@script:ocaml depends on !force@
e << lock_without_unlock.e;
p << lock_without_unlock.p;
@@

let line = (List.hd p).line in
let file = (List.hd p).file in
let file =
  if Str.string_match (Str.regexp_string "/tmp/") file 0
  then !cfile
  else file in
Printf.printf "lock on line %d in file %s has no unlock\n%s\n" line file e;
flush stdout;
Coccilib.exit()

@script:ocaml depends on force@
e << lock_without_unlock.e;
p << lock_without_unlock.p;
@@

let line = (List.hd p).line in
let file = (List.hd p).file in
let file =
  if Str.string_match (Str.regexp_string "/tmp/") file 0
  then !cfile
  else file in
Printf.printf "lock on line %d in file %s has no unlock\n%s\n" line file e;
flush stdout

@fix_lock_with_no_unlock@
identifier virtual.mutex_lock;
position lock_without_unlock.p;
expression e;
@@

- mutex_lock@p(e)
+ liblock_relock_in_cs(e)

@unlock_without_lock exists@
identifier virtual.mutex_lock;
identifier virtual.mutex_unlock;
expression list e;
position p;
@@

... when != mutex_lock(e)
mutex_unlock@p(e)

@script:ocaml depends on !force@
p << unlock_without_lock.p;
@@

let line = (List.hd p).line in
let file = (List.hd p).file in
let file =
  if Str.string_match (Str.regexp_string "/tmp/") file 0
  then !cfile
  else file in
Printf.printf "unlock on line %d in file %s has no lock\n" line file;
flush stdout;
Coccilib.exit()

@script:ocaml depends on force@
p << unlock_without_lock.p;
@@

let line = (List.hd p).line in
let file = (List.hd p).file in
let file =
  if Str.string_match (Str.regexp_string "/tmp/") file 0
  then !cfile
  else file in
Printf.printf "unlock on line %d in file %s has no lock\n" line file;
flush stdout

@@
identifier virtual.mutex_unlock;
position unlock_without_lock.p;
expression e;
@@

- mutex_unlock@p(e)
+ liblock_unlock_in_cs(e)

// ------------------------------------------------------------------

// give each critical section its own name

@ml@
identifier virtual.mutex_lock;
position pml;
@@

mutex_lock@pml(...)

@mu@
identifier virtual.mutex_unlock;
position pmu;
@@

mutex_unlock@pmu(...)

// find a block with a unique start and end

@unique@
position p1,p2,ml.pml,mu.pmu;
expression e;
identifier virtual.mutex_unlock;
identifier virtual.mutex_lock;
identifier virtual.good1;
identifier virtual.good2;
@@

 mutex_lock@p1@pml(e)
<... when strict
    when != e
(
good1(...,e,...)
|
good2(...,e,...)
|
mutex_lock(e)
)
...>
mutex_unlock@p2@pmu(e)

@build@
fresh identifier fn = "function";
fresh identifier inst = "instance";
fresh identifier input = "input";
fresh identifier output = "output";
fresh identifier incontext = "incontext";
fresh identifier outcontext = "outcontext";
fresh identifier ctx = "ctx";
position unique.p1,unique.p2;
identifier virtual.mutex_unlock;
identifier mutex_lock;
expression lock;
@@

// in the following, the code incontext(outcontext(ctx)) just serves to
// save the metavariables 

+ {union inst {
+   struct input { int __nothing__; } input;
+   struct output { int __nothing__; } output; } inst = {{ 0, }};
  mutex_lock@p1(
- lock
+ incontext(outcontext(ctx))
 );
+{
...
+}
mutex_unlock@p2(
-lock
+ctx
 );
+ liblock_exec(lock,&fn,&inst);}

@script:ocaml debug depends on debug_mode@
inst << build.inst;
fn << build.fn;
current;
@@
current := Printf.sprintf "\"%s\"" fn

@depends on debug_mode@
expression lock;
identifier build.inst, build.fn, debug.current;
@@

(
+printf("started cs %s\n",current);
 union inst { ... } inst = ...;
|
 liblock_exec(lock,&fn,&inst);
+printf("ended cs %s\n",current);
)

// ------------------------------------------------------------------------

@transformedl@
identifier virtual.mutex_lock, build.inst;
position p;
@@

union inst { ... } inst = ...;
mutex_lock@p(...);

@oopsl exists@
identifier virtual.mutex_lock;
position p!=transformedl.p;
expression e;
identifier f;
@@

f(...) { <+...
mutex_lock@p(e);
...+> }

@script:ocaml depends on !force@
p << oopsl.p;
f << oopsl.f;
e << oopsl.e;
@@

let line = (List.hd p).line in
let file = (List.hd p).file in
let file =
  if Str.string_match (Str.regexp_string "/tmp/") file 0
  then !cfile
  else file in
Printf.printf "lock on line %d in function %s in file %s\n%s\n" line f file e;
flush stdout;
Coccilib.exit()

@script:ocaml depends on force@
p << oopsl.p;
f << oopsl.f;
@@

let line = (List.hd p).line in
let file = (List.hd p).file in
let file =
  if Str.string_match (Str.regexp_string "/tmp/") file 0
  then !cfile
  else file in
Printf.printf "lock on line %d in function %s in file %s\n" line f file;
flush stdout

@@
identifier virtual.mutex_lock;
position oopsl.p;
expression e;
@@

- mutex_lock@p(e)
+ liblock_relock_in_cs(e)

@transformedu@
identifier virtual.mutex_unlock;
position p;
@@

mutex_unlock@p(...);
liblock_exec(...);

@@
identifier virtual.mutex_lock;
position p != transformedu.p;
expression e;
@@

- mutex_unlock@p(e)
+ liblock_unlock_in_cs(e)
