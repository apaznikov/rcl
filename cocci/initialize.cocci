virtual use_api
virtual force
virtual specific_locks
virtual debug_mode
virtual structure_info

@initialize:ocaml@

let cfile = ref "" (* set in header.cocci *)

let safe_int_of_string str =
  try int_of_string str
  with _ -> failwith ("bad int_of_string "^str)

type rd = READ | WRITE | READWRITE
let read_write_table =
  (Hashtbl.create(101) :
     ((string * string * string),(string list ref * rd ref)) Hashtbl.t)
let structure_output = ref ""

let all_mutexes =
  ref (None : ((string (*file*) * int (*line*)) * int (*num*)) list option)

let make_init_path = function
    "" -> "./"
  | s ->
      let len = String.length s in
      if String.get s (len-1) = '/'
      then String.sub s 0 (len-1)
      else s^"/"

let upcase s =
  String.uppercase
    (String.concat "_"
      (Str.split (Str.regexp "-")
        (String.concat "_" (Str.split (Str.regexp_string ".") s))))

let parse_profile i init_path header_file project =
  let init_path = make_init_path init_path in
  let info = ref [] in
  let partial_path = ref ("",None) in
  let partial_mutex = ref [] in
  let mutexes = ref [] in
  let stats = ref [] in
  let start = Str.regexp_string "==================================" in
  let init = Str.regexp_string "pthread_mutex_init" in
  let rec find_start _ =
    let l = input_line i in
    if Str.string_match start l 0
    then
      (let path = input_line i in
      let l = input_line i in
      if Str.string_match start l 0
      then
	begin
	  (match Str.split (Str.regexp " : ") path with
	    [path;aux] -> partial_path := (path,Some aux)
	  | [path] -> partial_path := (path,None)
	  | _ -> failwith ("unexpected path format: "^path));
	  mutex_info false
	end
      else failwith "expected double row of =s")
    else find_start()
  and mutex_info seen_mutexes =
    let l = input_line i in
    match Str.split (Str.regexp " ") l with
      "Mutex"::rest::_ ->
	let mutex_number =
	  List.hd
	    (Str.split (Str.regexp "#")
	       (List.hd (Str.split (Str.regexp " ") rest))) in
	mutexes := mutex_number::!mutexes;
	let call = input_line i in
	(match Str.split init call with
	  [before;after] ->
	    let real_call = input_line i in
	    (match Str.split (Str.regexp ":") real_call with
	      [before_and_file;line_and_after] ->
		(match Str.split (Str.regexp "\t") before_and_file with
		  [file] ->
		    (match Str.split (Str.regexp "\t") line_and_after with
		      line::_ ->
			partial_mutex :=
			  (mutex_number,
			   ((init_path ^ (fst !partial_path) ^ "/" ^ file),
			    (snd !partial_path)),
			   (safe_int_of_string line) - 1) :: // !!!!! -1
			  !partial_mutex;
			mutex_info true
		    | _ -> failwith "expected file and line")
		| _ -> failwith "expected file and line")
	    | _ -> failwith "expected file and line")
	| _ ->
	    Printf.fprintf stderr
	      "warning: mutex %s in %s not created with init\n"
	      mutex_number (fst !partial_path);
	    mutex_info seen_mutexes)
    | "mutrace:"::rest::_ when seen_mutexes ->
	let _blank = input_line i in
	let _legend = input_line i in
	mutex_stats ()
    | _ -> mutex_info seen_mutexes
  and mutex_stats _ =
    let l = input_line i in
    match Str.split (Str.regexp " +")  l with
      [mutex;locked;changed;cont;ttime;atime;mtime;_;flags] (*l2*)
      | [mutex;locked;changed;cont;ttime;atime;mtime;flags] ->
Printf.fprintf stderr "found someinformation\n";
	if List.mem mutex !mutexes
	then
	  begin
	    stats :=
              (mutex,safe_int_of_string locked,safe_int_of_string cont) ::
	      !stats;
	    mutex_stats()
	  end
	else restart()
    | _ -> Printf.printf "bad line %s\n" l; restart()
  and restart _ =
    let mutex_info =
      List.sort compare
	(List.map
	   (function (mutex_number, file, line) ->
	     let stats =
	       List.filter
		 (function (mutex_number1,locked,cont) ->
		   mutex_number = mutex_number1)
		 !stats in
	     match stats with
	       [stats] -> ((file,line),stats)
	     | _ -> failwith "no stats for mutex")
	   !partial_mutex) in
    let mutex_info =
      let rec loop = function
	  [] -> []
	| (location,mutex_and_stat)::xs ->
	    (match loop xs with
	      (location1,mutexes_and_stats)::rest when location = location1 ->
		(location1,mutex_and_stat::mutexes_and_stats)::rest
	    | rest -> (location,[mutex_and_stat]) :: rest) in
      loop mutex_info in
    let mutex_info =
      List.map
	(function (location,mutex_and_stat) ->
	  (location,
	   List.rev
	     (List.sort
		(function (mutex_number,locked,cont) ->
		  function (mutex_number_a,locked_a,cont_a) ->
		    compare cont cont_a)
		mutex_and_stat)))
	mutex_info in
    (if List.mem project (Str.split (Str.regexp "/") (fst !partial_path))
    then info := (!partial_path,mutex_info) :: !info);
    partial_path := ("",None);
    partial_mutex := [];
    mutexes := [];
    stats := [];
    find_start() in
  try find_start()
  with End_of_file ->
    (match (!partial_path,!mutexes,!stats) with
      (("",None),[],[]) ->
	let ctr = ref 0 in
	let o = open_out header_file in
	Printf.fprintf o "#ifndef LIBLOCK_CONFIG\n";
	Printf.fprintf o "#define LIBLOCK_CONFIG\n";
	Printf.fprintf o "#define TYPE_POSIX \"posix\"\n";
	Printf.fprintf o "#define DEFAULT_ARG NULL\n";
	Printf.fprintf o "#define TYPE_NOINFO TYPE_POSIX\n";
	Printf.fprintf o "#define ARG_NOINFO DEFAULT_ARG\n";
	Printf.fprintf o "#endif\n\n";
	let info = List.concat (snd (List.split !info)) in
	let info =
	  List.map
	    (function (((file,extra),line),mutex_and_stat) ->
	      ((file,line),(extra,mutex_and_stat)))
	    info in
	let info = List.sort compare info in
	let info =
	  let rec loop = function
	      [] -> []
	    | ((file,line),mutex_and_stat)::rest ->
		match loop rest with
		  ((filea,linea),mutex_and_stata)::resta
		  when file = filea && line = linea ->
		    ((filea,linea),mutex_and_stat::mutex_and_stata)::resta
		| resta -> ((file,line),[mutex_and_stat])::resta in
	  loop info in
	let info =
	  List.map
	    (function ((file,line),mutex_and_stats) ->
	      ctr := !ctr + 1;
	      Printf.fprintf o "// %s:%d\n" file line;
	      let print_rest mutex_and_stat =
		List.iter
		(function (mutex_number,locked,cont) ->
		  Printf.fprintf o
		    "// mutex %s \tlocked %d \tcont %d\n"
		    mutex_number locked cont)
		mutex_and_stat in
	      List.iter
		(function
		    (Some extra,mutex_and_stat) ->
		      Printf.fprintf o "// %s\n" extra;
		      print_rest mutex_and_stat
		  | (None,mutex_and_stat) ->
		      print_rest mutex_and_stat)
		mutex_and_stats;
	      let project = upcase project in
	      Printf.fprintf o
		"\n#define TYPE_%s_%d TYPE_POSIX\n" project !ctr;
	      Printf.fprintf o
		"#define ARG_%s_%d DEFAULT_ARG\n\n" project !ctr;
	      ((file,line),!ctr))
	    info in
	close_out o;
	all_mutexes := Some info;
	info
    | _ -> failwith "incomplete information")
