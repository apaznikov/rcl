// @@
// typedef DB;
// expression a;
// DB *b;
// @@

// (
// - MUTEX_LOCK(a,b->mutex);
// + if (b->mutex != MUTEX_INVALID) {DB_MUTEX *mutexp = MUTEXP_SET(a,b->mutex); 
// + pthread_mutex_lock(&mutexp->u.m.mutex);}
// |
// - MUTEX_UNLOCK(a,b->mutex);
// + if (b->mutex != MUTEX_INVALID) {DB_MUTEX *mutexp = MUTEXP_SET(a,b->mutex); 
// + pthread_mutex_unlock(&mutexp->u.m.mutex);}
// )


@@
typedef DBC;
expression ENV;
DBC * DBP;
@@

(
- MUTEX_LOCK(ENV,DBP->dbp->mutex);
+ MUTEX_LOCK_MOD(ENV,DBP->dbp->mutex);
|
- MUTEX_UNLOCK(ENV,DBP->dbp->mutex);
+ MUTEX_UNLOCK_MOD(ENV,DBP->dbp->mutex);
)


@@
typedef DB;
typedef FNAME;
expression ENV;
{DB *,FNAME *} DBP;
@@

(
- MUTEX_LOCK(ENV,DBP->mutex);
+ MUTEX_LOCK_MOD(ENV,DBP->mutex);
|
- MUTEX_UNLOCK(ENV,DBP->mutex);
+ MUTEX_UNLOCK_MOD(ENV,DBP->mutex);
)

@r@
position p;
expression e,a;
identifier x;
@@

MUTEX_LOCK@p(e,<+...a->x->mutex...+>)

@script:python@
p << r.p;
@@

cocci.print_main("",p)

@s@
position p;
{DB *,FNAME *} a;
DBC *b;
expression e;
@@

(
e = a->mutex@p
|
e = b->dbp->mutex@p
)

@script:python@
p << s.p;
@@

cocci.print_main("",p)
