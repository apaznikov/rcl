@pre_field_write depends on structure_info exists@
identifier build.fn, build.inst, build.input, build.output;
type T;
T e;
{union inst,struct input,struct output} e1;
identifier fld;
position p;
expression e2;
@@

fn(...) { <...
(
 e1.fld=e2
|
 e.fld@p=e2
) ...>
 }

@field_write exists@
position pre_field_write.p;
type T;
T e;
identifier fld;
identifier fn;
@@

fn(...) { ... when any
 e.fld@p ... when any
 }

@writelockname@
expression lock;
identifier field_write.fn;
expression e;
@@

liblock_exec(lock, &fn, e);

// ---------------------------------------------------------------------

@pre_field_readwrite depends on structure_info exists@
identifier build.fn, build.inst, build.input, build.output;
type T;
T e;
{union inst,struct input,struct output} e1;
identifier fld;
position p;
expression e2;
@@

fn(...) { <...
(
 e1.fld+=e2
|
 e1.fld-=e2
|
 e1.fld++
|
 e1.fld--
|
 e.fld@p+=e2
|
 e.fld@p-=e2
|
 e.fld@p++
|
 e.fld@p--
) ...>
 }

@field_readwrite exists@
position pre_field_readwrite.p;
type T;
T e;
identifier fld;
identifier fn;
@@

fn(...) { ... when any
 e.fld@p ... when any
 }

@readwritelockname@
expression lock;
identifier field_readwrite.fn;
expression e;
@@

liblock_exec(lock, &fn, e);

// ---------------------------------------------------------------------

@pre_field_read depends on structure_info exists@
identifier build.fn, build.inst, build.input, build.output;
type T;
T e;
{union inst,struct input,struct output} e1;
identifier fld;
position p != {pre_field_write.p,pre_field_readwrite.p};
@@

fn(...) { <...
(
 e1.fld
|
 e.fld@p
) ...>
 }

@field_read exists@
position pre_field_read.p;
type T;
T e;
identifier fld;
identifier fn;
@@

fn(...) { ... when any
 e.fld@p ... when any
 }

@readlockname@
expression lock;
identifier field_read.fn;
expression e;
@@

liblock_exec(lock, &fn, e)

// ---------------------------------------------------------------------

@script:ocaml@
t << field_write.T;
fld << field_write.fld;
f << field_write.fn;
lock << writelockname.lock;
out << virtual.structure_output;
@@

structure_output := out;
try
  let (functions,info) = Hashtbl.find read_write_table (lock,t,fld) in
  (match !info with
    READ -> info := READWRITE
  | WRITE -> ()
  | READWRITE -> () );
  if not (List.mem f !functions) then functions := f::!functions
with Not_found ->
  Hashtbl.add read_write_table (lock,t,fld) (ref [f],ref WRITE)

@script:ocaml@
t << field_readwrite.T;
fld << field_readwrite.fld;
f << field_readwrite.fn;
lock << readwritelockname.lock;
out << virtual.structure_output;
@@

structure_output := out;
try
  let (functions,info) = Hashtbl.find read_write_table (lock,t,fld) in
  (match !info with
    READ -> info := READWRITE
  | WRITE -> info := READWRITE
  | READWRITE -> () );
  if not (List.mem f !functions) then functions := f::!functions
with Not_found ->
  Hashtbl.add read_write_table (lock,t,fld) (ref [f],ref READWRITE)

@script:ocaml@
t << field_read.T;
fld << field_read.fld;
f << field_read.fn;
lock << readlockname.lock;
out << virtual.structure_output;
@@

structure_output := out;
try
  let (functions,info) = Hashtbl.find read_write_table (lock,t,fld) in
  (match !info with
    READ -> ()
  | WRITE -> info := READWRITE
  | READWRITE -> () );
  if not (List.mem f !functions) then functions := f::!functions
with Not_found ->
  Hashtbl.add read_write_table (lock,t,fld) (ref [f],ref READ)

@finalize:ocaml@

match !structure_output with
  "" -> ()
| out ->
  let o = open_out out in
  let l =
    Hashtbl.fold
      (function k -> function (fns,kd) -> function acc ->
  	(k,List.sort compare !fns,!kd) :: acc)
      read_write_table [] in
  let prev = ref "" in
  List.iter
    (function ((lock,t,fld),fns,kd) ->
      (if not (!prev = "") && not (lock = !prev)
      then
        begin
	  Printf.fprintf o "\n";
	  prev := lock
        end);
      Printf.fprintf o "%s: %s: %s.%s \t%s\n"
	(String.concat "" (Str.split (Str.regexp " ") lock) )
  	(match kd with READ -> " R" | WRITE -> " W" | READWRITE -> "RW")
	t fld
  	(String.concat " "
	  (List.map
	    (function f ->
              List.hd (Str.split (Str.regexp "function") f ) )
            fns) ) )
    (List.sort compare l);
  close_out o
