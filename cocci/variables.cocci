// manage variables that are live across critical section boundaries

@ides_in_cs exists@
identifier build.incontext,build.outcontext,build.ctx;// uniquely identify a CS
type T,T1;
local idexpression T i;
local idexpression T1[] i1;
position ip;
identifier virtual.mutex_unlock;
identifier virtual.mutex_lock;
@@

mutex_lock(incontext(outcontext(ctx) ) );
... when any
    when != mutex_unlock(ctx);
    when != mutex_lock(incontext(outcontext(ctx) ) );
(
i1@ip
|
i@ip
)
... when any
    when != mutex_unlock(ctx);
    when != mutex_lock(incontext(outcontext(ctx) ) );
mutex_unlock(ctx);

@id@
position ides_in_cs.ip;
identifier i;
@@

i@ip

@prevector exists@
position ip;
typedef vector;
typedef va_list;
identifier I;
idexpression {vector,va_list,struct I} i;
identifier virtual.mutex_lock;
@@

i
...
mutex_lock(...);
... when any
i@ip

@nonprevector exists@
position ip != prevector.ip;
idexpression {vector,va_list,struct I} i;
identifier virtual.mutex_lock;
@@

mutex_lock(...);
... when exists
    when any
i@ip

@pre_defined_before_and_used_inside exists@
identifier build.incontext,build.outcontext,build.ctx; // uniquely identify a CS
identifier id.i;
position ip != nonprevector.ip;
identifier virtual.mutex_unlock;
identifier virtual.mutex_lock;
expression E2,E3;
type T2;
statement S;
@@

// Here we assume that if there is no definition in the critical section,
// then there must be a definition before.  This solves the parameter case,
// as well as definitions by &.  Perhaps we should check for & within the
// lock as well, but it's not certain that that is a definition...

mutex_lock(incontext(outcontext(ctx) ) );
... when != i = E2
    when != T2 i;
    when != mutex_unlock(ctx);
    when != mutex_lock(incontext(outcontext(ctx) ) );
(
for (<+...i=E3...+>; ... ; ...) S
|
for (<+...i=E3...+>; ... ; ) S
|
for (<+...i=E3...+>; ; ... ) S
|
i = <+...i@ip...+>
|
 (<+...i@ip...+> && ...)
|
 (<+...i@ip...+> || ...)
|
i = E3
|
 i@ip
)
... when any
    when != mutex_unlock(ctx);
    when != mutex_lock(incontext(outcontext(ctx) ) );
mutex_unlock(ctx);

@defined_before_and_used_inside@
identifier id.i;
position pre_defined_before_and_used_inside.ip;
@@

i@ip

@pre_defined_inside_and_used_after exists@
identifier build.fn,build.inst; // uniquely identify a CS
identifier build.incontext, build.outcontext, build.ctx;
identifier id.i;
position ip;
identifier virtual.mutex_unlock;
identifier virtual.mutex_lock;
expression E,E1,E2,E3;
type T1,T2;
expression l;
@@

mutex_lock(incontext(outcontext(ctx) ) );
... when != mutex_unlock(ctx);
    when != mutex_lock(incontext(outcontext(ctx) ) );
    when any
 \(i = E\|i += E\|i -= E\|++i\|--i\|++i\|i--\)
... when != \(i = E1\|i += E1\|i -= E1\)
    when != T1 i;
mutex_unlock(ctx);
liblock_exec(l,&fn,&inst);
... when != i = E2
    when != T2 i;
(
i = <+...i@ip...+>
|
i = E3
|
i@ip
)

@defined_inside_and_used_after@
identifier id.i;
position pre_defined_inside_and_used_after.ip;
@@

i@ip

@undefined_inside_and_used_after depends on defined_inside_and_used_after
 exists@
identifier build.incontext, build.outcontext, build.ctx;
identifier id.i;
identifier virtual.mutex_unlock;
identifier virtual.mutex_lock;
expression E;
@@

mutex_lock(incontext(outcontext(ctx) ) );
++ i = i;
... when != i = E
mutex_unlock(ctx);
