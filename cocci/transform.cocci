// Add the fields to the structure declaration
// Add the corresponding assignments

@depends on defined_inside_and_used_after disable add_signed, const_decl_init@
identifier build.inst,build.output;
type ides_in_cs.T;
identifier id.i;
@@

union inst { ...
   struct output {
     int __nothing__;
++   T i;
     ... } output;
 } inst = ...;

@tyd1@
identifier I;
type T1;
@@

typedef struct I { ... } T1;

@tyd2@ // clunky
identifier I;
type T2;
@@

typedef struct I T2;

@fix_flat_output1 disable const_decl_init@
identifier build.inst,build.output;
identifier id.i,I;
@@

union inst { ...
   struct output {
     ...
(
-    va_list i;
+    va_list *i;
|
-    struct I i;
+    struct I *i;
)
     ... } output;
 } inst = ...;

@fix_flat_output2 disable const_decl_init@
identifier build.inst,build.output;
identifier id.i;
type tyd1.T1;
@@

union inst { ...
   struct output {
     ...
-    T1 i;
+    T1 *i;
     ... } output;
 } inst = ...;

@fix_flat_output3 disable const_decl_init@
identifier build.inst,build.output;
identifier id.i;
type tyd2.T2;
@@

union inst { ...
   struct output {
     ...
-    T2 i;
+    T2 *i;
     ... } output;
 } inst = ...;

@depends on defined_inside_and_used_after disable add_signed, const_decl_init@
identifier build.inst,build.output;
type ides_in_cs.T1;
identifier id.i;
@@

union inst { ...
   struct output {
     int __nothing__;
++   T1 *i;
     ... } output;
 } inst = ...;

@addinput depends on defined_before_and_used_inside || undefined_inside_and_used_after
disable add_signed, const_decl_init@
identifier build.inst,build.input,build.incontext;
type ides_in_cs.T;
identifier id.i;
identifier virtual.mutex_lock;
expression e;
@@

union inst { 
  struct input {
    int __nothing__;
++  T i;
    ... } input; ... } inst = {{
0,
++ i,
...}};
mutex_lock(e);
++ T i = incontext->i;

// need three rules to avoid already tagged token, due to multiple orthogonal
// inherited values (tyds)
@fix_flat_input1 disable const_decl_init@
identifier build.inst,build.input;
identifier id.i,I;
identifier virtual.mutex_lock;
identifier virtual.mutex_unlock;
identifier build.incontext,build.outcontext,build.ctx;
@@

union inst {
  struct input {
    ...
(
-    va_list i;
+    va_list *i;
|
-    struct I i;
+    struct I *i;
)
    ... } input; ... } inst = {{
...,
- i,
+ &i,
...}};
mutex_lock(incontext(outcontext(ctx) ) );
<...
(
- va_list i = incontext->i;
+ va_list *i = incontext->i;
|
-    struct I i = incontext->i;
+    struct I *i = incontext->i;
|
-  i
+ (*i)
)
...>
mutex_unlock(ctx);

@fix_flat_input2 disable const_decl_init@
identifier build.inst,build.input;
identifier id.i;
identifier virtual.mutex_lock;
identifier virtual.mutex_unlock;
identifier build.incontext,build.outcontext,build.ctx;
type tyd1.T1;
@@

union inst {
  struct input {
    ...
-    T1 i;
+    T1 *i;
    ... } input; ... } inst = {{
...,
- i,
+ &i,
...}};
mutex_lock(incontext(outcontext(ctx) ) );
<...
(
-    T1 i = incontext->i;
+    T1 *i = incontext->i;
|
-  i
+ (*i)
)
...>
mutex_unlock(ctx);

@fix_flat_input3 disable const_decl_init@
identifier build.inst,build.input;
identifier id.i;
identifier virtual.mutex_lock;
identifier virtual.mutex_unlock;
identifier build.incontext,build.outcontext,build.ctx;
type tyd2.T2;
@@

union inst {
  struct input {
    ...
-    T2 i;
+    T2 *i;
    ... } input; ... } inst = {{
...,
- i,
+ &i,
...}};
mutex_lock(incontext(outcontext(ctx) ) );
<...
(
-    T2 i = incontext->i;
+    T2 *i = incontext->i;
|
-  i
+ (*i)
)
...>
mutex_unlock(ctx);

// Not sure how to do the T1 case (array).  Don't have the size.
@depends on !defined_before_and_used_inside &&
 !undefined_inside_and_used_after@
identifier build.incontext,build.outcontext,build.ctx;
type ides_in_cs.T;
identifier id.i;
identifier virtual.mutex_lock;
@@

mutex_lock(incontext(outcontext(ctx) ) );
++ T i;

@depends on defined_before_and_used_inside || undefined_inside_and_used_after
disable add_signed, const_decl_init@
identifier build.inst,build.input,build.incontext;
type ides_in_cs.T1;
identifier id.i;
identifier virtual.mutex_lock;
expression e;
@@

union inst { 
  struct input {
    int __nothing__;
++  T1 *i;
    ... } input; ... } inst = {{
0,
++ i,
...}};
mutex_lock(e);
++ T1 *i = incontext->i;

@depends on defined_inside_and_used_after@
identifier build.fn,build.inst,build.outcontext,build.output;
identifier id.i;
identifier virtual.mutex_unlock;
expression e,l;
@@

++ outcontext->i = i;
mutex_unlock(e);
liblock_exec(l,&fn,&inst);
++ i = inst.output.i;

// ------------------------------------------------------------------------

@last_output disable add_signed, const_decl_init@
identifier build.inst,build.output,build.fn,build.outcontext;
identifier virtual.mutex_unlock;
expression l,e;
type T;
identifier i;
typedef uintptr_t;
@@

(
 union inst {
   ...
   struct output { int __nothing__; } output; } inst = ...;
|
 union inst { // other big or unconvertable types?
   ...
   struct output { ... 
(
 MYREAL i;
|
 double i;
|
 float i;
|
 quad i;
)
 } output; } inst = ...;
|
 union inst {
   ...
   struct output { ...
- T i;
 } output; } inst = ...;
 ...
- outcontext->i = i;
...
+ __RETURN__((void *)(uintptr_t)i);
mutex_unlock(e);
+i = (T)(uintptr_t)(
 liblock_exec(l, &fn ,&inst)
+ )
;
...
- i = inst.output.i;
)

@one_input disable add_signed, const_decl_init@
identifier build.ctx,build.inst,build.input,build.output,build.fn;
identifier build.incontext;
identifier virtual.mutex_lock;
expression l,e,e1,v;
type T,T1,T2;
identifier i;
@@

(
 union inst { // other big or unconvertable types?
   struct input { int __nothing__;
(
 MYREAL i;
|
 double i;
|
 float i;
|
 quad i;
)
 } input; ... } inst = ...;
|
 union inst {
   struct input { int __nothing__; 
-  T i;
   } input;
   struct output { int __nothing__; } output; } inst
  = {{ 0,
- v,
  }};
mutex_lock(e);
...
-T i = incontext->i;
+T i = (T)(uintptr_t)ctx;
 ...
(
 liblock_exec(l, &fn, 
+v,
 &inst);
|
 e1 = (T1)(T2)(liblock_exec(l, &fn, 
+v,
 &inst));
)
)

@no_fields disable add_signed, const_decl_init@
identifier build.inst,build.input,build.output,build.fn;
expression l;
@@

- union inst {
-   struct input { int __nothing__; } input;
-   struct output { int __nothing__; } output; } inst = {{ 0, }};
 ...
(
liblock_exec(l,&fn,
- &inst
+ NULL
  )
|
liblock_exec(...)
)

@@
identifier inst,fn;
expression l,e;
@@

liblock_exec(l,&fn,e
- ,&inst
  )

// clean up, even ok for NULL
@fix_call_type@
expression e1,e2,e3;
@@

liblock_exec(e1,e3,
- e2
+ (void *)(uintptr_t)(e2)
 )

@no_inputs disable add_signed, const_decl_init@
identifier build.inst,build.input;
@@

  union inst {
-   struct input { int __nothing__; } input;
    ... } inst
-              = {{ 0, }}
    ;

@no_outputs disable add_signed, const_decl_init@
identifier build.inst,build.output;
@@

  union inst { ...
-   struct output { int __nothing__; } output;
    } inst = ...;

@disable add_signed, const_decl_init@
identifier build.inst,build.input;
@@

  union inst {
    struct input {
-                  int __nothing__;
                   ... } input;
    ... } inst
               = {{ 
-                 0,
                  ... }};

@disable add_signed@
identifier build.inst,build.output;
@@

  union inst { ...
    struct output {
-      int __nothing__;
       ... } output;
    } inst;

@depends on !no_inputs && !no_fields@
identifier build.inst, build.input, build.incontext, build.ctx;
identifier virtual.mutex_lock;
@@

  union inst { ... } inst;
  mutex_lock(...);
+ struct input *incontext = &(((union inst *)ctx)->input);

@depends on !no_outputs && !no_fields@
identifier build.inst, build.output, build.outcontext, build.ctx;
identifier virtual.mutex_lock;
@@

  union inst { ... } inst;
  mutex_lock(...);
+ struct output *outcontext = &(((union inst *)ctx)->output);

// ------------------------------------------------------------------------
// add alignment attributes

@@
identifier build.inst;
@@

union inst { ... }
+ __attribute__ ((aligned (CACHE_LINE_SIZE)))
inst;

@with_return@
position p;
identifier virtual.mutex_unlock;
@@

__RETURN__(...);
mutex_unlock@p(...);

@@
position p != with_return.p;
identifier virtual.mutex_unlock;
@@

+ __RETURN__(NULL);
mutex_unlock@p(...);

@exists@
identifier build.incontext, build.outcontext, build.ctx;
identifier virtual.mutex_unlock;
identifier mutex_lock;
@@

mutex_lock(incontext(outcontext(ctx) ) );
+{
...
+}
mutex_unlock(ctx);

@exists@
identifier build.inst;
type T;
identifier f;
field list fields;
initializer E;
@@

++ union inst { fields };
T f(...) {
... when any
(
- union inst { fields } inst = E;
+ union inst inst = E;
|
- union inst { fields } inst;
+ union inst inst;
)
... when any
}


@exists@
identifier build.fn, build.incontext, build.outcontext, build.ctx;
identifier virtual.mutex_unlock;
identifier virtual.mutex_lock;
statement S;
type T;
identifier f;
@@

++ void *fn(void *ctx);
++ void *fn(void *ctx) { S }
T f(...) {
... when any
mutex_lock(incontext(outcontext(ctx) ) );
S
-mutex_unlock(ctx);
... when any
}

@exists@
identifier build.incontext, build.outcontext, build.ctx;
identifier virtual.mutex_lock;
statement S;
@@

-mutex_lock(incontext(outcontext(ctx) ) );
+#if 0
S
+#endif

// cleanup return added for label case

@@
identifier l;
expression e;
@@

  l:
(
- __RETURN__ (e);
+ return e;
)

@@
expression e;
@@

(
- __RETURN__(e);
+ return e;
)

@@
identifier done;
@@

- __GOTO__(done);
+ goto done;

