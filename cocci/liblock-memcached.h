#ifndef LIBLOCK_CONFIG
#define LIBLOCK_CONFIG
#define TYPE_POSIX "posix"
#define DEFAULT_ARG NULL
#define TYPE_NOINFO TYPE_POSIX
#define ARG_NOINFO DEFAULT_ARG
#endif

// ../memcached-1.4.6/thread.c:71
// get
// mutex 27 	locked 48 	cont 1 	l2 0.000
// mutex 51 	locked 48 	cont 0 	l2 0.375
// mutex 25 	locked 48 	cont 0 	l2 0.917
// mutex 23 	locked 48 	cont 0 	l2 0.208
// mutex 21 	locked 48 	cont 0 	l2 1.271
// set
// mutex 45 	locked 6 	cont 0 	l2 3.167
// mutex 33 	locked 6 	cont 0 	l2 2.500
// mutex 23 	locked 8 	cont 0 	l2 0.000
// mutex 21 	locked 8 	cont 0 	l2 2.625

#define TYPE_MEMCACHED_1_4_6_1 TYPE_POSIX
#define ARG_MEMCACHED_1_4_6_1 DEFAULT_ARG

// ../memcached-1.4.6/thread.c:581
// get
// mutex 68 	locked 13830001 	cont 5598789 	l2 3.262
// set
// mutex 68 	locked 755538 	cont 342684 	l2 45.134

#define TYPE_MEMCACHED_1_4_6_2 TYPE_POSIX
#define ARG_MEMCACHED_1_4_6_2 DEFAULT_ARG

// ../memcached-1.4.6/thread.c:583
// get
// mutex 65 	locked 11470 	cont 86 	l2 0.992
// set
// mutex 65 	locked 466541 	cont 17 	l2 0.686

#define TYPE_MEMCACHED_1_4_6_3 TYPE_POSIX
#define ARG_MEMCACHED_1_4_6_3 DEFAULT_ARG

// ../memcached-1.4.6/thread.c:584
// get
// mutex 67 	locked 30 	cont 13 	l2 5.538
// set
// mutex 67 	locked 31 	cont 10 	l2 5.286

#define TYPE_MEMCACHED_1_4_6_4 TYPE_POSIX
#define ARG_MEMCACHED_1_4_6_4 DEFAULT_ARG

// ../memcached-1.4.6/thread.c:589
// get
// mutex 66 	locked 1010 	cont 25 	l2 0.438
// set
// mutex 66 	locked 131 	cont 2 	l2 0.443

#define TYPE_MEMCACHED_1_4_6_5 TYPE_POSIX
#define ARG_MEMCACHED_1_4_6_5 DEFAULT_ARG

