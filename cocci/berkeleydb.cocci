@@
@@

struct {
  ...
  pthread_mutex_t mutex;
+ liblock_lock_t lmutex;
  ...
};

@pre_db@
typedef DB;
typedef DBC;
typedef FNAME;
expression a;
{DB *,FNAME *} b;
DBC * c;
@@

(
- MUTEX_LOCK(a,b->mutex)
+ BDMUTEX_LOCK(MLOCK(a,b->mutex))
|
- MUTEX_UNLOCK(a,b->mutex)
+ BDMUTEX_UNLOCK(MLOCK(a,b->mutex))
|
- MUTEX_LOCK(a,c->dbp->mutex)
+ BDMUTEX_LOCK(MLOCK(a,c->dbp->mutex))
|
- MUTEX_UNLOCK(a,c->dbp->mutex)
+ BDMUTEX_UNLOCK(MLOCK(a,c->dbp->mutex))
)

@depends on use_api && specific_locks@
{DB *,FNAME *} e;
DBC *f;
expression e1;
@@

- pthread_cond_wait
+ liblock_cond_wait
  (e1,&\(e\|f->dbp\)->mutex)

@depends on use_api && specific_locks@
{DB *,FNAME *} e;
DBC *f;
expression e1,e2;
@@
- pthread_cond_timedwait
+ liblock_cond_timedwait
  (e1,&\(e\|f->dbp\)->mutex,e2)

@depends on use_api && specific_locks@
{DB *,FNAME *} e;
DBC *f;
@@

(
- pthread_cond_signal
+ liblock_cond_signal
|
- pthread_cond_broadcast
+ liblock_cond_broadcast

)
  (&\(e\|f->dbp\)->mutex)

@depends on use_api && specific_locks@
{DB *,FNAME *} e;
DBC *f;
@@

- pthread_mutex_destroy
+ liblock_lock_destroy
  (&\(e\|f->dbp\)->mutex)

// ---------------------------------------------------------------------
// Some macros that introduce free variables or assignments

@@
expression f;
@@

(
- LF_CLR(f)
+               ((flags) &= ~(f))
|
- LF_ISSET(f)
+             ((flags) & (f))
|
- LF_SET(f)
+               ((flags) |= (f))
)

@@
expression x,e1,e2;
iterator name TAILQ_FOREACH;
statement S;
@@

TAILQ_FOREACH(x,e1,e2) S
+x=x;
