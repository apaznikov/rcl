// @@
// @@

// -static liblock_lock_t stats_lock;
// +static pthread_mutex_t stats_lock;
// +#define DONT_TOUCH_MUTEX_LOCK pthread_mutex_lock
// +#define DONT_TOUCH_MUTEX_UNLOCK pthread_mutex_unlock
// +#define DONT_TOUCH_MUTEX_INIT pthread_mutex_init

// @@
// @@

// - pthread_mutex_lock
// + DONT_TOUCH_MUTEX_LOCK
//    (&stats_lock);

// @@
// @@

// - pthread_mutex_unlock
// + DONT_TOUCH_MUTEX_UNLOCK
//    (&stats_lock);

// @@
// @@

// - pthread_mutex_init
// + DONT_TOUCH_MUTEX_INIT
//    (&stats_lock,...);

// ------------------------------------------------------------------------
// only do cache_lock

@@
identifier virtual.the_lock;
@@

- pthread_mutex_lock(&the_lock);
+ liblock(&the_lock);

@mem_unlock@
identifier virtual.the_lock;
@@

- pthread_mutex_unlock(&the_lock);
+ libunlock(&the_lock);

@@
identifier virtual.the_lock;
@@

- pthread_mutex_t
+ liblock_lock_t
  the_lock;

@@
identifier virtual.the_lock;
expression e;
@@

- pthread_cond_wait
+ liblock_cond_wait
  (e, &the_lock);

@@
identifier virtual.the_lock;
@@

(
- pthread_cond_signal
+ liblock_cond_signal
|
- pthread_cond_broadcast
+ liblock_cond_broadcast
)
  (&the_lock);

@@
identifier virtual.the_lock;
expression e;
@@

- pthread_mutex_init
+ libinit
  (&the_lock,e);
