// ------------------------------------------------------------------------
// variable

@@
identifier virtual.pre_mutex_lock;
identifier virtual.the_lock;
@@

- pre_mutex_lock
+ liblock
  (...,&the_lock,...);

@@
identifier virtual.pre_mutex_unlock;
identifier virtual.the_lock;
@@

- pre_mutex_unlock
+ libunlock
  (...,&the_lock,...);

@depends on use_api@
identifier virtual.the_lock;
@@

- pthread_mutex_t
+ liblock_lock_t
  the_lock;

@depends on use_api@
identifier virtual.the_lock;
expression e;
@@

- pthread_cond_wait
+ liblock_cond_wait
  (e, &the_lock);

@depends on use_api@
identifier virtual.pre_mutex_init;
identifier virtual.the_lock;
expression e;
@@

- pre_mutex_init
+ libinit
  (&the_lock,e);

@depends on use_api@
identifier virtual.the_lock;
@@

(
- pthread_cond_signal
+ liblock_cond_signal
|
- pthread_cond_broadcast
+ liblock_cond_broadcast
)
  (&the_lock);

// ------------------------------------------------------------------------
// variable

@@
identifier virtual.pre_mutex_lock;
identifier virtual.the_second_lock;
@@

- pre_mutex_lock
+ liblock
  (...,&the_second_lock,...);

@@
identifier virtual.pre_mutex_unlock;
identifier virtual.the_second_lock;
@@

- pre_mutex_unlock
+ libunlock
  (...,&the_second_lock,...);

@depends on use_api@
identifier virtual.the_second_lock;
@@

- pthread_mutex_t
+ liblock_lock_t
  the_second_lock;

@depends on use_api@
identifier virtual.the_second_lock;
expression e;
@@

- pthread_cond_wait
+ liblock_cond_wait
  (e, &the_second_lock);

@depends on use_api@
identifier virtual.pre_mutex_init;
identifier virtual.the_second_lock;
expression e;
@@

- pre_mutex_init
+ libinit
  (&the_second_lock,e);


@depends on use_api@
identifier virtual.the_second_lock;
@@

(
- pthread_cond_signal
+ liblock_cond_signal
|
- pthread_cond_broadcast
+ liblock_cond_broadcast
)
  (&the_second_lock);

// ------------------------------------------------------------------------
// field

@@
identifier virtual.pre_mutex_lock;
expression p;
identifier virtual.the_field_lock;
@@

- pre_mutex_lock
+ liblock
  (&(p->the_field_lock));

@@
identifier virtual.pre_mutex_unlock;
expression p;
identifier virtual.the_field_lock;
@@

- pre_mutex_unlock
+ libunlock
  (...,&(p->the_field_lock),...);

@depends on use_api@
identifier I;
identifier virtual.the_field_lock;
@@

struct I { ...
- pthread_mutex_t
+ liblock_lock_t
  the_field_lock;
  ...
};

@depends on use_api@
expression p;
identifier virtual.the_field_lock;
expression e;
@@

- pthread_cond_wait
+ liblock_cond_wait
  (e, &(p->the_field_lock));

@depends on use_api@
identifier virtual.pre_mutex_init;
expression p;
identifier virtual.the_field_lock;
expression e;
@@

- pre_mutex_init
+ libinit
  (&(p->the_field_lock),e);

@depends on use_api@
expression p;
identifier virtual.the_field_lock;
@@

(
- pthread_cond_signal
+ liblock_cond_signal
|
- pthread_cond_broadcast
+ liblock_cond_broadcast
)
  (&(p->the_field_lock));

// ------------------------------------------------------------------------
// field

@@
identifier virtual.pre_mutex_lock;
expression p;
identifier virtual.the_second_field_lock;
@@

- pre_mutex_lock
+ liblock
  (...,&(p->the_second_field_lock),...);

@@
identifier virtual.pre_mutex_unlock;
expression p;
identifier virtual.the_second_field_lock;
@@

- pre_mutex_unlock
+ libunlock
  (...,&(p->the_second_field_lock),...);

@depends on use_api@
identifier I;
identifier virtual.the_second_field_lock;
@@

struct I { ...
- pthread_mutex_t
+ liblock_lock_t
  the_second_field_lock;
  ...
};

@depends on use_api@
expression p;
identifier virtual.the_second_field_lock;
expression e;
@@

- pthread_cond_wait
+ liblock_cond_wait
  (e, &(p->the_second_field_lock));

@depends on use_api@
identifier virtual.pre_mutex_init;
expression p;
identifier virtual.the_second_field_lock;
expression e;
@@

- pre_mutex_init
+ libinit
  (&(p->the_second_field_lock),e);

@depends on use_api@
expression p;
identifier virtual.the_second_field_lock;
@@

(
- pthread_cond_signal
+ liblock_cond_signal
|
- pthread_cond_broadcast
+ liblock_cond_broadcast
)
  (&(p->the_second_field_lock));
