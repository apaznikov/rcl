// Generic renamings

@static_init depends on use_api && !specific_locks@
identifier the_lock;
@@

pthread_mutex_t the_lock = PTHREAD_MUTEX_INITIALIZER;

@script:ocaml static_init_fn@
the_lock << static_init.the_lock;
fn;
@@

fn :=
Printf.sprintf "%s;\nstatic __attribute__ ((constructor (105))) void init_%s() {\n  liblock_lock_init(TYPE_NOINFO, ARG_NOINFO, &%s, 0);\n}\n//"
the_lock the_lock the_lock

@@
identifier the_lock;
identifier static_init_fn.fn;
typedef liblock_lock_t;
@@

- pthread_mutex_t the_lock = PTHREAD_MUTEX_INITIALIZER;
+ liblock_lock_t fn;

// -----------------------------------------------------------------

@depends on use_api && !specific_locks@
@@

- pthread_mutex_t
+ liblock_lock_t

@depends on use_api@
@@

- pthread_create
+ liblock_thread_create
  (...)

@depends on use_api && !specific_locks@
@@

(
- pthread_cond_wait
+ liblock_cond_wait
|
- pthread_cond_timedwait
+ liblock_cond_timedwait
|
- pthread_cond_signal
+ liblock_cond_signal
|
- pthread_cond_broadcast
+ liblock_cond_broadcast
|
- pthread_mutex_destroy
+ liblock_lock_destroy
)
  (...)
