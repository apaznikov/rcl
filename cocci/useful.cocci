
// ---------------------------------------------------------------------
// ignore files where there is nothing else to do

@useful@
identifier virtual.mutex_lock;
position p;
@@

mutex_lock@p(...)

@script:ocaml depends on !useful@
ml << virtual.mutex_lock;
@@

Coccilib.exit()
