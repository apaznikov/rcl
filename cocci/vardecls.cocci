// move declarations under locks and not in braces up over the top of the lock

@@
identifier virtual.mutex_lock;
identifier virtual.mutex_unlock;
type T;
identifier x;
expression E,l;
@@

++T x;
mutex_lock(l);
... when != mutex_unlock(l)
    when any
    when != { ... }
(
-T x;
|
-T x 
+x
= E;
)
