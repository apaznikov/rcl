@@
void *l;
identifier virtual.mutex_lock;
@@

- mutex_lock(l)
+ mutex_lock((liblock_lock_t *)(l))

@@
void *l;
identifier virtual.mutex_unlock;
@@

- mutex_unlock(l)
+ mutex_unlock((liblock_lock_t *)(l))

@@
typedef mr_lock_t;
mr_lock_t l;
@@

- liblock_lock_destroy(l)
+ liblock_lock_destroy((liblock_lock_t *)(l))
