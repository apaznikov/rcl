
@initcall@
identifier virtual.mutex_init;
position p;
@@

mutex_init@p(...)

@script:ocaml make_names@
profile_file << virtual.profile_file;
header_file  << virtual.header_file;
init_path    << virtual.init_path;
project      << virtual.project;
p << initcall.p;
ty;
arg;
@@

let all_mutexes =
  match !all_mutexes with
    None ->
      let i = open_in profile_file in
      let res = parse_profile i init_path header_file project in
      close_in i;
      res
  | Some all_mutexes -> all_mutexes in
let project = upcase project in
let current_file = (List.hd p).file in
let current_line = (List.hd p).line in
try
  let ((file,line),number) =
    List.find
      (function ((file,line),numbernn) ->
	let file_len = String.length file in
	let pfile_len = String.length  current_file in
	let diff = pfile_len - file_len in
	if diff >= 0
	then (String.sub current_file diff file_len) = file &&
	  line = current_line
	else false)
      all_mutexes in
  ty  := Printf.sprintf "TYPE_%s_%d" project number;
  arg := Printf.sprintf "ARG_%s_%d"  project number
with Not_found ->
  Printf.fprintf stderr "no information about mutex in file %s on line %d\n"
    current_file current_line;
  ty  := "TYPE_NOINFO";
  arg := "ARG_NOINFO"

@@
identifier virtual.mutex_init;
position initcall.p;
expression list es;
identifier make_names.ty;
identifier make_names.arg;
@@

-mutex_init@p(es)
+liblock_lock_init(ty,arg,es)
