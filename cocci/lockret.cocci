// ifs - not safe, but looks like paranoia

@@
identifier virtual.mutex_lock;
identifier virtual.mutex_unlock;
statement S;
expression x,l;
@@

(
- if ((x = mutex_lock(l)) != 0) S
+ mutex_lock(l);
|
- if ((x = mutex_unlock(l)) != 0) S
+ mutex_unlock(l);
|
- if ((mutex_lock(l)) != 0) S
+ mutex_lock(l);
|
- if ((mutex_unlock(l)) != 0) S
+ mutex_unlock(l);
)

@normalize_lock_arg@
expression e;
identifier fld;
identifier virtual.mutex_lock;
identifier virtual.mutex_unlock;
@@

(
- mutex_lock(&e->fld);
+ mutex_lock(&(e->fld));
|
- mutex_unlock(&e->fld);
+ mutex_unlock(&(e->fld));
)

// find inner mutexes and functions that contain them
// got tired of trylock...

@@
expression e1;
@@

(
-  break;
+  __BREAK__;
|
-  return
+  __RETURN__
   ;
|
-  return
+  __RETURN__(
  e1
+ )
  ;
)

@exists@
identifier virtual.mutex_lock;
identifier virtual.mutex_unlock;
expression e1,e3;
fresh identifier key = "";
statement S;
@@

if (...) { ... when != mutex_lock(e1);
               when any
 mutex_unlock(e1
+, key+1
 );
+{
... when != mutex_lock(e1);
(
  __BREAK__;
|
  __RETURN__;
|
  __RETURN__(e3);
|
  pthread_exit(e3);
|
  exit(e3);
)
+}
} else S

// do it again, to get else case
@exists@
identifier virtual.mutex_lock;
identifier virtual.mutex_unlock;
expression e1,e3;
fresh identifier key = "";
statement S;
@@

if (...) { ... when != mutex_lock(e1);
               when any
 mutex_unlock(e1
+, key+1
 );
+{
... when != mutex_lock(e1);
(
  __BREAK__;
|
  __RETURN__;
|
  __RETURN__(e3);
|
  pthread_exit(e3);
|
  exit(e3);
)
+}
} else S

@marklocks exists@
expression e,key;
fresh identifier done = "done";
fresh identifier ret = "ret";
identifier virtual.mutex_unlock;
identifier virtual.mutex_lock;
@@

mutex_lock(e
+, ret, done
 );
... when != mutex_unlock(e);
mutex_unlock(e,key);

// find a block with a unique start and end
@acceptable exists@
expression e;
identifier virtual.mutex_unlock;
identifier virtual.mutex_lock;
identifier ret,done;
identifier f;
statement S;
expression key;
@@

f(...) {
++ int ret;
... when any
mutex_lock(e
- , ret, done
 );
+ ret = 0;
<... when != mutex_unlock(e);
{
  ... 
+  ret = key;
+  __GOTO__(done);
-  mutex_unlock(e,key);
   S
}
 ...>
+ done: {}
mutex_unlock(e);
+ if (ret) { }
... when any
}

@exists@
identifier acceptable.done, acceptable.ret;
expression e;
identifier virtual.mutex_unlock;
identifier virtual.mutex_lock;
statement S;
expression key;
@@

mutex_lock(e)
... when != mutex_unlock(e)
    when any
  ret = key;
  __GOTO__(done);
- S
... when != mutex_unlock(e)
    when any
done: {}
mutex_unlock(e);
if (ret) {
++if (ret == key) S
...
}

@cleanup@
expression e;
@@

(
- __BREAK__;
+ break;
|
- __RETURN__;
+ return;
|
- __RETURN__ (
+ return
 (e)
- )
 ;
|
- __RETURN__
+ return
   (e);
)
