#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/mman.h>


#define NUM_WORKER_THREADS                                                 15
#define CACHE_LINE_SIZE                                                    16
#define NUMBER_OF_ITERATIONS                                               100

#if defined(__i386__) || defined(__x86_64__)
#define PAUSE() asm volatile("pause"::)
#elif defined(__sparc__)
#define PAUSE() asm volatile(".word 0x91408000"::)
#endif


pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t inner_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_barrier_t barrier;

volatile int number_of_accesses = 0;

volatile int sync = 0;
volatile int global_last = -1;
volatile int remaining_clients = NUM_WORKER_THREADS;

typedef struct cache_line {
    void *word[CACHE_LINE_SIZE];
} cache_line_t;

volatile cache_line_t *global_array;
int *global_array_permutation;

void *thread_function(void *args);


int main(int argc, char **argv)
{
	int i, j, index, next_index;
    pthread_t thread[NUM_WORKER_THREADS];


    srand((unsigned int)time(NULL));


	if (argc == 2)
		number_of_accesses = atoi(argv[1]);
	else if (argc != 2)
		fprintf(stderr, "usage: %s <number_of_accesses>.\n", argv[0]);


	printf("Cache line size : %ld\n", sizeof(cache_line_t));

    if(pthread_barrier_init(&barrier, NULL, NUM_WORKER_THREADS))
        fprintf(stderr, "Warning: pthread_barrier_init failed.");


    global_array = (cache_line_t *)
                   mmap(0,
                        sizeof(cache_line_t) * number_of_accesses * 2,
                        PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE,
                        -1, 0);

    if(global_array == MAP_FAILED)
		fprintf(stderr, "Warning: mmap failed.\n");

    global_array_permutation = (int *)malloc(number_of_accesses * sizeof(int));

    if(!global_array_permutation)
		fprintf(stderr, "Warning: malloc failed.\n");


    for (i = 0; i < number_of_accesses; i++)
        global_array[i << 1].word[0] = NULL;

    for (i = 0; i < number_of_accesses; i++)
        global_array_permutation[i] = 0;

    for (i = 0; i < number_of_accesses; i++)
    {
        for (;;)
        {
            j = rand() % number_of_accesses;

            if (!global_array_permutation[j])
            {
                global_array_permutation[j] = i;
                break;
            }
            else continue;
       }
    }

    for (i = 0; i < number_of_accesses; i++)
    {
        index = global_array_permutation[i % number_of_accesses];
        next_index = global_array_permutation[(i + 1) % number_of_accesses];

        global_array[index << 1].word[0] =
            (void *)&global_array[next_index << 1].word[0];
    }


	for (i = 0; i < NUM_WORKER_THREADS; i++)
		pthread_create(&thread[i], NULL, thread_function, (void*)(intptr_t)i);

	for (i = 0; i < NUM_WORKER_THREADS; i++)
		pthread_join(thread[i], NULL);


	for (i = 0; i < number_of_accesses; i++)
		printf("%llu ", global_array[i << 1].word[0]);

	printf("\n");


	return 0;
}

void *thread_function(void *args)
{
	int i, j, k, result;
	uint64_t tmp_word;
	int tid = (int)(long)args;
    int local_sum;

    result = pthread_barrier_wait(&barrier);
    if (result != 0 && result != PTHREAD_BARRIER_SERIAL_THREAD)
        fprintf(stderr, "Warning: pthread_barrier_wait failed.");

	for (i = 0; i < NUMBER_OF_ITERATIONS; i++)
	{
		while (remaining_clients > 1 && global_last == tid)
			PAUSE();

	    if (remaining_clients <= 1) break;


        pthread_mutex_lock(&mutex);

        //void *volatile *p, *volatile *next_p;
        void **p, **next_p;

        p = &global_array[0].word[0];

		for (j = 0; j < number_of_accesses; j++)
        {
			next_p = *p;

            (*(p + 1))++;
            p = next_p;
        }
        pthread_mutex_unlock(&mutex);

        global_last = tid;
	}

	__sync_fetch_and_add(&remaining_clients, -1);
}

