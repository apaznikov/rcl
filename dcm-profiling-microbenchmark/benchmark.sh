#!/bin/bash
#
# benchmark.sh
# ============
# (C) Jean-Pierre Lozi, 2011
#
# Examples :
# ----------
#
# amd48:
# THREAD_BINDING="BY_CORE" EVENT="0x80000002" ./benchmark.sh
#
# niagara2:
# THREAD_BINDING="BY_CORE" LIBRARY="CPC" EVENT="DC_miss" ./benchmark.sh
# THREAD_BINDING="BY_CORE" LIBRARY="PAPI" EVENT="0x4000000c" ./benchmark.sh
#

if [[ -z $PROFILER ]]; then PROFILER="lock-profiler"; fi
if [[ -z $LIBRARY ]]; then LIBRARY="PAPI"; fi
if [[ -z $THREAD_BINDING ]]; then THREAD_BINDING="BY_NODE"; fi
if [[ -z $THREAD_MONITORING ]]; then THREAD_MONITORING="PER_NODE"; fi
if [[ -z $EVENT ]]
then
    if [[ $LIBRARY == "PAPI" ]]; then EVENT="0x80000002";
    else EVENT="L2_dmiss_ld"; fi
fi


if [[ $(hostname) == "amd48-systeme" || $(hostname) == "amd48b-systeme"  ]]
then
    BENCHMARK=./dcm-profiling-microbenchmark-amd48
elif [[ $(hostname) == "niagara2.cs.rochester.edu" ]]
then
    BENCHMARK=./dcm-profiling-microbenchmark-niagara2
else
    echo "Unsupported machine."
    exit -1
fi


if [[ $(uname) == "Linux" ]]
then
    sysctl -w vm.nr_hugepages=1000 > /dev/null
fi

for i in {1..40}
do
    echo -n "$i,"

    if [[ $PROFILER == "lock-profiler" ]]
    then
        ../lock-profiler/lock-profiler -e $EVENT -i -l $LIBRARY                \
            -b $THREAD_BINDING -m $THREAD_MONITORING $BENCHMARK $i 2>&1        \
            | grep "Global" | tr -d '\n' | awk '{print $7}'
    elif [[ $PROFILER == "mutrace-rcl" ]]
    then
        ../mutrace-rcl/mutrace-rcl --papi-event $EVENT                         \
            $BENCHMARK $i 2>&1 | grep \\$ | awk '{print $7}'
    else
        echo "Unknown profiler. PROFILER=(\"lock-profiler\"|\"mutrace-rcl\")."
        exit -1
    fi
done

if [[ $(uname) == "Linux" ]]
then
    sysctl -w vm.nr_hugepages=0 > /dev/null
fi

