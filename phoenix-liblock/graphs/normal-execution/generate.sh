#!/bin/bash
#
# generate.sh
# ===========
# (C) Jean-Pierre Lozi, 2013
#


. ../../../figures/bench.inc.sh

MACHINES=("amd48b" "niagara2")
BENCHMARKS=("histogram-med.bmp" "linear_regression-key_file_100MB.txt"         \
            "matrix_multiply-500" "string_match-key_file_100MB.txt")
YTICS_LINES=("set ytics 2" "set ytrics 2" "set ytics 2" "set ytics 1")

if [[ $# -lt 2 ]]
then
	TAGS=("140225-phd-normal-execution" "140127-phd-normal-execution")
else
    TAGS=("$1" "$2")
fi


function replace_variable
{
    VARIABLE_NAME=$1
    VARIABLE=${!VARIABLE_NAME}

    cat tmp.plt | sed "s/@@$VARIABLE_NAME@@/$VARIABLE/g"                       \
        > tmp-tmp.plt

    mv tmp-tmp.plt tmp.plt
}

for MACHINE_ID in ${!MACHINES[*]}
do
    MACHINE=${MACHINES[$MACHINE_ID]}
    TAG=${TAGS[$MACHINE_ID]}

    for BENCHMARK_ID in ${!BENCHMARKS[*]}
    do

        BENCHMARK=${BENCHMARKS[$BENCHMARK_ID]}
		YTICS_LINE=${YTICS_LINE[$BENCHMARK_ID]}

        RESULTS_PATH="../../all-results/results-$TAG-$MACHINE"
        RESULTS_PATH=$(echo $RESULTS_PATH | sed 's/\//\\\//g')

        for KEY in "" "-no-key"
        do

            cat base-${MACHINE}.plt > tmp.plt

            if [[ $KEY == "" ]]
            then
                KEY_LINE="set key"
            else
                KEY_LINE="unset key"
            fi

            replace_variable RESULTS_PATH
            replace_variable BENCHMARK
            replace_variable KEY_LINE
            replace_variable YTICS_LINE

            replace_variable POSIX_SETTINGS
            replace_variable SPINLOCK_SETTINGS
            replace_variable MCS_SETTINGS
            replace_variable MCSTP_SETTINGS
            replace_variable FLAT_SETTINGS
            replace_variable CCSYNCH_SETTINGS
            replace_variable DSMSYNCH_SETTINGS
            replace_variable RCL_SETTINGS

            cat tmp.plt | gnuplot \
                > output/phoenix-${BENCHMARK/./_}-${TAG}-${MACHINE}${KEY}.pdf

            rm tmp.plt

        done
    done
done

for KEY in "key" "key-wide"
do

    cat ${KEY}.plt > tmp.plt

    replace_variable POSIX_SETTINGS
    replace_variable SPINLOCK_SETTINGS
    replace_variable MCS_SETTINGS
    replace_variable MCSTP_SETTINGS
    replace_variable FLAT_SETTINGS
    replace_variable CCSYNCH_SETTINGS
    replace_variable DSMSYNCH_SETTINGS
    replace_variable RCL_SETTINGS

    cat tmp.plt | gnuplot > output/${KEY}.pdf 2> /dev/null

    rm tmp.plt

done

