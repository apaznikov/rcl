#!/bin/bash
#
# benchmark.sh
# ============
# (C) Jean-Pierre Lozi,
#     Florian David,
#     Gaël Thomas,
#     Julia Lawall,
#     Gilles Muller
#     2013
#
# Accepted variables: START_THREADS, END_THREADS, DIR, N_RUNS, METRIC,
# PROFILING_INTERVAL, DATASETS_DIR.
#

CMD_LOG=$HOME/rcl/rcl/phoenix-liblock/scripts/benchmark.log

cd $(dirname "${BASH_SOURCE[0]}")/..


if [[ $(uname) == "Linux" ]]
then
    NPROCS=$(cat /proc/cpuinfo | grep processor | wc -l)
elif [[ $(uname) == "SunOS" ]]
then
    NPROCS=$(kstat cpu_info | grep "instance" | wc -l | tr -d ' ')
fi


if [[ -z "$START_THREADS" ]]  ;      then START_THREADS=1;                    fi
if [[ -z "$END_THREADS" ]];          then END_THREADS=$((NPROCS-1));          fi
if [[ -z "$DIR" ]];                  then DIR=phoenix-rcl;                    fi
if [[ -z "$N_RUNS" ]];               then N_RUNS=30;                          fi
if [[ -z "$METRIC" ]];               then METRIC="EXECUTION_TIME";            fi
if [[ -z "$WARMUP_MR_NUMTHREADS" ]]; then WARMUP_MR_NUMTHREADS=20;            fi

if [[ -z "$PROFILING_INTERVAL" ]]
then
    PROFILING_INTERVAL="1 4 8 16 32 48"
fi

if [[ -z "$DATASETS_DIR" ]]
then
    DATASETS_DIR="/tmp/rcl-phoenix-datasets/";
fi

if [[ ! -d /tmp/rcl-phoenix-datasets ]]
then
    echo "No datasets."
    exit
fi

# LOCKS="flat spinlock mcs posix dsmsynch ccsynch rcl"
LOCKS="rcl"

export EXP
export REDIRECTION
export PARSING_COMMANDS


if [[ $(hostname) == "niagara2.cs.rochester.edu" ]]
then
    USE_SUPERUSER=0
else
    USE_SUPERUSER=1
fi


_seq()
{
    if [[ $(uname) == "Linux" ]]
    then
        seq $1 $2
    else
        nawk "BEGIN{ for(i = $1 ; i <= $2 ; i++) print i}"
    fi
}


#SINGLE_FILE_PER_LOCK=1
INTERVAL=`_seq $START_THREADS $END_THREADS`
BASEDIR=`pwd`
CODEDIR=$BASEDIR/$DIR/tests
#DATASETS_DIR=$BASEDIR/datasets/
LIB_PATH="LD_LIBRARY_PATH=$BASEDIR/../liblock"


# if [[ $USE_SUPERUSER == "1" && "$(id -u)" != "0" ]]
# then
#     echo "Must be root"
#     exit 1
# fi


run_histogram()
{
    if [[ -f $CODEDIR/histogram/histogram$EXT ]]
    then
        echo "$PREFIX $CODEDIR/histogram/histogram$EXT $1" >>$CMD_LOG

        $PREFIX $CODEDIR/histogram/histogram$EXT $1
    fi
}

run_linear_regression()
{
    if [[ -f $CODEDIR/linear_regression/linear_regression$EXT ]]
    then
        echo "$PREFIX $CODEDIR/linear_regression/linear_regression$EXT $1" >>$CMD_LOG

        $PREFIX $CODEDIR/linear_regression/linear_regression$EXT $1
    fi
}

run_kmeans()
{
    if [[ -f $CODEDIR/kmeans/kmeans$EXT ]]
    then
        echo "$PREFIX $CODEDIR/kmeans/kmeans$EXT $1 $2" >>$CMD_LOG

        $PREFIX $CODEDIR/kmeans/kmeans$EXT $1 $2
    fi
}

run_pca()
{
    if [[ -f $CODEDIR/pca/pca$EXT ]]
    then
        echo "$PREFIX $CODEDIR/pca/pca$EXT $1 $2 $3 $4" >>$CMD_LOG

        $PREFIX $CODEDIR/pca/pca$EXT $1 $2 $3 $4
    fi
}

run_matrix_multiply()
{
    if [[ -f $CODEDIR/matrix_multiply/matrix_multiply$EXT ]]
    then
        echo "$PREFIX $CODEDIR/matrix_multiply/matrix_multiply$EXT $1 $2" >>$CMD_LOG

        $PREFIX $CODEDIR/matrix_multiply/matrix_multiply$EXT $1 $2
    fi
}

run_word_count()
{
    if [[ -f $CODEDIR/word_count/word_count$EXT ]]
    then
        echo "$PREFIX $CODEDIR/word_count/word_count$EXT $1" >>$CMD_LOG

        $PREFIX $CODEDIR/word_count/word_count$EXT $1
    fi
}

run_string_match()
{
    if [[ -f $CODEDIR/string_match/string_match$EXT ]]
    then
        echo "$PREFIX $CODEDIR/string_match/string_match$EXT $1" >>$CMD_LOG

        $PREFIX $CODEDIR/string_match/string_match$EXT $1
    fi
}

points_and_average()
{
    TOT=0
    N=0

    rm -f tmp_points

    while read LINE
    do
        echo -n "$LINE," >> tmp_points
        TOT="$TOT+$LINE"
        N=$(($N+1))
    done

    TOT=`echo "scale=6;(($TOT)/$N)" | bc`
    echo -n "$TOT,"
    cat tmp_points
    echo
}

eval_exp()
{
    N_RUNSS=$1
    shift;
    FILE=$1
    shift;

    # echo -n "-- Running: [$@] with '$LIBLOCK_LOCK_NAME' lock and "
    # echo "$MR_NUMTHREADS cores --"

    echo

    # TODO: Use points_and_average here?
    for F in $(_seq 1 $N_RUNSS)
    do
        eval $@ $REDIRECTION $PARSING_COMMANDS > $FILE-run$F.csv
        echo -ne "\e[0;38;05;10m"
        cat $FILE-run$F.csv
        echo -e "\e[0m"
    done
}

profile_exp()
{
    N_RUNSS=$1
    shift;
    FILE=$1
    shift;

    # echo -n "-- Running: [$@] with '$LIBLOCK_LOCK_NAME' lock and "
    # echo "$MR_NUMTHREADS cores --"

    for F in $(_seq 1 $N_RUNSS)
    do
        eval $@ $REDIRECTION $PARSING_COMMANDS
    done
}

moyenne()
{
    let TOT=0
    let N=0
    while read line
    do
        let TOT=$TOT+$line
        let N=$N+1
    done
    let TOT=$TOT/$N
    echo "$TOT"
}

run_all_of()
{
    NAME=$1
    N_RUNS=$2
    VERBOSE=$3
    AVERAGE=$4
    shift; shift; shift; shift


    # Warmup run
    if [[ $VERBOSE != 0 ]]
    then
        echo "Warmup $NAME"
    fi

    EXT=-rcl MR_NUMTHREADS=$WARMUP_MR_NUMTHREADS eval $@ > /dev/null


    # Sequential run
    if [[ $VERBOSE != 0 ]]
    then
        echo "Run sequential $@"
    fi

    EXT=-seq eval_exp $N_RUNS results/phoenix-$NAME-sequential-$EXP "$@"


    # Runs with no liblock infrastructure
    for y in 1
    do ##$INTERVAL; do
        echo "Run $@ with no liblock infrastructure $y threads."
        MR_NUMTHREADS=$y eval_exp $N_RUNS \
        results/phoenix-$NAME-baseline-$y-$EXP "$@"
    done


    for x in $LOCKS
    do
        for y in $INTERVAL
        do
            echo "Run $@ with lock $x and $y threads."
            EXT=-rcl MR_NUMTHREADS=$y LIBLOCK_LOCK_NAME=$x eval_exp $N_RUNS    \
                results/phoenix-$NAME-$x-$y-$EXP "$@"
        done
    done

    echo
}

profile_all_of()
{
    NAME=$1
    N_RUNS=$2
    VERBOSE=$3
    AVERAGE=$4
    shift; shift; shift; shift


    # Warmup run
    if [[ $VERBOSE != 0 ]]
    then
        echo "Warmup $NAME"
    fi

    EXT=-rcl MR_NUMTHREADS=$WARMUP_MR_NUMTHREADS eval $@ > /dev/null


    if [[ $VERBOSE != 0 ]]
    then
        echo "Results for $NAME. These results won't be saved in a file!"
    fi

    # Runs with no liblock infrastructure
    for y in $PROFILING_INTERVAL
    do
        echo -n "$y,"

        if [[ $AVERAGE != "0" ]]
        then
            EXT=$EXT MR_NUMTHREADS=$y LIBLOCK_LOCK_NAME=$LIBLOCK_LOCK_NAME     \
                profile_exp $N_RUNS results/phoenix-$NAME-baseline-$y-$EXP     \
                "$@" | points_and_average
            #MR_NUMTHREADS=$y     \
            #    profile_exp $N_RUNS results/phoenix-$NAME-baseline-$y-$EXP     \
            #    "$@" | points_and_average
        else
            MR_NUMTHREADS=$y profile_exp $N_RUNS                               \
                results/phoenix-$NAME-baseline-$y-$EXP "$@"
        fi
    done

    echo
}


eval_all()
{
    export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$BASEDIR/../liblock"

    echo "N_RUNS: $N_RUNS"
    if [[ $METRIC == "TIMES_IN_CS" ]]
    then
        echo "THREADS: from $START_THREADS to $END_THREADS"
    fi
    echo

    rm -Rf results
    mkdir results

    # Linear_regression
    ARG_LIST="key_file_100MB.txt"
    # ARG_LIST="key_file_50MB.txt key_file_100MB.txt key_file_500MB.txt"
             # lr_4GB.txt"
    for ARG in $ARG_LIST
    do
        eval "$EVAL_FUNCTION linear_regression-$ARG $N_RUNS 1 1                \
              \"run_linear_regression                                          \
              $DATASETS_DIR/linear_regression_datafiles/$ARG 2>&1 >&-\""
    done

    # PCA
    ARG_LIST="1000"
    # ARG_LIST="500 1000 1500"
    for ARG in $ARG_LIST
    do
        eval "$EVAL_FUNCTION pca-$ARG $N_RUNS 1 1                              \
              \"run_pca -r $ARG -c $ARG 2>&1 >&-\""
    done

    # Kmeans
    ARG_LIST="50000"
    # ARG_LIST="10000 50000 100000"
    for ARG in $ARG_LIST
    do
        eval "$EVAL_FUNCTION kmeans-$ARG $N_RUNS 1 1 \"run_kmeans -p $ARG 2>&1 \
              >&-\""
    done

    # String_match
    ARG_LIST="key_file_100MB.txt"
    # ARG_LIST="key_file_50MB.txt key_file_100MB.txt key_file_500MB.txt"
    for ARG in $ARG_LIST
    do
        eval "$EVAL_FUNCTION string_match-$ARG $N_RUNS 1 1                     \
              \"run_string_match                                               \
              $DATASETS_DIR/string_match_datafiles/$ARG 2>&1 >&-\""
    done

    # Word_count
    ARG_LIST="word_50MB.txt"
    # ARG_LIST="word_10MB.txt word_50MB.txt word_100MB.txt"
    for ARG in $ARG_LIST
    do
        eval "$EVAL_FUNCTION word_count-$ARG $N_RUNS 1 1                       \
              \"run_word_count $DATASETS_DIR/word_count_datafiles/$ARG 2>&1    \
              >&-\""
    done

    # Histogram
    ARG_LIST="med.bmp"
    # ARG_LIST="small.bmp med.bmp large.bmp" # hist-2.6g.bmp"
    for ARG in $ARG_LIST
    do
        eval "$EVAL_FUNCTION histogram-$ARG $N_RUNS 1 1                        \
              \"run_histogram $DATASETS_DIR/histogram_datafiles/$ARG 2>&1 >&-\""
    done

    # Matrix_multiply
    ARG_LIST="500"
    # ARG_LIST="100 500 1000"
    for ARG in $ARG_LIST
    do
        # Matrix creation
        MR_NUMTHREADS=$WARMUP_MR_NUMTHREADS                                    \
            $CODEDIR/matrix_multiply/matrix_multiply-rcl $ARG 1 1 2>&1         \
            >&/dev/null # matrix_side row_block_length create_files
        eval "$EVAL_FUNCTION matrix_multiply-$ARG $N_RUNS 1 1                  \
              \"run_matrix_multiply $ARG 1 2>&1 >&-\""
        rm -f matrix_file_A.txt matrix_file_B.txt
    done

    # chown -R florian:florian results
    date
}

eval_main()
{
    export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$BASEDIR/../liblock"

    echo "N_RUNS: $N_RUNS"
    if [[ $METRIC == "TIMES_IN_CS" ]]
    then
        echo "THREADS: from $START_THREADS to $END_THREADS"
    fi
    echo

    rm -Rf results
    mkdir results

    # Linear_regression
    ARG_LIST="key_file_100MB.txt"
    # ARG_LIST="key_file_50MB.txt"
    for ARG in $ARG_LIST
    do
        eval "$EVAL_FUNCTION linear_regression-$ARG $N_RUNS 1 1                \
              \"run_linear_regression                                          \
              $DATASETS_DIR/linear_regression_datafiles/$ARG 2>&1 >&-\""
    done

    # String_match
    ARG_LIST="key_file_100MB.txt"
    # ARG_LIST="key_file_50MB.txt"
    for ARG in $ARG_LIST
    do
        eval "$EVAL_FUNCTION string_match-$ARG $N_RUNS 1 1                     \
              \"run_string_match $DATASETS_DIR/string_match_datafiles/$ARG 2>&1\
              >&-\""
    done

    # Matrix_multiply
    ARG_LIST="500"
    # ARG_LIST="100"
    for ARG in $ARG_LIST
    do
        # Matrix creation
        MR_NUMTHREADS=$WARMUP_MR_NUMTHREADS                                    \
            $CODEDIR/matrix_multiply/matrix_multiply-rcl $ARG 1 1 2>&1         \
            >&/dev/null # matrix_side row_block_length create_files
        eval "$EVAL_FUNCTION matrix_multiply-$ARG $N_RUNS 1 1                  \
              \"run_matrix_multiply $ARG 1 2>&1 >&-\""
        rm -f matrix_file_A.txt matrix_file_B.txt
    done

    # Histogram
    ARG_LIST="med.bmp"
    # ARG_LIST="small.bmp"
    for ARG in $ARG_LIST
    do
        eval "$EVAL_FUNCTION histogram-$ARG $N_RUNS 1 1                        \
              \"run_histogram $DATASETS_DIR/histogram_datafiles/$ARG 2>&1 >&-\""
    done

    # chown -R florian:florian results
    date
}

hugepages_flag="0"
# if [[ $(uname) = "Linux" && $(sysctl vm.nr_hugepages) != "vm.nr_hugepages = 2000" ]]
# then
#     sudo sysctl -w vm.nr_hugepages=2000 
#     hugepages_flag="1"
# fi


if [[ $METRIC == "EXECUTION_TIME" ]]
then

    EXP="execution-time"
    PREFIX=""
    EVAL_FUNCTION="run_all_of"

    eval_main

elif [[ $METRIC == "EXECUTION_TIME_ALL" ]]
then

    EXP="execution-time"
    PREFIX=""
    EVAL_FUNCTION="run_all_of"
    INTERVAL="48 128"
    LOCKS="posix"

    eval_all

elif [[ $METRIC == "SERVER_CACHE_MISSES" ]]
then

    cd ../liblock
    echo "Compiling the liblock with DCM profiling for rcl..."

    make distclean

    make ADDITIONAL_CFLAGS="-DPRECISE_DCM_PROFILING_INSIDE_ON"

    echo "Done."
    cd -
    trap "echo; echo 'Removing profiling from the liblock...';                 \
cd ../liblock; make distclean; make; cd -; exit" SIGINT

    EXP="server-cache-misses"
    PREFIX=""
    EVAL_FUNCTION="profile_all_of"
    LOCKS="rcl"
    PARSING_COMMANDS="| grep '\/cs' | awk '{print \$2}'"

    EXT=-rcl
    LIBLOCK_LOCK_NAME="rcl"

    if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
    then
        PROFILING_INTERVAL="47"
    elif [[ $(hostname) == "niagara2.cs.rochester.edu" ]]
    then
        PROFILING_INTERVAL="127"
    fi

    eval_main

    cd ../liblock
    echo "Removing profiling from the liblock..."
    make distclean
    make
    echo "Done."
    cd -

elif [[ $METRIC == "TIME_IN_CS" ]]
then

    EXP="time-in-cs"
    PREFIX="$BASEDIR/../lock-profiler/lock-profiler -b NONE -l NONE_CYCLES"
    EVAL_FUNCTION="profile_all_of"
    PARSING_COMMANDS="| grep 'Global statistics' | cut -d ' ' -f 4 | tr -d '%'"

    if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
    then
        PROFILING_INTERVAL="1 4 8 16 32 48"
    elif [[ $(hostname) == "niagara2.cs.rochester.edu" ]]
    then
        PROFILING_INTERVAL="1 4 8 16 32 64 128"
    fi

    eval_all

elif [[ $METRIC == "TIME_IN_CS_AND_DCM_FOR_MCL" ]]
then

    EXP="ticadfm"
    EVAL_FUNCTION="profile_all_of"

    if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
    then
        PROFILING_INTERVAL="48"
    elif [[ $HOSTNAME == "niagara2.cs.rochester.edu" ]]
    then
        PROFILING_INTERVAL="128"
    fi

    BENCHMARK_ARGS=("key_file_100MB.txt" "1000" "50000" "key_file_100MB.txt"
                    "word_50MB.txt" "med.bmp" "500")

    BENCHMARK_NAMES=(linear_regression pca kmeans string_match word_count
                     histogram matrix_multiply)

    BENCHMARK_COMMANDS=(
        "$EVAL_FUNCTION linear_regression-${BENCHMARK_ARGS[0]} @@N_RUNS 0 @@AVG  \
            \"run_linear_regression                                            \
            $DATASETS_DIR/linear_regression_datafiles/${BENCHMARK_ARGS[0]}     \
            2>&1 >&-\""
        "$EVAL_FUNCTION
            pca-${BENCHMARK_ARGS[1]} @@N_RUNS 0 @@AVG \"run_pca -r               \
            ${BENCHMARK_ARGS[1]} -c ${BENCHMARK_ARGS[1]} 2>&1 >&-\""
        "$EVAL_FUNCTION kmeans-${BENCHMARK_ARGS[2]} @@N_RUNS 0 0 \"run_kmeans    \
            -p ${BENCHMARK_ARGS[2]} 2>&1 >&-\""
        "$EVAL_FUNCTION string_match-${BENCHMARK_ARGS[3]} @@N_RUNS 0 @@AVG       \
            \"run_string_match                                                 \
            $DATASETS_DIR/string_match_datafiles/${BENCHMARK_ARGS[3]}          \
            2>&1 >&-\""
        "$EVAL_FUNCTION word_count-${BENCHMARK_ARGS[4]} @@N_RUNS 0 @@AVG         \
            \"run_word_count                                                   \
            $DATASETS_DIR/word_count_datafiles/${BENCHMARK_ARGS[4]} 2>&1 >&-\""
        "$EVAL_FUNCTION histogram-${BENCHMARK_ARGS[5]} @@N_RUNS 0 @@AVG          \
            \"run_histogram                                                    \
            $DATASETS_DIR/histogram_datafiles/${BENCHMARK_ARGS[5]} 2>&1 >&-\""
        "$EVAL_FUNCTION matrix_multiply-${BENCHMARK_ARGS[6]} @@N_RUNS 0 @@AVG    \
            \"run_matrix_multiply ${BENCHMARK_ARGS[6]} 1 2>&1 >&-\""
    )

    echo -n "Matrix creation for matrix_multiply... "
    MR_NUMTHREADS=$WARMUP_MR_NUMTHREADS                                        \
        $CODEDIR/matrix_multiply/matrix_multiply-rcl 500 1 1 2>&1 >&/dev/null
    echo "Done."


    for BENCHMARK_N in 0 3 5 6
    do
        BENCHMARK_NAME=${BENCHMARK_NAMES[$BENCHMARK_N]}
        BENCHMARK_NAME=${BENCHMARK_NAMES[$BENCHMARK_N]}
        BENCHMARK_COMMAND=${BENCHMARK_COMMANDS[$BENCHMARK_N]}

        echo
        echo "BENCHMARK = "$BENCHMARK_NAME
        echo


        ########################################################################
        # ID of the most contended lock                                        #
        ########################################################################

        OUTPUT_FILE_SUFFIX="_ticadfm"
        PREFIX="$BASEDIR/../lock-profiler/lock-profiler -b NONE                \
                -l NONE_CYCLES -a"
        #REDIRECTION="2>&1 > /dev/null"
        PARSING_COMMANDS="| grep \"#\" | sort -k 6 -n | tail -n 1 | tr -d \#   \
                          | awk '{ print \$3\" \"\$4\" \"\$5\" \"\$6 }'"

        RES=`eval $(echo $BENCHMARK_COMMAND | sed "s/@@N_RUNS/1/g"             \
                                            | sed "s/@@AVG/0/g")`

        ID="`echo $RES | tr  ',' ' ' | awk '{ print $2 }'`"
        ALLOC_ID="`echo $RES | tr ',' ' ' | awk '{ print $3 }'`"
        ADDRESS="`echo $RES | tr ',' ' ' | awk '{ print $4 }'`"
        TIME_IN_CS="`echo $RES | tr ',' ' ' | awk '{ print $5 }'`"

        echo -n "Found that the lock with the most contention spends "
        echo    "$TIME_IN_CS% of its time in CS."
        echo "Lock info: id=$ID, alloc_id=$ALLOC_ID, address=$ADDRESS."
        echo


        ########################################################################
        # Time in CS                                                           #
        ########################################################################

        echo "Time in CS for this lock :"
        echo "=========================="

        OUTPUT_FILE_SUFFIX="-mcl-time-in-cs"
        PREFIX="$BASEDIR/../lock-profiler/lock-profiler -b NONE \
            -l NONE_CYCLES -a"
        PARSING_COMMANDS="| grep \"$ALLOC_ID \" | awk '{ print \$6 }'"
        eval $(echo $BENCHMARK_COMMAND | sed "s/@@N_RUNS/$N_RUNS/g"            \
                                       | sed "s/@@AVG/1/g")


        ########################################################################
        # Data cache misses                                                    #
        ########################################################################

        echo "Data cache misses for this lock :"
        echo "================================="

        OUTPUT_FILE_SUFFIX="-mcl-l2-dcm"

        if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
        then
            PREFIX="$BASEDIR/../lock-profiler/lock-profiler -l PAPI            \
                    -e 0x80000002 -i -b BY_HW_THREAD -m PER_NODE -a"
        elif [[ $(hostname) == "niagara2.cs.rochester.edu" ]]
        then
            PREFIX="$BASEDIR/../lock-profiler/lock-profiler -l CPC             \
                    -e DC_miss -i -b BY_HW_THREAD -m PER_CORE -a"
        else
            echo "Unsupported machine."
            exit
        fi

        PARSING_COMMANDS="| grep \"$ALLOC_ID \" | awk '{ print \$7 }'"

        eval $(echo $BENCHMARK_COMMAND | sed "s/@@N_RUNS/$N_RUNS/g"            \
                                       | sed "s/@@AVG/1/g")

    done

    rm -f matrix_file_A.txt matrix_file_B.txt

else

    echo "Unsupported metric. Exiting."

fi

# if [[ $(uname) = "Linux" && $hugepages_flag == "1" ]]
# then
#     sudo sysctl -w vm.nr_hugepages=0
# fi
