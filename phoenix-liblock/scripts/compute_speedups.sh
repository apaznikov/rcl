#!/bin/bash
#
# compute_accelerations.sh
# ========================
# (C) Jean-Pierre Lozi,
#     Florian David,
#     Gaël Thomas,
#     Julia Lawall,
#     Gilles Muller
#     2013
#

RESULTS_FOLDER=$1
END_THREADS=$2

if [[ -z "$TOKEN" ]];         then TOKEN="library: ";                         fi
if [[ -z "$START_THREADS" ]]; then START_THREADS=1;                           fi
if [[ -z "$END_THREADS" ]];   then END_THREADS=47;                            fi

TOKEN_FILE_SUFFIX=`echo $TOKEN | tr -d ' :'`


compute_var()
{
	BASE=$1
	MEAN=$2
	ACC=0
	N=0

    while read LINE
    do
		let N=N+1
		ACC=$(echo "$ACC + ($BASE/$LINE - $MEAN)^2" | bc -l)
	done
	echo "sqrt($ACC/$N)" | bc -l
}

average()
{
    let TOT=0

    while read LINE
    do
        let TOT=$TOT+$LINE
    done

    let TOT=$TOT/$1
    echo "$TOT"
}

compute_average_run()
{
    NB_RUNS=`ls $1-execution-time-run*.csv | wc -l`
    for file in `ls $1-execution-time-run*.csv`
    do

        cat $file | grep "$TOKEN" | grep -v "inter library"                    \
                  | sed -e "s/$TOKEN//" | sed -e "s/ //g"

    done | average $NB_RUNS
}

compute_acceleration()
{
	BASE=$1
	BENCH=$2
	LOCK=$3

    while read LINE
    do

		N=`echo $LINE | cut -d',' -f1`
		VAL=`echo $LINE | cut -d',' -f2`
		AVG=`echo $BASE/$VAL | bc -l`
        VAR=`compute_average_run $RESULTS_FOLDER/phoenix-$BENCH-$LOCK-$N         \
             | compute_var $BASE $AVG`

		if [ $LOCK = "rcl" ]
        then
			echo $(echo $N+1 | bc), $AVG, $VAR
		else
			echo $N, $AVG, $VAR
		fi

    done
}

build_data()
{
    ARG_LIST=$1
    PROG=$2
    LOCKS="posix rcl mcs spinlock flat ccsynch dsmsynch"
    INTERVAL=`seq $START_THREADS $END_THREADS`

    for ARG in $ARG_LIST
    do

        for INPUT in ${PROG}-${ARG}
        do

            echo $INPUT

			for LOCK in $LOCKS
            do
				for N_THREADS in $INTERVAL
                do
					echo -n "$N_THREADS, "
					compute_average_run \
                        $RESULTS_FOLDER/phoenix-$INPUT-$LOCK-$N_THREADS
				done > $RESULTS_FOLDER/phoenix-$INPUT-$LOCK.csv
			done

            BASE=$(compute_average_run $RESULTS_FOLDER/phoenix-$INPUT-baseline-1)

            echo "BASE=$BASE"

			for LOCK in $LOCKS
            do
				compute_acceleration $BASE $INPUT $LOCK                        \
                    < $RESULTS_FOLDER/phoenix-$INPUT-$LOCK.csv                 \
                    > $RESULTS_FOLDER/acc-$INPUT-$TOKEN_FILE_SUFFIX-$LOCK.csv
			done

		done

    done
}

build_all()
{
    ARG_LIST="key_file_50MB.txt key_file_100MB.txt key_file_500MB.txt"
    build_data "$ARG_LIST" linear_regression

    ARG_LIST="500 1000 1500"
    build_data "$ARG_LIST" pca

    ARG_LIST="10000 50000 100000"
    build_data "$ARG_LIST" kmeans

    ARG_LIST="key_file_50MB.txt key_file_100MB.txt key_file_500MB.txt"
    build_data "$ARG_LIST" string_match

    ARG_LIST="word_10MB.txt word_50MB.txt word_100MB.txt"
    build_data "$ARG_LIST" word_count

    ARG_LIST="small.bmp med.bmp large.bmp" # hist-2.6g.bmp"
    build_data "$ARG_LIST" histogram

    ARG_LIST="100 500 1000"
    build_data "$ARG_LIST" matrix_multiply
}

build_main()
{
    ARG_LIST="key_file_100MB.txt"
    build_data "$ARG_LIST" linear_regression

    ARG_LIST="key_file_100MB.txt"
    build_data "$ARG_LIST" string_match

    ARG_LIST="500"
    build_data "$ARG_LIST" matrix_multiply

    ARG_LIST="med.bmp"
    build_data "$ARG_LIST" histogram
}


build_main

