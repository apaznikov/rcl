#!/usr/bin/python
#
# get_maximums.py
# ===============
# (C) Jean-Pierre Lozi,
#     Florian David,
#     Gael Thomas,
#     Julia Lawall,
#     Gilles Muller
#     2013
#

import csv
import glob
import os
import sys

if len(sys.argv) < 2:
    print "usage: ", sys.argv[0], " <results-directory>"
    sys.exit(0)

os.chdir(sys.argv[1])

for exp in ['histogram-med.bmp', 'linear_regression-key_file_100MB.txt',
            'matrix_multiply-500', 'string_match-key_file_100MB.txt']:

    posix_max = -1.0

    #print "%s:" % exp
    print "%s" % exp,

    locks = ["posix", "spinlock", "mcs", "flat", "ccsynch", "dsmsynch", "rcl"]

    for lock in locks:

        file = open("acc-%s-library-%s.csv" % (exp, lock), "rb")
        reader = csv.reader(file)

        row_num = 0
        for row in reader:

            col_num = 0
            for col in row:

                if col_num > 1:
                    continue

                f_col=float(col)

                if row_num == 0 and col_num == 1:
                    maximum = float(col)
                else:
                    if col_num == 0:
                        index = f_col
                    if col_num == 1 and f_col > maximum:
                        maximum = f_col
                        max_index = int(index)

                col_num += 1

            row_num += 1

        if posix_max < 0:
            posix_max = maximum

        #print "%-65s: x%f\t/ %2d" % (file, maximum, max_index)
        print "%9.6f  %3d  %9.6f" % (maximum/ posix_max, max_index, maximum),

        file.close()

    print "  ?"

