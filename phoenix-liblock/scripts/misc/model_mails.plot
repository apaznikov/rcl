
reset;
set ytics auto;
set xtics auto;
#set terminal aqua dashed enhanced font "Helvetica,18";
set terminal pngcairo  font "Terminus, 9" dashed size 640, 480;
set key default;
set key above;
set key center;
set datafile separator ",";

#set size ratio 0.5
#set lmargin 8
#set bmargin 2.8
#set rmargin 1.2
#set tmargin 0.7

set xrange[1:end_threads];
#set yrange[1:20];

set xlabel "Number of cores";
set ylabel "Speedup";
set y2label "";

#set size 1.0, 0.8

set ytics 1
set xtics x_tics

set grid nopolar
set grid xtics nomxtics ytics nomytics noztics nomztics \
 nox2tics nomx2tics noy2tics nomy2tics nocbtics nomcbtics
set grid back   linetype 0 linecolor rgb "#808080"  linewidth 1.000,  linetype 0 linecolor rgb "#808080"  linewidth 1.000


plot "results/acceleration-radiosity-posix.csv" using 1:2:3 axes x1y1 title "posix"      with yerrorlines lc rgb "#015C65" lt 1 pt 13,\
		 "results/acceleration-radiosity-spinlock.csv"   using 1:2:3 axes x1y1 title "spin"   with yerrorlines lc rgb "#409300" lt 1 pt 1,\
		 "results/acceleration-radiosity-mcs.csv"        using 1:2:3 axes x1y1 title "mcs"        with yerrorlines lc rgb "#62AA2A" lt 2 pt 9,\
		 "results/acceleration-radiosity-flat.csv"       using 1:2:3 axes x1y1 title "flat"       with yerrorlines lc rgb "#95002B" lt 1 pt 5,\
		 "results/acceleration-radiosity-rcl.csv"   using 1:2:3 axes x1y1 title "rcl"        with yerrorlines lc rgb "#FF7800" lw 1 lt 1 pt 7,\
		 "results/acceleration-radiosity-ccsynch.csv"   using 1:2:3 axes x1y1 title "ccsynch"        with yerrorlines lc rgb "#AC2B50" lw 1 lt 2 pt 6,\
		 "results/acceleration-radiosity-dsmsynch.csv"   using 1:2:3 axes x1y1 title "dsmsynch"        with yerrorlines lc rgb "#E60042" lw 1 lt 3 pt 4
#		 "results/acceleration-radiosity-no-liblock.csv"      using 1:2:3 axes x1y1 title "no-liblock" with yerrorlines lc rgb "#000094" lt 3 pt 6

