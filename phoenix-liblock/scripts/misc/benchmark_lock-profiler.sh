#!/bin/bash
cd $( dirname "${BASH_SOURCE[0]}")/..

DIR=phoenix-rcl
#INTERVAL=`seq $START_THREADS $END_THREADS`
#INTERVAL="1 4 8 16 32 48"
INTERVAL="47"


BASEDIR=`pwd`
CODEDIR=$BASEDIR/$DIR/tests
DATASETS=$BASEDIR/datasets
MUTRACE_RCL="/home/florian/margaux/rcl/lock-profiler/lock-profiler -a"


run_histogram() {
	if [ -f $CODEDIR/histogram/histogram$EXT ]; then
	        $MUTRACE_RCL $CODEDIR/histogram/histogram$EXT $1
	fi
}

run_linear_regression() {
	if [ -f $CODEDIR/linear_regression/linear_regression$EXT ]; then
	        $MUTRACE_RCL $CODEDIR/linear_regression/linear_regression$EXT $1
	fi
}

run_kmeans() {
	if [ -f $CODEDIR/kmeans/kmeans$EXT ]; then
	        MUTRACE_MAX=100 $MUTRACE_RCL $CODEDIR/kmeans/kmeans$EXT $1 $2
	fi
}

run_pca() {
	if [ -f $CODEDIR/pca/pca$EXT ]; then
	        $MUTRACE_RCL $CODEDIR/pca/pca$EXT $1 $2 $3 $4
	fi
}

run_matrix_multiply() {
	if [ -f $CODEDIR/matrix_multiply/matrix_multiply$EXT ]; then
	        $MUTRACE_RCL $CODEDIR/matrix_multiply/matrix_multiply$EXT $1
	fi
}

run_word_count() {
	if [ -f $CODEDIR/word_count/word_count$EXT ]; then
	        $MUTRACE_RCL $CODEDIR/word_count/word_count$EXT $1
	fi
}

run_string_match() {
	if [ -f $CODEDIR/string_match/string_match$EXT ]; then
	        $MUTRACE_RCL $CODEDIR/string_match/string_match$EXT $1
	fi
}


eval_exp() {
		RUNSS=$1
		shift;
		FILE=$1
		shift;

		#echo "-- Running: $@ with '$LIBLOCK_LOCK_NAME' lock and $MR_NULTHREADS cores --"
		eval $@ > $FILE.log #| awk -f grep_mutrace > $FILE.log
}

eval_all_of() {
		NAME=$1
		RUNS=$2
		shift; shift

		# Warmup run
		echo "Warmup $NAME"
 		MR_NUMTHREADS=20 eval $@ > /dev/null

		# Lock-profilers runs
 		for y in $INTERVAL; do
		    echo "Run $NAME for $y threads: $@"
		    MR_NUMTHREADS=$y eval_exp $RUNS results/phoenix-lock-profiler-$NAME-$y "$@"
 		done
}


eval_all() {
		export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$BASEDIR/../liblock

		RUNS=1
		echo "RUNS: $RUNS - THREADS: $INTERVAL"
		#\rm -Rf results
		mkdir results

                # Linear_regression
                ARG_LIST="key_file_100MB.txt" #"key_file_50MB.txt key_file_100MB.txt key_file_500MB.txt"
		for ARG in $ARG_LIST; do
                    eval_all_of linear_regression-$ARG $RUNS "run_linear_regression $DATASETS/linear_regression_datafiles/$ARG 2>&1"
                done

		# PCA
                ARG_LIST="1000" #"500 1000 1500"
                for ARG in $ARG_LIST; do
		    eval_all_of pca-$ARG $RUNS "run_pca -r $ARG -c $ARG 2>&1"
                done

                # Kmeans
                ARG_LIST="50000" #"10000 50000 100000"
                for ARG in $ARG_LIST; do
		    eval_all_of kmeans-$ARG $RUNS "run_kmeans -p $ARG 2>&1"
                done

		# String_match
                ARG_LIST="key_file_100MB.txt" #"key_file_50MB.txt key_file_100MB.txt key_file_500MB.txt"
		for ARG in $ARG_LIST; do
                    eval_all_of string_match-$ARG $RUNS "run_string_match $DATASETS/string_match_datafiles/$ARG 2>&1"
                done

                # Word_count
                ARG_LIST="word_50MB.txt" #"word_10MB.txt word_50MB.txt word_100MB.txt"
		for ARG in $ARG_LIST; do
                    eval_all_of word_count-$ARG $RUNS "run_word_count $DATASETS/word_count_datafiles/$ARG 2>&1"
		done

                # Histogram
                ARG_LIST="med.bmp" #"small.bmp med.bmp large.bmp"
		for ARG in $ARG_LIST; do
                    eval_all_of histogram-$ARG $RUNS "run_histogram $DATASETS/histogram_datafiles/$ARG 2>&1"
		done

		# Matrix_multiply
                ARG_LIST="500" #"100 500 1000"
		for ARG in $ARG_LIST; do
		    # Matric creation
		    MR_NUMTHREADS=20 $CODEDIR/matrix_multiply/matrix_multiply-rcl $ARG 1 1 2>&1 >&/dev/null # matrix_side row_block_length create_files
                    eval_all_of matrix_multiply-$ARG $RUNS "run_matrix_multiply $ARG 2>&1"
		    rm -f matrix_file_A.txt matrix_file_B.txt
                done

		date
}


eval_all
