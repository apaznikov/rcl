#!/bin/bash

if [[ $(uname) == "SunOS" ]]
then
    TAR=gtar
else
    TAR=tar
fi



# http://mapreduce.stanford.edu/
if [[ ! -f histogram.tar.gz ]]
then
    wget http://mapreduce.stanford.edu/datafiles/histogram.tar.gz
fi
$TAR -xvzf histogram.tar.gz


if [[ ! -f linear_regression.tar.gz ]]
then
    wget http://mapreduce.stanford.edu/datafiles/linear_regression.tar.gz
fi
$TAR -xvzf linear_regression.tar.gz
cd linear_regression_datafiles
cp key_file_500MB.txt key_file_1GB.txt
cat key_file_500MB.txt >> key_file_1GB.txt
cp key_file_1GB.txt key_file_1.5GB.txt
cat key_file_500MB.txt >> key_file_1.5GB.txt
cp key_file_1.5GB.txt key_file_3GB.txt
cat key_file_1.5GB.txt >> key_file_3GB.txt
cd ..


if [[ ! -f string_match.tar.gz ]]
then
    wget http://mapreduce.stanford.edu/datafiles/string_match.tar.gz
fi
$TAR -xvzf string_match.tar.gz
cd string_match_datafiles
cp key_file_500MB.txt key_file_1GB.txt
cat key_file_500MB.txt >> key_file_1GB.txt
cp key_file_1GB.txt key_file_1.5GB.txt
cat key_file_500MB.txt >> key_file_1.5GB.txt
cd ..


# wget http://mapreduce.stanford.edu/datafiles/reverse_index.tar.gz
# $(TAR) xvf reverse_index.tar.gz
# rm reverse_index.tar.gz


if [[ ! -f word_count.tar.gz ]]
then
    wget http://mapreduce.stanford.edu/datafiles/word_count.tar.gz
fi
$TAR -xzvf word_count.tar.gz
cd word_count_datafiles
cp word_100MB.txt word_200MB.txt
cat word_100MB.txt >> word_200MB.txt
cp word_200MB.txt word_300MB.txt
cat word_100MB.txt >> word_300MB.txt
cp word_300MB.txt word_500MB.txt
cat word_200MB.txt >> word_500MB.txt
cd ..


rm -rf /tmp/rcl-phoenix-datasets
cp -r . /tmp/rcl-phoenix-datasets
rm -rf /tmp/rcl-phoenix-datasets/.svn

