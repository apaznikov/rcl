#define _GNU_SOURCE
#include <sched.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <liblock.h>
#include <liblock-fatal.h>
#include <liblock-phoenix.h>
#ifdef __sun__
#include <errno.h>
#include <sys/types.h>
#include <sys/processor.h>
#include <sys/procset.h>
#endif

const  char*  liblock_lock_name;
struct hw_thread* server_hw_thread_1;
struct hw_thread* server_hw_thread_2;

static int volatile go = 0;

static void do_go()
{
	go = 1;
}

extern struct hw_thread* get_server_core_1()
{
//  static int zzz=0;
// 	struct hw_thread* res;
// 	if(__sync_fetch_and_add(&zzz, 1) % 2)
// 	 	res = topology->nodes[0].hw_threads[1];
// 	else
// 	 	res = topology->nodes[0].hw_threads[2];
//  printf("---> hw_thread %d (%d)\n", res->hw_thread_id, servers[res->hw_thread_id].state);

// 	return res;
	return server_hw_thread_1;
}

extern struct hw_thread* get_server_core_2()
{
	return server_hw_thread_2;
}

static int cur_hw_thread = 0;

void liblock_auto_bind()
{
	int n;
	struct hw_thread*      hw_thread;
	struct numa_node* node;

	do
    {
		n = __sync_fetch_and_add(&cur_hw_thread, 1) % topology->nb_hw_threads;
		for(node=topology->nodes, hw_thread=0; !hw_thread; node++)
			if(n < node->nb_hw_threads)
				hw_thread = node->hw_threads[n];
			else
				n -= node->nb_hw_threads;
	}
    while(hw_thread->server_type);

	self.running_hw_thread = hw_thread;

#ifdef __linux__
	cpu_set_t    cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(hw_thread->hw_thread_id, &cpuset);
	if(pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset))
	 	fatal("pthread_setaffinity_np");
#else
    if(processor_bind(P_LWPID, P_MYID, hw_thread->hw_thread_id, NULL))
        fatal("processor_bind (%s)", strerror(errno));
#endif

//  printf("thread %d bound to %d\n", self.id, sched_getcpu());
}

int liblock_find_free_cpu()
{
  int n;
  struct hw_thread*      hw_thread;
  struct numa_node* node;

    do
    {
        n = __sync_fetch_and_add(&cur_hw_thread, 1) % topology->nb_hw_threads;
        for(node=topology->nodes, hw_thread=0; !hw_thread; node++)
            if(n < node->nb_hw_threads)
	            hw_thread = node->hw_threads[n];
            else
	            n -= node->nb_hw_threads;
    }
    while(hw_thread->server_type);

//  self.running_hw_thread = hw_thread;

    printf("Return %d\n", hw_thread->hw_thread_id);
    return hw_thread->hw_thread_id;
}

void start_hw_thread(struct hw_thread* hw_thread, const char* name)
{
	liblock_start_server_threads_by_hand = 1;
	liblock_reserve_hw_thread_for(hw_thread, name);
	liblock_lookup(name)->run(do_go); /* launch the liblock threads */

	// if(servers[hw_thread->hw_thread_id].state != 3)
	while(!go)
		PAUSE();

	usleep(50000);
}

#ifdef _DEFAULT_LIBLOCK_PHOENIX_INIT
__attribute__ ((constructor (103))) static void liblock_phoenix()
{
	char get_cmd[128];
	const char* sc = getenv("LIBLOCK_SERVER_CORE");
	int is_rcl;

    liblock_init_library();

	server_hw_thread_1 = topology->nodes[0].hw_threads[1];
//  server_hw_thread_2 = topology->nodes[0].hw_threads[1];

	liblock_lock_name = getenv("LIBLOCK_LOCK_NAME");

	if(!liblock_lock_name)
		liblock_lock_name = "posix";

	is_rcl = !strcmp(liblock_lock_name, "rcl") ||
             !strcmp(liblock_lock_name, "multircl");

	liblock_servers_always_up = 1;
    liblock_rcl_no_local_cas = 1;

#ifdef __linux__
	sprintf(get_cmd, "/proc/%d/cmdline", (int)getpid());
	FILE* f=fopen(get_cmd, "r");
	if(!f)
		printf("!!! warning: unable to find command line\n");

	char buf[1024];
 	buf[0] = 0;
	if(!fgets(buf, 1024, f))
		printf("fgets\n");

	fprintf(stdout, "**** testing %s with lock %s placed on hw_thread %d/%d\n",
                    buf, liblock_lock_name, server_hw_thread_1->hw_thread_id,
                    -1);
	//              server_hw_thread_2->hw_thread_id);
#endif

	if(is_rcl)
    {
		go = 0;
		start_hw_thread(server_hw_thread_1, liblock_lock_name);
//      start_hw_thread(server_hw_thread_2, liblock_lock_name);
	}

	liblock_auto_bind();
}
#endif

