#!/bin/bash
# color-echo.sh

# FIXME: these color codes are wrong.
black='\E[30;208m'
red='\E[31;208m'
ltred='\e[38;05;161m'
green='\E[32;208m'
ltgreen='\e[38;05;118m'
yellow='\E[33;208m'
blue='\E[34;208m'
magenta='\E[35;208m'
cyan='\E[36;208m'
white='\E[37;208m'
orange='\e[38;05;208m'

cecho ()                     # Color-echo.
                             # Argument $1 = message
                             # Argument $2 = color
{
local default_msg="No message passed."
                             # Doesn't really need to be a local variable.

message=${1:-$default_msg}   # Defaults to default message.
color=${2:-$black}           # Defaults to black, if not specified.

  echo -ne "$color"
  echo $3 "$message"
  tput sgr0

  return
}

