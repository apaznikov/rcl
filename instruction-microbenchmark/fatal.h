/******************************************************************************/
/* CAS vs. Store                                                              */
/* (C) Jean-Pierre Lozi, 2013                                                 */
/******************************************************************************/
#ifndef _FATAL_H_
#define _FATAL_H_

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define echo(msg, ...)                                                         \
    do                                                                         \
    {   											                           \
		fprintf(stderr, "%-15s", "["msg"]: ");                                 \
		fprintf(stderr, __VA_ARGS__);                                          \
		fprintf(stderr, "\n");                                                 \
	}                                                                          \
    while(0)

#define warning(...) echo("warning", __VA_ARGS__)

#define fatal(...)                                                             \
    do                                                                         \
    {	      					            								   \
		echo("error", __VA_ARGS__);											   \
		fprintf(stderr, "   at %s::%d (%s)\n", __FILE__, __LINE__,             \
                __PRETTY_FUNCTION__);                                          \
		abort();															   \
	}                                                                          \
    while(0)

#endif
