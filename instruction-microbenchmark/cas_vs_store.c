/******************************************************************************/
/* CAS vs. Store                                                              */
/* (C) Jean-Pierre Lozi, 2013                                                 */
/******************************************************************************/
#define _GNU_SOURCE

#include <errno.h>
#include <pthread.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#ifndef WITH_GETHRTIME
#include <papi.h>
#endif

#ifdef __sun__
#include <sys/types.h>
#include <sys/procset.h>
#include <sys/processor.h>
#elif defined(__linux__)
#include <numa.h>
#endif

#include "fatal.h"

#ifndef WITH_GETHRTIME
#define get_time() get_cycles()
#else
#define get_time() gethrtime()
#endif

#define SYNC_VARIABLE_NUMA_NODE                                       2
#define CPU_FREQUENCY_GHZ                                             1.165
#define N_RUNS                                                        30
#define OVERHEAD_ITERATIONS                                           1000
#define NUM_OPERATIONS_PER_THREAD                                     1000000
#define SAMPLING_RATE                                                 1000
#define AMOUNT_OF_NON_MONITORED_OPERATIONS                            0.4


#ifdef WITH_CAS
#define OPERATION { __sync_val_compare_and_swap(&global_variables_p->sync,\
                                                 0, 1); }
#elif defined(WITH_STORE)
#define OPERATION { &global_variables_p->sync = 1; __sync_synchronize(); }
#else
#error "Unsupported operation."
#endif


static void *background_thread_function(void *argument);
static void *monitored_thread_function(void *argument);
static void bind_thread(int core_id);
static inline long long get_cycles( void );

struct global_variables {
    uint64_t padding1[16];
    int sync;
    uint64_t padding2[16];
};


static int num_hw_threads;
static int num_hw_threads_per_core;

static volatile struct global_variables *global_variables_p;
static pthread_barrier_t barrier;

int main (int argc, char **argv)
{
    long i, j;
    void *result;
    int num_threads;
    long long before, after, elapsed_time, total_elapsed_time;
    double operation_time, operation_time_sum, operation_time_average;
    pthread_t *background_threads;
    pthread_t monitored_thread;

#ifdef __linux__
    global_variables_p = numa_alloc_onnode(sizeof(struct global_variables),
                                           SYNC_VARIABLE_NUMA_NODE);
    if (!global_variables_p)
        fatal("numa_alloc_onnode");
#else
    global_variables_p = malloc(sizeof(struct global_variables));
    if (!global_variables_p)
        fatal("malloc");
#endif

#ifdef WITH_CAS
    printf("Operation = CAS, ");
#elif defined(WITH_STORE)
    printf("Operation = STORE, ");
#endif

#ifndef WITH_GETHRTIME
    printf("using get_cycles().\n\n");
#else
    printf("using gethrtime().\n\n");
#endif

/*
#ifndef WITH_GETHRTIME
    printf("Computing get_cycles() overhead...\n");

    total_elapsed_time = 0;

    for (i = 0; i < OVERHEAD_ITERATIONS; i++)
    {
        before = get_cycles();
        after = get_cycles();
        elapsed_time = after - before;


        if (i < 10) printf("%lld,", elapsed_time);

        total_elapsed_time += elapsed_time;
    }

    printf("...\n");

    printf("get_cycles() overhead = %f cycles\n\n",
           (double)total_elapsed_time / OVERHEAD_ITERATIONS);
#else
    printf("Computing gethrtime() overhead...\n");

    total_elapsed_time = 0;

    for (i = 0; i < OVERHEAD_ITERATIONS; i++)
    {
        before = gethrtime();
        after = gethrtime();
        elapsed_time = after - before;

        if (i < 10) printf("%lld,", elapsed_time);

        total_elapsed_time += elapsed_time;
    }

    printf("...\n");

    printf("gethrtime() overhead = %f nanoseconds\n",
           (double)total_elapsed_time / OVERHEAD_ITERATIONS);
    printf("                     = %f cycles "
           "(with a clock speed of %f Ghz)\n\n",
           (double)total_elapsed_time / OVERHEAD_ITERATIONS
                                      * CPU_FREQUENCY_GHZ,
           CPU_FREQUENCY_GHZ);
#endif
*/

    if (argc != 4)
    {
        fprintf(stderr, "Usage: %s <num_hw_threads_per_core> <num_hw_threads> "
                        "<num_threads> (recommended value: "
                        "<num_threads>=<num_cores>)\n", argv[0]);
        return EXIT_FAILURE;
    }

    num_hw_threads_per_core = atoi(argv[1]);
    num_hw_threads = atoi(argv[2]);
    num_threads = atoi(argv[3]);
    printf("Starting the benchmark with %d threads.\n", num_threads);

    background_threads = malloc(num_threads * sizeof(pthread_t));
    if (!background_threads)
        fatal("malloc");


/*
    printf("\n");
    printf("================\n");
    printf("Uncontended case\n");
    printf("================\n");
    printf("\n");
    printf("Running");
    fflush(stdout);


    operation_time_sum = 0;

    for (i = 0 ; i < N_RUNS; i++)
    {
        global_variables_p->sync = 0;

        pthread_create(&monitored_thread, NULL, monitored_thread_function,
                       (void *)0);

        pthread_join(monitored_thread, &result);

        operation_time = *((double *)result);
        free(result);

        operation_time_sum += operation_time;

        printf(".");
        fflush(stdout);
    }


    printf("\n");

#ifndef WITH_GETHRTIME
    printf("Average operation time = %f cycles\n", operation_time_sum / N_RUNS);
#else
    printf("Average operation time = %f nanoseconds\n",
           operation_time_sum / N_RUNS);
    printf("                       = %f cycles\n",
           (operation_time_sum / N_RUNS) * CPU_FREQUENCY_GHZ);
#endif
*/

    printf("\n");
    printf("==============\n");
    printf("Contended case\n");
    printf("==============\n");
    printf("\n");
    printf("Running, values = [");
    fflush(stdout);


    operation_time_sum = 0;

    for (i = 0 ; i < N_RUNS; i++)
    {
        global_variables_p->sync = 0;

        if(pthread_barrier_init(&barrier, NULL, num_threads))
            fatal("pthread_barrier_init");

        for (j = 0 ; j < num_threads - 1 ; j++)
        {
            pthread_create(&background_threads[j], NULL,
                           background_thread_function, (void*)j);
        }

        pthread_create(&monitored_thread, NULL, monitored_thread_function,
                       (void *)1);

        for (j = 0 ; j < num_threads - 1 ; j++)
        {
            pthread_join(background_threads[j], NULL);
        }

        pthread_join(monitored_thread, &result);

        operation_time = *((double *)result);
        free(result);

        operation_time_sum += operation_time;

        printf("%f", operation_time);
        if (i != N_RUNS - 1) printf(",");

        fflush(stdout);
    }


    printf("]\n");

#ifndef WITH_GETHRTIME
    printf("Average operation time = %f cycles\n", operation_time_sum / N_RUNS);
#else
    printf("Average operation time = %f nanoseconds\n",
           operation_time_sum / N_RUNS);
    printf("                       = %f cycles\n",
           (operation_time_sum / N_RUNS) * CPU_FREQUENCY_GHZ);
#endif

    printf("\n");
    printf("Final value of sync = %d.\n", global_variables_p->sync);


    free(background_threads);


    return EXIT_SUCCESS;
}

static void *background_thread_function(void *argument)
{
    int result, i, hardware_thread_id;
    int thread_id = (int)(long)argument;

    hardware_thread_id = ((thread_id + 1) * num_hw_threads_per_core) % num_hw_threads
                       + ((thread_id + 1) * num_hw_threads_per_core) / num_hw_threads;

    bind_thread(hardware_thread_id);

    result = pthread_barrier_wait(&barrier);

    if (result != 0 && result != PTHREAD_BARRIER_SERIAL_THREAD)
        fatal("pthread_barrier_wait");

    for (i = 0 ; i < NUM_OPERATIONS_PER_THREAD ; i++)
    {
/*
        // Implementation #1
        // =================

        OPERATION

        global_variables_p->sync = 0;
*/

/*
        // Implementation #2
        // =================

        global_variables_p->sync = 1;
        __sync_synchronize();
        global_variables_p->sync = 0;
*/

/*
        // Implementation #3a
        // ==================
        // (Different workload for background threads)

        int value = i % 2;

#ifdef WITH_CAS
        __sync_val_compare_and_swap(&global_variables_p->sync,
                                    1 - value, value);
#elif defined(WITH_STORE)
        global_variables_p->sync = value;
        __sync_synchronize();
#endif
*/

        // Implementation #3b
        // ==================
        // (Same workload for background threads)

        int value = i % 2;

        global_variables_p->sync = value;
        __sync_synchronize();
    }

    return EXIT_SUCCESS;
}

static void *monitored_thread_function(void *argument)
{
    int number_of_samples, first_monitored_operation, last_monitored_operation,
        result, i;
    long long operation_beginning, operation_end;
    unsigned long long total_time;
    double average_operation_time, *thread_result_p;
    int contended = (int)(long)argument;

    bind_thread(0);
    total_time = 0;
    number_of_samples = 0;

    first_monitored_operation =
        NUM_OPERATIONS_PER_THREAD * (AMOUNT_OF_NON_MONITORED_OPERATIONS / 2);

    last_monitored_operation =
        NUM_OPERATIONS_PER_THREAD - first_monitored_operation - 1;

    if (contended)
    {
        result = pthread_barrier_wait(&barrier);
        if (result != 0 && result != PTHREAD_BARRIER_SERIAL_THREAD)
            fatal("pthread_barrier_wait");
    }

    for (i = 0 ; i < NUM_OPERATIONS_PER_THREAD ; i++)
    {
/*
        // Implementation #1 & #2
        // ======================

        if (i % SAMPLING_RATE == 0
            && i >= first_monitored_operation
            && i <= last_monitored_operation)
        {
            operation_beginning = get_time();

            OPERATION

            operation_end = get_time();

            total_time += operation_end - operation_beginning;

            number_of_samples++;
        }
        else
        {
            OPERATION
        }

        global_variables_p->sync = 0;
*/

        // Implementation #3
        // =================

        int value = i % 2;

        if (i % SAMPLING_RATE == 0
            && i >= first_monitored_operation
            && i <= last_monitored_operation)
        {
            operation_beginning = get_cycles();
#ifdef WITH_CAS
            __sync_val_compare_and_swap(&global_variables_p->sync, 1 - value,
                                        value);
#elif defined(WITH_STORE)
            global_variables_p->sync = value;
            __sync_synchronize();
#endif
            operation_end = get_cycles();

            total_time += operation_end - operation_beginning;

            number_of_samples++;
        }
        else
        {
#ifdef WITH_CAS
           __sync_val_compare_and_swap(&global_variables_p->sync,
                                       1 - value, value);
#elif defined(WITH_STORE)
            global_variables_p->sync = value;
            __sync_synchronize();
#endif
        }
    }

    average_operation_time = total_time / number_of_samples;

    if((thread_result_p = malloc(sizeof(double))) == NULL)
        fatal("malloc");

    *thread_result_p = average_operation_time;

    return thread_result_p;
}

static void bind_thread(int core_id)
{
#ifdef __linux__
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(core_id, &cpuset);
    if (sched_setaffinity(0, sizeof(cpu_set_t), &cpuset))
        fatal("pthread_setaffinity");
#elif defined(__sun__)
    if (processor_bind(P_LWPID, P_MYID, core_id, NULL))
        fatal("processor_bind (%s)", strerror(errno));
#endif
}


#if (defined(__i386__) || defined(__x86_64__))
__attribute__((always_inline))
static inline long long get_cycles(void)
{
    long long ret = 0;

#ifdef __x86_64__
/*
    unsigned int a, d;

    asm volatile ( "rdtsc":"=a" (a), "=d" (d));
    ret = ((long long)a) | (((long long)d) << 32);
*/
    uint64_t rax, rdx;
    uint32_t aux;

    asm volatile ( "rdtscp\n" : "=a" (rax), "=d" (rdx), "=c" (aux): : );
    ret = (rdx << 32) + rax;
#else
    __asm__ __volatile__("rdtsc" : "=A"(ret):);
#endif

    return ret;
}
#elif defined(__sparc__)
__attribute__((always_inline))
static inline long long get_cycles(void)
{
    register unsigned long ret asm("g1");

    asm volatile(".word 0x83410000" /* rd %tick, %g1 */
                 : "=r"(ret));

    return ret;
}
#else
#error Unsupported architecture.
#endif

