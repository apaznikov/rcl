#!/bin/bash
#
# benchmark.sh
# ============
# (C) Jean-Pierre Lozi, 2011
#

if [[ $(hostname) == "amd48-systeme" || $(hostname) == "amd48b-systeme"  ]]
then
    N_HW_THREADS_PER_CORE=1
    N_HW_THREADS=48
    NS_THREADS="1 2 24 48"
elif [[ $(hostname) == "niagara2.cs.rochester.edu" ]]
then
    N_HW_THREADS_PER_CORE=8
    N_HW_THREADS=128
    NS_THREADS="1 2 24 48 64 128"
else
    echo "Unsupported machine."
    exit -1
fi

for APP in cas store
do
    for I in $NS_THREADS
    do
        ./$APP $N_HW_THREADS_PER_CORE $N_HW_THREADS $I | grep Average \
                | cut -d " " -f 5 > tmp
        cat tmp | sed 's/^/scale=0\;/' | sed 's/$/\/1/' | bc \
                | sed 's/$/c (/' | tr -d '\n'
        cat tmp | sed 's/^/scale=1\;/' | sed 's/$/\/1.165/' | bc \
                | sed 's/$/ns) \& /' | tr -d '\n'
        rm tmp
    done

    echo
done
