#!/bin/bash
#
# to3d.sh
# =======
# (C) Jean-Pierre Lozi, 2013
#

y=0

x=0
echo "0,0,0"
for x in {0..127}
do
    echo $((y+k))","$x","0
    echo $((y+k))","$((x+1))","0
    x=$(($x+1))
done
echo "0,128,0"

echo

while read inputline
do
	for k in {0..1}
	do
		x=0

		echo $((y+k))","$x","0

		for x in {0..127}
		do
            num="$(echo $inputline | cut -d";" -f$((x+1)))"
			echo $((y+k))","$x","$num
			echo $((y+k))","$((x+1))","$num
			x=$(($x+1))
		done

        echo $((y+k))","$x","0

		echo
	done

	y=$(($y+1))
done

y=$((y-1))

x=0

echo "128,0,0"
for x in {0..127}
do
    echo $((y+k))","$x","0
    echo $((y+k))","$((x+1))","0
    x=$(($x+1))
done
echo "128,128,0"

