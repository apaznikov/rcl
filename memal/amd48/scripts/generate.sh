#!/bin/bash
#
# generate.sh
# ===========
# (C) Jean-Pierre Lozi, 2013
#

if [[ -z "$RESULTS_FOLDER" ]]
then
    RESULTS_FOLDER="results-phd-140410"
fi

for BENCHMARK in l2 #l1 l2
do
    cat ../all-results/$RESULTS_FOLDER/${BENCHMARK}_raw.csv | ./to3d.sh        \
        | tee ../all-results/$RESULTS_FOLDER/$BENCHMARK.csv
done

