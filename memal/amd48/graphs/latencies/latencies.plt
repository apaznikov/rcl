#!/opt/local/bin/gnuplot -persist
#
#
#    	G N U P L O T
#    	Version 4.2 patchlevel 5
#    	last modified Mar 2009
#    	System: Darwin 10.4.0
#
#    	Copyright (C) 1986 - 1993, 1998, 2004, 2007 - 2009
#    	Thomas Williams, Colin Kelley and many others
#
#    	Type `help` to access the on-line reference manual.
#    	The gnuplot FAQ is available from http://www.gnuplot.info/faq/
#
#    	Send bug reports and suggestions to <http://sourceforge.net/projects/gnuplot>
#
set terminal pdfcairo  transparent enhanced font "Helvetica, 13" fontscale 0.5 dashed size 5.50in, 3.00in
set output "output/@@BENCHMARK@@.pdf"
unset clip points
set clip one
unset clip two
set bar 1.000000
set border 31 front linetype -1 linewidth 1.000
set xdata
set ydata
set zdata
set x2data
set y2data
set timefmt x "%d/%m/%y,%H:%M"
set timefmt y "%d/%m/%y,%H:%M"
set timefmt z "%d/%m/%y,%H:%M"
set timefmt x2 "%d/%m/%y,%H:%M"
set timefmt y2 "%d/%m/%y,%H:%M"
set timefmt cb "%d/%m/%y,%H:%M"
set boxwidth
set style fill  empty border
set style rectangle back fc  lt -3 fillstyle  solid 1.00 border -1
set dummy x,y
set format x "% g"
set format y "% g"
set format x2 "% g"
set format y2 "% g"
set format z "% g"
set format cb "% g"
set angles radians
unset grid
set key title ""
set key inside right top vertical Right noreverse enhanced autotitles nobox
set key noinvert samplen 4 spacing 1 width 0 height 0
unset label
#set label 1 "4" at 0.43, second 0.45, 0 left norotate front nopoint offset character 0, 0, 0
#set label 2 "72" at -0.01, second 3.75, 0 left norotate front nopoint offset character 0, 0, 0
#set label 3 "126" at -0.4, second 8, 0 left norotate front nopoint offset character 0, 0, 0
set label 10 "Latency (cycles)" at screen 0.025, 0.5, 0 left rotate by 90 back nopoint offset character 0, 0, 0
unset arrow
set style increment default
unset style line
set style line 1  linetype 1 linecolor rgb "black" linewidth 0.4 pointtype 1 pointsize default
set style line 100  linetype 5 linecolor rgb "black" linewidth 0.4 pointtype 100 pointsize default
unset style arrow
set style histogram clustered gap 2 title  offset character 0, 0, 0
unset logscale
set offsets 0, 0, 0, 0
set pointsize 1
set encoding default
unset polar
unset parametric
unset decimalsign
set view 12, 310, 1, 1
set view
set samples 100, 100
set isosamples 10, 10
set surface
unset contour
set clabel '%8.3g'
set mapping cartesian
set datafile separator ","
unset hidden3d
set cntrparam order 4
set cntrparam linear
set cntrparam levels auto 5
set cntrparam points 5
set size ratio 0 1,1
set origin 0,0
set style data points
set style function lines
set xzeroaxis linetype -2 linewidth 1.000
set yzeroaxis linetype -2 linewidth 1.000
set zzeroaxis linetype -2 linewidth 1.000
set x2zeroaxis linetype -2 linewidth 1.000
set y2zeroaxis linetype -2 linewidth 1.000
set ticslevel 0
set mxtics default
set mytics default
set mztics default
set mx2tics default
set my2tics default
set mcbtics default
#<<<<<<< .mine
#set xtics border in scale 1,0.5 mirror norotate  offset character -2.2, 0, 0
#=======
set xtics border in scale 1,0.5 mirror norotate  offset character -2.8, -0.3, 0
#>>>>>>> .r9333
set xtics  norangelimit
#<<<<<<< .mine
#set xtics 6, 6
#set ytics border in scale 1,0.5 mirror norotate  offset character 0.5, -0.4, 0
#=======
#set xtics   ("1" 0.500000, "2" 1.50000, "3" 2.50000, "4" 3.50000, "5" 4.50000, "6" 5.50000, "7" 6.50000, "8" 7.50000)
set xtics 8
set ytics border in scale 1,0.5 mirror norotate  offset character 0.1, -0.525, 0
#>>>>>>> .r9333
set ytics  norangelimit
set ytics 8, 8
set ztics border in scale 1,0.5 nomirror norotate  offset character 0, 0, 0
set ztics 100
set nox2tics
set noy2tics
set cbtics border in scale 1,0.5 mirror norotate  offset character 0, 0, 0
set cbtics autofreq  norangelimit
set title ""
set title  offset character 0, 0, 0 font "" norotate
set timestamp bottom
set timestamp ""
set timestamp  offset character 0, 0, 0 font "" norotate
set rrange [ * : * ] noreverse nowriteback  # (currently [0.00000:10.0000] )
set trange [ * : * ] noreverse nowriteback  # (currently [-5.00000:5.00000] )
set urange [ * : * ] noreverse nowriteback  # (currently [-5.00000:5.00000] )
set vrange [ * : * ] noreverse nowriteback  # (currently [-5.00000:5.00000] )
set xlabel "Hardware thread \#"
set xlabel  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set x2label ""
set x2label  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set xrange [ 0.00000 : 48.00000 ] noreverse nowriteback
set x2range [ * : * ] noreverse nowriteback  # (currently [-10.0000:10.0000] )
set ylabel "Hardware thread \#"
set ylabel  offset character 0, 0, 0 font "" textcolor lt -1 rotate by 90
set y2label ""
set y2label  offset character 0, 0, 0 font "" textcolor lt -1 rotate by 90
set yrange [ 0.00000 : 48.00000 ] noreverse nowriteback
set y2range [ * : * ] noreverse nowriteback  # (currently [-10.0000:10.0000] )
set zlabel ""
set zlabel  offset character 0, 0, 0 font "" textcolor lt -1 norotate
set zrange [ 0.00000 : 500.000 ] noreverse nowriteback
set ztics 250
set cblabel ""
set cblabel  offset character 0, 0, 0 font "" textcolor lt -1 rotate by 90
set cbrange [ 0.00000 : 500.000 ] noreverse nowriteback
set cbtics 100
set zero 1e-08
set lmargin  -1
set bmargin  -1
set rmargin  -1
set tmargin  -1
set locale "C"
set pm3d implicit at s
set pm3d scansautomatic
set pm3d interpolate 1,1 flush begin noftriangles hidden3d 1 corners2color max
set palette positive nops_allcF maxcolors 0 gamma 1.5 color model RGB
#set palette defined ( 0 0.318 0.176 0.737, 0.08333 .384313 .666666 .164705, 0.4167 0.651 0.184 0.106,  0.6944 0.906 0.678 0.157, 0.8611 1 0.937 0.184, 1 1 1 1 )
set palette defined ( 0 .384313 .666666 .164705, 0.08333 0.318 0.176 0.737, 0.4167 0.651 0.184 0.106,  0.6944 0.906 0.678 0.157, 0.8611 1 0.937 0.184, 1 1 1 1 )
#set palette defined (0 .384313 .666666 .164705, 0.05 .384313 .666666 .164705, 0.068 0.318 0.176 0.737, 0.45 0.906 0.678 0.157,  0.65 0.651 0.184 0.106, 1 0.651 0.184 0.106)
set colorbox user
set colorbox vertical origin screen 0.89, 0.6, 0 size screen 0.037, 0.3, 0 front bdefault
set loadpath
set fontpath
set fit noerrorvariables
GNUTERM = "aqua"
splot "../../all-results/results-phd-140410/@@BENCHMARK@@.csv" using 1:2:3 with pm3d title ""
#    EOF
