#!/bin/bash
#
# plot.sh
# =======
# (C) Jean-Pierre Lozi, 2013
#

for BENCHMARK in l1 l2 l3
do
    cat latences.plt | sed "s/@@BENCHMARK@@/$BENCHMARK/g" | gnuplot
done

