#!/bin/bash
#
# plot.sh
# =======
# (C) Jean-Pierre Lozi, 2013
#

for BENCHMARK in l1 l2
do
    cat latencies.plt | sed "s/@@BENCHMARK@@/$BENCHMARK/g" | gnuplot
done

