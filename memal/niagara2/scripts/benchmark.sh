#!/bin/bash
#
# benchmark.sh
# ============
# (C) Jean-Pierre Lozi, 2013
#

for C in 1 2
do
    for I in {0..127}
    do
        for J in {0..127}
        do
            ../memal $I $J $C | cut -d" " -f 6 | tr -d '\n'

            if [[ "$J" != "127" ]]
            then
                echo -n ";"
            fi
        done
        echo
    done | tee ../results/l${C}_raw.csv
done

