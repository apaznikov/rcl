#ifndef SBW_CACHE_INFO_H
#define SBW_CACHE_INFO_H

void init_cacheinfo (void);

extern unsigned long int data_cache_size;
extern unsigned long int shared_cache_size;

extern unsigned int level;
extern unsigned int linesize;
extern unsigned long int l1_cache_size;
extern unsigned long int l2_cache_size;
extern unsigned long int l3_cache_size;

#endif

