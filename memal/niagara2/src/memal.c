#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/mman.h>
#include <sched.h>
#include <pthread.h>
#include <string.h>

#include "lib/bench.h"
#include "lib/cacheinfo.h"

/** +EDIT */
unsigned int linesize = 64; // L2
//unsigned int linesize = 16; // L1
unsigned long int l1_cache_size = 8192;
unsigned long int l2_cache_size = 4194304;
unsigned long int l3_cache_size = 0;
unsigned int level = 2;
/** -EDIT */

static volatile char *l1_buf = NULL;
static volatile char *l2_buf = NULL;
static volatile char *l3_buf = NULL;
static unsigned int ipc_buf_size = 0;

static const int iterations = 5;

struct worker_args {
    int fd_read;
    int fd_write;
    volatile void *buf;
    int buf_n;
    int proc_id;
};

enum { WORKER_READY, READ_BUF, DELAY_READ_BUF, EXIT };

struct worker_ipc {
    int cmd;
    int reads;
    uint64_t result;
};

static void __attribute__((noinline, used))
use_arg(void *v)
{
    printf("", v);
}

static void
stride_write_buf(char *buf, uint64_t stride, uint64_t size)
{
    assert(size % stride == 0);
    uint64_t n = size / stride;
    uint64_t rand_stride[n];
    memset(rand_stride, 0, sizeof(rand_stride));

    uint64_t i = 0;
    for (uint64_t k = 0; k < n - 1; k++) {
        uint64_t r = 1 + (uint64_t) ((float)(n - 1) * (rand() / (RAND_MAX + 1.0)));
retry:
        for (uint64_t j = 0; j < n; j++)  {
            if (rand_stride[j] == r || r == i) {
                r = (r + 1) % n;
                if (!r)
                    r = 1;
                goto retry;
            }
        }
        rand_stride[i] = r;
        i = r;
    }
    rand_stride[i] = 0;

    for (uint64_t i = 0; i < n; i++)
        *((uint64_t *)&buf[i * stride]) = (uint64_t)&buf[(rand_stride[i] * stride)];
}

static void
touch_buf(volatile char *buf, uint64_t size)
{
    for (uint64_t i = 0; i < size; i++)
        buf[i] = buf[i] + i;
}

static void __attribute__((noreturn))
xperror(const char *msg)
{
    perror(msg);
    exit(-1);
}

static void
xread(int s, void *b, int count)
{
    while (count) {
        int r = read(s, b, count);
        if (r < 0)
            xperror("xread");
        if (r == 0)
            xperror("xread: eof");
        count -= r;
        b += r;
    }
}

static void
xwrite(int s, void *b, int count)
{
    while (count) {
        int r = write(s, b, count);
        if (r < 0)
            xperror("xwrite");
        if (r == 0)
            xperror("xwrite: unable to write");
        count -= r;
        b += r;
    }
}

static uint64_t
access_buf(uint64_t *addr_buf, int times)
{
    uint64_t start, end;
    uint64_t rest = times;
    start = read_tsc();
    uint64_t *addr = (uint64_t *) *(addr_buf);
    while (rest) {
/** +EDIT */

        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;
        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;
        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;
        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;

        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;
        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;
        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;
        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;

        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;
        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;
        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;
        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;

        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;
        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;
        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;
        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;

        rest -= 64;
/*
        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;
        addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr; addr = (uint64_t *)*addr;
        rest -= 8;
*/
/** -EDIT */
    }
    end = read_tsc();
    use_arg(addr);
    touch_buf(l1_buf, l1_cache_size);
    touch_buf(l2_buf, l2_cache_size);
    if (level == 3)
        touch_buf(l3_buf, l3_cache_size);
    return (end - start) / times;
}

static void*
worker_thread(void *arg)
{
    struct worker_args *a = arg;
    affinity_set(a->proc_id);

    uint64_t x = 0;
    for (uint64_t i = 0; i < a->buf_n; i++)
        x += ((char *)a->buf)[i];
    use_arg((void *)x);

    struct worker_ipc ipc;
    ipc.cmd = WORKER_READY;
    xwrite(a->fd_write, &ipc, sizeof(ipc));

    xread(a->fd_read, &ipc, sizeof(ipc));
    while (ipc.cmd != EXIT) {
        usleep(200000);
        ipc.result = access_buf((uint64_t *)a->buf, ipc.reads);
        xwrite(a->fd_write, &ipc, sizeof(ipc));
        xread(a->fd_read, &ipc, sizeof(ipc));
    }

    return 0;
}

static void
setup()
{
/** +EDIT */
//    init_cacheinfo();
/** -EDIT */
    l1_buf = (volatile char *) malloc(l1_cache_size);
    l2_buf = (volatile char *) malloc(l2_cache_size);
    l3_cache_size *= 2;
    if (level >= 3)
        l3_buf = (volatile char *) malloc(l3_cache_size);
    ipc_buf_size = l1_cache_size / 2;
}

static uint64_t
local_al(uint64_t *addr_buf, int  cache)
{
    uint64_t total = 0;
    for (int i = 0; i < iterations; i++) {
        stride_write_buf((void *)addr_buf, linesize, ipc_buf_size);
        if (cache >= 2)
            touch_buf(l1_buf, l1_cache_size);
        if (cache >= 3 && level >= 2)
            touch_buf(l2_buf, l2_cache_size);
        if (cache >= 4 && level >= 3)
            touch_buf(l3_buf, l3_cache_size);
        total += access_buf((uint64_t *)addr_buf, ipc_buf_size / linesize);
    }
    return total / iterations;

}

static uint64_t
remote_al(uint64_t *addr_buf, int affinity2, int cache)
{
    int to_worker[2];
    int from_worker[2];
    if (pipe(to_worker) < 0 || pipe(from_worker) < 0)
        xperror("pipe");

    struct worker_args a;
    a.fd_read = to_worker[0];
    a.fd_write = from_worker[1];
    a.buf = addr_buf;
    a.buf_n = ipc_buf_size;
    a.proc_id = affinity2;

    pthread_t tid;
    assert(0 == pthread_create(&tid, 0, &worker_thread, &a));

    struct worker_ipc ipc;
    xread(from_worker[0], &ipc, sizeof(ipc));
    if (ipc.cmd != WORKER_READY)
        xperror("worker error\n");

    uint64_t total = 0;
    for (int i = 0; i < iterations; i++) {
        stride_write_buf((void *)addr_buf, linesize, ipc_buf_size);
        if (cache >= 2)
            touch_buf(l1_buf, l1_cache_size);
        if (cache >= 3 && level >= 2)
            touch_buf(l2_buf, l2_cache_size);
        if (cache >= 4 && level >= 3)
            touch_buf(l3_buf, l3_cache_size);
        ipc.cmd = DELAY_READ_BUF;
        ipc.reads = ipc_buf_size / linesize;
        xwrite(to_worker[1], &ipc, sizeof(ipc));
        xread(from_worker[0], &ipc, sizeof(ipc));
        total += ipc.result;
    }
    return total / iterations;
}

int
main(int ac, char **av)
{
    if (ac != 3 && ac < 4) {
        printf("Usage: %s affinity1 affinity2 [cache]\n", av[0]);
        exit(-1);
    }

/** +EDIT */
//    if (setpriority(PRIO_PROCESS, 0, -20) < 0)
//        xperror("unable to set process priority. Need to be privileged\n");
/** -EDIT */

    int affinity1 = atoi(av[1]);
    int affinity2 = atoi(av[2]);
    int cache = 1;
    if (ac >= 4)
        cache = atoi(av[3]);
    affinity_set(affinity1);
    setup();
    volatile uint64_t *addr_buf =(volatile uint64_t *) malloc(ipc_buf_size);
    touch_buf((volatile char *)addr_buf, ipc_buf_size);
    touch_buf(l1_buf, l1_cache_size);
    touch_buf(l2_buf, l2_cache_size);
    if (level >= 3)
        touch_buf(l3_buf, l3_cache_size);

    uint64_t latency = 0;
    if (affinity1 != affinity2)
        latency = remote_al((uint64_t *)addr_buf, affinity2, cache);
    else
        latency = local_al((uint64_t *)addr_buf, cache);

    if (cache == 1)
        printf("cpu%d reads from cpu%d l1: %ld cycles\n", affinity2, affinity1, latency);
    else if (cache == 2)
        printf("cpu%d reads from cpu%d l2: %ld cycles\n", affinity2, affinity1, latency);
    else if (cache == 3 && level >= 3)
        printf("cpu%d reads from cpu%d l3: %ld cycles\n", affinity2, affinity1, latency);
    else
        printf("cpu%d reads from cpu%d memory: %ld cycles\n", affinity2, affinity1, latency);
    return 0;
}
