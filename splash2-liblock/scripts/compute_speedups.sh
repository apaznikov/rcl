#!/bin/bash
#
# compute_accelerations.sh
# ========================
# (C) Jean-Pierre Lozi,
#     Florian David,
#     Gaël Thomas,
#     Julia Lawall,
#     Gilles Muller
#     2013
#
# Example usage:
# ./compute_acceleration.sh \
#	../all-results/results-190513/execution-times-amd48-noprofiling
#

RESDIR=$1
TOKEN=`basename $RESDIR`
EXPS="radiosity raytrace-car raytrace-balls4"
BENCHES="posix flat mcs ccsynch dsmsynch spinlock rcl"


compute_acceleration()
{
	BASE=$1
	BENCH=$2
	LOCK=$3

    while read LINE
    do
		N=`echo $LINE | cut -d',' -f1`
		AVERAGE=`echo $LINE | cut -d',' -f2`
        POINTS=`echo $LINE | cut -d',' -f3-`
		ACCELERATION=`echo $BASE/$AVERAGE | bc -l`

		echo $N,$ACCELERATION,$AVERAGE,$POINTS
	done
}

build_data()
{
    for EXP in $EXPS
    do
 		BASE=$(cat $RESDIR/splash2-$EXP-posix.csv | head -n 1 | cut -d ',' -f2)

        for BENCH in $BENCHES
        do
            echo "Computing acceleration for $EXP/$BENCH..."

            compute_acceleration $BASE $EXP $BENCH \
                < $RESDIR/splash2-$EXP-$BENCH.csv \
                > $RESDIR/acc-$EXP-$BENCH.csv
  	    done
    done
}

build_data

