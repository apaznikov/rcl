#!/usr/bin/python
#
# get_maximums.py
# ===============
# (C) Jean-Pierre Lozi,
#     Florian David,
#     Gael Thomas,
#     Julia Lawall,
#     Gilles Muller
#     2013
#

import csv
import glob
import os
import sys

if len(sys.argv) < 2:
    print "usage: ", sys.argv[0], " <results-directory>"
    sys.exit(0)

os.chdir(sys.argv[1])

for exp in ['radiosity', 'raytrace-balls4', 'raytrace-car']:

    posix_max = -1.0

    #print "%s\t" % exp
    print "%s\t" % exp,

    locks = ["posix", "spinlock", "mcs", "flat", "ccsynch", "dsmsynch", "rcl"]
    for lock in locks:

        file = open("acc-%s-%s.csv" % (exp, lock), "rb")
        reader = csv.reader(file)

        row_num = 0
        for row in reader:

            col_num = 0
            for col in row:

                if col_num > 1:
                    continue

                f_col=float(col)

                if row_num == 0 and col_num == 1:
                    maximum = float(col)
                else:
                    if col_num == 0:
                        index = f_col
                    if col_num == 1 and f_col > maximum:
                        maximum = f_col
                        max_index = int(index)

                col_num += 1

            row_num += 1

        if posix_max < 0:
            posix_max = maximum

        #print "%-65s: %f x%f\t/ %3d" % (file, maximum / posix_max, maximum, max_index)
        print "%9.6f  %3d  %9.6f" % (maximum / posix_max, max_index, maximum),

        file.close()

    print "  ?"

