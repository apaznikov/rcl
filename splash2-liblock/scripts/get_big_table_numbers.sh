#!/bin/bash
#
# get_big_table_numbers.sh
# ========================
# (C) Jean-Pierre Lozi,
#     Florian David,
#     Gaël Thomas,
#     Julia Lawall,
#     Gilles Muller
#     2013
#

if [[ -z "$COLS" ]];   then COLS="ALL";                                       fi
if [[ -z "$N_RUNS"   ]]; then N_RUNS=5;                                       fi


if [[ $COLS == "LEFT" || $COLS == "ALL" ]]
then
    echo -ne "\e[1;38;05;161m"
    echo "#####################################################################"
    echo "# Left part                                                         #"
    echo "#####################################################################"
    echo -ne "\e[0m"
    echo

    METRIC="TIME_IN_CS" N_RUNS="$N_RUNS" SCALE="2" ./benchmark.sh
fi

if [[ $COLS == "ALL" ]]
then
    echo
    echo
fi

if [[ $COLS == "RIGHT" || $COLS == "ALL" ]]
then
    echo -ne "\e[1;38;05;161m"
    echo "#####################################################################"
    echo "# Right part                                                        #"
    echo "#####################################################################"
    echo -ne "\e[0m"
    echo

    METRIC="TIME_IN_CS_AND_DCM_FOR_MCL" N_RUNS="$N_RUNS" SCALE="2"             \
    ./benchmark.sh
fi

