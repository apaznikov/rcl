#!/bin/bash
#
# benchmark.sh
# ============
# (C) Jean-Pierre Lozi,
#     Florian David,
#     Gaël Thomas,
#     Julia Lawall,
#     Gilles Muller
#     2013
#

CMD_LOG=$HOME/rcl/rcl/splash2-liblock/scripts/log

. ../src/.root
. ../../figures/bench.inc.sh
cd $( dirname "${BASH_SOURCE[0]}")

nawk=awk


SINGLE_FILE_PER_LOCK="1"
CODEDIR="../src/$DIR/codes"
# PREFIX="gdb"



HOSTNAME=$(hostname)

if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
then
    USE_SUPERUSER=1
elif [[ $(hostname) == "niagara2.cs.rochester.edu" ]]
then
    USE_SUPERUSER=0
fi

if [[ $(uname) == "Linux" ]]
then
    DEFAULT_N_PROCS=$(cat /proc/cpuinfo | grep processor | wc -l)
else
    DEFAULT_N_PROCS=$(kstat cpu_info | grep "instance" | wc -l | tr -d ' ')
fi

if [[ -z "$N_PROCS"  ]]; then N_PROCS=$DEFAULT_N_PROCS;                       fi
if [[ -z "$DIR"      ]]; then DIR="splash2-patched";                          fi
if [[ -z "$N_RUNS"   ]]; then N_RUNS=30;                                      fi
if [[ -z "$METRIC"   ]]; then METRIC="EXECUTION_TIME";                        fi
if [[ -z "$SCALE"    ]]; then SCALE=6;                                        fi
if [[ -z "$BENCHES"  ]]
then BENCHES="rcl ccsynch dsmsynch mcs spinlock flat posix";                  fi

BENCHES="rcl"

export N_PROCS


if [[ $USE_SUPERUSER == "1" && "$(id -u)" != "0" ]]
then
    echo "Must be root"
    exit 1
fi


seq()
{
    if [[ $(uname) == "Linux" ]]
    then
        seq $1 $2
    else
        $nawk "BEGIN{ for(i = $1 ; i <= $2 ; i++) print i}"
    fi
}

run_barnes()
{
    if [ -f $CODEDIR/apps/barnes/BARNES ]
    then
        echo "cat $CODEDIR/apps/barnes/input | sed -e s/46/$N_PROCS/ $PREFIX $CODEDIR/apps/barnes/BARNES" >>$CMD_LOG

        cat $CODEDIR/apps/barnes/input | sed -e "s/46/$N_PROCS/" \
                                       | $PREFIX $CODEDIR/apps/barnes/BARNES
    fi
}

run_ocean_non()
{
    if [ -f $CODEDIR/apps/ocean/non_contiguous_partitions/OCEAN ]
    then
        echo "$PREFIX $CODEDIR/apps/ocean/non_contiguous_partitions/OCEAN -p$N_PROCS" >>$CMD_LOG

        $PREFIX $CODEDIR/apps/ocean/non_contiguous_partitions/OCEAN -p$N_PROCS
    fi
}

run_ocean_cont()
{
    if [ -f $CODEDIR/apps/ocean/contiguous_partitions/OCEAN ]
    then
        echo "$PREFIX $CODEDIR/apps/ocean/contiguous_partitions/OCEAN -p $N_PROCS" >>$CMD_LOG

        $PREFIX $CODEDIR/apps/ocean/contiguous_partitions/OCEAN -p $N_PROCS
    fi
}

run_water_nsquared()
{
    if [ -f $CODEDIR/apps/water-nsquared/WATER-NSQUARED ]
    then
        cd $CODEDIR/apps/water-nsquared
        echo "cat input | sed -e s/46/$N_PROCS/ | $PREFIX ./WATER-NSQUARED" >>$CMD_LOG

        cat input | sed -e "s/46/$N_PROCS/" | $PREFIX ./WATER-NSQUARED
        cd $CODEDIR
    fi
}

run_water_spatial()
{
    if [ -f $CODEDIR/apps/water-spatial/WATER-SPATIAL ]
    then
        cd $CODEDIR/apps/water-spatial
        echo "cat input | sed -e s/46/$N_PROCS/ | $PREFIX ./WATER-SPATIAL" >>$CMD_LOG

        cat input | sed -e "s/46/$N_PROCS/" | $PREFIX ./WATER-SPATIAL
        cd $CODEDIR
    fi
}

run_fmm()
{
    if [ -f $CODEDIR/apps/fmm/FMM ]
    then
        cd $CODEDIR/apps/fmm
        echo "cat inputs/input.16384 | sed -e "s/46/1/" | $PREFIX ./FMM" >>$CMD_LOG

        cat inputs/input.16384 | sed -e "s/46/1/" | $PREFIX ./FMM
        cd $CODEDIR
    fi
}

run_kernels_fft()
{
    if [ -f $CODEDIR/kernels/fft/FFT ]
    then
        cd $CODEDIR/kernels/fft
        echo "$PREFIX ./FFT -p$N_PROCS -l6 -n64" >>$CMD_LOG

        $PREFIX ./FFT -p$N_PROCS -l6 -n64
        cd $CODEDIR
    fi
}

run_kernels_lu_contiguous()
{
    if [ -f $CODEDIR/kernels/lu/contiguous_blocks/LU ]
    then
        cd $CODEDIR/kernels/lu/contiguous_blocks/
        echo "$PREFIX ./LU -p$N_PROCS -b8" >>$CMD_LOG

        $PREFIX ./LU -p$N_PROCS -b8
        cd $CODEDIR
    fi
}

run_volrend()
{
    if [ -f $CODEDIR/apps/volrend/VOLREND ]
    then
        cd $CODEDIR/apps/volrend
        echo "$PREFIX ./VOLREND $N_PROCS inputs/head" >>$CMD_LOG

        $PREFIX ./VOLREND $N_PROCS inputs/head
        cd $CODEDIR
    fi
}

run_volrend_timed()
{
    if [ -f $CODEDIR/apps/volrend/VOLREND ]
    then
        cd $CODEDIR/apps/volrend
        echo "$PREFIX ./VOLREND $N_PROCS inputs/head" >>$CMD_LOG

        (time $PREFIX ./VOLREND $N_PROCS inputs/head >/dev/null 2>/dev/null) 2>&1
        cd $CODEDIR
    fi
}


run_cholesky()
{
    if [ -f $CODEDIR/kernels/cholesky/CHOLESKY ]
    then
        cd $CODEDIR/kernels/cholesky
        echo "$PREFIX ./CHOLESKY -p$N_PROCS -C64 inputs/tk29.O" >>$CMD_LOG

        $PREFIX ./CHOLESKY -p$N_PROCS -C64 inputs/tk29.O
        cd $CODEDIR
    fi
}

run_radiosity()
{
#   echo -n "-- run-radiosity: '$LIBLOCK_LOCK_NAME' lock, " 1>&2
#   echo    "$LIBLOCK_CORES cores and $N_PROCS threads --" 1>&2

    if [ -f $CODEDIR/apps/radiosity/RADIOSITY ]
    then
        cd $CODEDIR/apps/radiosity
        echo "$PREFIX ./RADIOSITY -p $N_PROCS -batch -ae 500 -bf 0.005" >>$CMD_LOG

        $PREFIX ./RADIOSITY -p $N_PROCS -batch -ae 500 -bf 0.005
        cd $CODEDIR
    fi
}

run_radiosity_orig()
{
#   echo -n "-- run-radiosity_orig: '$LIBLOCK_LOCK_NAME' lock, " 1>&2
#           "$LIBLOCK_CORES cores and $N_PROCS threads --" 1>&2

    if [ -f $CODEDIR/apps/radiosity/RADIOSITY ]
    then
        cd $CODEDIR/apps/radiosity
        echo "$PREFIX ./RADIOSITY -p $N_PROCS -batch" >>$CMD_LOG

        $PREFIX ./RADIOSITY -p $N_PROCS -batch
        cd $CODEDIR
    fi
}

run_raytrace()
{
#   echo -n "-- run-raytrace: '$LIBLOCK_LOCK_NAME' lock, " 1>&2
#   echo    "$LIBLOCK_CORES cores and $N_PROCS threads in $CODEDIR --" 1>&2

    if [ -f $CODEDIR/apps/raytrace/RAYTRACE ]
    then
        cd $CODEDIR/apps/raytrace
        echo "$PREFIX ./RAYTRACE -m1024 -p$N_PROCS inputs/$1.env" >>$CMD_LOG

        $PREFIX ./RAYTRACE -m1024 -p$N_PROCS inputs/$1.env
        cd $CODEDIR
    fi
}

eval_exp()
{
    N_RUNS=$1
    shift

#   echo -n "--  $N_RUNS runs of $1 with '$LIBLOCK_LOCK_NAME' lock, " 1>&2
#   echo    "$LIBLOCK_CORES cores and $N_PROCS threads --" 1>&2
#   echo    "[$@]" 1>&2

    for F in `$nawk "BEGIN{ for(i = 1 ; i <= $N_RUNS ; i++) print i}"`
    do
        eval "$@"
    done
}

average()
{
    TOT=0
    N=0

    while read LINE
    do
        TOT="$TOT+$LINE"
        N=$(($N+1))
    done

    TOT=`echo "scale=$SCALE;(($TOT)/$N)" | bc`
    echo "$TOT"
}

points_and_average()
{
    TOT=0
    N=0

    rm -f tmp_points

    while read LINE
    do
        if [[ $QUIET == "0" ]]
        then
            echo -n $LINE"," 1>&2
        fi

        echo -n "$LINE," >> tmp_points
        TOT="$TOT+$LINE"
        N=$(($N+1))
    done

    TOT=`echo "scale=$SCALE;(($TOT)/$N)" | bc`
    echo -n "$TOT,"
    cat tmp_points
    echo
}

eval_all_of()
{
    NAME=$1
    N_RUNS=$2
    RCLDEC=$3
    LOCKS=$4
    QUIET=$5
    AVERAGED=$6
    START=1
    END=$N_PROCS
    shift; shift; shift; shift; shift; shift

    if [[ $QUIET == "0" && $METRIC != "TIME_IN_CS_AND_DCM_FOR_MCL" ]]
    then
        echo 1>&2
        echo "eval: $NAME - $N_RUNS - $LOCKS - $START/$END/$RRR" 1>&2
    fi

    if [[ -z $NS_THREADS ]]
    then
        NS_THREADS=`$nawk "BEGIN{ for(i = $START ; i <= $END ; i++) print i}"`
    fi

    for LOCK in $LOCKS
    do
        DEC=0

        if [[ $LOCK = "rcl" ]]
        then
            DEC=$RCLDEC;
            END=$(echo "$END-$RCLDEC" | bc -l)
        fi

        if [[ $QUIET == "0" && $METRIC != "TIME_IN_CS_AND_DCM_FOR_MCL" ]]
        then
            echo "Lock = "$LOCK 1>&2
        fi

        for I in $NS_THREADS
        do
            NTHREADS=$(echo $I+$DEC | bc)

            if [[ $QUIET == "0" ]]
            then
                echo -ne "$NTHREADS threads:\t" 1>&2
            fi

            echo -n "$NTHREADS,"

            if [[ $METRIC = "TIME_IN_CS_AND_DCM_FOR_MCL" ||
                  $METRIC = "TIME_IN_CS" ]]
            then
            #        DIR=splash2-original
# Using splash2-bound-threads makes the most sense because that is how the
# benchmark was run in the USENIX paper, and we bind threads in the
# experiments. We don't bind threads during profiling for Phoenix but they
# are automatically bound by the map/reduce algorithm. When we profile
# memcached, clients are bound by the profiler in order to keep threads on
# the first cores. And with Berkeley DB, we don't bind threads on amd48b
# but we do on niagara2 (through BOUND_CLIENTS), which is the same
# configuration used for the graphs.
                    DIR=splash2-bound-threads
            elif [[ $LOCK = "posix" ]]
            then
                    DIR=splash2-bound-threads
            else
                    DIR=splash2-patched
            fi

            if [[ $SINGLE_FILE_PER_LOCK != "0" ]]
            then
                if [[ $AVERAGED != "0" ]]
                then
                    CODEDIR=$BASEDIR/$DIR/codes LIBLOCK_LOCK_NAME=$LOCK        \
                    N_PROCS=$I eval_exp $N_RUNS "$@" | points_and_average
                else
                    CODEDIR=$BASEDIR/$DIR/codes LIBLOCK_LOCK_NAME=$LOCK        \
                    N_PROCS=$I eval_exp $N_RUNS "$@"
                fi
            else
                if [[ $AVERAGED != "0" ]]
                then
                    CODEDIR=$BASEDIR/$DIR/codes LIBLOCK_LOCK_NAME=$LOCK        \
                    N_PROCS=$I eval_exp $N_RUNS "$@"                           \
                        | tee -a ../results/splash2-$NAME-$LOCK-$NTHREADS.csv  \
                        | average
                else
                    CODEDIR=$BASEDIR/$DIR/codes LIBLOCK_LOCK_NAME=$LOCK        \
                    N_PROCS=$I eval_exp $N_RUNS "$@"                           \
                        | tee -a ../results/splash2-$NAME-$LOCK-$NTHREADS.csv
                fi
            fi

            if [[ $QUIET == "0" ]]
            then
                echo 1>&2
            fi
        done > ../results/splash2-$NAME-$LOCK$OUTPUT_FILE_SUFFIX.csv

        if [[ $QUIET == "0" ]]
        then
            echo 1>&2
            echo "Output file contents: " 1>&2
        fi

        cat ../results/splash2-$NAME-$LOCK$OUTPUT_FILE_SUFFIX.csv
        echo
    done
}

run_main_benchmarks()
{
    rm -Rf ../results
    mkdir ../results


    if [[ -z $PARSING_COMMANDS ]]
    then
        PARSING_COMMANDS='| grep  "Total time with initialization"             \
                          | sed -e "s/Total time with initialization//"'
    fi

    eval_all_of radiosity $N_RUNS 1 "$BENCHES" 0 1                             \
        "LIBLOCK_CORES=0:1 run_radiosity $REDIRECTION $PARSING_COMMANDS"

    eval_all_of raytrace-balls4 $N_RUNS 1 "$BENCHES" 0 1                       \
        "run_raytrace balls4 $REDIRECTION $PARSING_COMMANDS"

    eval_all_of raytrace-car $N_RUNS 1 "$BENCHES" 0 1                          \
        "run_raytrace car $REDIRECTION $PARSING_COMMANDS"
}

run_all_benchmarks()
{
    rm -Rf ../results
    mkdir ../results


    if [[ $METRIC = "EXECUTION_TIME" || $METRIC = "EXECUTION_TIME_ALL" ]]
    then
        PARSING_COMMANDS='| grep "Total time with initialization"              \
                          | sed -e "s/Total time with initialization//"'
    fi

#    eval_all_of radiosity $N_RUNS 1 "$BENCHES" 0 1                             \
#        "LIBLOCK_CORES=0:1 run_radiosity $REDIRECTION $PARSING_COMMANDS"

#    eval_all_of raytrace-balls4 $N_RUNS 1 "$BENCHES" 0 1                       \
#        "run_raytrace balls4 $REDIRECTION $PARSING_COMMANDS"

#    eval_all_of raytrace-car $N_RUNS 1 "$BENCHES" 0 1                          \
#        "run_raytrace car $REDIRECTION $PARSING_COMMANDS"


    if [[ $METRIC = "EXECUTION_TIME" || $METRIC = "EXECUTION_TIME_ALL" ]]
    then
        PARSING_COMMANDS='| grep COMPUTETIME | cut -d"=" -f2'
    fi

#    eval_all_of barnes $N_RUNS 1 "$BENCHES" 0 1                                \
#        "run_barnes $REDIRECTION $PARSING_COMMANDS"


    if [[ $METRIC = "EXECUTION_TIME" || $METRIC = "EXECUTION_TIME_ALL" ]]
    then
        PARSING_COMMANDS='| grep "Total time with initialization"              \
                          | cut -d":" -f2'
    fi

#    eval_all_of fmm $N_RUNS 1 "$BENCHES" 0 1                                   \
#        "run_fmm $REDIRECTION $PARSING_COMMANDS"


    if [[ $METRIC = "EXECUTION_TIME" || $METRIC = "EXECUTION_TIME_ALL" ]]
    then
        PARSING_COMMANDS='| grep "Total time with initialization"              \
            | sed -e "s/Total time with initialization//" | sed -e "s/://"'
    fi

    OLD_NS_THREADS="$NS_THREADS"

    if [[ $METRIC != "EXECUTION_TIME_ALL" ]]
    then
        if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
        then
            # N must be a power of 2
            NS_THREADS="1 4 8 16 32"
        fi
    else
        NS_THREADS="1 32 128"
    fi

#    eval_all_of ocean-cont $N_RUNS 1 "$BENCHES" 0 1                            \
#        "run_ocean_cont $REDIRECTION $PARSING_COMMANDS"

#    eval_all_of ocean-non-cont $N_RUNS 1 "$BENCHES" 0 1                        \
#        "run_ocean_non $REDIRECTION $PARSING_COMMANDS"

    NS_THREADS="$OLD_NS_THREADS"


    if [[ $METRIC = "EXECUTION_TIME" || $METRIC = "EXECUTION_TIME_ALL" ]]
    then
        # FIXME: write the parsing commands to make volrend work with the
        # EXECUTION_TIME metric.
        # PARSING_COMMANDS='| grep ms | cut -d " " -f 9 | tr "\n" "x"          \
        #                  | xargs echo | sed -e "s/x/*1000+/g"                \
        #                  | sed -e "s/+$//g" | xargs echo | bc'

        PARSING_COMMANDS='| xargs echo 1000000 \*| bc'
        export TIMEFORMAT="%R"

        eval_all_of volrend $N_RUNS 1 "$BENCHES" 0 1                           \
            "run_volrend_timed $REDIRECTION $PARSING_COMMANDS"
    else
        eval_all_of volrend $N_RUNS 1 "$BENCHES" 0 1                           \
            "run_volrend $REDIRECTION $PARSING_COMMANDS"
    fi


    if [[ $METRIC = "EXECUTION_TIME" || $METRIC = "EXECUTION_TIME_ALL" ]]
    then
        PARSING_COMMANDS='| grep "COMPUTETIME (after initialization) ="        \
                          | sed -e "s/COMPUTETIME (after initialization) =//"'
    fi

#    eval_all_of water-nsquared $N_RUNS 1 "$BENCHES" 0 1                        \
#         "run_water_nsquared $REDIRECTION $PARSING_COMMANDS"

    eval_all_of water-spatial $N_RUNS 1 "$BENCHES" 0 1                         \
         "run_water_spatial $REDIRECTION $PARSING_COMMANDS"
}



#if [[ $(uname) == "Linux" ]]
#then
#    sysctl -w vm.nr_hugepages=2000
#    echo
#fi

if [[ $METRIC = "EXECUTION_TIME" ]]
then

    OUTPUT_FILE_SUFFIX=""
    # REDIRECTION="2> /dev/null"

    run_main_benchmarks

elif [[ $METRIC = "EXECUTION_TIME_ALL" ]]
then

    OUTPUT_FILE_SUFFIX=""
    # REDIRECTION="2> /dev/null"
    NS_THREADS="1 48 128"
    BENCHES="posix"

    run_all_benchmarks

elif [[ $METRIC = "USE_RATE" ]]
then

    cd ../../liblock
    echo "Compiling the liblock with profiling for rcl..."
    make distclean
    make ADDITIONAL_CFLAGS="-DPROFILING_ON"
    echo "Done."
    cd -
    trap "echo; echo 'Removing profiling from the liblock...';                 \
cd ../../liblock; make distclean; make; cd -; exit" SIGINT

    BENCHES="rcl"
    OUTPUT_FILE_SUFFIX="-use-rates"
    # The use rates are on stderr by the profiler, we redirect it to stdout
    # since only stdout is parsed.
    # REDIRECTION="2>&1"
    PARSING_COMMANDS='| grep "use rate:" | cut -d " " -f 7'

    run_main_benchmarks

    cd ../../liblock
    echo "Removing profiling from the liblock..."
    make distclean
    make
    echo "Done."
    cd -

elif [[ $METRIC = "SERVER_CACHE_MISSES" ]]
then

    cd ../../liblock
    echo "Compiling the liblock with DCM profiling for rcl..."

    make distclean

    make ADDITIONAL_CFLAGS="-DPRECISE_DCM_PROFILING_INSIDE_ON"

    echo "Done."
    cd -
    trap "echo; echo 'Removing profiling from the liblock...';                 \
cd ../../liblock; make distclean; make; cd -; exit" SIGINT

    BENCHES="rcl"
    OUTPUT_FILE_SUFFIX="-server-cache-misses"
    REDIRECTION="2>&1"
    PARSING_COMMANDS="| grep '\/cs' | awk '{print \$2}'"

    if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
    then
        NS_THREADS="47"
    elif [[ $(hostname) == "niagara2.cs.rochester.edu" ]]
    then
        NS_THREADS="127"
    fi

    run_main_benchmarks

    cd ../../liblock
    echo "Removing profiling from the liblock..."
    make distclean
    make
    echo "Done."
    cd -

elif [[ $METRIC = "TIME_IN_CS" ]]
then

    BENCHES="posix"
    OUTPUT_FILE_SUFFIX="-times-in-cs"

    if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
    then
        NS_THREADS="1 4 8 16 32 48"
    elif [[ $(hostname) == "niagara2.cs.rochester.edu" ]]
    then
        NS_THREADS="1 4 8 16 32 64 128"
    fi

    # Binding by thread or not binding doesn't seem to change results much. At
    # least with radiosity on niagara2. We choose to not bind.
    # PREFIX="$(pwd)/../../lock-profiler/lock-profiler -b BY_HW_THREAD -l NONE_CYCLES"
    PREFIX="$(pwd)/../../lock-profiler/lock-profiler -b NONE -l NONE_CYCLES"
    # The execution times are output on stderr by the profiler.
    REDIRECTION="2>&1 > /dev/null"
    PARSING_COMMANDS='| grep "Global statistics:" | cut -d " " -f 4 | tr -d "%"'


    run_all_benchmarks

elif [[ $METRIC = "TIME_IN_CS_AND_DCM_FOR_MCL" ]] # MCL = Most Contended Lock
then

    BENCHES="posix"

    if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
    then
        NS_THREADS="48"
    elif [[ $(hostname) == "niagara2.cs.rochester.edu" ]]
    then
        NS_THREADS="128"
    fi

    MAIN_BENCHMARK_NAMES=(radiosity raytrace-balls4 raytrace-car)
    MAIN_BENCHMARK_COMMANDS=('LIBLOCK_CORES=0:1 run_radiosity'
                             'run_raytrace balls4'
                             'run_raytrace car')

    for BENCHMARK_N in 0 1 2
    do
        BENCHMARK_NAME=${MAIN_BENCHMARK_NAMES[$BENCHMARK_N]}
        BENCHMARK_COMMAND=${MAIN_BENCHMARK_COMMANDS[$BENCHMARK_N]}

        echo
        echo "BENCHMARK = "$BENCHMARK_NAME
        echo


        ########################################################################
        # ID of the most contended lock                                        #
        ########################################################################

        OUTPUT_FILE_SUFFIX="-mcl-ids"
        PREFIX="$(pwd)/../../lock-profiler/lock-profiler -b NONE               \
                -l NONE_CYCLES -a"
        PARSING_COMMANDS="| grep \"#\" | sort -k 6 -n | tail -n 1 | tr -d \#   \
                          | awk '{ print \$3\" \"\$4\" \"\$5\" \"\$6 }'"
        REDIRECTION="2>&1 > /dev/null"
        OLD_N_RUNS=$N_RUNS
        N_RUNS=1

        RES=`eval_all_of $BENCHMARK_NAME $N_RUNS 1 "$BENCHES" 1 0                \
             "$BENCHMARK_COMMAND $REDIRECTION $PARSING_COMMANDS"`

        N_RUNS=$OLD_N_RUNS

        FILE="../results/splash2-$BENCHMARK_NAME-posix$OUTPUT_FILE_SUFFIX.csv"

        ID="`cat $FILE | tr  ',' ' ' | awk '{ print $2 }'`"
        ALLOC_ID="`cat $FILE | tr ',' ' ' | awk '{ print $3 }'`"
        ADDRESS="`cat $FILE | tr ',' ' ' | awk '{ print $4 }'`"
        TIME_IN_CS="`cat $FILE | tr ',' ' ' | awk '{ print $5 }'`"

        echo -n "Found that the lock with the most contention spends "
        echo    "$TIME_IN_CS% of its time in CS."
        echo "Lock info: id=$ID, alloc_id=$ALLOC_ID, address=$ADDRESS."
        echo


        ########################################################################
        # Time in CS                                                           #
        ########################################################################

        echo "Time in CS for this lock :"
        echo "=========================="

        OUTPUT_FILE_SUFFIX="-mcl-time-in-cs"
        PREFIX="$(pwd)/../../lock-profiler/lock-profiler -b NONE               \
                -l NONE_CYCLES -a"
        PARSING_COMMANDS="| grep \"$ALLOC_ID \" | awk '{ print \$6 }'"

        eval_all_of $BENCHMARK_NAME $N_RUNS 1 "$BENCHES" 0 1                     \
             "$BENCHMARK_COMMAND $REDIRECTION $PARSING_COMMANDS"


        ########################################################################
        # Data cache misses                                                    #
        ########################################################################

        echo "Data cache misses for this lock :"
        echo "================================="

        OUTPUT_FILE_SUFFIX="-mcl-l2-dcm"

        if [[ "${HOSTNAME#*amd48}" != $HOSTNAME ]]
        then
            PREFIX="$(pwd)/../../lock-profiler/lock-profiler -l PAPI           \
                    -e 0x80000002 -i -b BY_HW_THREAD -m PER_NODE -a"
        elif [[ $(hostname) == "niagara2.cs.rochester.edu" ]]
        then
            PREFIX="$(pwd)/../../lock-profiler/lock-profiler -l CPC            \
                    -e DC_miss -i -b BY_HW_THREAD -m PER_CORE -a"
        else
            echo "Unsupported machine."
            exit
        fi

        PARSING_COMMANDS="| grep \"$ALLOC_ID \" | awk '{ print \$7 }'"

        eval_all_of $BENCHMARK_NAME $N_RUNS 1 "$BENCHES" 0 1                     \
             "$BENCHMARK_COMMAND $REDIRECTION $PARSING_COMMANDS"

    done

else

    echo "Unsupported metric."

fi


# if [[ $(uname) == "Linux" ]]
# then
#     echo
#     sysctl -w vm.nr_hugepages=0
# fi





###############################################################################
# Old experiments                                                             #
###############################################################################

# for F in posix multircl spinlock mcs flat; do
#       echo -n $F:
#       N_PROCS=1 LIBLOCK_LOCK_NAME=$F    run_water_spatial | grep COMPUTETIME
# done

# run_raytrace car
# run_radiosity
# LIBLOCK_CORES="0:1,1:1,2:1,0:1" N_PROCS=45 run_radiosity

# eval_exp 30 'run_raytrace balls4 2>/dev/null \
#     | grep "Total time with initialization" \
#     | sed -e "s/Total time with initialization//"'

# eval_exp 30 'run_raytrace car 2>/dev/null \
#     | grep "Total time with initialization" \
#     | sed -e "s/Total time with initialization//"'

# eval_exp 30 'run_radiosity | grep "Total time with initialization" \
#     | sed -e "s/Total time with initialization//"'

# eval_exp "Ray Trace car" 30 'run_raytrace car 2>/dev/null \
#     | grep "Total time with initialization" \
#     | sed -e "s/Total time with initialization//"'

# eval_exp Barnes 30 'run_barnes | grep COMPUTETIME | cut -d"=" -f2'

# eval_exp "Ocean contigous" 30 'run_ocean_cont \
#     | grep "Total time with initialization" \
#     | sed -e "s/Total time with initialization//" | sed -e "s/://"'

# eval_exp "Ocean non contigous" 30 'run_ocean_non \
#     | grep "Total time with initialization" \
#     | sed -e "s/Total time with initialization//" | sed -e "s/://"'

# run_barnes

# run_ocean_non
# eval_exp "Ocean Non Contigous" 30 'run_ocean_non \
#     | grep "Total time with initialization" | cut -d":" -f2'

# eval_exp "Ocean Contigous" 30 'run_ocean_cont \
#     | grep "Total time with initialization" | cut -d":" -f2'

# eval_exp "Water nsquared" 30 'run_water_nsquared' \
#     | grep "COMPUTETIME (after initialization) =" | \
#     | sed -e "s/COMPUTETIME (after initialization) =//"'

# eval_exp "Water spatial" 30 'run_water_spatial \
#     | grep "COMPUTETIME (after initialization) =" \
#     | sed -e "s/COMPUTETIME (after initialization) =//"'

# run_water_nsquared
# run_water_spatial
# run_ocean_cont

# run_kernels_fft
# run_kernels_lu_contiguous

# run not ok, compile ok (3)
# run_radiosity
# run_volrend
# run_cholesky

# cocci not ok (2)
#   apps/fmm
#   kernels/radix
#
#
#
#
#
# eval_all_of radiosity-0-1 $N_RUNS 2 "rcl" \
#      "LIBLOCK_CORES=0:1,1:1 run_radiosity $REDIRECTION $PARSING_COMMANDS"
#
# eval_all_of radiosity-0-1-0 $N_RUNS 2 "rcl" \
#      "LIBLOCK_CORES=0:1,1:1,0:1 run_radiosity $REDIRECTION $PARSING_COMMANDS"
#
# eval_all_of radiosity-0-1-2 $N_RUNS 3 "rcl" \
#      "LIBLOCK_CORES=0:1,1:1,2:1 run_radiosity $REDIRECTION $PARSING_COMMANDS"
#
# eval_all_of radiosity-0-1-2-0 $N_RUNS 3 "rcl" \
#      "LIBLOCK_CORES=0:1,1:1,2:1,0:1 \
#       run_radiosity $REDIRECTION $PARSING_COMMANDS"
#
# eval_all_of radiosity $N_RUNS 1 "$BENCHES" \
#      "LIBLOCK_CORES=1:63 run_radiosity $REDIRECTION $PARSING_COMMANDS"
#
# eval_all_of radiosity_orig $N_RUNS 1 "$BENCHES" \
#      "LIBLOCK_CORES=0:1 run_radiosity_orig $REDIRECTION $PARSING_COMMANDS"
#
# eval_all_of raytrace-car $N_RUNS 1 "$BENCHES" \
#      "LIBLOCK_CORES=1:63 run_raytrace car $REDIRECTION $PARSING_COMMANDS"
#
# eval_all_of raytrace-balls4 $N_RUNS 1 "$BENCHES" \
#     "LIBLOCK_CORES=1:63 run_raytrace balls4 $REDIRECTION $PARSING_COMMANDS"
#

