#!/bin/bash

if [[ $(uname) == "Linux" ]]
then
    MAKE=make
else
    MAKE=gmake
fi


generate()
{
    $MAKE distclean
    $MAKE splash2-original
    $MAKE splash2-bound-threads
    $MAKE splash2-patched
}

compile()
{
	APP=$1

	$MAKE rebuild SPLASH=splash2-original                                      \
                  DO=splash2-original/codes/apps/$APP                          \
    && $MAKE rebuild SPLASH=splash2-bound-threads                              \
                     DO=splash2-bound-threads/codes/apps/$APP                  \
    && $MAKE rebuild SPLASH=splash2-patched                                    \
                     DO=splash2-patched/codes/apps/$APP
}


# generate

compile radiosity/glibdumb
compile radiosity/glibps
compile radiosity
compile raytrace
compile barnes
compile fmm
compile ocean/contiguous_partitions
compile ocean/non_contiguous_partitions
compile volrend/libtiff
compile volrend
compile water-nsquared
compile water-spatial

