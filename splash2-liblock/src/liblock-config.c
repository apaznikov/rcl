#define _GNU_SOURCE
#include <errno.h>
#include <sched.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#ifdef __sun__
#include <sys/types.h>
#include <sys/processor.h>
#include <sys/procset.h>
#include <sys/pset.h>
#endif
#include <liblock.h>
#include <liblock-fatal.h>
#include <liblock-splash2.h>


struct servers {
	unsigned int nb_hw_threads;
	struct hw_thread** hw_threads;
};

const char* liblock_lock_name;

static struct servers servers;

static int is_rcl = 0;
static int volatile go = 0;
static int volatile current_nb_threads = 0;
static int volatile wait_nb_threads = 0;

static void do_go()
{
	go = 1;
}

extern struct hw_thread* get_server_core_1()
{
	struct hw_thread* res = servers.hw_threads[0];
	//printf("get_server_hw_thread_1: hw_thread %d\n", res->hw_thread_id);
	return res;
}

extern struct hw_thread* get_server_core_2()
{
	struct hw_thread* res;

	if(servers.nb_hw_threads == 1)
		res = servers.hw_threads[0];
	else
    {
		static int n = 0;
		res = servers.hw_threads[1 + __sync_fetch_and_add(&n, 1)
                                 % (servers.nb_hw_threads - 1)];
	}
	// printf("get_server_hw_thread_2: hw_thread %d (%d)\n",
    // res->hw_thread_id, servers.nb_hw_threads);
	return res;
}

#ifdef __linux__
__attribute__ ((constructor)) static void liblock_splash()
#elif defined(__sun__)
__attribute__ ((constructor (103))) static void liblock_splash()
#endif
{
	char get_cmd[128];
	char* nthreads = getenv("NPROCS");
	char* var, *cur;
	unsigned int node, hw_thread;

    servers.nb_hw_threads = 0;
	servers.hw_threads = 0;

    var = getenv("LIBLOCK_CORES");
	var = var ? var : "0:1";

    for(cur=var-1; cur; cur=strchr(cur, ','))
    {
		sscanf(++cur, "%d:%d,", &node, &hw_thread);

		if(node >= topology->nb_nodes ||
           hw_thread >= topology->nodes[node].nb_hw_threads)
			fatal("no such hw_thread: %d - %d", node, hw_thread);
		servers.hw_threads = realloc(servers.hw_threads,
                                     ++servers.nb_hw_threads
                                     * sizeof(struct hw_thread*));
		servers.hw_threads[servers.nb_hw_threads-1] =
            topology->nodes[node].hw_threads[hw_thread];
	}


    if(!servers.nb_hw_threads)
		fatal("no server specified: use: "
              "LIBLOCK_CORES=node:hw_thread,node:hw_thread...");

	liblock_lock_name = getenv("LIBLOCK_LOCK_NAME");
	if(!liblock_lock_name)
		liblock_lock_name = "rcl";

	is_rcl = !strcmp(liblock_lock_name, "rcl");

	liblock_start_server_threads_by_hand = 1;
	liblock_servers_always_up = 1;
    liblock_rcl_no_local_cas = 1;

#ifdef __linux__
	sprintf(get_cmd, "/proc/%d/cmdline", getpid());

	FILE* f=fopen(get_cmd, "r");

    if(!f)
    {
		printf("!!! warning: unable to find command line\n");
	}

	char buf[1024];
	buf[0] = 0;

    if(!fgets(buf, 1024, f))
		printf("fgets\n");

	printf("**** testing %s with lock %s\n", buf, liblock_lock_name);
#endif

	liblock_auto_bind();

	if(is_rcl)
    {
		int i;

		go = 0;

		for(i=0; i<servers.nb_hw_threads; i++)
        {
//			printf("  server[%d]: hw_thread %-2d (on node %-2d)\n", i,
//                 servers.hw_threads[i]->hw_thread_id, "
//                 "servers.hw_threads[i]->node->node_id);
			liblock_reserve_hw_thread_for(servers.hw_threads[i],
                                          liblock_lock_name);
		}

        /* launch the liblock threads */
		liblock_lookup(liblock_lock_name)->run(do_go);
        while(!go)
			PAUSE();
	}

#if 1
	if(nthreads)
    {
		wait_nb_threads = atoi(nthreads);
	}
#endif
}

//__thread __a = 0;
void liblock_auto_bind()
{
    if(!self.running_hw_thread)
    {
		static int hw_threads_per_node;
		static int cur_hw_thread = 0;
		struct hw_thread*      hw_thread;
		uintptr_t cont;

        do
        {
			int k = __sync_fetch_and_add(&cur_hw_thread, 1)
                    % topology->nb_hw_threads;
			int c = k % topology->nodes[0].nb_hw_threads;
			int n = (k / topology->nodes[0].nb_hw_threads) % topology->nb_nodes;
			int i;

			hw_thread = topology->nodes[n].hw_threads[c];
			cont = (uintptr_t)hw_thread->server_type;

			if(is_rcl)
            {
				for(i=0; i<servers.nb_hw_threads; i++)
					cont |= hw_thread == servers.hw_threads[i];
			}
		}
        while(cont);

		self.running_hw_thread = hw_thread;

#ifdef __linux__
		cpu_set_t    cpuset;
		CPU_ZERO(&cpuset);
		CPU_SET(hw_thread->hw_thread_id, &cpuset);
		if(pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset))
			fatal("pthread_setaffinity_np");
#elif defined(__sun__)
        if(processor_bind(P_LWPID, P_MYID, hw_thread->hw_thread_id, NULL))
            fatal("processor_bind (%s)", strerror(errno));
#endif
/*
	    printf("autobind thread %d to hw_thread %d/%d (%d threads out of %d)\n",
               self.id, sched_getcpu(), self.running_hw_thread->hw_thread_id,
               current_nb_threads, wait_nb_threads);
*/

		__sync_fetch_and_add(&current_nb_threads, 1);

		while(current_nb_threads < wait_nb_threads)
			PAUSE();
	}
}

int splash2_thread_create(pthread_t *thread, const pthread_attr_t *attr,
                          void *(*start_routine) (void *),
                          void *arg)
{
    return liblock_thread_create(thread, attr, start_routine, arg);
}

