#!/bin/bash
#
# enable-usr-msr.sh 
# =================
# (C) Jean-Pierre Lozi, 2012
#

sudo modprobe msr
sudo ./MAKEDEV-cpuid-msr

#
# On AMD family 15h processors, MonMwaitUserEn is bit #10 of 0xC0010015.
# Default value = 0x1001011, set to 0x1001411
#
# See:
# http://support.amd.com/us/Processor_TechDocs/42301_15h_Mod_00h-0Fh_BKDG.pdf
#

for i in `seq 0 47`
do
    sudo wrmsr -p $i 0xC0010015 0x1001411
done

