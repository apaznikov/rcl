#!/bin/bash
#
# allocate-huge-pages.sh 
# ======================
# (C) Jean-Pierre Lozi, 2012
#

if [[ -z $1 ]]; then
    N=1000
else
    N=$1
fi

sudo sysctl -w vm.nr_hugepages=$N

