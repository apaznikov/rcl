#!/bin/bash
#
# get_big_table_numbers.sh
# ========================
# (C) Jean-Pierre Lozi,
#     Florian David,
#     Gaël Thomas,
#     Julia Lawall,
#     Gilles Muller
#     2013
#

if [[ -z "$BENCHMARKS" ]]
then
    BENCHMARKS="splash2-liblock phoenix-liblock berkeleydb-liblock             \
                memcached-liblock"
fi

DATE=`date +%y%m%d`

if [[ "$(hostname)" == "amd48-systeme" ]]
then
    MACHINE="amd48"
elif [[ "$(hostname)" == "amd48b-systeme" ]]
then
    MACHINE="amd48b"
elif [[ "$(hostname)" == "niagara2.cs.rochester.edu" ]]
then
    MACHINE="niagara2"
else
    MACHINE="$(hostname)"
fi

if [[ "$(id -u)" != "0" && ( $MACHINE = "amd48" || $MACHINE == "amd48b" ) ]]
then
    echo "Must be root"; exit
fi

echo "MACHINE = "$MACHINE

cd ..

for BENCHMARK in $BENCHMARKS
do
    echo -ne "\e[1;38;05;11m\e[1;48;05;16m"
    echo "====================================================================="
    echo "$BENCHMARK"
    echo -n "=================================================================="
    echo -e "===\e[0m"
    echo

    cd $BENCHMARK/scripts/
    ./get_big_table_numbers.sh 2>&1 \
        | tee ../all-results/results-${DATE}-big-table-${MACHINE}.txt
    cd ../all-results/
    svn add results-${DATE}-big-table-${MACHINE}.txt
    svn commit -m "Adding results for the big table."
    cd ../../
done

