#!/bin/bash
#
# get_server_cache_misses.sh
# ==========================
# (C) Jean-Pierre Lozi,
#     Florian David,
#     Gaël Thomas,
#     Julia Lawall,
#     Gilles Muller
#     2013
#

DATE=`date +%y%m%d`

if [[ "$(hostname)" == "amd48-systeme" ]]
then
    MACHINE="amd48"
elif [[ "$(hostname)" == "amd48b-systeme" ]]
then
    MACHINE="amd48b"
elif [[ "$(hostname)" == "niagara2.cs.rochester.edu" ]]
then
    MACHINE="niagara2"
else
    MACHINE="$(hostname)"
fi

if [[ -z "$BENCHMARKS" ]]
then
    if [[ "$MACHINE" == "niagara2" ]]
    then
        BENCHMARKS=("splash2-liblock" "memcached-liblock" "berkeleydb-liblock")
        COMMANDS=('METRIC="SERVER_CACHE_MISSES" N_RUNS=5 ./benchmark.sh'
          'METRIC="SERVER_CACHE_MISSES" N_RUNS=5 ./benchmark.sh'
          'EXECUTION_TYPE="WITH_SERVER_CACHE_MISSES" N_RUNS=5 ./benchmark.sh')
    else
        BENCHMARKS=("splash2-liblock" "phoenix-liblock" "memcached-liblock"    \
                    "berkeleydb-liblock")
        COMMANDS=( 'METRIC="SERVER_CACHE_MISSES" N_RUNS=5 ./benchmark.sh'
            'METRIC="SERVER_CACHE_MISSES" N_RUNS=5 ./benchmark.sh'
            'METRIC="SERVER_CACHE_MISSES" N_RUNS=5 ./benchmark.sh'
            'EXECUTION_TYPE="WITH_SERVER_CACHE_MISSES" N_RUNS=5 ./benchmark.sh')
    fi
fi

if [[ "$(id -u)" != "0" && ( "$MACHINE" == "amd48" || "$MACHINE" == "amd48b" ) ]]
then
    echo "Must be root"; exit
fi

echo "MACHINE = "$MACHINE

cd ..

for BENCHMARK_ID in 0 1 2 3
do
    BENCHMARK=${BENCHMARKS[$BENCHMARK_ID]}
    COMMAND=${COMMANDS[$BENCHMARK_ID]}

    echo -ne "\e[1;38;05;11m\e[1;48;05;16m"
    echo "====================================================================="
    echo "$BENCHMARK"
    echo -n "=================================================================="
    echo -e "===\e[0m"
    echo

    cd $BENCHMARK/scripts/
    eval $COMMAND 2>&1 \
        | tee ../all-results/results-${DATE}-server-cache-misses-${MACHINE}.txt
    cd ../all-results/
    svn add results-${DATE}-server-cache-misses-${MACHINE}.txt
    svn commit -m "Adding results for the big table."
    cd ../../
done

