/**
 * prog.c: Simple example for RCL (remote core locking)
 *
 * (C) 2015 Alexey Paznikov <apaznikov@gmail.com>
 */

#define _GNU_SOURCE

#include <stdlib.h>

#include "liblock.h"

/* Environment data ========================================================= */
/* Number of hw_threads */
volatile int g_number_of_hw_threads;
/* CPU frequency, in MHz */
volatile float g_cpu_frequency;
/* This array maps physical to virtual core IDs. */
volatile int *g_physical_to_virtual_hw_thread_id;
/* Has the server started? */
volatile int g_server_started;

// Output error messages and so on
#define echo(msg, ...)                                                         \
    do                                                                         \
    {                                                                          \
        fprintf(stderr, "%-15s", "["msg"]: ");                                 \
        fprintf(stderr, __VA_ARGS__);                                          \
        fprintf(stderr, "\n");                                                 \
    }                                                                          \
    while(0)

#define info(...) echo("info", __VA_ARGS__)
#define warning(...) echo("warning", __VA_ARGS__)

#define fatal(...)                                                             \
    do                                                                         \
    {                                                                          \
        echo("error", __VA_ARGS__);                                            \
        fprintf(stderr, "   at %s::%d (%s)\n", __FILE__, __LINE__,             \
                __PRETTY_FUNCTION__);                                          \
        abort();                                                               \
    }                                                                          \
    while(0)

struct hw_thread* get_core(unsigned int physical_core)
{
    return &topology->
        hw_threads[g_physical_to_virtual_hw_thread_id[physical_core]];
}

/* This function gets the CPU speed then allocates and fills the
   virtual_to_physical_hw_thread_id array. */
void get_cpu_info()
{
    int i = 0, j, n;
    float /*previous_core_frequency = -1,*/ core_frequency = 0;

    /* Compute frequency. */
    for(i=0; i < topology->nb_hw_threads; i++) {
        core_frequency = topology->hw_threads[i].frequency;
    }

    /* We update the global cpu_frequency variable. */
    g_cpu_frequency = core_frequency;

    g_number_of_hw_threads = topology->nb_hw_threads;

    /* We allocate the physical_to_virtual_hw_thread_id array. */
    g_physical_to_virtual_hw_thread_id =
        malloc(g_number_of_hw_threads * sizeof(int));

    for(i=0, n=0; i < topology->nb_nodes; i++)
    {
        for(j=0; j < topology->nodes[i].nb_hw_threads; j++)
        {
            g_physical_to_virtual_hw_thread_id[n++] =
                topology->nodes[i].hw_threads[j]->hw_thread_id;
        }
    }
}
