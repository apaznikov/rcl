/**
 * liblock.h: Simple example for RCL (remote core locking)
 *
 * (C) 2015 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef LIBLOCK_CONFIG_H_
#define LIBLOCK_CONFIG_H_

extern struct hw_thread* get_server_core_1();
extern struct hw_thread* get_server_core_2();
extern struct hw_thread* get_server_core_3();

//#ifdef __linux__
//__attribute__ ((constructor)) static void liblock_splash()
//#elif defined(__sun__)
//__attribute__ ((constructor (103))) static void liblock_splash()
//#endif

void liblock_splash();

#endif /* LIBLOCK_CONFIG_H_ */
