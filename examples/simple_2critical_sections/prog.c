/**
 * prog.c: Simple example for RCL (remote core locking)
 *
 * (C) 2015 Alexey Paznikov <apaznikov@gmail.com> 
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <stdlib.h>
#include <unistd.h>

#include "liblock.h"
#include "utils.h"
#include "liblock-config.h"

// Number of working threads
enum {
    NTHREADS = 2,
    MAX_CNTR = 100000,
    NLIBLOCKS = 50,
    USLEEP_BETWEEN_CS = 100,
    USLEEP_IN_CS = 1000,
};

liblock_lock_t g_liblock1, g_liblock2;
liblock_lock_t liblock_arr[NLIBLOCKS];
const char* g_liblock_name = "rcl";

volatile int *context1, *context2;
volatile int global_cntr1 = 0, global_cntr2 = 0;

volatile int g_server_core = 0;

void *cs1(void *arg)
{
    global_cntr1++;
    /* usleep(USLEEP_IN_CS); */
    return NULL;
}

void *cs2(void *arg)
{
    global_cntr2++;
    /* usleep(USLEEP_IN_CS); */
    return NULL;
}

void *thread(void *arg)
{
    int i;

    printf("hello i'm 0x%llx\n", (unsigned long long) pthread_self());

    for (i = 0; i < MAX_CNTR; i++) {
        liblock_exec(&g_liblock1, cs1, (void *) context1);
        /* usleep(USLEEP_BETWEEN_CS); */
        liblock_exec(&g_liblock2, cs2, (void *) context2);
        /* usleep(USLEEP_BETWEEN_CS); */
    }

    return NULL;
}

int main(int argc, const char *argv[])
{
    printf("try building locks...\n");

    liblock_lock_init(g_liblock_name, get_server_core_1(), &g_liblock1, 0);
    liblock_lock_init(g_liblock_name, get_server_core_1(), &g_liblock2, 0);

    pthread_t tids[NTHREADS];

    for (int i = 0; i < NTHREADS; i++) {
        liblock_thread_create(&tids[i], NULL, thread, NULL);
    }

    for (int i = 0; i < NTHREADS; i++) {
        pthread_join(tids[i], NULL);
    }

    liblock_lock_destroy(&g_liblock1);
    liblock_lock_destroy(&g_liblock2);

    printf("global_cntr1 = %d\n", global_cntr1);
    printf("global_cntr2 = %d\n", global_cntr2);

    return 0;
}
