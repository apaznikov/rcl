set term pngcairo transparent enhanced font "Times,34" size 1200,800
set xlabel "%%XLABEL%%" 
set ylabel "%%YLABEL%%" 
set output "graphs/%%NAME%%.png"
set key inside bottom nobox

set border lw 3
set grid lw 3
set pointsize 2.0

plot "results/rcl_%%NAME%%.dat" using 1:2  ti "RCL" \
     with lp lw 5 pt 4 ps 3 lc rgb '#C40D28', \
     "results/mutex_%%NAME%%.dat" using 1:2  ti "mutex" \
     with lp lw 5 pt 7 ps 3 lc rgb '#003399', \
     "results/spinlock_%%NAME%%.dat" using 1:2  ti "spinlock" \
     with lp lw 5 pt 5 ps 3 lc rgb '#007B00'
