// 
// prog.h: Simple lock implementations
// (C) 2015 Alexey Paznikov <apaznikov@gmail.com> 
// 

#pragma once

#include <atomic>

// Simple spinlock implementation
class spinlock_t
{
public:
    void lock()
    {
        // FIXME: this is very naive implementation, better improve it
        while(flag.test_and_set(std::memory_order_acquire))
        { }
    }

    void unlock()
    {
        flag.clear(std::memory_order_release);
    }

private:
    std::atomic_flag flag = ATOMIC_FLAG_INIT;
};
