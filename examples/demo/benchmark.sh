# 
# benchmark.sh: Simple benchmark script for RCL
# (C) 2015 Alexey Paznikov <apaznikov@gmail.com> 
# 

#!/bin/sh

# Delay between critical sections (in nanoseconds)

# DELAY_CONTENTION="1 2 3 4 5 6 7 8 9 10"
DELAY_CONTENTION="100 200 300 400 500 600 700 800 900 1000"
# DELAY_CONTENTION="20000 25000 30000 35000 40000 45000"
# DELAY_CONTENTION="24000 26000 28000 30000 32000 34000 36000 38000 40000"
DELAY_SCALABILITY_MANY_THR=10000
DELAY_SCALABILITY_FEW_THR=0
DELAY_CACHE_MISSES=$DELAY_SCALABILITY_MANY_THR

# Total number of iterations 
# (each thread does this number devided by number of threads)

NITERS_SCALABILITY_MANY_THR=20000000
NITERS_SCALABILITY_FEW_THR=5000000
# NITERS_SCALABILITY_FEW_THR=20000000
NITERS_CONTENTION=$NITERS_SCALABILITY_FEW_THR
NITERS_CACHE_MISSES=$NITERS_SCALABILITY_MANY_THR

# Number of threads in each test

NTHREADS="20 40 60 80 100 120 140 160 180 200"
# NTHREADS="20 40 60 80 100 120 140"
hwthreads=`cat /proc/cpuinfo | grep "processor" | wc -l`
NTHREADS_CONTENTION=$(($hwthreads - 1))
NTHREADS_HW="`seq 2 $NTHREADS_CONTENTION`"

LOCKTYPES="rcl mutex spinlock"

# NRUNS=5
NRUNS=10

function measure_throughput
{
    niters=$7
    # time=`(/usr/bin/time -f %e $@) 2>&1`
    time=`$@ | egrep "Elapsed time" | awk '{print $3}'`
    echo $time
    throughput=`echo $niters $time | awk '{ print $1 / $2 / 1000 }'`
    echo $throughput >>results.tmp
}

function measure_cache_misses
{
    perf stat -e cache-references,cache-misses $@ 2>&1 \
        | egrep "cache-misses" | awk '{ print $4 }' >results.tmp
}

function measure 
{
    T=$1; D=$2; I=$3; L=$4
    dat=$5
    dat_first_col=$6
    measure_type=$7

    cat /dev/null >results.tmp

    for ((i = 1; i <= NRUNS; i++)); do
        cmd="./bench -t $T -d $D -i $I -l $L"
        echo $cmd

        if [ "$measure_type" == "throughput" ]; then
            measure_throughput $cmd
        elif [ "$measure_type" == "cache-misses" ]; then
            measure_cache_misses $cmd
        fi
    done

    avg=`awk '{ total += $1; count++ } END { print total / count }' results.tmp`

    echo -e "$dat_first_col \t\t $avg" >>$dat

    rm results.tmp
}

[ ! -d "results" ] && mkdir results

#
# Test for SCALABILITY (NO MORE than hardware threads)
#
function scalability_hw
{
    for locktype in $LOCKTYPES; do
        datfile="results/${locktype}_scalability_hw.dat"
        echo -e "nthreads \t throughput, op/s" >$datfile

        for nthreads in $NTHREADS_HW; do
            measure $nthreads $DELAY_SCALABILITY_FEW_THR \
                    $NITERS_SCALABILITY_FEW_THR $locktype \
                    $datfile $nthreads throughput
        done
    done
}

#
# Test for SCALABILITY (MORE than hardware threads)
#
function scalability
{
    for locktype in $LOCKTYPES; do
        datfile="results/${locktype}_scalability.dat"
        echo -e "nthreads \t throughput, op/s" >$datfile

        for nthreads in $NTHREADS; do
            measure $nthreads $DELAY_SCALABILITY_MANY_THR \
                    $NITERS_SCALABILITY_MANY_THR $locktype \
                    $datfile $nthreads throughput
        done
    done
}

#
# Test for CONTENTION
#
function contention
{
    for locktype in $LOCKTYPES; do
        datfile="results/${locktype}_contention.dat"
        echo -e "delay \t\t throughput, op/s" >$datfile

        for delay in $DELAY_CONTENTION; do
            measure $NTHREADS_CONTENTION $delay \
                    $NITERS_CONTENTION $locktype \
                    $datfile $delay throughput
        done
    done
}

#
# Test for CACHE
#
function cache_misses 
{
    for locktype in $LOCKTYPES; do
        datfile="results/${locktype}_cache-misses.dat"
        echo -e "nthreads \t cache-misses, %" >$datfile

        for nthreads in $NTHREADS; do
            measure $nthreads $DELAY_CACHE_MISSES \
                    $NITERS_CACHE_MISSES $locktype \
                    $datfile $nthreads cache-misses
        done
    done
}

scalability_hw
scalability
contention
cache_misses
