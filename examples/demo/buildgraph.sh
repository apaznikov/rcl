#!/bin/sh

function build
{
    name="$1"
    xlabel="$2"
    ylabel="$3"

    [ ! -d "graphs" ] && mkdir graphs

    cat tmpl.gp | sed "s/%%NAME%%/$name/g" \
                | sed "s/%%XLABEL%%/$xlabel/g" \
                | sed "s/%%YLABEL%%/$ylabel/g" >graphs/$name.gp

    gnuplot graphs/$name.gp
}

build "scalability"    "nthreads"   "throughput,\ 1000 op\/s"
build "scalability_hw" "nthreads"   "throughput,\ 1000 op\/s"
build "contention"     "delay,\ ns" "throughput,\ 1000 op\/s"
build "cache-misses"   "nthreads"   "cache-misses,\ %"
