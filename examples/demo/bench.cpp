// 
// prog.c: Simple example for RCL (remote core locking)
// (C) 2015 Alexey Paznikov <apaznikov@gmail.com> 
// 

#include <cstdio>
#include <cstdlib>

#include <pthread.h>
#include <getopt.h>

#include <thread>
#include <iostream>
#include <vector>
#include <chrono>
#include <random>
#include <map>
#include <mutex>
#include <unordered_map>

#include <boost/thread/barrier.hpp>

#include "liblock.h"
#include "locks.h"

// Default parameters
using namespace std::literals;
auto NTHREADS            = 100;
auto NITERS              = 20000;
auto DELAY_BETWEEN_CS    = 10000ns;
auto get_randmax         = [](auto niters){ return niters / 2; };
auto RANDMAX             = get_randmax(NITERS);
auto const SEARCH_NITERS = 5;

enum locktype_t { rcl_type, mutex_type, spinlock_type } LOCKTYPE;

volatile auto global_cntr   = 0;
volatile auto found_value   = 0;

// Map alternatives
std::unordered_map<int, int> map;
// std::map<int, int> map;

// RCL lock
liblock_lock_t g_liblock;
auto* const g_liblock_name  = "rcl";
volatile int *context;

// PThread mutex lock
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

// PThread spinlock lock
spinlock_t spinlock;

std::function<void()> cs_wrapper;

std::shared_ptr<boost::barrier> barrier;

std::once_flag onceflag1, onceflag2;

// Time measurement function
auto get_time = std::chrono::steady_clock::now;
decltype(get_time()) start, end;

// get_rand: Thread-safe random generator
auto get_rand(int min, int max)
{
    static thread_local std::unique_ptr<std::mt19937> generator;
    static thread_local std::hash<std::thread::id> hasher;

    if (!generator)
        generator = std::make_unique<std::mt19937>(
                clock() + hasher(std::this_thread::get_id()));

    std::uniform_int_distribution<int> distribution(min, max);
    return distribution(*generator);
}

// cs: Critical section
void *cs(void *arg)
{
    auto key = get_rand(0, RANDMAX);
    auto value = get_rand(0, RANDMAX);

    // Do insert into map
    map.insert(std::make_pair(key, value));

    // Do search in map
    for (auto i = 0; i < SEARCH_NITERS; i++) {
        key = get_rand(0, RANDMAX);
        found_value = map[key]; 
    }

    global_cntr++;

    return nullptr;
}

// thread: Thread implementation
void *thread(void *arg)
{
#ifdef _DEBUG
    std::cout << "hello i'm 0x" << std::hex << std::this_thread::get_id() 
              << std::dec << std::endl;
#endif

    // Wait until all threads started
    barrier->wait();

    std::call_once(onceflag1, []{ start = get_time(); });

    // Execute critical sections
    for (auto i = 0; i < NITERS / NTHREADS; i++) {
        cs_wrapper();
        std::this_thread::sleep_for(DELAY_BETWEEN_CS);
    }

    barrier->wait();

    std::call_once(onceflag2, []{ end = get_time(); });

    return NULL;
}

int main(int argc, char* const argv[])
{
    auto command = 0;

    while ((command = getopt(argc, argv, "t:d:i:l:")) != -1) {
        switch (command) {
            case 't':
                NTHREADS = atoi(optarg);
                break;

            case 'd':
                DELAY_BETWEEN_CS = std::chrono::nanoseconds(atoi(optarg));
                break;

            case 'i':
                NITERS = atoi(optarg);
                RANDMAX = get_randmax(NITERS / 2);
                break;

            case 'l':
                auto locktype_s = std::string{optarg};

                if (locktype_s == "rcl")
                    LOCKTYPE = rcl_type;
                else if (locktype_s == "mutex")
                    LOCKTYPE = mutex_type;
                else if (locktype_s == "spinlock")
                    LOCKTYPE = spinlock_type;

                break;
        }
    }

    if (LOCKTYPE == rcl_type) {
        liblock_lock_init(g_liblock_name, &topology->hw_threads[0], 
                          &g_liblock, 0);

        cs_wrapper = [&](){
            liblock_exec(&g_liblock, cs, (void *) context);
        };

    } else if (LOCKTYPE == mutex_type) {
        cs_wrapper = [&](){
            pthread_mutex_lock(&mutex);
            cs((void *) context);
            pthread_mutex_unlock(&mutex);
        };

    } else if (LOCKTYPE == spinlock_type) {
        cs_wrapper = [&](){
            spinlock.lock();
            cs((void *) context);
            spinlock.unlock();
        };
    }

    std::vector<pthread_t> tids(NTHREADS);

    std::shared_ptr<boost::barrier> 
        barrier_ptr{new boost::barrier(uint(NTHREADS))};
    barrier = barrier_ptr;

    for (auto &tid: tids) {
        liblock_thread_create(&tid, NULL, thread, NULL);
    }

    for (auto tid: tids) {
        pthread_join(tid, NULL);
    }

    if (LOCKTYPE == rcl_type)
        liblock_lock_destroy(&g_liblock);

#ifdef _DEBUG
    std::cout << "global_cntr = " << global_cntr << std::endl;
#endif

    auto elapsed = std::chrono::duration_cast
                   <std::chrono::milliseconds>(end - start).count();

    std::cout << "Elapsed time: " << double(elapsed) / 1000 << " seconds" 
              << std::endl;

    return 0;
}
