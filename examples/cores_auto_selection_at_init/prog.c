/**
 * prog.c: Simple example for RCL (remote core locking)
 *
 * (C) 2015 Alexey Paznikov <apaznikov@gmail.com> 
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <stdlib.h>

#include "liblock.h"
#include "utils.h"
#include "liblock-config.h"

// Number of working threads
enum {
    NTHREADS = 2,
    MAX_CNTR = 100000,
    NLIBLOCKS = 50
};

liblock_lock_t g_liblock1, g_liblock2;
liblock_lock_t g_liblock3, g_liblock4;
liblock_lock_t liblock_arr[NLIBLOCKS];
const char* g_liblock_name = "rcl";

volatile int *context1, *context2;
volatile int *context3, *context4;
volatile int global_cntr1 = 0, global_cntr2 = 0;
volatile int global_cntr3 = 0, global_cntr4 = 0;

volatile int g_server_core = 0;

void *cs1(void *arg)
{
    global_cntr1++;
    return NULL;
}

void *cs2(void *arg)
{
    global_cntr2++;
    return NULL;
}

void *cs3(void *arg)
{
    global_cntr3++;
    return NULL;
}

void *cs4(void *arg)
{
    global_cntr4++;
    return NULL;
}

void *thread(void *arg)
{
    int i;

    printf("hello i'm %d\n", (int) pthread_self());

    for (i = 0; i < MAX_CNTR; i++) {
        liblock_exec(&g_liblock1, cs1, (void *) context1);
        liblock_exec(&g_liblock2, cs2, (void *) context2);
//        liblock_exec(&g_liblock3, cs3, (void *) context3);
//        liblock_exec(&g_liblock4, cs4, (void *) context4);
    }

    return NULL;
}

int main(int argc, const char *argv[])
{
    printf("try building locks...\n");

#ifdef OLD_INIT
    get_cpu_info();

    liblock_define_hw_thread(get_core(g_server_core));

    liblock_lock_init(g_liblock_name, self.running_hw_thread, &g_liblock1, 0);
    liblock_lock_init(g_liblock_name, self.running_hw_thread, &g_liblock2, 0);
    liblock_lock_init(g_liblock_name, self.running_hw_thread, &g_liblock3, 0);
    liblock_lock_init(g_liblock_name, self.running_hw_thread, &g_liblock4, 0);
#endif

#if 0
//    liblock_init_library();
//    liblock_splash();

    liblock_lock_init(g_liblock_name, get_server_core_3(), &g_liblock1, 0);
    liblock_lock_init(g_liblock_name, get_server_core_3(), &g_liblock2, 0);
//    liblock_lock_init(g_liblock_name, &topology->hw_threads[2], &g_liblock1, 0);
//    liblock_lock_init(g_liblock_name, &topology->hw_threads[2], &g_liblock2, 0);
//    liblock_lock_init(g_liblock_name, &topology->hw_threads[0], &g_liblock3, 0);
//    liblock_lock_init(g_liblock_name, &topology->hw_threads[1], &g_liblock4, 0);

    pthread_t tids[NTHREADS];

    for (int i = 0; i < NTHREADS; i++) {
        liblock_thread_create(&tids[i], NULL, thread, NULL);
    }

    for (int i = 0; i < NTHREADS; i++) {
        pthread_join(tids[i], NULL);
    }

    liblock_lock_destroy(&g_liblock1);
    liblock_lock_destroy(&g_liblock2);
//    liblock_lock_destroy(&g_liblock3);
//    liblock_lock_destroy(&g_liblock4);

    printf("cntr1 = %d cntr2 = %d\n", global_cntr1, global_cntr2);
    printf("cntr3 = %d cntr4 = %d\n", global_cntr3, global_cntr4);
#endif

//    liblock_lock_init(g_liblock_name, &topology->hw_threads[0], &g_liblock3, 0);
//    liblock_lock_init(g_liblock_name, &topology->hw_threads[1], &g_liblock4, 0);

    for (int i = 0; i < NLIBLOCKS; i++) {
        liblock_lock_init(g_liblock_name, get_server_core_3(), &liblock_arr[i], 0);
    }

    for (int i = 0; i < NLIBLOCKS; i++) {
        liblock_lock_destroy(&liblock_arr[i]);
    }

    printf("ok!\n");

    return 0;
}
