/**
 * prog.c: Simple example for RCL (remote core locking)
 *
 * (C) 2016 Alexey Paznikov <apaznikov@gmail.com> 
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <stdlib.h>
#include <unistd.h>

#include "liblock.h"

enum {
    NTHREADS = 2,
    NITERS = 1000000
};

liblock_lock_t lock;
const char* liblock_name = "rcl";

/* int global_var = 0; */
int *global_var = NULL;

pthread_mutex_t mut = PTHREAD_MUTEX_INITIALIZER;
/* pthread_spinlock_t spinlock; */

void *cs(void *arg)
{
    (*global_var)++;
    return NULL;
}

void *thread(void *arg)
{
    int i;
    for (i = 0; i < NITERS; i++) {
        liblock_exec(&lock, cs, NULL);
        /* pthread_mutex_lock(&mut); */
        /* pthread_spin_lock(&spinlock); */
        /* cs(NULL); */
        /* pthread_spin_unlock(&spinlock); */
        /* pthread_mutex_unlock(&mut); */
    }

    return NULL;
}

int main(int argc, const char *argv[])
{
    liblock_lock_init(liblock_name, &topology->hw_threads[0], &lock, 0);
    /* pthread_spin_init(&spinlock, PTHREAD_PROCESS_PRIVATE); */
    global_var = malloc(sizeof(*global_var));
    *global_var = 0;
    assert(global_var != NULL);

    pthread_t tids[NTHREADS];

    for (int i = 0; i < NTHREADS; i++) {
        /* pthread_create(&tids[i], NULL, thread, NULL); */
        liblock_thread_create(&tids[i], NULL, thread, NULL);
    }

    for (int i = 0; i < NTHREADS; i++) {
        pthread_join(tids[i], NULL);
    }

    liblock_lock_destroy(&lock);

    printf("global_var = %d\n", *global_var);
    free(global_var);

    return 0;
}
