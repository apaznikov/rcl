// 
// access_struct.cpp: Structure for test access 
// (C) 2015-2016 Alexey Paznikov <apaznikov@gmail.com> 
// 

#include "access_struct.h"
#include "utils.h"

#include <memory>
#include <iostream>

#include <cmath>

#include <numa.h>
#include <numaif.h>

#include <sys/mman.h>

#define _NUMA_ALLOC 
// #define _NUMA_ALLOC_ONNODE
#define _LOCK_PAGES_ON

// elem_modify: Modify element
inline void access_struct::elem_modify(elemtype &elem)
{
    // for (auto i = 0; i < 1; i++) {
        // elem += 1;
        // elem = sqrt(elem + 0.5);
        // elem = sin(elem + 2);
        // elem = cos(elem + 2);
        // elem = sqrt(elem + 2);
        // elem = sin(elem + 2);
        // elem = cos(elem + 2);
    // }
    elem += 1;
}

// init: Initalize access structures
void access_struct::init(unsigned _bufsize,
                         const affinity &_rcl_server_affin, 
                         const mem_affin_t &_mem_affin,
                         const access_pattern_t &access_pattern,
                         unsigned _stride)
{
    bufsize = _bufsize;
    stride = _stride;
    mem_affin = _mem_affin;
    rcl_server_affin = _rcl_server_affin;

    auto mem_func_search = mem_func_map.find(mem_affin);
    if (mem_func_search != mem_func_map.end()) {
        mem_alloc = mem_func_search->second.first;
        mem_free = mem_func_search->second.second;
    } else {
        throw std::runtime_error(
                "access_struct::init(): Invalid memory affinity policy");
    }

    buf = mem_alloc();

    if (buf == nullptr)
        throw std::runtime_error("Can't allocate memory for access_struct");

    auto access_pattern_search = access_pattern_map.find(access_pattern);
    if (access_pattern_search != access_pattern_map.end())
        access_func = access_pattern_search->second;
    else
        throw std::runtime_error(
                "access_struct::init(): Invalid access pattern");
}

////////////////////////////////////////////////////////////////////////////////
//                        Memory handling function                            //
////////////////////////////////////////////////////////////////////////////////

// init_mem_func_map: Init memory handling functions by memory allocation policy
void access_struct::init_mem_func_map()
{
   mem_func_map = {
       { std::make_pair(mem_affin_t::default_alloc, 
                        std::make_pair([this]{ return default_alloc(); }, 
                                       [this]{ return default_free(); }))
       },
       { std::make_pair(mem_affin_t::rcl_server_node, 
                        std::make_pair([this]{ return rcl_server_node_alloc();},
                                       [this]{ return rcl_server_node_free();}))
       },
       { std::make_pair(mem_affin_t::interleaved, 
                        std::make_pair([this]{ return interleaved_alloc(); }, 
                                       [this]{ return interleaved_free(); }))
       }
    };
}

// default_alloc: Default memory allocation
access_struct::elemtype *access_struct::default_alloc()
{
    // std::cerr << "default_alloc()\n";
    auto const bufsize_alloc = bufsize + 1;

    auto buf = (elemtype*) malloc(bufsize_alloc * sizeof(elemtype));

    if (buf == nullptr)
        throw std::runtime_error("Can't allocate memory for access_struct");

#ifdef _LOCK_PAGES_ON
    // Lock pages to prevent swapping
    mlock(buf, bufsize_alloc * sizeof(elemtype));
#endif

    // Touch pages
    memset(buf, 0, bufsize_alloc * sizeof(elemtype));

    return buf;
}

// rcl_server_node_alloc: Allocate on node where rcl server is running
access_struct::elemtype *access_struct::rcl_server_node_alloc()
{
    if (numa_available() < 0) {
        // If no NUMA available, allocate by default
        return default_alloc();
    }

    std::shared_ptr<bitmask> nodemask(numa_get_mems_allowed(),
                                      numa_free_nodemask);

    auto rcl_node = rcl_server_affin.node;

    if (!numa_bitmask_isbitset(nodemask.get(), rcl_node)) {
        std::cerr << "NUMA node " << rcl_node 
                  << " is not allowed for memory allocation" 
                  << std::endl;
        return nullptr;
    }

    auto const bufsize_alloc = bufsize + 1;

    // std::cout << "affin: " << rcl_server_affin.core << std::endl;
    // std::cout << "rcl_node: " << rcl_node << std::endl;

#if defined _NUMA_ALLOC
    printf("%ld %ld %ld\n", 
           *nodemask.get()->maskp & 1, 
           *nodemask.get()->maskp & 2,
           *nodemask.get()->maskp & 4);

    numa_bitmask_clearall(nodemask.get());
    numa_bitmask_setbit(nodemask.get(), rcl_node);
    numa_set_bind_policy(1);
    numa_set_membind(nodemask.get());

    printf("size = %ld\n", nodemask.get()->size);
    printf("after %d set %ld %ld %ld\n", 
           rcl_node, 
           *nodemask.get()->maskp & 1, 
           *nodemask.get()->maskp & 2,
           *nodemask.get()->maskp & 4);

    // std::cout << "preferred: " << numa_preferred() << std::endl;

    auto buf = (elemtype*) numa_alloc(bufsize_alloc * sizeof(elemtype));
    if (buf == nullptr)
        throw std::runtime_error("Can't allocate memory for access_struct");

    // TODO restore to default
#ifdef _LOCK_PAGES_ON
    // Lock pages to prevent swapping
    mlock(buf, bufsize_alloc * sizeof(elemtype));
#endif

    // Touch pages
    memset(buf, 0, bufsize_alloc * sizeof(elemtype));

    // numa_set_preferred(-1);
    // numa_set_bind_policy(0);
#elif defined _NUMA_ALLOC_ONNODE
    // Allocate strict on specified node in numa_alloc_onnode()
    numa_set_strict(1);

    auto buf = (elemtype*) numa_alloc_onnode(bufsize_alloc * sizeof(elemtype),
                                             rcl_node);
    if (buf == nullptr)
        throw std::runtime_error("Can't allocate memory for access_struct");

#ifdef _LOCK_PAGES_ON
    // Lock pages to prevent swapping
    mlock(buf, bufsize_alloc * sizeof(elemtype));
#endif

    // Touch pages
    memset(buf, 0, bufsize_alloc * sizeof(elemtype));

#else
    throw std::runtime_error("Allocation runtime error");
#endif

    return buf;
}

// interleaved_alloc: Allocate in interleaved manner
access_struct::elemtype *access_struct::interleaved_alloc()
{
    // std::cerr << "interleaved_alloc()\n";
    if (numa_available() < 0) {
        // If no NUMA available, allocate by default
        return default_alloc();
    }

    auto const bufsize_alloc = bufsize + 1;

    auto buf = (elemtype*) numa_alloc_interleaved(bufsize_alloc * 
                                                  sizeof(elemtype));

    if (buf == nullptr)
        throw std::runtime_error("Can't allocate memory for access_struct");

#ifdef _LOCK_PAGES_ON
    // Lock pages to prevent swapping
    mlock(buf, bufsize_alloc * sizeof(elemtype));
#endif

    // Touch pages
    memset(buf, 0, bufsize_alloc * sizeof(elemtype));

    return buf;
}

// memory_free: Free memory allocated in NUMA-aware fashion
void access_struct::default_free()
{
    if (buf != nullptr) {
        free(buf);
    }
}

// rcl_node_server_free: Free memory allocated on RCL server node
void access_struct::rcl_server_node_free()
{
    if (buf != nullptr) {
        munlockall();

        auto const bufsize_alloc = bufsize + 1;
        numa_free(buf, bufsize_alloc * sizeof(elemtype));
    }
}

// interleaved_free: Free memory allocated in interleaved manner
void access_struct::interleaved_free()
{
    // std::cout << "interleaved_free()\n";
    if (buf != nullptr) {
        auto const bufsize_alloc = bufsize + 1;
        numa_free(buf, bufsize_alloc * sizeof(elemtype));
    }
}

////////////////////////////////////////////////////////////////////////////////
//                         Memory access patterns                             //
////////////////////////////////////////////////////////////////////////////////

// init_access_pattern_map: Init assignment the access patterns to functions.
void access_struct::init_access_pattern_map()
{
    access_pattern_map = {
        { std::make_pair(access_pattern_t::random, [this]{ random_access(); })},
        { std::make_pair(access_pattern_t::seq,    [this]{ seq_access(); })},
        { std::make_pair(access_pattern_t::strided,[this]{ strided_access(); })}
    };
}

// random_access: Random access pattern
inline void access_struct::random_access()
{
    // std::cout << "random_access\n";
    auto const ind = get_rand(0, bufsize);
    elem_modify(buf[ind]);
}

// random_access: Random access pattern
inline void access_struct::seq_access()
{
    thread_local static auto ind = 0;
    // std::cout << "thread " << std::this_thread::get_id() 
    //           << " seq_access ind " << ind << std::endl;
    elem_modify(buf[ind]);
    ind = (ind + 1) % bufsize;
}

// random_access: Strided access pattern
inline void access_struct::strided_access()
{
    thread_local static auto ind = 0;
    // std::cout << "thread " << std::this_thread::get_id() 
    //           << " seq_access ind " << ind << std::endl;
    elem_modify(buf[ind]);
    ind = (ind + stride) % bufsize;
}

///////////////////////////////////////////////////////////////////////////////
