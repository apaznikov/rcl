#!/bin/sh

# NITERS=20000000
# NITERS=50000000
# NITERS=50000000
NITERS=100000000
# NITERS=500000000
# BUFSIZE=2000
# BUFSIZE=1000000000
BUFSIZE=500000000
# ELEMSIZE=500
NTHREADS=4
STRIDE=1000
# LOCKTYPES="rcl mutex spinlock"
LOCKTYPES="rcl"
# MEM_AFFIN="default rcl_server_node interleaved"
# MEM_AFFIN="default"
MEM_AFFIN="default rcl_server_node"
# MEM_AFFIN="rcl_server_node"
# MEM_AFFIN="interleaved"
ACCESS_PATTERN="random seq strided"
# ACCESS_PATTERN="random seq"
# ACCESS_PATTERN="strided"

for locktype in $LOCKTYPES; do
    for mem_affin in $MEM_AFFIN; do
        for access_pattern in $ACCESS_PATTERN; do
            if !( [ "$locktype" != "rcl" ] &&
                  [ "$mem_affin" == "rcl_server_node" ] ); then
                cmd="./membench -i $NITERS \
                                -b $BUFSIZE \
                                -t $NTHREADS \
                                -l $locktype \
                                -s $STRIDE \
                                -m $mem_affin \
                                -p $access_pattern"
                echo $cmd
                $cmd 
                echo
            fi
        done
    done
done
