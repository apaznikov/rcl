// 
// utils.h: Utilities
// (C) 2015-2016 Alexey Paznikov <apaznikov@gmail.com> 
// 

#pragma once

// get_rand: Thread-safe random generator
int get_rand(int min, int max);
