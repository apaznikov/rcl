# 
# benchmark.sh: Simple benchmark script for RCL
# (C) 2015-2016 Alexey Paznikov <apaznikov@gmail.com> 
# 

#!/bin/sh

# NITERS=1000000
NITERS=100000000      # This is it
# NITERS=50000000
BUFSIZE=500000000       # This is it
# BUFSIZE=50000000       
# BUFSIZE=200000000
# BUFSIZE=50000000
STRIDE=1000
MEMAFFIN="default rcl_server_node"
ACCESS_PATTERN="random seq strided"
AFFINITY_FILE="thread_affinity"
DELAY=0
hwthreads=`cat /proc/cpuinfo | grep "processor" | wc -l`
NTHREADS_CONTENTION=$(($hwthreads - 1))
NTHREADS_HW="`seq 2 $NTHREADS_CONTENTION`"
# BENCHNAME=membench_debug.sh
BENCHNAME=membench

NRUNS=3

function measure_throughput
{
    # time=`(/usr/bin/time -f %e $@) 2>&1`
    time=`$@ | egrep "Elapsed time" | awk '{print $3}'`
    throughput=`echo $niters $time | awk '{ print $1 / $2 / 1000 }'`
    echo $throughput >>results.tmp
}

function measure_cache_misses
{
    perf stat -e cache-references,cache-misses $@ 2>&1 \
        | egrep "cache-misses" | awk '{ print $4 }' >results.tmp
}

function measure 
{
    bufsize=$1
    nthreads=$2
    affinity_file=$3
    memaffin=$4
    access_pattern=$5
    delay=$6
    stride=$7
    niters=$8
    locktype=$9

    dat=${10}
    dat_first_col=${11}
    measure_type=${12}

    cat /dev/null >results.tmp

    for ((i = 1; i <= NRUNS; i++)); do
        cmd="./$BENCHNAME -b $bufsize \
                          -t $nthreads \
                          -a $affinity_file \
                          -m $memaffin \
                          -p $access_pattern \
                          -d $delay \
                          -s $stride \
                          -i $niters \
                          -l $locktype"
        echo $cmd

        if [ "$measure_type" == "throughput" ]; then
            measure_throughput $cmd
        elif [ "$measure_type" == "cache_misses" ]; then
            measure_cache_misses $cmd
        fi
    done

    avg=`awk '{ total += $1; count++ } END { print total / count }' results.tmp`

    echo -e "$dat_first_col \t\t $avg" >>$dat

    rm results.tmp
}

[ ! -d "results" ] && mkdir results

# TODO make beautiful function

#
# SCALABILITY test
#
function scalability
{
    for memaffin in $MEMAFFIN; do
        for access_pattern in $ACCESS_PATTERN; do
            datfile="results/${memaffin}_${access_pattern}_scalability.dat"
            echo -e "nthreads \t throughput, op/s" >$datfile

            for nthreads in $NTHREADS_HW; do
                affinity_file="thread_affinity_${nthreads}threads"

                measure $BUFSIZE \
                        $nthreads \
                        $affinity_file \
                        $memaffin \
                        $access_pattern \
                        $DELAY \
                        $STRIDE \
                        $NITERS \
                        rcl \
                        $datfile $nthreads throughput
            done
        done
    done
}

#
# CACHE MISSES test
#
function cache_misses 
{
    for memaffin in $MEMAFFIN; do
        for access_pattern in $ACCESS_PATTERN; do
            datfile="results/${memaffin}_${access_pattern}_cache_misses.dat"
            echo -e "nthreads \t throughput, op/s" >$datfile

            for nthreads in $NTHREADS_HW; do
                affinity_file="thread_affinity_${nthreads}threads"

                measure $BUFSIZE \
                        $nthreads \
                        $affinity_file \
                        $memaffin \
                        $access_pattern \
                        $DELAY \
                        $STRIDE \
                        $NITERS \
                        rcl \
                        $datfile $nthreads cache_misses
            done
        done
    done
}

# scalability
cache_misses
