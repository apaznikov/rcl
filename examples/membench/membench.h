// 
// membench.h: Memory benchmark for RCL (Remote Core Locking)
// (C) 2015-2016 Alexey Paznikov <apaznikov@gmail.com> 
// 

#pragma once

#include <map>
#include <string>

// #include "liblock.h"

// Thread affinity
struct affinity {
    bool enable = false;
    int node = -1;
    int core = -1;
    int hw_thread_id = -1;
};

enum class mem_affin_t { default_alloc, rcl_server_node, interleaved };

const std::map<std::string, mem_affin_t> mem_affin_arg_map = {
    { std::make_pair("default",         mem_affin_t::default_alloc) },
    { std::make_pair("rcl_server_node", mem_affin_t::rcl_server_node) },
    { std::make_pair("interleaved",     mem_affin_t::interleaved) }
};

enum class locktype_t { rcl, mutex, spinlock};

const std::map<std::string, locktype_t> locktype_arg_map = {
    { std::make_pair("rcl",      locktype_t::rcl) },
    { std::make_pair("mutex",    locktype_t::mutex) },
    { std::make_pair("spinlock", locktype_t::spinlock) }
};

enum class access_pattern_t { random, seq, strided };

const std::map<std::string, access_pattern_t> access_pattern_arg_map = {
    { std::make_pair("random", access_pattern_t::random) },
    { std::make_pair("seq", access_pattern_t::seq) },
    { std::make_pair("strided", access_pattern_t::strided) }
};
