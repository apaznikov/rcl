// 
// membench.cpp: Memory benchmark for RCL (Remote Core Locking)
// (C) 2015-2016 Alexey Paznikov <apaznikov@gmail.com> 
// 

#include <thread>
#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <vector>
#include <chrono>
#include <random>
#include <mutex>
#include <stdexcept>
#include <memory>

#include <cstdio>
#include <cstdlib>

#include <pthread.h>
#include <getopt.h>

#include <boost/thread/barrier.hpp>

#include "liblock.h"
#include "locks.h"
#include "utils.h"
#include "access_struct.h"
#include "membench.h"

#define _VERBOSE

////////////////////////////////////////////////////////////////////////////////
//                          Benchmark parameters                              //

using namespace std::literals;

// Memory access struct buffer size
auto bufsize = 1000u;

// Number of working threads
auto nthreads = 4u;

// File with thread affinity
auto thr_affinity_file = "thread_affinity";

// Number of memory access iterations
auto niters = 2000u;

// Delay between
auto delay_between_cs = 0us;

// Memory affinity policy
mem_affin_t mem_affin = mem_affin_t::default_alloc; 

// Access pattern
access_pattern_t access_pattern = access_pattern_t::random;

// Stride in strided access patter
auto access_stride = 2u;

// Lock type
locktype_t locktype = locktype_t::rcl;

//                                                                            //
////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
volatile auto global_cntr   = 0;
#endif
volatile auto found_value   = 0;

// RCL lock
liblock_lock_t g_liblock;
auto* const g_liblock_name  = "rcl";
volatile int *context;

// PThread mutex lock
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

// PThread spinlock lock
spinlock_t spinlock;

// Wrapper for critical section: RCL, mutex, spinlock, etc
std::function<void()> cs_wrapper;

// Barrier to synchronize threads before test
std::shared_ptr<boost::barrier> barrier;

std::once_flag onceflag1, onceflag2;

// Time measurement function
auto get_time = std::chrono::steady_clock::now;
decltype(get_time()) start, end;

// Test data structure
access_struct test_array;

// Threads affinity
std::vector<affinity> thread_affin;
affinity rcl_server_affin, allocator_affin;

// cs: Critical section
void *cs(void *arg)
{
    test_array.access();

#ifdef _DEBUG
    global_cntr++;
#endif

    return nullptr;
}

// thread: Thread implementation
void *thread(void *arg)
{
#ifdef _DEBUG
    std::cout << "hello i'm 0x" << std::hex << std::this_thread::get_id() 
              << std::dec << std::endl;
#endif

    // Wait until all threads started
    barrier->wait();

    std::call_once(onceflag1, []{ start = get_time(); });

    // Execute critical sections
    for (auto i = 0u; i < niters / nthreads; i++) {
        // std::cout << "iter " << i << std::endl;
        cs_wrapper();
        // std::this_thread::sleep_for(delay_between_cs);
    }

    barrier->wait();

    std::call_once(onceflag2, []{ end = get_time(); });

    return NULL;
}

// retrieve_affinity: Read file with thread assignment to cores
void retrieve_affinity()
{
    auto init_affinity = [](const auto &enable, 
                            const auto &node_s, const auto &core_s) {
        affinity affin;
        affin.enable = enable;
        if (affin.enable) {
            affin.node = std::stoi(node_s);
            affin.core = std::stoi(core_s);
            affin.hw_thread_id = topology->nodes[affin.node].
                                    hw_threads[affin.core]->hw_thread_id;
        }
        return affin;
    };

    std::ifstream affinity_file(thr_affinity_file);
    std::string line;

    while (std::getline(affinity_file, line)) {
        std::regex thread_affin_regex(
            "thread\\s([0-9]+)\\s*-->\\s*node\\s([0-9]+)\\score\\s([0-9]+)");
        std::regex rclserver_affin_regex(
            "RCL\\sserver\\s*-->\\s*node\\s([0-9]+)\\score\\s([0-9]+)");
        std::regex allocator_affin_regex(
            "allocator\\s*-->\\s*node\\s([0-9]+)\\score\\s([0-9]+)");
        std::smatch match;

        if (std::regex_search(line, match, thread_affin_regex) == true) {
            thread_affin[std::stoi(match[1])] = 
                init_affinity(true, match[2], match[3]);
        } else if (std::regex_search(line, match, 
                                     rclserver_affin_regex) == true) {
            rcl_server_affin = init_affinity(true, match[1], match[2]);
        } else if (std::regex_search(line, match, 
                                     allocator_affin_regex) == true) {
            allocator_affin = init_affinity(true, match[1], match[2]);
        }
    }

    if (thread_affin.size() != nthreads) {
        throw std::domain_error(
                "Affinity size must be equal to number of threads\n");
    }
}

// print_config: Print benchmark parameters
void print_config()
{
    auto print_thread = [](const auto &name, const auto &affin){
        if (affin.enable) {
            std::cout << name << " --> " 
                      << "node "  << affin.node 
                      << " core " << affin.core
                      << " hw_thread_id " << affin.hw_thread_id
                      << std::endl;
        } else {
            std::cout << name << " --> none" << std::endl;
        }
    };

    std::cout << "---------------------------------------------" << std::endl;
    std::cout << "Benchmark configuration:" 
              << "\nBUFSIZE = " << bufsize 
              << "\nNTHREADS = " << nthreads 
              << "\nNITERS = " << niters 
              << "\nSTRIDE = " << access_stride 
              << "\nDELAY_BETWEEN_CS = " 
              << double(delay_between_cs.count()) << std::endl;

    std::cout << "\nWorking threads affinity:" << std::endl;
    auto i = 0;
    for (const auto &affin: thread_affin) {
        print_thread(std::string{"thread"} + std::to_string(i), affin);
        i++;
    }

    print_thread(std::string{"allocator"}, allocator_affin);

    if (locktype == locktype_t::rcl) 
        print_thread(std::string{"RCL server"}, rcl_server_affin);

    std::cout << "---------------------------------------------" << std::endl;
}

// allocator: Initialize access struct: allocate memory
void *allocator(void *arg)
{
    std::cout << "Allocation..." << std::flush;
    test_array.init(bufsize, rcl_server_affin, mem_affin, 
                    access_pattern, access_stride);
    std::cout << "ok" << std::endl;
    return NULL;
}

// thread_launch: Create thread and bind to hw_thread if affinity is set
void thread_launch(pthread_t &tid, const affinity &affin,
                   void *(*start_routine) (void *))
{
    if (affin.enable) {
        const auto hw_thread = 
            &topology->hw_threads[affin.hw_thread_id];
        liblock_thread_create_and_bind(hw_thread, 0, &tid, 
                                       NULL, start_routine, NULL);
    } else {
        pthread_create(&tid, NULL, thread, NULL);
    }
}

// run_allocator: Run thread which allocates memory
void run_allocator()
{
    pthread_t tid;
    thread_launch(tid, allocator_affin, allocator);
    pthread_join(tid, NULL);
}

// init_cs: Initialize critical section
void init_cs()
{
    if (locktype == locktype_t::rcl) {
        const auto hw_thread = 
            &topology->hw_threads[rcl_server_affin.hw_thread_id];

        liblock_lock_init(g_liblock_name, hw_thread, &g_liblock, 0);

        cs_wrapper = [&](){
            liblock_exec(&g_liblock, cs, (void *) context);
        };

    } else if (locktype == locktype_t::mutex) {
        cs_wrapper = [&](){
            pthread_mutex_lock(&mutex);
            cs((void *) context);
            pthread_mutex_unlock(&mutex);
        };

    } else if (locktype == locktype_t::spinlock) {
        cs_wrapper = [&](){
            spinlock.lock();
            cs((void *) context);
            spinlock.unlock();
        };
    }
}

// init_barrier:
void init_barrier()
{
    std::shared_ptr<boost::barrier> 
        barrier_ptr{new boost::barrier(uint(nthreads))};
    barrier = barrier_ptr;
}

// run_bench:
void run_bench()
{
    std::vector<pthread_t> tids(nthreads);

    for (auto i = 0u; i < thread_affin.size(); i++) 
        thread_launch(tids[i], thread_affin[i], thread);

    for (const auto &tid: tids) 
        pthread_join(tid, NULL);

    if (locktype == locktype_t::rcl)
        liblock_lock_destroy(&g_liblock);
}

// get_elapsed_time:
void get_elapsed_time()
{
    auto elapsed = std::chrono::duration_cast
                   <std::chrono::milliseconds>(end - start).count();

    std::cout << "Elapsed time: " << double(elapsed) / 1000 << " seconds" 
              << std::endl;
}

// parse_options: Parse command line options
void parse_options(int argc, char* const argv[])
{
    auto opt = 0;

    auto init_param_by_arg = [=](const auto &arg_param_map, auto &item) {
        auto search = arg_param_map.find(optarg);
        if (search != arg_param_map.end())
            item = search->second;
        else
            throw std::runtime_error("invalid parameter: " + 
                                     std::string{optarg});
    };

    while ((opt = getopt(argc, argv, "hb:t:a:m:p:d:s:i:l:")) != -1) {
        switch (opt) {
            case 'h':
                std::cout << "Usage: " << argv[0] << "[args]\n"
                  << "    -b <bufsize>   -- buffer size\n"
                  << "    -t <nthreads>  -- number of threads\n"
                  << "    -a <filename>  -- threads affinity file\n"
                  << "    -m <mem_affin> -- memory affinity policy\n"
                  << "    -p <pattern>   -- memory access pattern\n"
                  << "    -d <delay>     -- delay between two accesses\n"
                  << "    -s <stride>    -- interval in stride access pattern\n"
                  << "    -i <niters>    -- number of iterations\n"
                  << "    -l <locktype>  -- type of the lock\n"
                  << "    -h             -- show this message" << std::endl;
                exit(0);

            case 'b':
                bufsize = atoi(optarg);
                break;

            case 't':
                nthreads = atoi(optarg);
                thread_affin.resize(nthreads);
                break;

            case 'a':
                thr_affinity_file = optarg;
                break;

            case 'm':
                init_param_by_arg(mem_affin_arg_map, mem_affin);
                break;

            case 'p':
                init_param_by_arg(access_pattern_arg_map, access_pattern);
                break;

            case 'd':
                delay_between_cs = std::chrono::microseconds(atoi(optarg));
                break;

            case 's':
                access_stride = atoi(optarg);
                break;

            case 'i':
                niters = atoi(optarg);
                break;

            case 'l':
                init_param_by_arg(locktype_arg_map, locktype);
                break;

            default:
                throw std::runtime_error("Invalid option");
        }
    }

    if ((mem_affin == mem_affin_t::rcl_server_node) && 
        (locktype != locktype_t::rcl))
        throw std::runtime_error(
     "Memory affinity policy is \"rcl_server_node\" but lock type is not RCL!");
}

int main(int argc, char* const argv[])
{
    try {
#ifdef _VERBOSE
        std::cout << "System topology:" << std::endl;
        print_topology();
#endif

        parse_options(argc, argv);

        retrieve_affinity();

#ifdef _VERBOSE
        print_config();
#endif
        run_allocator();

        init_cs();

        init_barrier();

        run_bench();

#ifdef _DEBUG
        std::cout << "global_cntr = " << global_cntr << std::endl;
#endif

        get_elapsed_time();
    } 
    
    catch(const std::exception& e) {
        std::cerr << "Exception: " << e.what() << std::endl;
        return 1;
    }

    return 0;
}
