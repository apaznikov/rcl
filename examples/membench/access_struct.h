// 
// access_struct.h: Structure for test access 
// (C) 2015-2016 Alexey Paznikov <apaznikov@gmail.com> 
// 

#pragma once

#include "membench.h"

#include <functional>

// Access memory structure
class access_struct
{
public:
    access_struct() 
    {
        init_access_pattern_map();
        init_mem_func_map();
    }

    void init(unsigned _bufsize,
              const affinity &_rcl_server_affin, 
              const mem_affin_t &_mem_affin,
              const access_pattern_t &access_pattern,
              unsigned _stride);

    ~access_struct()
    {
        mem_free();
    }

    inline void access()
    {
        access_func();
    }

private:
    unsigned bufsize = 0;
    unsigned elemsize = 2;

    using elemtype = long double;
    elemtype *buf = nullptr;

    inline void elem_modify(elemtype &elem); 

    ///////////////////////////////////////////////////////////////////////////
    //                      Memory handling function                         //
    ///////////////////////////////////////////////////////////////////////////
    elemtype *memory_alloc(const affinity &rcl_server_affin);

    std::function<elemtype*()> mem_alloc;
    std::function<void()> mem_free;

    elemtype *default_alloc();
    elemtype *rcl_server_node_alloc();
    elemtype *interleaved_alloc();

    void default_free();
    void rcl_server_node_free();
    void interleaved_free();

    std::map<mem_affin_t,
             std::pair<decltype(mem_alloc), decltype(mem_free)>> mem_func_map;
    void init_mem_func_map();

    mem_affin_t mem_affin;
    affinity rcl_server_affin;
    ///////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////
    //                      Memory access patterns                           //
    ///////////////////////////////////////////////////////////////////////////
    std::function<void()> access_func;

    inline void random_access();
    inline void seq_access();
    inline void strided_access();

    unsigned stride = 1;

    std::map<access_pattern_t, decltype(access_func)> access_pattern_map;
    void init_access_pattern_map();
    ///////////////////////////////////////////////////////////////////////////
};
