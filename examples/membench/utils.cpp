// 
// utils.h: Utilities
// (C) 2015-2016 Alexey Paznikov <apaznikov@gmail.com> 
//

#include "utils.h"

#include <memory>
#include <random>
#include <thread>

// get_rand: Thread-safe random generator
int get_rand(int min, int max)
{
    static thread_local std::unique_ptr<std::mt19937> generator;
    static thread_local std::hash<std::thread::id> hasher;

    if (!generator)
        generator = std::make_unique<std::mt19937>(
                clock() + hasher(std::this_thread::get_id()));

    std::uniform_int_distribution<int> distribution(min, max);
    return distribution(*generator);
}
