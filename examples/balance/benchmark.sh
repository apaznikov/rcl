#!/bin/sh

mkdir -p results

NRUNS=7
# NTHREADS="20 40 60 80 100"
# NTHREADS="20 80"
# NTHREADS="40 60 80 100"
NTHREADS="100"
NCS="22"

BENCHMARK=$HOME/rcl/rcl/examples/imbalance/prog
# BENCHMARK='./rand_sleep.sh' 

loglabel=$1
logfile="benchmark-$loglabel-`date +%d%H%M%S`.log"
cat /dev/null >$logfile

run_benchmark()
{
    total_time=0
    for i in `seq 1 $NRUNS`
    do
        \time -f %e -o /tmp/rcl_time $BENCHMARK $@ >/dev/null 2>&1
        exec_time=`cat /tmp/rcl_time`
        total_time=`echo $total_time $exec_time | awk '{print $1 + $2}'`
        # echo exec $exec_time total $total_time
        echo `hostname` $BENCHMARK $@ -- $exec_time s >>$logfile
    done

    mean_time=`echo $total_time $NRUNS | awk '{print $1 / $2}'`
    echo $mean_time
}

nthreads_tests()
{
    nthreads_dat=results/nthreads.dat
    [ ! -d "results" ] && mkdir results

    echo -e "nthreads \t time" >$nthreads_dat
    echo -e "nthreads \t time" 

    for nthreads in $NTHREADS
    do
        mean_time=`run_benchmark -t $nthreads -c $NCS`
        echo -e "$nthreads \t\t $mean_time" >>$nthreads_dat

        echo -e "\nnthreads \t time"        >>$logfile
        echo -e "$nthreads \t\t $mean_time" >>$logfile

        echo -e "$nthreads \t\t $mean_time"
    done
}

nthreads_tests
