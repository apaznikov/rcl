/**
 * prog.c: Simple example for RCL (remote core locking)
 *
 * (C) 2015 Alexey Paznikov <apaznikov@gmail.com> 
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <pthread.h>
#include <sched.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <ctype.h>

#include "liblock.h"
#include "utils.h"
#include "liblock-config.h"

// Number of working threads with default values
int NTHREADS             = 90;      // t
int NCS                  = 32;      // c
int NLIBLOCKS            = 8;       // l
int USLEEP_BETWEEN_CS    = 5;       // b
int USLEEP_IN_CS         = 5000;    // d
int OUTER_LOOP_NITERS    = 700;     // o
int INNER_LOOP_NITERS    = 700;     // i

enum {
    PRINT_CS_FACTOR      = 2
};

struct lock_pack_t {
    liblock_lock_t lock;
    double freq;
    struct hw_thread* hw_thread;
};

const char* liblock_name = "rcl";

struct lock_pack_t *locks = NULL;
int* volatile context = NULL;
int* volatile global_cntr = NULL;

volatile int g_server_core = 0;

void *cs(void *arg)
{
    int *lock_i = (int*) arg;
    global_cntr[*lock_i]++;

#ifdef CS_AS_SLEEP
    usleep(USLEEP_IN_CS);
#else
    int i;
    for (i = 0; i < OUTER_LOOP_NITERS; i++)
    {
        int j;
        for (j = 0; j < INNER_LOOP_NITERS; j++)
            ;
        /* for (j = 0; j < 20000; j++) */
        pthread_yield();
    }
#endif

    return NULL;
}

void *thread(void *arg)
{
    printf("%p thread -- begin\n", (void *) pthread_self());
    struct drand48_data rand_buf;
    double randval;

    srand48_r(time(NULL), &rand_buf);

    int cs_i;
    for (cs_i = 0; cs_i < NCS; cs_i++) 
    {
        if (cs_i && (cs_i % PRINT_CS_FACTOR == 0))
            printf("CS %d\n", cs_i);

        int lock_i;
        for (lock_i = 0; lock_i < NLIBLOCKS; lock_i++) 
        {
            drand48_r(&rand_buf, &randval);
            if (randval < locks[lock_i].freq) 
            {
                liblock_exec(&locks[lock_i].lock, cs, (void *) &context[lock_i]);
                usleep(USLEEP_BETWEEN_CS);
            }
        }
    }

    printf("thread %p -- end\n", (void *) pthread_self());

    return NULL;
}

static void init_lock_packs(void)
{
    locks = malloc(NLIBLOCKS * sizeof(*locks));
    context = malloc(NLIBLOCKS * sizeof(*context));
    global_cntr = malloc(NLIBLOCKS * sizeof(*global_cntr));

    assert(NLIBLOCKS == 8);

    locks[0].hw_thread = &topology->hw_threads[1];
    locks[1].hw_thread = &topology->hw_threads[1];
    locks[2].hw_thread = &topology->hw_threads[1];
    locks[3].hw_thread = &topology->hw_threads[1];
    locks[4].hw_thread = &topology->hw_threads[2];
    locks[5].hw_thread = &topology->hw_threads[2];
    locks[6].hw_thread = &topology->hw_threads[2];
    locks[7].hw_thread = &topology->hw_threads[2];
    /* locks[8].hw_thread = &topology->hw_threads[1]; */
    /* locks[9].hw_thread = &topology->hw_threads[1]; */

    locks[0].freq = 0.5;
    locks[1].freq = 0.5;
    locks[2].freq = 0.5;
    locks[3].freq = 0.5;
    locks[4].freq = 0.5;
    locks[5].freq = 0.5;
    locks[6].freq = 0.5;
    locks[7].freq = 0.5;
    /* locks[8].freq = 0.1; */
    /* locks[9].freq = 0.1; */
}

static void destroy_lock_packs()
{
    free(locks);
    free(context);
    free(global_cntr);
}

static int parse_arguments(int argc, char* const argv[])
{
    int index;
    int c;

    while ((c = getopt(argc, argv, "t:c:l:b:d:o:i:")) != -1)
    {
        switch (c)
        {
            case 't':
                NTHREADS = atoi(optarg);
                break;
            case 'c':
                NCS = atoi(optarg);
                break;
            case 'l':
                NLIBLOCKS = atoi(optarg);
                break;
            case 'b':
                USLEEP_BETWEEN_CS = atoi(optarg);
                break;
            case 'd':
                USLEEP_IN_CS = atoi(optarg);
                break;
            case 'o':
                OUTER_LOOP_NITERS = atoi(optarg);
                break;
            case 'i':
                INNER_LOOP_NITERS = atoi(optarg);
                break;
            case '?':
                if (optopt == 'c')
                    fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf (stderr,
                            "Unknown option character `\\x%x'.\n",
                            optopt);
                return 1;
            default:
                abort ();
        }

        for (index = optind; index < argc; index++)
            printf ("Non-option argument %s\n", argv[index]);

        /* return 0; */
    }

    return 0;
}

int main(int argc, char* argv[])
{
    printf("try building locks...\n");

    parse_arguments(argc, argv);

    init_lock_packs();

    int lock_i;
    for (lock_i = 0; lock_i < NLIBLOCKS; lock_i++) 
    {
        liblock_lock_init(liblock_name, locks[lock_i].hw_thread, 
                          &locks[lock_i].lock, 0);
        global_cntr[lock_i] = 0;
        context[lock_i] = lock_i;
    }

    printf("NTHREADS = %d\n", NTHREADS);

    pthread_t tids[NTHREADS];

    for (int i = 0; i < NTHREADS; i++) 
        liblock_thread_create(&tids[i], NULL, thread, NULL);

    for (int i = 0; i < NTHREADS; i++) 
        pthread_join(tids[i], NULL);

    for (lock_i = 0; lock_i < NLIBLOCKS; lock_i++) 
    {
        liblock_lock_destroy(&locks[lock_i].lock);
        printf("global_cntr[%d] = %d (expected %d)\n", 
               lock_i, global_cntr[lock_i], NCS * NTHREADS);
    }

    destroy_lock_packs();

    return 0;
}
