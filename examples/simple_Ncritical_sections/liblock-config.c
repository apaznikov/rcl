#define _GNU_SOURCE
#include <errno.h>
#include <sched.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#ifdef __sun__
#include <sys/types.h>
#include <sys/processor.h>
#include <sys/procset.h>
#include <sys/pset.h>
#endif
//#include <liblock.h>
//#include <liblock-fatal.h>
//#include <liblock-splash2.h>
#include "liblock.h"
#include "liblock-fatal.h"
#include "liblock-config.h"


struct servers {
    unsigned int nb_hw_threads;
    struct hw_thread** hw_threads;
};

const char* liblock_lock_name;

extern struct hw_thread* get_server_core_1()
{
//    struct hw_thread* res = servers.hw_threads[0];
    struct hw_thread* res = &topology->hw_threads[0];
    //printf("get_server_hw_thread_1: hw_thread %d\n", res->hw_thread_id);
    return res;
}

extern struct hw_thread* get_server_core_2()
{
    struct hw_thread* res;

    if(topology->nb_hw_threads == 1)
        res = &topology->hw_threads[0];
    else
    {
        static int n = 0;
//        res = servers.hw_threads[1 + __sync_fetch_and_add(&n, 1)
//                                 % (servers.nb_hw_threads - 1)];
        res = &topology->hw_threads[1 + __sync_fetch_and_add(&n, 1)
                                 % (topology->nb_hw_threads - 1)];
    }
    // printf("get_server_hw_thread_2: hw_thread %d (%d)\n",
    // res->hw_thread_id, servers.nb_hw_threads);
    return res;
}

extern struct hw_thread* get_server_core_3()
{
    struct hw_thread* res;
    // The fraction of cores which may be occupied by RCL servers.
    const double rcl_core_limit = 0.8;

    if(topology->nb_hw_threads * rcl_core_limit < 1)
        res = &topology->hw_threads[0];
    else
    {
        static int n = 0;
        res = &topology->hw_threads[__sync_fetch_and_add(&n, 1)
                 % ((int) (topology->nb_hw_threads * rcl_core_limit))];
        // printf("res = %d\n", n % ((int) (topology->nb_hw_threads * rcl_core_limit)));
    }
    return res;
}
