/**
 * utils.h: Simple example for RCL (remote core locking)
 *
 * (C) 2015 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef UTILS_H_
#define UTILS_H_

/* This function gets the CPU speed then allocates and fills the
   virtual_to_physical_hw_thread_id array. */
void get_cpu_info();

struct hw_thread* get_core(unsigned int physical_core);

#endif /* UTILS_H_ */
